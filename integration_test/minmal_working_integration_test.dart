import 'dart:io';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';

import 'package:muwi/main.dart' as tonfall;


/// This test checks, if the app starts correctly.
/// It's more a test, if the integration test framework works than a test for
/// functionality.
void main() {
  //Uncomment, if you just see a black screen with the text "test started"
  //source: https://stackoverflow.com/a/68099497
  //final binding = IntegrationTestWidgetsFlutterBinding.ensureInitialized();
  //binding.framePolicy = LiveTestWidgetsFlutterBindingFramePolicy.fullyLive;
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();


  group('minimal working test', () {
    testWidgets('start tonfall',
            (tester) async {
              IntegrationTestWidgetsFlutterBinding.ensureInitialized();
            //without this line, we get the following error message:
              // 'package:flutter/src/widgets/binding.dart': Failed assertion: line 894 pos 16: 'sendFramesToEngine':
              // is not true.
              //fix found at: https://github.com/flutter/flutter/issues/89651#issuecomment-1372649683
            await Future.delayed(const Duration(seconds: 2));
            tonfall.main();
            await tester.pumpAndSettle();
            sleep(const Duration(seconds: 7));
        });
  });
}
