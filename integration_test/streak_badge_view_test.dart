import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';
import 'package:muwi/enums/badge_period_under_consideration.dart';
import 'package:muwi/model/badge_model.dart';
import 'package:muwi/model/tonfall_shared_preferences.dart';
import 'package:muwi/view/badges_view.dart';
import 'package:muwi/view/theme/custom_theme.dart';
import 'package:shared_preferences/shared_preferences.dart';

const int showEachScreenSeconds = 5;
DateTime lastGame = DateTime.parse('2023-01-01 00:00');
DateTime now = DateTime.parse('2023-01-01 01:00');

///Test streak badge in badge view with all kind of combinations.
///Manually watch, if test description matches emulator.
void main() async {
  await testStreakBadgeView('Nothing 1', currentStreak: 0, bestStreak: 0, lastGame: lastGame, now: now,);

  await testStreakBadgeView('nothing and bronze', currentStreak: 0, bestStreak: 7, lastGame: lastGame, now: now,);
  await testStreakBadgeView('nothing and silver', currentStreak: 0, bestStreak: 14, lastGame: lastGame, now: now,);
  await testStreakBadgeView('nothing and gold', currentStreak: 0, bestStreak: 21, lastGame: lastGame, now: now,);

  await testStreakBadgeView('both are bronze', currentStreak: 7, bestStreak: 7, lastGame: lastGame, now: now,);
  await testStreakBadgeView('both are silver', currentStreak: 14, bestStreak: 14, lastGame: lastGame, now: now,);
  await testStreakBadgeView('both are gold', currentStreak: 21, bestStreak: 21, lastGame: lastGame, now: now,);

  await testStreakBadgeView('bronze and silver', currentStreak: 7, bestStreak: 14, lastGame: lastGame, now: now,);
  await testStreakBadgeView('bronze and gold', currentStreak: 7, bestStreak: 21, lastGame: lastGame, now: now,);
  await testStreakBadgeView('silver and gold', currentStreak: 14, bestStreak: 21, lastGame: lastGame, now: now,);

  await testStreakBadgeView('to-long-ago and bronze', currentStreak: 7, bestStreak: 13, lastGame: DateTime.parse('2022-12-30 23:55'), now: DateTime.parse('2023-01-01 12:00'),);
  await testStreakBadgeView('to-long-ago and silver', currentStreak: 14, bestStreak: 15, lastGame: DateTime.parse('2022-05-08 23:55'), now: DateTime.parse('2022-05-10 00:00'),);
  await testStreakBadgeView('to-long-ago and gold', currentStreak: 14, bestStreak: 30, lastGame: DateTime.parse('2022-05-08 23:55'), now: DateTime.parse('2022-05-10 23:55'),);
  await testStreakBadgeView('max time span that is allowed (bronze and bronze)', currentStreak: 13, bestStreak: 13, lastGame: DateTime.parse('2022-05-10 00:00'), now: DateTime.parse('2022-05-11 23:59'),);
  await testStreakBadgeView('min time span over a day (silver and silver)', currentStreak: 14, bestStreak: 20, lastGame: DateTime.parse('2022-05-10 23:59'), now: DateTime.parse('2022-05-11 00:00'),);
  await testStreakBadgeView('min time span new year (gold and gold)', currentStreak: 21, bestStreak: 500, lastGame: DateTime.parse('2000-12-31 23:59'), now: DateTime.parse('2001-01-01 00:01'),);
  await testStreakBadgeView('max time span new year (gold and gold)', currentStreak: 31, bestStreak: 365, lastGame: DateTime.parse('2000-12-31 00:00'), now: DateTime.parse('2001-01-01 23:59'),);

}

Future<void> testStreakBadgeView(
    String testTitle,
    {
      required int currentStreak,
      required DateTime lastGame,
      required DateTime now,
      required int bestStreak,
    }) async {
  testWidgets(testTitle, (tester) async {
    IntegrationTestWidgetsFlutterBinding.ensureInitialized();
    SharedPreferences.setMockInitialValues({});
    await Future.delayed(const Duration(seconds: 1));

    await TonfallSharedPreferences.setAccomplishedDateTimeForBadge(BadgeModel.streak(), lastGame);
    await TonfallSharedPreferences.setValueFromBadge(BadgeModel.streak(), BadgePeriodUnderConsideration.twoWeeks, currentStreak);
    await TonfallSharedPreferences.setValueFromBadge(BadgeModel.streak(), BadgePeriodUnderConsideration.allTime, bestStreak);
    runApp(
      MaterialApp(
        theme: lightTheme.copyWith(
          extensions: <ThemeExtension<dynamic>>[
            CustomColors.light,
          ],
        ),
        home: BadgesView(now: now),
      ),
    );
    await tester.pumpAndSettle();
    await tester.scrollUntilVisible(find.text('GLOBAL'), -10, maxScrolls: 200);
    await tester.pumpAndSettle();
    sleep(const Duration(seconds: showEachScreenSeconds));
  });
}
