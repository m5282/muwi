import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';
import 'package:muwi/enums/badge_period_under_consideration.dart';

import 'package:muwi/model/badge_model.dart';
import 'package:muwi/model/tonfall_shared_preferences.dart';
import 'package:muwi/view/badges_view.dart';
import 'package:muwi/view/theme/custom_theme.dart';
import 'package:shared_preferences/shared_preferences.dart';

const int showEachScreenInSeconds = 2;
//Change constants below, if new badges have been implemented.
///For 2-weeks and allTime there are 6 Triad Badges, 6 Interval and 3 Global Badges
///2 * (6 + 6 + 3) = 30
const int numberOfBadges = 30;
///For 2-weeks and allTime there are 6 Triad Badges, 6 Interval Badges á 3 Stars
///2 * (6 + 6) * 3 = 72
const int numberOfStarts = 72;

///Draw a lesson completed screen, where every badge is accomplished
///Checks, if the number of images matches the number of expected badges.
void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  testWidgets('show fully accomplished badge screen',
          (tester) async {
        SharedPreferences.setMockInitialValues({});
        await SharedPreferences.getInstance();
        for (var i = 0; i <= BadgeModel.highestID; i++){
          await TonfallSharedPreferences.setAccomplishedDateTimeForBadge(BadgeModel.idToBadgeModel[i]!, DateTime.now());
        }
        await TonfallSharedPreferences.setValueFromBadge(BadgeModel.streak(), BadgePeriodUnderConsideration.twoWeeks, 21);
        await TonfallSharedPreferences.setValueFromBadge(BadgeModel.streak(), BadgePeriodUnderConsideration.allTime, 21);
        await Future.delayed(const Duration(seconds: 2));

        runApp(MaterialApp(
          debugShowCheckedModeBanner: false,
          theme: lightTheme.copyWith(
            extensions: <ThemeExtension<dynamic>>[
              CustomColors.light,
            ],
          ),
          home: const BadgesView(),
        ),
        );
        await tester.pumpAndSettle();
        sleep(const Duration(seconds: showEachScreenInSeconds));
        await tester.scrollUntilVisible(find.text('INTERVALLE'), -10, maxScrolls: 200);
        sleep(const Duration(seconds: 5));
        //await tester.dragUntilVisible(find.text(BadgeModel.intervalCorrectlySolvedInARowGoldEasy().germanDescription), find.byType(BadgesView), Offset.infinite);
        final gesture = await tester.startGesture(const Offset(0, 300)); //Position of the scrollview
        await gesture.moveBy(const Offset(0, -1000)); //-700//How much to scroll by
        await gesture.cancel();
        await tester.pumpAndSettle();
        sleep(const Duration(seconds: showEachScreenInSeconds));
        await tester.scrollUntilVisible(find.text('GLOBAL'), -10, maxScrolls: 200);
        await tester.pumpAndSettle();
        sleep(const Duration(seconds: showEachScreenInSeconds));
        expect(find.byIcon(Icons.star), findsNWidgets(numberOfStarts));

        final badgeList = tester.widgetList(find.byType(Image)).toList();
        assert(badgeList.length == numberOfBadges, 'Expect $numberOfBadges Badges, so 30 images');
      });
}
