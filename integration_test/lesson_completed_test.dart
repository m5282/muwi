import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';
import 'package:muwi/enums/badge_medal_color.dart';
import 'package:muwi/enums/lesson_training_names.dart';
import 'package:muwi/enums/level_names.dart';
import 'package:muwi/enums/task_mode.dart';
import 'package:muwi/model/badge_model.dart';
import 'package:muwi/model/settings/triad_settings.dart';
import 'package:muwi/model/tonfall_shared_preferences.dart';
import 'package:muwi/view/badges_view.dart';
import 'package:muwi/view/lesson_completed.dart';
import 'package:muwi/view/theme/custom_theme.dart';
import 'package:muwi/view_model/keyboard_vm/triad_keyboard_vm.dart';
import 'package:muwi/view_model/taskproviders/triad_provider.dart';
import 'package:muwi/view_model/triad_quiz_page_view_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

const int showEachScreenSeconds = 1;
final DateTime longAgo = DateTime.parse('2023-01-01 00:00:00.000');
final DateTime today = DateTime.parse('${DateTime.now().year}-${DateTime.now().month.toString().padLeft(2, '0')}-${DateTime.now().day.toString().padLeft(2, '0')}');
Set<BadgeModel> subsetBadges = {BadgeModel.streak()};

///In every test, exactly one badge is accomplished.
///Afterwards we check, that (only) the accomplished badge is saved in shared preferences.
///Next we check, that the right symbol is shown in the lesson completed screen.
Future<void> main() async{
  await triadTests();
  await intervalTests();
}


Future<void> triadTests() async{

  for (final startTime in [longAgo, today]) {
    //Easy
    await testBadge(
      badgeModel: BadgeModel.triadSpeedBronzeEasy(),
      shownTasks: 12,
      pointsAchieved: 12,
      pointsPerCorrectTask: 2,
      difficulty: Difficulty.easy,
      taskMode: TaskMode.triad,
      startTime: startTime,
      duration: const Duration(minutes: 4),
      subsetBadges: subsetBadges,
    );
    await testBadge(
      badgeModel: BadgeModel.triadSpeedSilverEasy(),
      shownTasks: 12,
      pointsAchieved: 12,
      pointsPerCorrectTask: 2,
      difficulty: Difficulty.easy,
      taskMode: TaskMode.triad,
      startTime: startTime,
      duration: const Duration(minutes: 2),
      subsetBadges: subsetBadges.union({BadgeModel.triadSpeedBronzeEasy()}),
    );
    await testBadge(
      badgeModel: BadgeModel.triadSpeedGoldEasy(),
      shownTasks: 12,
      pointsAchieved: 12,
      pointsPerCorrectTask: 2,
      difficulty: Difficulty.easy,
      taskMode: TaskMode.triad,
      startTime: startTime,
      duration: const Duration(minutes: 1),
      subsetBadges: subsetBadges.union({
        BadgeModel.triadSpeedBronzeEasy(),
        BadgeModel.triadSpeedSilverEasy(),
      }),
    );

    //Medium
    await testBadge(
      badgeModel: BadgeModel.triadSpeedBronzeMedium(),
      shownTasks: 12,
      pointsAchieved: 12,
      pointsPerCorrectTask: 2,
      difficulty: Difficulty.medium,
      taskMode: TaskMode.triad,
      startTime: startTime,
      duration: const Duration(minutes: 4),
      subsetBadges: subsetBadges.union({},
    ),);

    await testBadge(
      badgeModel: BadgeModel.triadSpeedSilverMedium(),
      shownTasks: 12,
      pointsAchieved: 12,
      pointsPerCorrectTask: 2,
      difficulty: Difficulty.medium,
      taskMode: TaskMode.triad,
      startTime: startTime,
      duration: const Duration(minutes: 2),
      subsetBadges: subsetBadges.union({BadgeModel.triadSpeedBronzeMedium()},
    ),);
    await testBadge(
      badgeModel: BadgeModel.triadSpeedGoldMedium(),
      shownTasks: 12,
      pointsAchieved: 12,
      pointsPerCorrectTask: 2,
      difficulty: Difficulty.medium,
      taskMode: TaskMode.triad,
      startTime: startTime,
      duration: const Duration(minutes: 1),
      subsetBadges: subsetBadges.union({
        BadgeModel.triadSpeedBronzeMedium(),
        BadgeModel.triadSpeedSilverMedium(),
      },
    ),);

    //Hard
    await testBadge(
      badgeModel: BadgeModel.triadSpeedBronzeHard(),
      shownTasks: 12,
      pointsAchieved: 12,
      pointsPerCorrectTask: 2,
      difficulty: Difficulty.hard,
      taskMode: TaskMode.triad,
      startTime: startTime,
      duration: const Duration(minutes: 4),
      subsetBadges: subsetBadges.union({},
    ),);
    await testBadge(
      badgeModel: BadgeModel.triadSpeedSilverHard(),
      shownTasks: 12,
      pointsAchieved: 12,
      pointsPerCorrectTask: 2,
      difficulty: Difficulty.hard,
      taskMode: TaskMode.triad,
      startTime: startTime,
      duration: const Duration(minutes: 2),
      subsetBadges: subsetBadges.union({BadgeModel.triadSpeedBronzeHard()},
    ),);
    await testBadge(
      badgeModel: BadgeModel.triadSpeedGoldHard(),
      shownTasks: 12,
      pointsAchieved: 12,
      pointsPerCorrectTask: 2,
      difficulty: Difficulty.hard,
      taskMode: TaskMode.triad,
      startTime: startTime,
      duration: const Duration(minutes: 1),
      subsetBadges: subsetBadges.union({
        BadgeModel.triadSpeedBronzeHard(),
        BadgeModel.triadSpeedSilverHard(),
      },
    ),);
    //Triad - Row - Bronze
    //The points and and the "correctlySolvedInRowBest" do not always match.
    //There is no function that checks them for credibility.
    // They have to be set correctly.
    // In this test, we just pass the minimal requirements accomplish a badge.
    //Easy
    await testBadge(
      badgeModel: BadgeModel.triadsCorrectlySolvedInARowBronzeEasy(),
      correctlySolvedInRowBest: 12,
      shownTasks: 12,
      pointsAchieved: 12,
      pointsPerCorrectTask: 2,
      difficulty: Difficulty.easy,
      taskMode: TaskMode.triad,
      startTime: startTime,
      duration: const Duration(minutes: 10),
      subsetBadges: subsetBadges.union({},
    ),);
    //Medium
    await testBadge(
      badgeModel: BadgeModel.triadsCorrectlySolvedInARowSilverEasy(),
      correctlySolvedInRowBest: 24,
      shownTasks: 12,
      pointsAchieved: 12,
      pointsPerCorrectTask: 2,
      difficulty: Difficulty.easy,
      taskMode: TaskMode.triad,
      startTime: startTime,
      duration: const Duration(minutes: 10),
      subsetBadges: subsetBadges.union({
        BadgeModel.triadsCorrectlySolvedInARowBronzeEasy(),
      },
    ),);

    //Hard
    await testBadge(
      badgeModel: BadgeModel.triadsCorrectlySolvedInARowGoldEasy(),
      correctlySolvedInRowBest: 36,
      shownTasks: 12,
      pointsAchieved: 12,
      pointsPerCorrectTask: 2,
      difficulty: Difficulty.easy,
      taskMode: TaskMode.triad,
      startTime: startTime,
      duration: const Duration(minutes: 10),
      subsetBadges: subsetBadges.union({
        BadgeModel.triadsCorrectlySolvedInARowBronzeEasy(),
        BadgeModel.triadsCorrectlySolvedInARowSilverEasy(),
      },
    ),);

    //Medium
    await testBadge(
      badgeModel: BadgeModel.triadsCorrectlySolvedInARowBronzeMedium(),
      correctlySolvedInRowBest: 12,
      shownTasks: 12,
      pointsAchieved: 12,
      pointsPerCorrectTask: 2,
      difficulty: Difficulty.medium,
      taskMode: TaskMode.triad,
      startTime: startTime,
      duration: const Duration(minutes: 10),
      subsetBadges: subsetBadges.union({},
    ),);
    await testBadge(
      badgeModel: BadgeModel.triadsCorrectlySolvedInARowSilverMedium(),
      correctlySolvedInRowBest: 24,
      shownTasks: 12,
      pointsAchieved: 12,
      pointsPerCorrectTask: 2,
      difficulty: Difficulty.medium,
      taskMode: TaskMode.triad,
      startTime: startTime,
      duration: const Duration(minutes: 10),
      subsetBadges: subsetBadges.union({
        BadgeModel.triadsCorrectlySolvedInARowBronzeMedium(),
      },
    ),);
    await testBadge(
      badgeModel: BadgeModel.triadsCorrectlySolvedInARowGoldMedium(),
      correctlySolvedInRowBest: 36,
      shownTasks: 12,
      pointsAchieved: 12,
      pointsPerCorrectTask: 2,
      difficulty: Difficulty.medium,
      taskMode: TaskMode.triad,
      startTime: startTime,
      duration: const Duration(minutes: 10),
      subsetBadges: subsetBadges.union({
        BadgeModel.triadsCorrectlySolvedInARowBronzeMedium(),
        BadgeModel.triadsCorrectlySolvedInARowSilverMedium(),
      },
    ),);

    //Hard
    await testBadge(
      badgeModel: BadgeModel.triadsCorrectlySolvedInARowBronzeHard(),
      correctlySolvedInRowBest: 12,
      shownTasks: 12,
      pointsAchieved: 12,
      pointsPerCorrectTask: 2,
      difficulty: Difficulty.hard,
      taskMode: TaskMode.triad,
      startTime: startTime,
      duration: const Duration(minutes: 10),
      subsetBadges: subsetBadges.union({},
    ),);
    await testBadge(
      badgeModel: BadgeModel.triadsCorrectlySolvedInARowSilverHard(),
      correctlySolvedInRowBest: 24,
      shownTasks: 12,
      pointsAchieved: 12,
      pointsPerCorrectTask: 2,
      difficulty: Difficulty.hard,
      taskMode: TaskMode.triad,
      startTime: startTime,
      duration: const Duration(minutes: 10),
      subsetBadges: subsetBadges.union({
        BadgeModel.triadsCorrectlySolvedInARowBronzeHard(),
      },
    ),);
    await testBadge(
      badgeModel: BadgeModel.triadsCorrectlySolvedInARowGoldHard(),
      correctlySolvedInRowBest: 36,
      shownTasks: 12,
      pointsAchieved: 12,
      pointsPerCorrectTask: 2,
      difficulty: Difficulty.hard,
      taskMode: TaskMode.triad,
      startTime: startTime,
      duration: const Duration(minutes: 10),
      subsetBadges: subsetBadges.union({
        BadgeModel.triadsCorrectlySolvedInARowBronzeHard(),
        BadgeModel.triadsCorrectlySolvedInARowSilverHard(),
      },
    ),);
  }
}

Future<void> intervalTests() async {
  for (final startTime in [longAgo, today]) {
    //Speed - Easy
    await testBadge(
      badgeModel: BadgeModel.intervalSpeedBronzeEasy(),
      shownTasks: 12,
      pointsAchieved: 12,
      pointsPerCorrectTask: 2,
      difficulty: Difficulty.easy,
      taskMode: TaskMode.interval,
      startTime: startTime,
      duration: const Duration(minutes: 4),
      subsetBadges: subsetBadges.union({},
    ),);
    await testBadge(
      badgeModel: BadgeModel.intervalSpeedSilverEasy(),
      shownTasks: 12,
      pointsAchieved: 12,
      pointsPerCorrectTask: 2,
      difficulty: Difficulty.easy,
      taskMode: TaskMode.interval,
      startTime: startTime,
      duration: const Duration(minutes: 2),
      subsetBadges: subsetBadges.union({BadgeModel.intervalSpeedBronzeEasy()},
    ),);
    await testBadge(
      badgeModel: BadgeModel.intervalSpeedGoldEasy(),
      shownTasks: 12,
      pointsAchieved: 12,
      pointsPerCorrectTask: 2,
      difficulty: Difficulty.easy,
      taskMode: TaskMode.interval,
      startTime: startTime,
      duration: const Duration(minutes: 1),
      subsetBadges: subsetBadges.union({
        BadgeModel.intervalSpeedBronzeEasy(),
        BadgeModel.intervalSpeedSilverEasy(),
      },
    ),);

    //Speed - Medium
    await testBadge(
      badgeModel: BadgeModel.intervalSpeedBronzeMedium(),
      shownTasks: 12,
      pointsAchieved: 12,
      pointsPerCorrectTask: 2,
      difficulty: Difficulty.medium,
      taskMode: TaskMode.interval,
      startTime: startTime,
      duration: const Duration(minutes: 4),
      subsetBadges: subsetBadges.union({},
    ),);
    await testBadge(
      badgeModel: BadgeModel.intervalSpeedSilverMedium(),
      shownTasks: 12,
      pointsAchieved: 12,
      pointsPerCorrectTask: 2,
      difficulty: Difficulty.medium,
      taskMode: TaskMode.interval,
      startTime: startTime,
      duration: const Duration(minutes: 2),
      subsetBadges: subsetBadges.union({BadgeModel.intervalSpeedBronzeMedium()},
    ),);
    await testBadge(
      badgeModel: BadgeModel.intervalSpeedGoldMedium(),
      shownTasks: 12,
      pointsAchieved: 12,
      pointsPerCorrectTask: 2,
      difficulty: Difficulty.medium,
      taskMode: TaskMode.interval,
      startTime: startTime,
      duration: const Duration(minutes: 1),
      subsetBadges: subsetBadges.union({
        BadgeModel.intervalSpeedBronzeMedium(),
        BadgeModel.intervalSpeedSilverMedium(),
      },
    ),);
    //Speed - Hard
    await testBadge(
      badgeModel: BadgeModel.intervalSpeedBronzeHard(),
      shownTasks: 12,
      pointsAchieved: 12,
      pointsPerCorrectTask: 2,
      difficulty: Difficulty.hard,
      taskMode: TaskMode.interval,
      startTime: startTime,
      duration: const Duration(minutes: 4),
      subsetBadges: subsetBadges.union({},
    ),);
    await testBadge(
      badgeModel: BadgeModel.intervalSpeedSilverHard(),
      shownTasks: 12,
      pointsAchieved: 12,
      pointsPerCorrectTask: 2,
      difficulty: Difficulty.hard,
      taskMode: TaskMode.interval,
      startTime: startTime,
      duration: const Duration(minutes: 2),
      subsetBadges: subsetBadges.union({BadgeModel.intervalSpeedBronzeHard()},
    ),);
    await testBadge(
      badgeModel: BadgeModel.intervalSpeedGoldHard(),
      shownTasks: 12,
      pointsAchieved: 12,
      pointsPerCorrectTask: 2,
      difficulty: Difficulty.hard,
      taskMode: TaskMode.interval,
      startTime: startTime,
      duration: const Duration(minutes: 1),
      subsetBadges: subsetBadges.union({
        BadgeModel.intervalSpeedBronzeHard(),
        BadgeModel.intervalSpeedSilverHard(),
      },
    ),);
    //Correctly Solved in row
    //Easy
    await testBadge(
      badgeModel: BadgeModel.intervalCorrectlySolvedInARowBronzeEasy(),
      correctlySolvedInRowBest: 12,
      shownTasks: 12,
      pointsAchieved: 12,
      pointsPerCorrectTask: 1,
      difficulty: Difficulty.easy,
      taskMode: TaskMode.interval,
      startTime: startTime,
      duration: const Duration(minutes: 15),
      subsetBadges: subsetBadges.union({},
    ),);
    await testBadge(
      badgeModel: BadgeModel.intervalCorrectlySolvedInARowSilverEasy(),
      correctlySolvedInRowBest: 24,
      shownTasks: 24,
      pointsAchieved: 24,
      pointsPerCorrectTask: 1,
      difficulty: Difficulty.easy,
      taskMode: TaskMode.interval,
      startTime: startTime,
      duration: const Duration(minutes: 10),
      subsetBadges: subsetBadges.union({
        BadgeModel.intervalCorrectlySolvedInARowBronzeEasy(),
      },
    ),);
    await testBadge(
      badgeModel: BadgeModel.intervalCorrectlySolvedInARowGoldEasy(),
      correctlySolvedInRowBest: 36,
      shownTasks: 36,
      pointsAchieved: 36,
      pointsPerCorrectTask: 1,
      difficulty: Difficulty.easy,
      taskMode: TaskMode.interval,
      startTime: startTime,
      duration: const Duration(minutes: 15),
      subsetBadges: subsetBadges.union({
        BadgeModel.intervalCorrectlySolvedInARowBronzeEasy(),
        BadgeModel.intervalCorrectlySolvedInARowSilverEasy(),
      },
    ),);

    //Medium
    await testBadge(
      badgeModel: BadgeModel.intervalCorrectlySolvedInARowBronzeMedium(),
      correctlySolvedInRowBest: 12,
      shownTasks: 12,
      pointsAchieved: 12,
      pointsPerCorrectTask: 1,
      difficulty: Difficulty.medium,
      taskMode: TaskMode.interval,
      startTime: startTime,
      duration: const Duration(minutes: 15),
      subsetBadges: subsetBadges.union({},
    ),);
    await testBadge(
      badgeModel: BadgeModel.intervalCorrectlySolvedInARowSilverMedium(),
      correctlySolvedInRowBest: 24,
      shownTasks: 24,
      pointsAchieved: 24,
      pointsPerCorrectTask: 1,
      difficulty: Difficulty.medium,
      taskMode: TaskMode.interval,
      startTime: startTime,
      duration: const Duration(minutes: 15),
      subsetBadges: subsetBadges.union({
        BadgeModel.intervalCorrectlySolvedInARowBronzeMedium(),
      },
    ),);
    await testBadge(
      badgeModel: BadgeModel.intervalCorrectlySolvedInARowGoldMedium(),
      correctlySolvedInRowBest: 36,
      shownTasks: 36,
      pointsAchieved: 36,
      pointsPerCorrectTask: 1,
      difficulty: Difficulty.medium,
      taskMode: TaskMode.interval,
      startTime: startTime,
      duration: const Duration(minutes: 15),
      subsetBadges: subsetBadges.union({
        BadgeModel.intervalCorrectlySolvedInARowBronzeMedium(),
        BadgeModel.intervalCorrectlySolvedInARowSilverMedium(),
      },
    ),);

    //Hard
    await testBadge(
      badgeModel: BadgeModel.intervalCorrectlySolvedInARowBronzeHard(),
      correctlySolvedInRowBest: 12,
      shownTasks: 12,
      pointsAchieved: 12,
      pointsPerCorrectTask: 1,
      difficulty: Difficulty.hard,
      taskMode: TaskMode.interval,
      startTime: startTime,
      duration: const Duration(minutes: 15),
      subsetBadges: subsetBadges.union({},
    ),);
    await testBadge(
      badgeModel: BadgeModel.intervalCorrectlySolvedInARowSilverHard(),
      correctlySolvedInRowBest: 24,
      shownTasks: 24,
      pointsAchieved: 24,
      pointsPerCorrectTask: 1,
      difficulty: Difficulty.hard,
      taskMode: TaskMode.interval,
      startTime: startTime,
      duration: const Duration(minutes: 15),
      subsetBadges: subsetBadges.union({
        BadgeModel.intervalCorrectlySolvedInARowBronzeHard(),
      },
    ),);
    await testBadge(
      badgeModel: BadgeModel.intervalCorrectlySolvedInARowGoldHard(),
      correctlySolvedInRowBest: 36,
      shownTasks: 36,
      pointsAchieved: 36,
      pointsPerCorrectTask: 1,
      difficulty: Difficulty.hard,
      taskMode: TaskMode.interval,
      startTime: startTime,
      duration: const Duration(minutes: 15),
      subsetBadges: subsetBadges.union({
        BadgeModel.intervalCorrectlySolvedInARowBronzeHard(),
        BadgeModel.intervalCorrectlySolvedInARowSilverHard(),
      },
    ),);
  }
}

///[badgeModel] the actual badge to test
///[pointsAchieved, shownTasks, pointsPerCorrectAnswer, difficulty, taskMode, correctlySolvedInRowBest] are the parameters who possible decide if the badge was accomplished.
///[subsetBadges] is a set of badges that are also accomplished, if this badge was accomplished.
Future<void> testBadge({
  required BadgeModel badgeModel,
  required int pointsAchieved,
  required Difficulty difficulty,
  required TaskMode taskMode,
  required int pointsPerCorrectTask,
  required int shownTasks,
  required DateTime startTime,
  required Duration duration,
  required Set<BadgeModel> subsetBadges,
  int correctlySolvedInRowBest = 0,
}) async {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();



  testWidgets('${badgeModel.shortName} accomplished ${startTime == today ? "today" : "long ago"}',
          (tester) async {
        SharedPreferences.setMockInitialValues({});
        await SharedPreferences.getInstance();

        await Future.delayed(const Duration(seconds: 1),);
        runApp(MaterialApp(
          theme: lightTheme.copyWith(
            extensions: <ThemeExtension<dynamic>>[
              CustomColors.light,
            ],
          ),
          home:
          LessonCompleted(
            lessonNameID: LessonIDName.freeTriadTrainingEasy,
            version: '0',
            currentQPVM: TriadQuizPageViewModel(
              settings: TriadSettings(),
              triadProvider: TriadProvider(),
              keyboardVM: TriadKeyboardVM(),
            ),
            startTime: startTime,
            pointsAchieved: pointsAchieved,
            numberOfTasks: double.infinity,
            difficulty: difficulty,
            taskMode: taskMode,
            pointsPerCorrectTask: pointsPerCorrectTask,
            shownTasks: shownTasks,
            endTime: startTime.add(duration),
            correctlySolvedInRowBest: correctlySolvedInRowBest,
          ),
        ),
        );

        //await SharedPreferences.getInstance();
        await tester.pumpAndSettle();

        sleep(const Duration(seconds: showEachScreenSeconds),);

        //Test, if the given badge was accomplished at the right time
        await TonfallSharedPreferences.getAccomplishedDateTimeForBadge(badgeModel).then((value) {
          assert(value == startTime.add(duration),
          'accomplished time for badge ${badgeModel.shortName} (id ${badgeModel.id}) should be ${startTime.add(duration)}, but was $value ',);
        }
        );
        //Test, if the badges included by this badge are also accomplished
        for (final incidentallyAccomplishedBadge in subsetBadges) {
          await TonfallSharedPreferences.getAccomplishedDateTimeForBadge(incidentallyAccomplishedBadge).then((incidentallyAccomplishedBadgeTime) {
            assert(incidentallyAccomplishedBadgeTime == startTime.add(duration),
            '"accomplished time for badge ${incidentallyAccomplishedBadge.shortName} (id $incidentallyAccomplishedBadge.id) should be ${startTime.add(duration)}, but was $incidentallyAccomplishedBadgeTime ");',);
          } );
        }

        //All other badges should not be accomplished
        BadgeModel.idToBadgeModel.forEach((id, badgeFromSet) async {
          if(!subsetBadges.contains(badgeFromSet) && badgeModel != badgeFromSet){
            await TonfallSharedPreferences.getAccomplishedDateTimeForBadge(BadgeModel.idToBadgeModel[id]!)
                .then((value) {
              assert(value == null, 'Badge "${BadgeModel.idToBadgeModel[id]?.shortName}" (id $id) has timestamp $value, but should be null');
            });
          }
        });



        await Future.delayed(const Duration(seconds: 1),);
        runApp(MaterialApp(
          theme: lightTheme.copyWith(
            extensions: <ThemeExtension<dynamic>>[
              CustomColors.light,
            ],
          ),
          home:
          const BadgesView(),
        ),
        );
        await tester.pumpAndSettle();

        if(taskMode == TaskMode.interval){
          await tester.scrollUntilVisible(find.text('INTERVALLE'), 10);
          await tester.pumpAndSettle();
        }

        sleep(const Duration(seconds: showEachScreenSeconds),);

        // A gold medal includes a silver and a bronze medal.
        // A silver medal includes a bronze medal.
        // A bronze medal is solitary.
        // Test, if number of badges match this assumption.
        //
        //One star symbolizes one difficulty step.
        //But the total number of stars also depends on the number of medals,
        //that's why it's a nested switch case.
        //
        ///if:   today <= twoWeeks  => We see every badge twice (allAtime and current)
        ///else: longAgo > twoWeeks => We only see the all-time badge
        final multiplier = startTime == today ? 2 : 1;
        switch (badgeModel.medalColor){
          case BadgeMedalColor.bronze:
            expect(
              find.image(
                AssetImage(
                  badgeModel.picturePath,)
                ,),
              findsNWidgets(1 * multiplier),
            );
            switch (badgeModel.difficulty){
              case Difficulty.easy:
                expect(find.byIcon(Icons.star), findsNWidgets(1 * multiplier),);
              case Difficulty.medium:
                expect(find.byIcon(Icons.star), findsNWidgets(2 * multiplier),);
              case Difficulty.hard:
                expect(find.byIcon(Icons.star), findsNWidgets(3 * multiplier),);
              case Difficulty.other:
                assert(false, 'A badge should be [easy, medium, hard], but not other');
            }

          case BadgeMedalColor.silver:
            expect(
              find.image(
                AssetImage(
                  badgeModel.picturePath,)
                ,),
              findsNWidgets(2 * multiplier),
            );
            switch (badgeModel.difficulty){
              case Difficulty.easy:
                expect(find.byIcon(Icons.star), findsNWidgets(2 * multiplier),);
              case Difficulty.medium:
                expect(find.byIcon(Icons.star), findsNWidgets(4 * multiplier),);
              case Difficulty.hard:
                expect(find.byIcon(Icons.star), findsNWidgets(6 * multiplier),);
              case Difficulty.other:
                assert(false, 'A badge should be [easy, medium, hard], but not other');
            }

          case BadgeMedalColor.gold:
            expect(
              find.image(
                AssetImage(
                  badgeModel.picturePath,)
                ,),
              findsNWidgets(3 * multiplier),
            );
            switch (badgeModel.difficulty){
              case Difficulty.easy:
                expect(find.byIcon(Icons.star), findsNWidgets(3 * multiplier),);
              case Difficulty.medium:
                expect(find.byIcon(Icons.star), findsNWidgets(6 * multiplier),);
              case Difficulty.hard:
                expect(find.byIcon(Icons.star), findsNWidgets(9 * multiplier),);
              case Difficulty.other:
                assert(false, 'A badge should be [easy, medium, hard], but not other');
            }
        }
      });
}

///A function to print everything inside shared preferences.
///Helpful for debugging.
///source: https://stackoverflow.com/a/63719042
Future<Map<String, String>> getAllPrefs() async {
  final prefs = await SharedPreferences.getInstance();
  return <String, String>{
    for (final String key in prefs.getKeys())
      ...{key: prefs.get(key).toString()},
  };
}
