import 'dart:io';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';
import 'package:muwi/enums/badge_period_under_consideration.dart';
import 'package:muwi/enums/lesson_training_names.dart';
import 'package:muwi/model/badge_model.dart';
import 'package:muwi/model/settings/triad_settings.dart';
import 'package:muwi/model/tonfall_shared_preferences.dart';
import 'package:muwi/view/lesson_completed.dart';
import 'package:muwi/view/theme/custom_theme.dart';
import 'package:muwi/view_model/keyboard_vm/triad_keyboard_vm.dart';
import 'package:muwi/view_model/taskproviders/triad_provider.dart';
import 'package:muwi/view_model/triad_quiz_page_view_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

const int showEachScreenSeconds = 1;

void main() async {
  await testStrakBadge('still today: just 5 Minutes',
      currentStreakLength: 6,
      newExpectedStreak: 6,
      lastGame: DateTime.parse('2023-10-10 12:00'),
      currentGame: DateTime.parse('2023-10-10 12:05'),
  );
  await testStrakBadge('still today: early morining and at nigth',
    currentStreakLength: 6,
    newExpectedStreak: 6,
    lastGame: DateTime.parse('2023-10-10 00:00'),
    currentGame: DateTime.parse('2023-10-10 23:55'),
  );
  await testStrakBadge('+1: exactly 24 hours',
    currentStreakLength: 6,
    newExpectedStreak: 7,
    lastGame: DateTime.parse('2023-10-10 12:00'),
    currentGame: DateTime.parse('2023-10-11 12:00'),
  );
  await testStrakBadge('+1: exactly 23 hours',
    currentStreakLength: 6,
    newExpectedStreak: 7,
    lastGame: DateTime.parse('2023-10-10 12:00'),
    currentGame: DateTime.parse('2023-10-11 11:00'),
  );
  await testStrakBadge('+1: max timespan',
    currentStreakLength: 6,
    newExpectedStreak: 7,
    lastGame: DateTime.parse('2023-10-10 00:00'),
    currentGame: DateTime.parse('2023-10-11 23:55'),
  );
  await testStrakBadge('to long 1',
    currentStreakLength: 7,
    newExpectedStreak: 0,
    lastGame: DateTime.parse('2023-10-10 00:00'),
    currentGame: DateTime.parse('2023-10-12 00:00'),
  );
  await testStrakBadge('to long 2',
    currentStreakLength: 7,
    newExpectedStreak: 0,
    lastGame: DateTime.parse('2023-10-10 23:59'),
    currentGame: DateTime.parse('2023-10-12 00:00'),
  );
  await testStrakBadge('to long 3',
    currentStreakLength: 7,
    newExpectedStreak: 0,
    lastGame: DateTime.parse('2023-10-10 11:11'),
    currentGame: DateTime.parse('2023-10-12 00:00'),
  );
}


Future<void> testStrakBadge(
    String testTitle,
    {
      required int currentStreakLength,
      required int newExpectedStreak,
      required DateTime lastGame,
      required DateTime currentGame,
    }) async {

  testWidgets(testTitle,
  (tester) async {

    await Future.delayed(const Duration(seconds: 1));
    IntegrationTestWidgetsFlutterBinding.ensureInitialized();
    SharedPreferences.setMockInitialValues({});
    await SharedPreferences.getInstance();
    await Future.delayed(const Duration(seconds: 1));

    await TonfallSharedPreferences.setValueFromBadge(BadgeModel.streak(), BadgePeriodUnderConsideration.allTime, currentStreakLength);
    await TonfallSharedPreferences.setValueFromBadge(BadgeModel.streak(), BadgePeriodUnderConsideration.twoWeeks, currentStreakLength);
    await TonfallSharedPreferences.setAccomplishedDateTimeForBadge(BadgeModel.streak(), lastGame);
      runApp(MaterialApp(
        theme: lightTheme.copyWith(
          extensions: <ThemeExtension<dynamic>>[
            CustomColors.light,
          ],
        ),
        home:
        LessonCompleted(
          lessonNameID: LessonIDName.bassClef,
          version: '0',
          currentQPVM: TriadQuizPageViewModel(
            settings: TriadSettings(),
            triadProvider: TriadProvider(),
            keyboardVM: TriadKeyboardVM(),
          ),
          startTime: currentGame.subtract(const Duration(minutes: 10)),
          pointsAchieved: 1,
          numberOfTasks: 10,
          //difficulty: difficulty,
          //taskMode: taskMode,
          //pointsPerCorrectTask: 2,
          shownTasks: 10,
          endTime: currentGame,
          //correctlySolvedInRowBest: correctlySolvedInRowBest,
        ),
      ),
      );
      await tester.pumpAndSettle();
      sleep(const Duration(seconds: showEachScreenSeconds));
      final actualNewStreakLengthCurrent = await TonfallSharedPreferences.getValueFromBadge(BadgeModel.streak(), BadgePeriodUnderConsideration.twoWeeks);
      final actualNewStreakLengthAllTime = await TonfallSharedPreferences.getValueFromBadge(BadgeModel.streak(), BadgePeriodUnderConsideration.allTime);

      assert(actualNewStreakLengthCurrent == newExpectedStreak, "Expected streak length dosn't match actual streak length.\n"
          'Actual streak length: $actualNewStreakLengthCurrent, expected streak length: $newExpectedStreak');
    assert(actualNewStreakLengthAllTime == max(currentStreakLength, newExpectedStreak), "Expected streak length dosn't match actual streak length.\n"
        'Actual streak length: $actualNewStreakLengthCurrent, expected streak length: $newExpectedStreak');
  });
}
