import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';
import 'package:muwi/enums/lesson_training_names.dart';
import 'package:muwi/model/settings/triad_settings.dart';
import 'package:muwi/view/lesson_completed.dart';
import 'package:muwi/view/lessons_training.dart';
import 'package:muwi/view/theme/custom_theme.dart';
import 'package:muwi/view_model/keyboard_vm/triad_keyboard_vm.dart';
import 'package:muwi/view_model/taskproviders/triad_provider.dart';
import 'package:muwi/view_model/triad_quiz_page_view_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

const int showEachScreenSeconds = 1;
final DateTime globalStartTime = DateTime.parse('2023-01-01');

/// Completes each lesson.
/// The purpose of this test is to check, if the progress bar and watch badge
/// is on the right button. Probably, there is a way to make this with assertions (find by key?),
/// but until I find out how to do this, just manually look at the simulator
void main() async {
  //Easy
  await testLessonTrainingView(lessonIDName: LessonIDName.trebleClef);
  await testLessonTrainingView(lessonIDName: LessonIDName.bassClef);
  await testLessonTrainingView(lessonIDName: LessonIDName.zeroSharps);
  //Medium
  await testLessonTrainingView(lessonIDName: LessonIDName.oneSharp);
  await testLessonTrainingView(lessonIDName: LessonIDName.oneFlat);
  await testLessonTrainingView(lessonIDName: LessonIDName.twoSharps);
  await testLessonTrainingView(lessonIDName: LessonIDName.twoFlats);
  await testLessonTrainingView(lessonIDName: LessonIDName.threeSharps);
  await testLessonTrainingView(lessonIDName: LessonIDName.threeFlats);
  await testLessonTrainingView(lessonIDName: LessonIDName.fourSharps);
  await testLessonTrainingView(lessonIDName: LessonIDName.fourFlats);
  await testLessonTrainingView(lessonIDName: LessonIDName.fiveSharps);
  await testLessonTrainingView(lessonIDName: LessonIDName.fiveFlats);
  await testLessonTrainingView(lessonIDName: LessonIDName.sixSharps);
  await testLessonTrainingView(lessonIDName: LessonIDName.sixFlats);
  await testLessonTrainingView(lessonIDName: LessonIDName.augmentedAndDiminished);
  await testLessonTrainingView(lessonIDName: LessonIDName.chordsWithAccidentals1);
  await testLessonTrainingView(lessonIDName: LessonIDName.chordsWithAccidentals2);
  //Hard
  await testLessonTrainingView(lessonIDName: LessonIDName.tenorClef);
  await testLessonTrainingView(lessonIDName: LessonIDName.altoClef);
  await testLessonTrainingView(lessonIDName: LessonIDName.allClefs1);
  await testLessonTrainingView(lessonIDName: LessonIDName.allClefs2);

}


Future<void> testLessonTrainingView({
  required LessonIDName lessonIDName,
  DateTime? startTime,
  int pointsAchieved = 12,
  Duration duration = const Duration(minutes: 4),
}) async{
  startTime ??= globalStartTime;
  testWidgets(lessonIDName.displayableGermanName,
          (tester) async {

        await Future.delayed(const Duration(seconds: 1));
        IntegrationTestWidgetsFlutterBinding.ensureInitialized();
        SharedPreferences.setMockInitialValues({});
        await SharedPreferences.getInstance();


        await Future.delayed(const Duration(seconds: 1));
        runApp(MaterialApp(
          theme: lightTheme.copyWith(
            extensions: <ThemeExtension<dynamic>>[
              CustomColors.light,
            ],
          ),
          home:
          LessonCompleted(
            lessonNameID: lessonIDName,
            version: '0',
            currentQPVM: TriadQuizPageViewModel(
              settings: TriadSettings(),
              triadProvider: TriadProvider(),
              keyboardVM: TriadKeyboardVM(),
            ),
            startTime: startTime,
            pointsAchieved: pointsAchieved,
            numberOfTasks: 12,
            //difficulty: difficulty,
            //taskMode: taskMode,
            //pointsPerCorrectTask: 2,
            shownTasks: 12,
            endTime: startTime!.add(duration),
            //correctlySolvedInRowBest: correctlySolvedInRowBest,
          ),
        ),
        );
        await tester.pumpAndSettle();
        sleep(const Duration(seconds: showEachScreenSeconds));

        runApp(MaterialApp(
          theme: lightTheme.copyWith(
            extensions: <ThemeExtension<dynamic>>[
              CustomColors.light,
            ],
          ),
          home: const LessonTraining(version: '0',),
        ),
        );
        await tester.pumpAndSettle();
        await tester.scrollUntilVisible(find.text(lessonIDName.displayableGermanName), 10);
        await tester.pumpAndSettle();
        sleep(const Duration(seconds: showEachScreenSeconds));
      });

}
