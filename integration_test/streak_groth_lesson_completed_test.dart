import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';
import 'package:muwi/enums/badge_period_under_consideration.dart';
import 'package:muwi/enums/lesson_training_names.dart';
import 'package:muwi/model/badge_model.dart';
import 'package:muwi/model/settings/triad_settings.dart';
import 'package:muwi/model/tonfall_shared_preferences.dart';
import 'package:muwi/view/lesson_completed.dart';
import 'package:muwi/view/theme/custom_theme.dart';
import 'package:muwi/view_model/keyboard_vm/triad_keyboard_vm.dart';
import 'package:muwi/view_model/taskproviders/triad_provider.dart';
import 'package:muwi/view_model/triad_quiz_page_view_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

const int showEachScreenSeconds = 2;

///Watch manually, how the streak grows.
///Auto tests, if the right values are written into shared preferences.
void main() async {
  await testStrakBadge('make streak from 0 to gold', ended: [for (var i = 1; i <= 23; i++) (i-1, DateTime.parse('2020-01-${i.toString().padLeft(2, '0')}'))]);
}


Future<void> testStrakBadge(
    String testTitle,
    {
      required List<(int, DateTime)> ended,
    }) async {

  //final started = ended.map((x) => x.subtract(const Duration(minutes: 10))).toList();
  testWidgets(testTitle,
  (tester) async {

    await Future.delayed(const Duration(seconds: 1));
    IntegrationTestWidgetsFlutterBinding.ensureInitialized();
    SharedPreferences.setMockInitialValues({});
    await SharedPreferences.getInstance();
    await Future.delayed(const Duration(seconds: 1));

    for(final i in ended) {
      runApp(MaterialApp(
        theme: lightTheme.copyWith(
          extensions: <ThemeExtension<dynamic>>[
            CustomColors.light,
          ],
        ),
        home:
        LessonCompleted(
          lessonNameID: LessonIDName.bassClef,
          version: '0',
          currentQPVM: TriadQuizPageViewModel(
            settings: TriadSettings(),
            triadProvider: TriadProvider(),
            keyboardVM: TriadKeyboardVM(),
          ),
          startTime: i.$2.subtract(const Duration(minutes: 10)),
          pointsAchieved: 1,
          numberOfTasks: 10,
          //difficulty: difficulty,
          //taskMode: taskMode,
          //pointsPerCorrectTask: 2,
          shownTasks: 10,
          endTime: i.$2,
          //correctlySolvedInRowBest: correctlySolvedInRowBest,
        ),
      ),
      );
      await tester.pumpAndSettle();
      sleep(const Duration(seconds: showEachScreenSeconds));
      final currentStreakLengthFromDisk = await TonfallSharedPreferences.getValueFromBadge(BadgeModel.streak(), BadgePeriodUnderConsideration.twoWeeks);
      final bestStreakLengthFromDisk = await TonfallSharedPreferences.getValueFromBadge(BadgeModel.streak(), BadgePeriodUnderConsideration.allTime);
      final lastGameTimeFromDisk = await TonfallSharedPreferences.getAccomplishedDateTimeForBadge(BadgeModel.streak());

      assert(currentStreakLengthFromDisk == i.$1, 'current streak length should be ${i.$1} but is $currentStreakLengthFromDisk');
      assert(bestStreakLengthFromDisk == i.$1, 'all time best streak length should be ${i.$1} but is $bestStreakLengthFromDisk');
      assert(lastGameTimeFromDisk == i.$2, 'last game time should be ${i.$2}, but was $lastGameTimeFromDisk');

    }
  });
}
