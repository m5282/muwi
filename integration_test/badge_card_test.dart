import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';
import 'package:muwi/model/badge_model.dart';
import 'package:muwi/model/tonfall_shared_preferences.dart';
import 'package:muwi/view/badges_view.dart';
import 'package:muwi/view/theme/custom_theme.dart';
import 'package:shared_preferences/shared_preferences.dart';



///This test throws every possible date and difficult combination into a
///badge card and looks at the outcome.
void main() {
  const DateTime? never = null;
  final now = DateTime.now();
  final longAgo = now.subtract(const Duration(days: 365));
  final dates = <DateTime?>[
    never,
    longAgo,
    now,
  ];

  //maybe someone has the cleverness to see the systematic in this list
  final expectedStars = [
    [0, 0],
    [0, 3],
    [3, 3],
    
    [0, 2],
    [0, 3],
    [3, 3],
    
    [2, 2],
    [2, 3],
    [3, 3],
    
    [0, 1],
    [0, 3],
    [3, 3],
    
    [0, 2],
    [0, 3],
    [3, 3],
    
    [2, 2],
    [2, 3],
    [3, 3],
    
    [1, 1],
    [1, 3],
    [3, 3],
    
    [1, 2],
    [1, 3],
    [3, 3],
    
    [2, 2],
    [2, 3],
    [3, 3],
  ]; 
  var i = 0;
  for (final dateEasy in dates) {
    for (final dateMedium in dates){
      for (final dateHard in dates) {
        testFunction(
            dateEasy: dateEasy, dateMedium: dateMedium, dateHard: dateHard, expectedStars: expectedStars[i],);
        i++;
      }  
    }
  }
}

void testFunction({
  required DateTime? dateEasy,
  required DateTime? dateMedium,
  required DateTime? dateHard,
  required List<int> expectedStars,
}) {
  final badgeEasy = BadgeModel.triadSpeedBronzeEasy();
  final badgeMedium = BadgeModel.triadSpeedBronzeMedium();
  final badgeHard = BadgeModel.triadSpeedBronzeHard();

  IntegrationTestWidgetsFlutterBinding.ensureInitialized();
    testWidgets('${expectedStars[0]} and ${expectedStars[1]} Stars',
            (tester) async {
          SharedPreferences.setMockInitialValues({});
          await SharedPreferences.getInstance();
          await TonfallSharedPreferences.setAccomplishedDateTimeForBadge(
            badgeEasy,
            dateEasy,
          );
          await TonfallSharedPreferences.setAccomplishedDateTimeForBadge(
            badgeMedium,
            dateMedium,
          );
          await TonfallSharedPreferences.setAccomplishedDateTimeForBadge(
            badgeHard,
            dateHard,
          );
          await Future.delayed(const Duration(seconds: 2));
          runApp(MaterialApp(
            theme: lightTheme.copyWith(
              extensions: <ThemeExtension<dynamic>>[
                CustomColors.light,
              ],
            ),
            home:  const BadgesView(),
          ),
          );
          await tester.pumpAndSettle();
          sleep(Duration.zero);
          expect(find.byIcon(Icons.star), findsNWidgets(expectedStars[0] + expectedStars[1]));
        });
}
