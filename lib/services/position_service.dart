import 'dart:collection';
import 'dart:math';

import 'package:analyzer_plugin/utilities/pair.dart';
import 'package:flutter/cupertino.dart';
import 'package:muwi/constants/view_constants.dart';
import 'package:muwi/constants/viewmodel_constants.dart';
import 'package:muwi/enums/button_width.dart';
import 'package:muwi/view_model/real_coordinates.dart';

class PositionService {
  /// Stage 0 of the initialisation.
  static void _init0(RealCoordinates coordinates) {
    const keyboardProportion = ViewConstants.keyBoardFlexFactor + ViewConstants.resultBarFlexFactor;
    final keyboardArea =
        coordinates.keyboardWithResultBar * ViewConstants.keyBoardFlexFactor / keyboardProportion;
    coordinates
      ..resultBarHeight = coordinates.keyboardWithResultBar * 1 / keyboardProportion
      ..bottomTopKeyboardMargin = max(
        ViewConstants.minKeyboardVerticalMargin,
        (keyboardArea - ViewConstants.maxKeyboardHeight) / 2,
      )
      ..keyboardHeight = keyboardArea - 2 * coordinates.bottomTopKeyboardMargin;
  }

  /// First stage of the initialisation. We need to fragment this part, because of some interdependence between the shared computations and the orientation-specific computations.
  static void _init1(RealCoordinates coordinates) {
    // This is the y-coordinate of the middle line of one staff without the offset added for the other staff.
    // This corresponds to position -12 relative to the vertical center of the display.
    coordinates
      ..verticalCenterOfStaff =
          coordinates.displayHeight / 2 - 12 * coordinates.spaceBetweenTwoYCoordinates
      // The height of a note.
      ..heightOfANote = coordinates.spaceBetweenTwoYCoordinates * 2.0
      // Fill the map which stores the real positions for each coordinate in the system used by the quiz page view model.
      ..verticalPositions = calcVerticalPositions(coordinates)
      ..horizontalPositions = calcHorizontalPositions(coordinates);
  }

  /// Initialisation, second stage. These computations rely on some of the orientation-specific values.
  static void _init2(RealCoordinates coordinates) {
    // Adding some abstraction to the numbers
    const topDisplayID = 1;
    const bottomDisplayID = 0;

    // The width of a ledger line is twice the height of a note.
    coordinates
      ..widthOfLedgerLine = coordinates.heightOfANote * 2
      // We need this variable for centering the note on the ledger line. The width of a note 1.4 is times its height. The height extends over two y-coordinates.
      ..widthOfANote = coordinates.heightOfANote * 1.4
      // The bottom of the brace is the position of the lowest regular line in the lower display.
      ..bottomOfBrace = coordinates.verticalPositions[Pair(
        bottomDisplayID,
        ViewModelConstants.lowestRegularLineIndex,
      )]!
      // The brace extends from the lowest to the highest regular line.
      ..heightOfBrace = coordinates.verticalPositions[Pair(
            topDisplayID,
            ViewModelConstants.highestRegularLineIndex,
          )]! -
          coordinates.bottomOfBrace
      // The width of the brace is very close to one tenth of its height.
      ..widthOfBrace = coordinates.heightOfBrace / 10.4
      // The brace starts at the position of the vertical line minus the small offset defined in ViewConstants minus the width of the brace.
      ..leftBeginOfBrace = coordinates.horizontalPositions[0]! -
          coordinates.widthOfBrace -
          ViewConstants.spaceBetweenVerticalLineAndBrace;
  }

  /// Initialisation, third stage. Mostly keyboard specifications.
  static void _init3(RealCoordinates coordinates) {
    // The width of the keyboard itself is the width of the keyboard without the margins.
    // The width of the keyboard including the margins depends on the orientation of the screen.
    coordinates
      ..widthOfKeyboardWithoutMargins = min(
        ViewConstants.maxKeyboardWidth,
        coordinates.keyboardWidth - ViewConstants.horizontalMarginOfKeyboard * 2,
      )
      ..leftRightKeyboardMargin = max(
        ViewConstants.horizontalMarginOfKeyboard,
        (coordinates.keyboardWidth - ViewConstants.maxKeyboardWidth) / 2,
      )

      // The sizes of the buttons
      ..smallButtonWidth =
          computeButtonWidth(ButtonWidth.small, coordinates.widthOfKeyboardWithoutMargins)
      ..intervalButtonWidth =
          computeButtonWidth(ButtonWidth.interval, coordinates.widthOfKeyboardWithoutMargins)
      ..mediumSizedButtonWidth =
          computeButtonWidth(ButtonWidth.medium, coordinates.widthOfKeyboardWithoutMargins)
      ..doubledSmallButtonWidth =
          computeButtonWidth(ButtonWidth.smallTimesTwo, coordinates.widthOfKeyboardWithoutMargins)
      ..doubledMediumSizedButtonWidth =
          computeButtonWidth(ButtonWidth.mediumTimesTwo, coordinates.widthOfKeyboardWithoutMargins)
      ..buttonHeight = computeButtonHeight(coordinates.keyboardHeight);
  }

  /// This computes all the real positions and sizes needed by the QuizView in portrait orientation.
  static RealCoordinates initPortrait(Size screenSize) {
    const allFlexFactors = ViewConstants.keyBoardFlexFactor +
        ViewConstants.resultBarFlexFactor +
        ViewConstants.displayFlexFactor;

    final coordinates = RealCoordinates()
      ..displayHeight = screenSize.height * ViewConstants.displayFlexFactor / allFlexFactors -
          ViewConstants.appBarHeight
      ..keyboardWithResultBar = screenSize.height *
              (ViewConstants.keyBoardFlexFactor + ViewConstants.resultBarFlexFactor) /
              allFlexFactors -
          ViewConstants.appBarHeight;

    _init0(coordinates);

    /// In our custom grid, there are 120 vertical positions in total along the y-axis. Roughly 66 of them lie in the upper half reserved for the display.
    coordinates.spaceBetweenTwoYCoordinates = screenSize.height / 120;

    // Shared computations, part 1
    _init1(coordinates);

    // The width of a regular line is the width of the keyboards minus one fixed offset on both sides respectively.
    coordinates.widthOfRegularLine = max(
      coordinates.horizontalPositions[ViewConstants.maxDrawables]! - ViewConstants.xZeroCoordinate,
      screenSize.width - 2 * ViewConstants.xZeroCoordinate,
    );

    // Shared computations, part 2
    _init2(coordinates);

    // The width reserved for the keyboard is the entire width of the screen in portrait orientation.
    coordinates.keyboardWidth = screenSize.width;

    // Shared computations, part 3
    _init3(coordinates);

    return coordinates;
  }

  static RealCoordinates initLandscape(Size screenSize) {
    final coordinates = RealCoordinates();

    // We do not know the size of the toolbar above the AppBar. We can only estimate it for the time being.
    const estimatedToolbarHeight = 10;
    coordinates
      ..displayHeight = screenSize.height - ViewConstants.appBarHeight - estimatedToolbarHeight
      ..keyboardWithResultBar = coordinates.displayHeight;

    _init0(coordinates);

    // The width of the left display. The space reserved for displaying the task.
    final leftDisplayWidth = screenSize.width / 2;

    // We use 25 positions along the x-axis and we want to include some safety margins.
    // Therefore, we count 40 positions in total.
    // The space between two x-coordinates is 1.5 times the space between two y-coordinates. Hence, we have to reduce the space between the y-coordinates in order to shrink the x-axis.
    // On some screens (especially tablet screens), the space for the display is rather narrow. On such screens, we compress the grid such that there is enough space for 40 x-coordinates.
    // This amounts to choosing the second value.
    // The meaning of coordinates.displayHeight / 57 is the following: The two systems + three ledger lines use 45 positions. If we want to add three imaginary ledger lines to the top and to the bottom, this makes 57.
    coordinates.spaceBetweenTwoYCoordinates =
        min(coordinates.displayHeight / 57, (leftDisplayWidth / 40) / 1.5);

    _init1(coordinates);

    // The width of a regular line is a fraction of the space reserved for the display, adjusted in ViewConstants.
    coordinates.widthOfRegularLine =
        (leftDisplayWidth - coordinates.horizontalPositions[0]!) * ViewConstants.rangeOfRegularLine;

    _init2(coordinates);

    // The size of the keyboard.
    coordinates.keyboardWidth = screenSize.width - leftDisplayWidth;

    _init3(coordinates);

    return coordinates;
  }

  /// This method fills the map verticalPositions, mapping each integer y-coordinate to a real coordinate.
  ///
  /// System for the y-coordinates which are used as keys for the map verticalPositions:
  ///
  ///
  ///            --            <- 10 (highest possible position. Treble: e6, Bass: g4)
  ///                          <- 9
  ///            --            <- 8
  ///                          <- 7
  ///            --            <- 6
  ///                          <- 5
  /// ------------------------ <- 4
  ///                          <- 3
  /// ------------------------ <- 2
  ///                          <- 1
  /// ------------------------ <- 0
  ///                          <- -1
  /// ------------------------ <- -2
  ///                          <- -3
  /// ------------------------ <- -4
  ///                          <- -5
  ///             --           <- -6
  ///                          <- -7
  ///             --           <- -8
  ///                          <- -9
  ///             --           <- -10 (lowest possible position. Treble: f3, Bass: a1)
  static HashMap<Pair<int, int>, double> calcVerticalPositions(RealCoordinates coordinates) {
    final verticalPositions = HashMap<Pair<int, int>, double>();

    // j = 0 <==> upper display, no offset.
    // j = 1 <==> lower display with vertical offset.
    for (var j = 0; j < ViewConstants.numSystems; j++) {
      coordinates.verticalCenterOfStaff += j *
          ViewConstants.verticalDistanceBetweenStaffs *
          (coordinates.spaceBetweenTwoYCoordinates * 2);

      // See illustration above.
      for (var i = -10; i < 11; i++) {
        verticalPositions[Pair(j, i)] =
            coordinates.verticalCenterOfStaff + i * coordinates.spaceBetweenTwoYCoordinates;
      }
    }
    return verticalPositions;
  }

  /// This method fills the map horizontalPositions, mapping each integer x-coordinate to a real coordinate.
  /// This is the current grid
  ///
  ///
  /// 5                                             #
  /// 4     |----|----|----|----|----|----#----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|
  /// 3     |          SPACE         |    |    |    |    |    |    #    |    |    |    ♮    |    ♭    |    O    |    |    |    |    |
  /// 2     |----|----|----|----|----|----|----|----|----#----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|
  /// 1     |        RESERVED        |    |    #    |    |    |    |    |    |    ♮    |    ♭    |    |    O    |    |    |    |    |
  /// 0     |----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|
  /// -1    |          FOR           |    |    |    |    |    #    |    |    |    |    ♮    |    ♭    |    O    |    |    |    |    |
  /// -2    |----|----|----|----|----|----|----|----|----|----|----|----#----|----|----|----|----|----|----|----|----|----|----|----|
  /// -3    |         CLEFS          |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |
  /// -4    |----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|
  /// -5    0    1    2    3    4    5    6    7    8    9   10   11   12    13   14   15   16   17   18   19   20   21   22   23   24
  /// -6
  /// -7
  /// -8
  /// -9
  /// -10
  static HashMap<int, double> calcHorizontalPositions(RealCoordinates coordinates) {
    final horizontalPositions = HashMap<int, double>();

    // See illustration above:
    for (var i = 0; i <= ViewConstants.maxDrawables; i++) {
      horizontalPositions[i] =
          i * coordinates.spaceBetweenTwoYCoordinates * ViewConstants.horizontalScaling +
              ViewConstants.xZeroCoordinate;
    }
    return horizontalPositions;
  }

  /// Compute the width for the buttons in real numbers based on the button type:
  static double computeButtonWidth(
    ButtonWidth buttonWidthType,
    double widthOfKeyboardWithoutMargins,
  ) {
    switch (buttonWidthType) {
      case ButtonWidth.small:
        // The margin is counted twice because it is on each side of the button.
        // The width of one button is the screen size minus all margins divided by the number of buttons.
        return (widthOfKeyboardWithoutMargins -
                ViewConstants.maxNumberOfSmallButtons *
                    2 *
                    ViewConstants.horizontalPaddingOfOneButton) /
            ViewConstants.maxNumberOfSmallButtons;
      case ButtonWidth.medium:
        // Note that margins are not included in the calculations! Therefore, 6 buttons of this size would not fit in one row!
        // This is not a problem in this specific case, because we do not try to squeeze 6 of these buttons into one row.
        return widthOfKeyboardWithoutMargins / ViewConstants.maxNumberOfMediumSizedButtons;
      case ButtonWidth.smallTimesTwo:
        // These buttons have twice the width of the small buttons, MARGINS EXCLUDED.
        return 2 * computeButtonWidth(ButtonWidth.small, widthOfKeyboardWithoutMargins);
      case ButtonWidth.mediumTimesTwo:
        // These buttons have twice the size of the medium sized buttons, MARGINS INCLUDED.
        return 2 * computeButtonWidth(ButtonWidth.medium, widthOfKeyboardWithoutMargins) +
            2 * ViewConstants.horizontalPaddingOfOneButton;
      case ButtonWidth.interval:
        return (widthOfKeyboardWithoutMargins -
                ViewConstants.maxNumberOfIntervalButtons *
                    2 *
                    ViewConstants.horizontalPaddingOfOneButton) /
            ViewConstants.maxNumberOfIntervalButtons;
    }
  }

  /// Compute the height for the buttons in real numbers:
  static double computeButtonHeight(double heightOfKeyboard) {
    // The keyboard uses MainAxisAlignment.spaceEvenly for placing the buttons. Therefore, we indirectly specify the remaining blank space between the buttons by adapting the height of the buttons accordingly.
    final spaceForButtoms = heightOfKeyboard - ViewConstants.minVerticalEmptyKeyboardSpace;
    // There are 4 rows of buttons.
    return min(
      max(ViewConstants.minHeightOfButton, spaceForButtoms / 4),
      ViewConstants.maxHeightOfButton,
    );
  }
}
