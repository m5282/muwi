import 'dart:math';

import 'package:muwi/enums/bass.dart';
import 'package:muwi/model/chord.dart';
import 'package:muwi/model/chord_quality.dart';
import 'package:muwi/model/note.dart';
import 'package:muwi/view_model/position.dart';

//TODO: Make this static
class TriadArrangementService {
  TriadArrangementService({
    required this.rootPosition,
    required this.upperBound,
    required this.lowerBound,
    required this.snowmenOnly,
  }) {
    baseTriadNotes = rootPosition.notes;
    numberOfNotes = 3;
    choiceOfNotes = baseTriadNotes;
  }

  /// The triad to be rearranged.
  Chord rootPosition;

  /// The role of the lowest note in the rearrangement.
  Bass bass = Bass.root;

  /// The notes ot the triad to rearrange.
  /// Example: [C3,E3,G3] for a c major triad, octave 3.
  List<Note> baseTriadNotes = [];

  /// The choice of 2 to 6 notes of the given triad, chosen from the entire displayable range.
  List<Note> choiceOfNotes = [];

  /// The indices of the chosen notes selected from a scale where the index of the base note of the original triad is always 0.
  List<int> indicesInRange = [];

  bool snowmenOnly;

  int minIndex = 0;
  int maxIndex = 0;
  Position upperBound;
  Position lowerBound;
  int numberOfNotes = 3;
  Random rand = Random();

  /// This method rearranges the base triad.
  void rearrange() {
    indicesInRange = [];
    determineRange();
    numberOfNotes = randomNumberOfNotes();
    final choice = genRandomIndices(numberOfNotes);
    choiceOfNotes = indicesToNotes(choice);
    setBass();
  }

  /// This method sets the range for the given triad and the two given clefs.
  /// Example:
  /// Setting: C major triad, octave 4, bass and treble clef.
  /// c4 is at index 0, e4 at index 1, g4 at index 2.
  /// Since e6 is the highest displayable note, a1 the lowest displayable note and c2 the lowest displayable note for C major, the indices range from -6 (which is c2) to 7 (which is e6).
  /// Spelled out, we get the following indices: -6: c2, -5: e2, -4: g2, -3: c3, -2: e3, -1: g3, 0: c4, 1: e4, 2: g4, 3: c5, 4: e5, 5: g5, 6: c6, 7: e6
  void determineRange() {
    var shift = 0;
    final highestOctave = upperBound.octave;
    final lowestOctave = lowerBound.octave;

    /// Ternary system: Every octave has 3 notes.
    /// The octaves in the middle are entirely included in the range. If the highest octave is 6 and the lowest octave is 3, octaves 4 and 5 are entirely included in the range. This gives us already 6 possible notes.
    final intermediateRange = (highestOctave - lowestOctave - 1) * 3;

    /// The root note has position 0. We want to retrieve the possible indices below this note.
    /// We count three notes for each entirely included octave lying below the root note.
    /// If the root note is in octave 4 and the lowest octave is 3, the number is 0.
    /// If the value gets negative, this gets compensated later.
    final intermediateShiftRange = (baseTriadNotes[0].octave - lowestOctave - 1) * 3;

    /// Count possible notes in the lowest octave.
    var counter = 0;

    for (final note in baseTriadNotes) {
      if (lowerBound <= Position(note.letter, lowestOctave)) {
        counter++;
      }
    }

    /// Determine position of the root relative to the lower bound.
    /// Objective: Create a list of indices where
    /// 0 is the root, 1 the third and 2 the fifth.
    /// Negative indices stand for notes lower than the root.
    /// Positive indices > 2 stand for notes higher than the fifth.
    /// All indices stand for notes within the range.
    shift = baseTriadNotes
            .where(
              (n) => n.octave != baseTriadNotes[0].octave,
            ) // number of negative indices in the octave of the root.
            .length +
        intermediateShiftRange +
        counter; // number of positions in the lowest octave.

    /// Count possible notes in the highest octave.
    for (final note in baseTriadNotes) {
      if (Position(note.letter, highestOctave) <= upperBound) {
        counter++;
      }
    }

    /// The entire range of possible notes consists of:
    /// The positions of the notes from the octaves which are entirely included in the defined range.
    /// The positions of the notes in the lowest octave which are equal to or higher than the the lowest possible position.
    /// The positions of the notes in the highest octave which are equal to or lower than the the highest possible position.
    final entireRange = intermediateRange + counter;

    /// Lowest possible index:
    var pointer = -shift;

    /// All possible indices:
    for (var i = 0; i < entireRange; i++) {
      indicesInRange.add(pointer);
      pointer++;
    }

    minIndex = -shift;
    maxIndex = minIndex + entireRange - 1;
  }

  /// There is a 50% chance to get 4 notes and a 50% chance to get 2, 3, 5 or 6 notes.
  int randomNumberOfNotes() {
    if (snowmenOnly) {
      return 3;
    } else {
      final fourNotes = rand.nextBool();
      if (fourNotes) {
        return 4;
      } else {
        if (rootPosition.quality == ChordQuality.diminished ||
            rootPosition.quality == ChordQuality.augmented) {
          final choice = <int>[3, 5, 6];
          return choice[rand.nextInt(choice.length)];
        } else {
          final choice = <int>[2, 3, 5, 6];
          return choice[rand.nextInt(choice.length)];
        }
      }
    }
  }

  /// This method selects a number of indices within the calculated bounds, making sure at least one root index and one third index are among the choice of indices.
  /// The other indices are selected randomly.
  List<int> genRandomIndices(int num) {

    final choice = <int>[];
    final remainingIndices = indicesInRange;
    // If the number of ledger lines is set to 0 and both clefs are equal, there might not be enough positions for 6 notes. Therefore, take the minimum between the number of possible positions and the chosen number of notes.
    int numberOfIndices = min(remainingIndices.length, num);
    numberOfNotes = numberOfIndices;

    /// Get at least one root.
    final rootIndices = remainingIndices.where((i) => i % 3 == 0).toList();
    var mandatoryRoot = rootIndices[rand.nextInt(rootIndices.length)];

    // Shortcut for the snowmen option
    if (snowmenOnly) {
      // This is our root note and we want a snowman. Therefore, there must be enough space for the third and the fifth above the root.
      // If our root note is e6 and we have a G clef, we need to choose a lower root.
      if (mandatoryRoot > maxIndex - 2) {
        mandatoryRoot -= 3;
      }
      final indexOfThird = mandatoryRoot + 1;
      final indexOfFifth = mandatoryRoot + 2;
      return [mandatoryRoot, indexOfThird, indexOfFifth];
    }

    choice.add(mandatoryRoot);
    remainingIndices.remove(mandatoryRoot);
    numberOfIndices--;

    /// Get at least one third.
    final thirdIndices = remainingIndices.where((i) => i % 3 == 1).toList();
    final mandatoryThird = thirdIndices[rand.nextInt(thirdIndices.length)];
    choice.add(mandatoryThird);
    remainingIndices.remove(mandatoryThird);
    numberOfIndices--;

    if (rootPosition.quality == ChordQuality.diminished ||
        rootPosition.quality == ChordQuality.augmented) {
      /// Get at least one fifth if the triad is diminished or augmented.
      final fifthIndices = remainingIndices.where((i) => i % 3 == 2).toList();
      final mandatoryFifth = fifthIndices[rand.nextInt(fifthIndices.length)];
      choice.add(mandatoryFifth);
      remainingIndices.remove(mandatoryFifth);
      numberOfIndices--;
    }

    /// Get any other notes from the given triad.
    while (numberOfIndices > 0) {
      final index = rand.nextInt(remainingIndices.length);
      choice.add(remainingIndices[index]);
      remainingIndices.removeAt(index);
      numberOfIndices--;
    }

    /// The head of the list is the lowest note.
    choice.sort();
    return choice;
  }

  /// Converts indices to notes.
  /// Index % 3 represents the role of the note within the triad (root, third, fifth).
  /// Index div 3 represents the octaves to add (or to subtract).
  List<Note> indicesToNotes(List<int> indices) {
    final notes = <Note>[];

    for (final i in indices) {
      final role = i % 3;
      final octavesToAdd = (i / 3).floor();
      final note = Note(baseTriadNotes[role].noteClass, baseTriadNotes[role].octave + octavesToAdd);

      notes.add(note);
    }
    return notes;
  }

  /// Set the role of the lowest note: root, third or fifth.
  void setBass() {
    final head = choiceOfNotes.first;
    if (head.noteClass == baseTriadNotes[0].noteClass) {
      bass = Bass.root;
    } else if (head.noteClass == baseTriadNotes[1].noteClass) {
      bass = Bass.third;
    } else if (head.noteClass == baseTriadNotes[2].noteClass) {
      bass = Bass.fifth;
    } else {
      throw Exception('This should never happen. The note is none of the base triad notes.');
    }
  }
}
