import 'package:flutter/material.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:muwi/constants/runtime_constants.dart';
import 'package:muwi/model/tonfall_shared_preferences.dart';
import 'package:muwi/tonfall_scroll_behavior.dart';
import 'package:muwi/view/onboarding_view.dart';
import 'package:muwi/view/start_view.dart';
import 'package:muwi/view/update/update_view.dart';

// Copyright 2017 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
import 'package:package_info_plus/package_info_plus.dart';

// The purpose of this class is to decide which screen the user should see
// in the beginning.
// Option 1: Onboarding Screens.
// Option 2: Main Menu of the app.

// Copyright 2017 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
PackageInfo packageInfo = PackageInfo(
  appName: 'Unknown',
  packageName: 'Unknown',
  version: 'Unknown',
  buildNumber: 'Unknown',
  buildSignature: 'Unknown',
  installerStore: 'Unknown',
);
//---------------------------------------------------------

void main() async {
  // Here we are initializing the app.
  // I. e. some persistent variables have to be read from the disk.
  // For this time, we keep the splash screen open.
  final widgetsBinding = WidgetsFlutterBinding.ensureInitialized();

  // Uncomment this if you want to disable landscape mode.
  // SystemChrome.setPreferredOrientations(
  //     [DeviceOrientation.portraitDown, DeviceOrientation.portraitUp]);

  FlutterNativeSplash.preserve(widgetsBinding: widgetsBinding);
  // UNCOMMENT to reset app settings => see the onboarding screen
  // await TonfallSharedPreferences.removeOnboarding();
  final onboarding = await TonfallSharedPreferences.getOnboarding();

  // Let's get the information about this app:
  packageInfo = await PackageInfo.fromPlatform();

  // If there has been an update, the old version would be still stored in the shared preferences.
  // If there was none update, both versions are the same.
  //await TonfallSharedPreferences.setVersion("Test");
  final oldVersion = await TonfallSharedPreferences.getVersion();
  RuntimeConstants.version = packageInfo.version;
  // A couple of lines used for debugging purposes. We can remove these once we are sure that changes to classes stored in SharedPreferences do not cause problems.
  // await TonfallSharedPreferences.removeVersion();
  // await TonfallSharedPreferences.removeSettings("${GlobalConstants.uebungsmodus}settings");

  //String version = await TonfallSharedPreferences.getVersion();
  // Everything is loaded -> remove the splash screen
  FlutterNativeSplash.remove();

  if (onboarding) {
    runApp(
      MaterialApp(
        debugShowCheckedModeBanner: false,
        scrollBehavior: TonfallScrollBehavior(),
        home: OnboardingView(updateScreen: true, version: packageInfo.version),
      ),
    );
  } else if (!onboarding && packageInfo.version != oldVersion) {
    runApp(
      MaterialApp(
        home: UpdateView(version: packageInfo.version),
        debugShowCheckedModeBanner: false,
      ),
    );
  } else {
    runApp(
      MaterialApp(
        home: StartView(version: packageInfo.version),
        debugShowCheckedModeBanner: false,
      ),
    );
  }
}
