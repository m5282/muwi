import 'package:muwi/model/chord_quality.dart';
import 'package:muwi/model/displayable_task.dart';
import 'package:muwi/model/note_class.dart';
import 'package:muwi/model/settings/settings.dart';
import 'package:muwi/view_model/position.dart';

/// A facility that generates tasks. A task contains all parameters needed for the display and the evaluation.
abstract class ITaskProvider {
  DisplayableTask generateTask({
    required Settings settings,
    required Position lowerBound,
    required Position upperBound,
    Map<NoteClass, ChordQuality>? patternToChoseFrom,
  });
}
