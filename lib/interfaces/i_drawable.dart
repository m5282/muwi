import 'package:muwi/enums/color_code.dart';

class IDrawable {
  int get xCoordinate {
    return 0;
  }

  int get yCoordinate {
    return 0;
  }

  String get path {
    return '';
  }

  double get scalingFactor {
    return 1;
  }

  double get verticalOffset {
    return 0;
  }

  ColorCode get colorCode {
    return ColorCode.neutralNote;
  }

  bool get isNote {
    return false;
  }
}
