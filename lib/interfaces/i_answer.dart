/// Abstract class implemented by all classes representing selectable answers.
abstract class IAnswer {
  /// This returns the label to be displayed on a key.
  String get keyLabel => '';
}
