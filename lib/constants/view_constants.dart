import 'package:muwi/constants/viewmodel_constants.dart';

class ViewConstants {
  /// The height of the appBar.
  static const double appBarHeight = 50;

  // --------------------------------- DISPLAY

  /// The height of the display in proportion to the other components
  static const int displayFlexFactor = 6;

  /// The height of the result bar in proportion to the rest of the keyboard.
  static const int resultBarFlexFactor = 1;

  /// The height of the area containing the keys in proportion to the rest of the keyboard.
  static const int keyBoardFlexFactor = 4;

  /// The maximum number of keys.
  static const int maxKeys = 6;

  /// The current number of systems.
  static const int numSystems = 2;

  /// Number of lines between the middle of the lower systems and the middle of the upper system.
  /// We use this for computing the space between both systems.
  static const double verticalDistanceBetweenStaffs = 12;

  /// Position of the vertical line, which is also the position of the horizontal zero coordinate.
  static const double xZeroCoordinate = 50;

  /// x-coordinate of the Clefs
  static const int xCoordinateOfClefs = 1;

  /// The proportion of the length of a regular line to the width reserved for the staff,
  /// This is the computation: (leftDisplayWidth - coordinates.horizontalPositions[0]!) * ViewConstants.rangeOfRegularLine;
  static const double rangeOfRegularLine = 9 / 10;

  /// Top and bottom of the treble clef in number of lines
  static const int topOfGClef = 7;
  static const int bottomOfGClef = -6;

  /// Top and bottom of the tenor clef in number of lines
  static const int topOfTenorClef = 6;
  static const int bottomOfTenorClef = -2;

  /// Top and bottom of the bass clef in number of lines
  static const int topOfFClef = 4;
  static const int bottomOfFClef = -3;

  /// Top and bottom of the alto clef in number of lines
  static const int topOfAltoClef = 4;
  static const int bottomOfAltoClef = -4;

  /// Space between vertical line and right border of brace
  static const double spaceBetweenVerticalLineAndBrace = 2;

  /// For unison intervals, we need at most 7 positions to the right of the first note: 2 free slots + 3 accidentals + one free slot + the note.
  static const int maxDrawables = ViewModelConstants.xCoordinateOfSnowman;

  /// The x-coordinate of notes in the default case:
  static const int xCoordinateOfNotes = ViewModelConstants.xCoordinateOfNotes;

  /// Horizontal scaling factor:
  static const double horizontalScaling = 1.5;

  /// The maximum width of the keyboard area without the margins.
  static const double maxKeyboardWidth = 500;

  /// The maximum height of the keyboard area without the margins.
  static const double maxKeyboardHeight = 300;

  /// The horizontal Padding of one single button
  static const double horizontalPaddingOfOneButton = 2;

  /// The left and right margins of the keyboard.
  static const double horizontalMarginOfKeyboard = 4;

  /// Path to the pngs
  static const String braceImage = 'lib/assets/akkolade.png';
  static const String fClefImage = 'lib/assets/bass_clef.png';
  static const String gClefImage = 'lib/assets/violin_clef.png';
  static const String cClefImage = 'lib/assets/c_clef.png';

  // --------------------- KEYBOARD

  /// We want these 7 small buttons to fit in one row: c, d, e, f, g, a, b
  static const int maxNumberOfSmallButtons = 7;

  /// We want 5 interval buttons to fit in one row.
  static const int maxNumberOfIntervalButtons = 5;

  /// We want 6 medium sized buttons to fit in onw row, even if there are only 4 buttons of this size.
  static const int maxNumberOfMediumSizedButtons = 6;

  /// The maximum height of a button.
  static const double maxHeightOfButton = 40;

  /// The minimum height of  a button.
  static const double minHeightOfButton = 20;

  /// The maximum height of the text on a button.
  static const double maxHeightOfButtonText = 30;

  /// The minimum height of the text on a button.
  static const double minHeightOfButtonText = 10;

  /// The minimum total blank space between the buttons across the vertical axis of the keyboard. This does not include the top and bottom margins.
  static const double minVerticalEmptyKeyboardSpace = 20;

  /// The minimum margin at the top / bottom of the keyboard.
  static const double minKeyboardVerticalMargin = 10;

  /// The width of the icons for color-blind users.
  static const double iconWidth = 20;

  /// The height of the icons for color-blind users.
  static const double iconHeight = 20;

  /// If this is set to 0, the icon for color-blind people is positioned entirely within the button and does not protrude.
  static const double iconOffset = -12;

  /// The size of the symbol at the center of the icon.
  static const double iconSymbolSize = iconWidth - 5;

  static const String nextId = '\u{2192}';
  static const String abortId = '\u{2A09}';
  static const String finishId = 'Fertig';


  //------ Badges View
  ///Size of a star, symbolizing a level
  static const double sizeOfStars = 27;

  //Size between two badge cards
  static const double sizeBetweenRows = 10;
}
