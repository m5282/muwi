import 'package:muwi/model/clef.dart';

class ViewModelConstants {
  /// The position of the highest line of each accolade before the ledger lines start.
  static const highestRegularLineIndex = 4;

  /// The position of the lowest line of each accolade before the ledger lines start.
  static const lowestRegularLineIndex = -4;
  static const int beginOfKeySignatureKeys = 6;
  static const int xCoordinateOfNotes = 19;
  static const int snowmanShift = 8;
  static const int xCoordinateOfSnowman = xCoordinateOfNotes + snowmanShift;
  static const double noteScalingFactor = 1;
  static const double accScalingFactor = 2;
  static const double verticalOffsetForSharp = -2;
  static const double verticalOffsetForNatural = -2;
  static const double verticalOffsetForFlat = -1;
  static const double verticalOffsetForNotes = -1;

  /// Path of the image of the black note.
  static const String notePath = 'lib/assets/note.png';

  /// Path of the image of the white note.
  static const String whiteNotePath = 'lib/assets/whitenote.png';

  //--------------------- Default values for the settings:

  /// Maximum value for flats. This value is currently not modifiable by the user.
  static const int maxFlats = -1;

  /// Maximum value for sharps. This value is currently not modifiable by the user.
  static const int maxSharps = 1;

  /// Frequency of augmented triads.
  static const int augmentedFrequency = 1;

  /// Frequency of diminished triads.
  static const int diminishedFrequency = 1;

  /// Frequency of major triads.
  static const int majorFrequency = 2;

  /// Frequency of minor triads.
  static const int minorFrequency = 2;

  /// Set this flag to true if user wants to opt out chord rearrangements.
  static const bool snowmenOnly = false;

  /// Setting this flag to true disables augmented and diminished "imperfect" intervals, i.e., no augmented or diminished seconds, third, sixths, etc.
  static const bool maxTwoAccidentals = true;

  /// Set this flag to get g clef in upper display and f clef in lower display.
  static const bool standardClefArrangement = false;

  /// If this flag is set to true, x flat flat (x in [c,d,e,f,g,a,b]) will be displayed as x flat if x flat is already present in the key signature.
  /// If this flag is set to false, x flat flat will be displayed as x flat flat even if x flat is present in the key signature.
  /// Same for x sharp sharp.
  static const bool cumulativeAccidentals = true;

  /// Number of key signature accidentals allowed by the user. Default = 6;
  static const int maxKeySignatureAccidentals = 6;

  /// This map stores the choice of clefs occurring in the tasks. It maps each clef to a boolean value.
  static const Map<Clef, bool> clefPreferences = {
    Clef.treble: true,
    Clef.bass: true,
    Clef.tenor: false,
    Clef.alto: false,
  };

  /// The maximum number of ledger lines above and below the regular lines. Default = 1;
  static const int numberOfLedgerLines = 1;

  // This is the current grid.
  // - Slots 1 to 4 occupied by the clefs. Key signature starts at 6.
  // - At least 2 free slots between the key signature and the accidentals
  // - If notes are on top of each other and carry accidentals, offset each second line of conflicting accidentals and spread add one space between them.
  // - Notes are always in slot 21
  // - worst case: Two notes on top of each other carrying 3 accidentals each. Will take up 7 slots to the left of the note.
  //
  // 10
  //  9
  //  8
  //  7
  //  6
  //  5                                             #
  //  4     |----|----|----|----|----|----#----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|
  //  3     |          SPACE         |    |    |    |    |    |    #    |    |    |    ♮    |    ♭    |    O    |    |    |    |    |
  //  2     |----|----|----|----|----|----|----|----|----#----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|
  //  1     |        RESERVED        |    |    #    |    |    |    |    |    |    ♮    |    ♭    |    |    O    |    |    |    |    |
  // 0      |----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|
  // -1     |          FOR           |    |    |    |    |    #    |    |    |    |    ♮    |    ♭    |    O    |    |    |    |    |
  // -2     |----|----|----|----|----|----|----|----|----|----|----|----#----|----|----|----|----|----|----|----|----|----|----|----|
  // -3     |         CLEFS          |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |
  // -4     |----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|
  // -5     0    1    2    3    4    5    6    7    8    9   10   11   12    13   14   15   16   17   18   19   20   21   22   23   24
  // -6
  // -7
  // -8
  // -9
  // -10
}
