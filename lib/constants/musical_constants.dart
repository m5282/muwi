import 'package:muwi/enums/interval_enum.dart';
import 'package:muwi/enums/interval_quality.dart';
import 'package:muwi/model/chord_quality.dart';
import 'package:muwi/model/interval.dart';
import 'package:muwi/model/mode.dart';
import 'package:muwi/model/note.dart';
import 'package:muwi/model/note_class.dart';
import 'package:muwi/model/scale.dart';

/// Collects some constants. Might be split into categories (for view, model, etc.).
class MusicalConstants {
  /// Frequently used intervals, for convenience.
  static Interval perfectUnison = Interval.forQuality(IntervalEnum.unison, IntervalQuality.perfect);
  static Interval minorSecond = Interval.forQuality(IntervalEnum.second, IntervalQuality.minor);
  static Interval majorSecond = Interval.forQuality(IntervalEnum.second, IntervalQuality.major);
  static Interval minorThird = Interval.forQuality(IntervalEnum.third, IntervalQuality.minor);
  static Interval majorThird = Interval.forQuality(IntervalEnum.third, IntervalQuality.major);
  static Interval perfectFifth = Interval.forQuality(IntervalEnum.fifth, IntervalQuality.perfect);
  static Interval augmentedFifth =
      Interval.forQuality(IntervalEnum.fifth, IntervalQuality.augmented);
  static Interval diminishedFifth =
      Interval.forQuality(IntervalEnum.fifth, IntervalQuality.diminished);
  static Interval minorSixth = Interval.forQuality(IntervalEnum.sixth, IntervalQuality.minor);
  static Interval majorSixth = Interval.forQuality(IntervalEnum.sixth, IntervalQuality.major);
  static Interval minorSeventh = Interval.forQuality(IntervalEnum.seventh, IntervalQuality.minor);
  static Interval majorSeventh = Interval.forQuality(IntervalEnum.seventh, IntervalQuality.major);
  static Interval perfectOctave = Interval.forQuality(IntervalEnum.octave, IntervalQuality.perfect);

  /// This is the choice of root notes for major scales. Needed for the key signature generation.
  static const allowedBaseNotesForMajorScales = [
    NoteClass.c,
    NoteClass.g,
    NoteClass.d,
    NoteClass.a,
    NoteClass.e,
    NoteClass.b,
    NoteClass.f,
    NoteClass.gFlat,
    NoteClass.dFlat,
    NoteClass.aFlat,
    NoteClass.eFlat,
    NoteClass.bFlat,
    NoteClass.fSharp,
  ];

  /// This is the choice of root notes for major scales. Needed for the key signature generation.
  static const allowedBaseNotesForMinorScales = [
    NoteClass.c,
    NoteClass.g,
    NoteClass.d,
    NoteClass.a,
    NoteClass.e,
    NoteClass.f,
    NoteClass.b,
    NoteClass.fSharp,
    NoteClass.cSharp,
    NoteClass.gSharp,
    NoteClass.dSharp,
    NoteClass.eFlat,
    NoteClass.bFlat,
  ];

  static List<ChordQuality> majorScale = [
    ChordQuality.major,
    ChordQuality.minor,
    ChordQuality.minor,
    ChordQuality.major,
    ChordQuality.major,
    ChordQuality.minor,
    ChordQuality.diminished,
  ];

  static List<ChordQuality> minorScale = [
    ChordQuality.minor,
    ChordQuality.diminished,
    ChordQuality.major,
    ChordQuality.minor,
    ChordQuality.minor,
    ChordQuality.major,
    ChordQuality.major,
  ];

  static List<NoteClass> cMajorScale = const Scale(baseNote: Note(NoteClass.c, 1), mode: Mode.major)
      .notes
      .map((e) => e.noteClass)
      .toList();
  static List<NoteClass> gMajorScale = const Scale(baseNote: Note(NoteClass.g, 1), mode: Mode.major)
      .notes
      .map((e) => e.noteClass)
      .toList();
  static List<NoteClass> fMajorScale = const Scale(baseNote: Note(NoteClass.f, 1), mode: Mode.major)
      .notes
      .map((e) => e.noteClass)
      .toList();
  static List<NoteClass> dMajorScale = const Scale(baseNote: Note(NoteClass.d, 1), mode: Mode.major)
      .notes
      .map((e) => e.noteClass)
      .toList();
  static List<NoteClass> bFlatMajorScale =
      const Scale(baseNote: Note(NoteClass.bFlat, 1), mode: Mode.major)
          .notes
          .map((e) => e.noteClass)
          .toList();
  static List<NoteClass> aMajorScale = const Scale(baseNote: Note(NoteClass.a, 1), mode: Mode.major)
      .notes
      .map((e) => e.noteClass)
      .toList();
  static List<NoteClass> eFlatMajorScale =
      const Scale(baseNote: Note(NoteClass.eFlat, 1), mode: Mode.major)
          .notes
          .map((e) => e.noteClass)
          .toList();
  static List<NoteClass> eMajorScale = const Scale(baseNote: Note(NoteClass.e, 1), mode: Mode.major)
      .notes
      .map((e) => e.noteClass)
      .toList();
  static List<NoteClass> aFlatMajorScale =
      const Scale(baseNote: Note(NoteClass.aFlat, 1), mode: Mode.major)
          .notes
          .map((e) => e.noteClass)
          .toList();
  static List<NoteClass> bMajorScale = const Scale(baseNote: Note(NoteClass.b, 1), mode: Mode.major)
      .notes
      .map((e) => e.noteClass)
      .toList();
  static List<NoteClass> dFlatMajorScale =
      const Scale(baseNote: Note(NoteClass.dFlat, 1), mode: Mode.major)
          .notes
          .map((e) => e.noteClass)
          .toList();
  static List<NoteClass> fSharpMajorScale =
      const Scale(baseNote: Note(NoteClass.fSharp, 1), mode: Mode.major)
          .notes
          .map((e) => e.noteClass)
          .toList();
  static List<NoteClass> gFlatMajorScale =
      const Scale(baseNote: Note(NoteClass.gFlat, 1), mode: Mode.major)
          .notes
          .map((e) => e.noteClass)
          .toList();
}
