import 'package:flutter/material.dart';
import 'package:muwi/enums/badge_medal_color.dart';
import 'package:muwi/enums/badge_period_under_consideration.dart';
import 'package:muwi/model/badge_model.dart';
import 'package:muwi/model/tonfall_shared_preferences.dart';
import 'package:muwi/view/subwidgets/badge_card.dart';
import 'package:muwi/view/subwidgets/badge_head_row.dart';
import 'package:muwi/view/subwidgets/card_button_composer.dart';
import 'package:muwi/view/theme/custom_theme.dart';

class BadgesView extends StatelessWidget {
  const BadgesView({this.now, super.key});

  ///This value is here so that tests can set the current time.
  ///In a normal run, this value doesn't have to be set. Now is calculated when needed.
  final DateTime? now;
  static const double heightBetweenCards = 10;

  @override
  Widget build(BuildContext context) {
    final customColors = Theme.of(context).extension<CustomColors>()!;
    return FutureBuilder<List<dynamic>>(
      future: Future.wait([
        for (int i = 0; i <= BadgeModel.highestID; i++) TonfallSharedPreferences.getAccomplishedDateTimeForBadge(BadgeModel.idToBadgeModel[i]!),
        TonfallSharedPreferences.getValueFromBadge(BadgeModel.streak(), BadgePeriodUnderConsideration.twoWeeks), //highestID + 1 = 37
        TonfallSharedPreferences.getValueFromBadge(BadgeModel.streak(), BadgePeriodUnderConsideration.allTime),  //highestID + 2 = 38
      ]),
      builder: (context, AsyncSnapshot<List<dynamic>> snapshot) {
        if (snapshot.hasData) {
          return Scaffold(
            appBar: AppBar(
              title: const Text('Abzeichen'),
              leading: IconButton(
                icon: const BackButton(),
                onPressed: () => Navigator.of(context).pop(),
              ),
            ),
            body: SingleChildScrollView(
              child: Container(
                margin: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: Column(
                  children: <Widget>[
                    const SizedBox(
                      height: 40,
                    ),
                    CardButtonComposer(
                      headerText: 'DREIKLÄNGE',
                      backgroundColor: customColors.uebungsmodusLightColor,
                      children: [
                        BadgeHeadRow(
                          backgroundColor: customColors.uebungsmodusColor,
                          text1: '2 Wochen',
                          text2: 'Allzeit',
                          text3: 'Beschreibung',
                        ),
                        const SizedBox(
                          height: heightBetweenCards,
                        ),
                        for (int offset = 0; offset <= BadgeModel.highestTriadID; offset += 3)
                          Column(
                            children: [
                              BadgeCard.composeAllDifficulties(
                                badgeEasyTime: snapshot.data![offset + 0] as DateTime?,
                                badgeMediumTime: snapshot.data![offset + 1] as DateTime?,
                                oneConcreteBadge: BadgeModel.idToBadgeModel[offset + 2] ??
                                    BadgeModel.errorBadge(),
                                badgeHardTime: snapshot.data?[offset + 2] as DateTime?,
                                backgroundColor: customColors.notSelectedColor,
                                badgeColor: (BadgeModel.idToBadgeModel[offset + 2]?.medalColor == BadgeMedalColor.gold) ? customColors.goldColor :
                                (BadgeModel.idToBadgeModel[offset + 2]?.medalColor == BadgeMedalColor.silver) ? customColors.silverColor :
                                customColors.bronzeColor,
                              ),
                              const SizedBox(
                                height: heightBetweenCards,
                              ),
                            ],
                          ),
                      ],
                    ),
                    const SizedBox(
                      height: 4 * heightBetweenCards,
                    ),
                    CardButtonComposer(
                      headerText: 'INTERVALLE',
                      backgroundColor: customColors.klausurmodusLightColor,
                      children: [
                        BadgeHeadRow(
                          backgroundColor: customColors.klausurmodusColor,
                          text1: '2 Wochen',
                          text2: 'Allzeit',
                          text3: 'Beschreibung',
                        ),
                        const SizedBox(
                          height: heightBetweenCards,
                        ),
                        for (int offset = BadgeModel.highestTriadID + 1; offset <= BadgeModel.highestIntervalID; offset += 3)
                          Column(
                            children: [
                              BadgeCard.composeAllDifficulties(
                                badgeEasyTime: snapshot.data![offset + 0] as DateTime?,
                                badgeMediumTime: snapshot.data![offset + 1] as DateTime?,
                                oneConcreteBadge: BadgeModel.idToBadgeModel[offset + 2] ?? BadgeModel.errorBadge(),
                                badgeHardTime: snapshot.data![offset + 2] as DateTime?,
                                backgroundColor: customColors.notSelectedColor,
                                badgeColor: (BadgeModel.idToBadgeModel[offset + 2]?.medalColor == BadgeMedalColor.gold) ? customColors.goldColor :
                                (BadgeModel.idToBadgeModel[offset + 2]?.medalColor == BadgeMedalColor.silver) ? customColors.silverColor :
                                customColors.bronzeColor,
                              ),
                              const SizedBox(
                                height: heightBetweenCards,
                              ),
                            ],
                          ),
                      ],
                    ),
                    const SizedBox(
                      height: 4 * heightBetweenCards,
                    ),
                    CardButtonComposer(
                      headerText: 'GLOBAL',
                      backgroundColor: customColors.lernmodusLightColor,
                      children: [
                        BadgeHeadRow(
                          backgroundColor: customColors.lernmodusColor,
                          text1: '2 Wochen',
                          text2: 'Allzeit',
                          text3: 'Beschreibung',
                        ),
                        const SizedBox(
                          height: heightBetweenCards,
                        ),
                        for(final BadgeMedalColor medalColor in [BadgeMedalColor.bronze, BadgeMedalColor.silver, BadgeMedalColor.gold])
                          Column(
                            children: [
                              BadgeCard.streakBadge(
                                  medalColor: medalColor,
                                  badgeDescription: '${7 * medalColor.value} Tage in Folge benutzt',
                                  assetPath: BadgeModel.streak().picturePath,
                                  //Todo: pass this via construtor for testing
                                  now: now ?? DateTime.now(),
                                  lastGame: snapshot.data?[36] as DateTime?,
                                  currentValue: snapshot.data?[37] as int? ?? 0,
                                  allTimeBestValue: snapshot.data?[38] as int? ?? 0,
                              ),
                              const SizedBox(
                                height: heightBetweenCards,
                              ),
                            ],
                          ),
                      ],
                    ),
                    const SizedBox(
                      height: 4 * heightBetweenCards,
                    ),
                  ],
                ),
              ),
            ),
          );
        } else {
          return const CircularProgressIndicator();
        }
      },
    );
  }
}
