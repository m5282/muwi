import 'dart:math';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:muwi/constants/view_constants.dart';
import 'package:muwi/enums/color_code.dart';
import 'package:muwi/enums/operations.dart';
import 'package:muwi/model/clef.dart';
import 'package:muwi/services/position_service.dart';
import 'package:muwi/view/lesson_completed.dart';
import 'package:muwi/view/subwidgets/display.dart';
import 'package:muwi/view/subwidgets/keyboard/operations_key.dart';
import 'package:muwi/view/subwidgets/quiz_progress_indicator.dart';
import 'package:muwi/view/subwidgets/usersettings/custom_number_picker.dart';
import 'package:muwi/view/subwidgets/usersettings/menu_option.dart';
import 'package:muwi/view/theme/custom_theme.dart';
import 'package:muwi/view_model/quiz_page_view_model.dart';
import 'package:muwi/view_model/real_coordinates.dart';

abstract class QuizView extends StatefulWidget {
  const QuizView({
    required this.vm,
    required this.lessonCompleted,
    required this.modeName,
    super.key,
    this.numOfTasks = double.infinity,
    this.showSettingsDrawer = true,
  });

  /// The title of the View. This String is displayed on the AppBar.
  final String modeName;

  /// The ViewModel handling all the data needed for the generation and display of a task and the evaluation of the user input.
  final QuizPageViewModel vm;

  // number of tasks to show, before the lessonCompled Screen appears.
  // Can be infinite, if we want to play forever.
  final num numOfTasks;

  //The Screen shown, when the lesson is completed
  final LessonCompleted lessonCompleted;

  //Display the drawer, where the user can adjust the settings,
  //about what will be generated.
  final bool showSettingsDrawer;

  @override
  QuizViewState createState() => QuizViewState();
}

class QuizViewState<T extends QuizView> extends State<T> {
  int result = 0;

  // numOfTasks - "tasks already shown"
  int shownTasks = 1;

  /// FINISH
  void selectFinish() {
    setState(() {
      widget.vm.keyboardVM.finished = true;
      // Evaluate the input and retrieve the result for the statistics
      //widget.vm.keyboardVM.eval();
      result = widget.vm.keyboardVM.eval();
      widget.lessonCompleted.pointsAchieved = widget.lessonCompleted.pointsAchieved! + result;
      //If we solved this task correctly, our row of correctly solved tasks in a
      //row becomes one longer.
      if (result == widget.lessonCompleted.pointsPerCorrectTask) {
        widget.lessonCompleted.correctlySolvedInRowCurrent++;
        if (widget.lessonCompleted.correctlySolvedInRowCurrent >
            widget.lessonCompleted.correctlySolvedInRowBest) {
          widget.lessonCompleted.correctlySolvedInRowBest =
              widget.lessonCompleted.correctlySolvedInRowCurrent;
        }
      }
      // Update solvedTasks, shownTasks and the queue containing the last x results.
      // if (widget.vm.collectStatistics) {
      //   widget.vm.updateStatistics(result);
      // }
    });
  }

  /// ABORT
  void selectAbort() {
    setState(() {
      if (!widget.vm.keyboardVM.finished) {
        // Navigator.pop(context);
        widget.vm.keyboardVM.unselectAll();
      }
    });
  }

  /// NEXT
  void selectNext() {
    if (!widget.vm.keyboardVM.finished) {
      widget.lessonCompleted.correctlySolvedInRowCurrent = 0;
    }

    if (shownTasks < widget.numOfTasks) {
      shownTasks++;
      setState(() {
        // if (widget.vm.collectStatistics) {
        //   // The user wants to skip a task:
        //   if (!widget.vm.finished) {
        //     // A skipped task is a false task.
        //     widget.vm.updateStatistics(false);
        //   }
        // }
        // Get the next chord:
        widget.vm.nextChord();
      });
    } else {
      widget.lessonCompleted.endTime = DateTime.now();
      widget.lessonCompleted.shownTasks = shownTasks;
      Navigator.push(
        context,
        MaterialPageRoute<dynamic>(builder: (context) => widget.lessonCompleted),
      );
    }
  }

  //--------------------------- CALLBACK FUNCTION FOR SETTING THE LEVEL OF DIFFICULTY

  /// Set the maximum number of key signature accidentals
  void setMaxNumberOfKeySignatureAccidentals(int n) {
    setState(() {
      widget.vm.settings.maxKeySignatureAccidentals = n;
      widget.vm.settings.saveButtonText = widget.vm.settings.saveChanges;
    });
  }

  /// Set the max number of ledger lines.
  void setNumberOfLedgerLines(int n) {
    setState(() {
      widget.vm.settings.numberOfLedgerLines = n;
      widget.vm.settings.saveButtonText = widget.vm.settings.saveChanges;
    });
  }

  /// Standard clef arrangement: Bass clef on lower display, treble clef on upper display.
  void toggleStandardClefArrangement() {
    setState(() {
      widget.vm.settings.standardClefArrangement = !widget.vm.settings.standardClefArrangement!;
      widget.vm.settings.saveButtonText = widget.vm.settings.saveChanges;
    });
  }

  late List<List<Widget>> Function(RealCoordinates) rowsOfButtons;

  // List of Text Widgets showing the user input or the correct result:
  List<Widget Function(double)> resultBarWidgets = [];

  /// This list contains the entries for the specific settings. This list will be filled in the classes extending this widget.
  List<Widget> additionalDrawerElements = [];

  /// This list contains the entries additional display elements such as the snowman animation. The snowman animation is added by the TriadQuizView, which extends this class.
  List<Widget Function(RealCoordinates)> additionalDisplayElements = [];

  //-------------------------------------------------------------------
  @override
  Widget build(BuildContext context) {
    /// The colors defined in the theme, which do not correspond to an existing property
    final customColors = Theme.of(context).extension<CustomColors>()!;

    /// This method combines the display components used by all the modes with the display components required only by specific modes.
    /// Example: In triad mode, it adds the snowman animation to the displayed task.
    List<Widget> composeDisplay(RealCoordinates coordinates) {
      return [
        // The display of both systems and the given task.
        Display(
          upperDrawables: widget.vm.upperDrawables,
          lowerDrawables: widget.vm.lowerDrawables,
          upperClefCoordinate: widget.vm.upperClefCoordinate!,
          lowerClefCoordinate: widget.vm.lowerClefCoordinate!,
          realCoordinates: coordinates,
          lowerDisplayLedgerLines: widget.vm.lowerDisplayLedgerLines,
          upperDisplayLedgerLines: widget.vm.upperDisplayLedgerLines,
        ),
        ...additionalDisplayElements.map((f) => f(coordinates)),
        QuizProgressIndicator(numberOfTasks: widget.numOfTasks, shownTasks: shownTasks, height: coordinates.buttonHeight,),
      ];
    }

    //////////////////////////////////////////////////////////////////
    //------------------------- THE KEYBOARD

    /// This function assembles the four rows of either of the mode-specific keyboard.
    /// It relies on a callback function provided by the derived QuizView widget. This is necessary because the assembly of the keyboard depends on the positionService chosen by the OrientationBuilder, which in turn is only part of the general QuizView.
    List<Widget> assembleKeyboardRows(
      RealCoordinates coordinates,
      List<List<Widget>> Function(RealCoordinates) rowsOfButtons,
    ) {
      // The keyboard currently comes in a block of four rows:
      // The last one contains the operations "finish, next, abort" shared by all the modes we currently have (namely triads and intervals).
      // The first three rows are specific to each mode.

      // Extracting the rows, providing the argument for the callback function defined in the derived class.
      final List<Widget> keyBoardRows = rowsOfButtons(coordinates)
          .map((row) => Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: row))
          .toList()
        ..add(
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  OperationsKey(
                    operation: Operations.finish,
                    finished: widget.vm.keyboardVM.finished,
                    colorCode: widget.vm.keyboardVM.answerCodesForOperations[Operations.finish]!,
                    buttonSize:
                        Size(coordinates.doubledMediumSizedButtonWidth, coordinates.buttonHeight),
                    changeState: selectFinish,
                    createButtonLabel: (double height) => AutoSizeText(
                      maxLines: 1,
                      style: TextStyle(fontSize: height),
                      Operations.finish.name,
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  OperationsKey(
                    operation: Operations.next,
                    finished: widget.vm.keyboardVM.finished,
                    colorCode: ColorCode.notSelected,
                    buttonSize: Size(coordinates.doubledSmallButtonWidth, coordinates.buttonHeight),
                    changeState: selectNext,
                    createButtonLabel: (double height) =>
                        Icon(size: height * 1.5, Operations.next.symbol),
                  ),
                  OperationsKey(
                    operation: Operations.abort,
                    finished: widget.vm.keyboardVM.finished,
                    colorCode: ColorCode.notSelected,
                    buttonSize: Size(coordinates.doubledSmallButtonWidth, coordinates.buttonHeight),
                    changeState: selectAbort,
                    createButtonLabel: (double height) => Icon(size: height, Icons.close),
                  ),
                  //right arrow
                ],
              ),
            ],
          ),
        );

      //------- The horizontal bar displaying user input or correct result.
      return [
        Expanded(
          //flex: 1,
          child: Container(
            padding: EdgeInsets.zero,
            color: widget.vm.keyboardVM.resultBarStatusCode.getColor(context),
            child: Stack(
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: resultBarWidgets
                      .map(
                        (f) => Align(
                          child: f(
                            min(
                              ViewConstants.maxHeightOfButtonText,
                              max(
                                ViewConstants.minHeightOfButtonText,
                                (coordinates.resultBarHeight / resultBarWidgets.length) * 2 / 3,
                              ),
                            ),
                          ),
                        ),
                      )
                      .toList(),
                ),
                Align(
                  alignment: Alignment.centerRight,
                  child: Text(
                    widget.vm.keyboardVM.finished ? '+$result' : '',
                    style: TextStyle(
                      fontSize: min(
                        ViewConstants.maxHeightOfButtonText,
                        max(
                          ViewConstants.minHeightOfButtonText,
                          coordinates.resultBarHeight * 2 / 3,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),

        //------- The keyboard, consisting of four rows of buttons.
        Expanded(
          flex: ViewConstants.keyBoardFlexFactor,
          child: Container(
            width: coordinates.keyboardWidth,
            padding: EdgeInsets.fromLTRB(
              coordinates.leftRightKeyboardMargin,
              coordinates.bottomTopKeyboardMargin,
              coordinates.leftRightKeyboardMargin,
              coordinates.bottomTopKeyboardMargin,
            ),
            color: customColors.uebungsmodusLightColor,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: keyBoardRows,
            ),
          ),
        ),
      ];
    }

    /// This method creates one single Column of Expanded widgets with a ratio of 6 / 1 / 5  for display / result bar / keyboard.
    Widget portraitMode(BuildContext context, RealCoordinates coordinates) {
      final mainColumn = <Widget>[
        Expanded(
          // The ratio display/keyboard is 6 / 5 in portrait mode.
          flex: ViewConstants.displayFlexFactor,
          child: Stack(
            children: composeDisplay(coordinates),
          ),
        ),
        ...assembleKeyboardRows(coordinates, rowsOfButtons),
      ];

      return Column(children: mainColumn);
    }

    /// This method creates one row with the display to the left and the keyboard to the right.
    Widget landscapeMode(BuildContext context, RealCoordinates coordinates) {
      final mainColumn = <Widget>[
        Expanded(
          child: Stack(
            children: composeDisplay(coordinates),
          ),
        ),
        Expanded(
          child: Column(children: assembleKeyboardRows(coordinates, rowsOfButtons)),
        ),
      ];
      return Row(children: mainColumn);
    }

    /// This method combines the drawer entries shared by all the modes with the entries specific to individual modes.
    List<Widget> composeDrawer() {
      //--------------------- GENERAL SETTINGS
      final drawerElements = <Widget>[
        Divider(
          color: customColors.uebungsmodusLightColor,
          thickness: 4,
          height: 10,
        ),
        Row(
          children: [
            Expanded(
              child: Text(
                'Allgemeine Einstellungen:',
                style: Theme.of(context).textTheme.titleMedium,
              ),
            ),
          ],
        ),
        Divider(
          color: customColors.uebungsmodusLightColor,
          thickness: 4,
          height: 10,
        ),
        CustomNumberPicker(
          Text(
            'Maximale Anzahl an Generalvorzeichen:',
            style: Theme.of(context).textTheme.labelMedium,
          ),
          widget.vm.settings.maxKeySignatureAccidentals,
          0,
          6,
          setMaxNumberOfKeySignatureAccidentals,
        ),
        CustomNumberPicker(
          Text(
            'Maximale Anzahl an Hilfslinien:',
            style: Theme.of(context).textTheme.labelMedium,
          ),
          widget.vm.settings.numberOfLedgerLines,
          1,
          3,
          setNumberOfLedgerLines,
        ),
        Divider(
          color: customColors.uebungsmodusLightColor,
          thickness: 4,
          height: 10,
        ),
        Row(
          children: [
            Expanded(
              child: Text(
                'Anzeigeoptionen:',
                style: Theme.of(context).textTheme.titleMedium,
              ),
            ),
          ],
        ),
        Divider(
          color: customColors.uebungsmodusLightColor,
          thickness: 4,
          height: 10,
        ),
        Row(
          children: <Widget>[
            Expanded(
              child: MenuOption(
                title: Text(
                  'Kumulierende Vorzeichen',
                  style: Theme.of(context).textTheme.labelMedium,
                ),
                toggleValue: () => setState(
                  () => widget.vm.settings.cumulativeAccidentals =
                      !widget.vm.settings.cumulativeAccidentals,
                ),
                currentValue: widget.vm.settings.cumulativeAccidentals,
              ),
            ),
            IconButton(
              onPressed: () => showDialog<String>(
                context: context,
                builder: (BuildContext context) => AlertDialog(
                  title: const Text('Kumulierte Vorzeichen'),
                  content: SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        const Text(
                            'Diese Option entscheidet, wie viele Versetzungszeichen vor einer Noten stehen, die sowohl durch ein Generalvorzeichen, als auch durch ein Versetzungszeichen alteriert wurde.\n\n'
                            'Ein:\n'
                            'General- und Versetzungszeichen kumulieren. Es werden keine Zeichen wiederholt.'),
                        Image.asset(
                          'lib/assets/cumulative_accidentals_on.png',
                        ),
                        const Text('fisis mit kumulierten Vorzeichen'),
                        const Text('\n\nAus:\n'
                            'In diesem Fall wird das Generalvorzeichen vor der Note wiederholt. Doppelt alterierte Noten haben also immer ein Doppelkreuz.'),
                        Image.asset(
                          'lib/assets/cumulative_accidentals_off.png',
                        ),
                        const Text('fisis ohne kumulierte Vorzeichen'),
                      ],
                    ),
                  ),
                  actions: <Widget>[
                    TextButton(
                      onPressed: () => Navigator.pop(context),
                      child: const Text('Verstanden'),
                    ),
                  ],
                ),
              ),
              icon: const Icon(Icons.info_outline),
            ),
          ],
        ),
        Divider(
          color: customColors.uebungsmodusLightColor,
          thickness: 4,
          height: 10,
        ),
        Row(
          children: [
            Expanded(
              child: Text(
                'Einstellungen zu Schlüsseln:',
                style: Theme.of(context).textTheme.titleMedium,
              ),
            ),
          ],
        ),
        Divider(
          color: customColors.uebungsmodusLightColor,
          thickness: 4,
          height: 10,
        ),
        MenuOption(
          title: Text(
            'Mit Violinschlüssel.',
            style: Theme.of(context).textTheme.labelMedium,
          ),
          toggleValue: () => setState(() {
            widget.vm.settings.toggleClef(Clef.treble);
            widget.vm.settings.saveButtonText = widget.vm.settings.saveChanges;
          }),
          currentValue: widget.vm.settings.clefPreferences[Clef.treble]!,
        ),
        MenuOption(
          title: Text(
            'Mit Bassschlüssel.',
            style: Theme.of(context).textTheme.labelMedium,
          ),
          toggleValue: () => setState(() {
            widget.vm.settings.toggleClef(Clef.bass);
            widget.vm.settings.saveButtonText = widget.vm.settings.saveChanges;
          }),
          currentValue: widget.vm.settings.clefPreferences[Clef.bass]!,
        ),
        MenuOption(
          title: Text(
            'Mit Tenorschlüssel.',
            style: Theme.of(context).textTheme.labelMedium,
          ),
          toggleValue: () => setState(() {
            widget.vm.settings.toggleClef(Clef.tenor);
            widget.vm.settings.saveButtonText = widget.vm.settings.saveChanges;
          }),
          currentValue: widget.vm.settings.clefPreferences[Clef.tenor]!,
        ),
        MenuOption(
          title: Text(
            'Mit Altschlüssel.',
            style: Theme.of(context).textTheme.labelMedium,
          ),
          toggleValue: () => setState(() {
            widget.vm.settings.toggleClef(Clef.alto);
            widget.vm.settings.saveButtonText = widget.vm.settings.saveChanges;
          }),
          currentValue: widget.vm.settings.clefPreferences[Clef.alto]!,
        ),
        MenuOption(
          title: Text(
            'Höher gelegener Schlüssel immer im oberen System.',
            style: Theme.of(context).textTheme.labelMedium,
          ),
          toggleValue: toggleStandardClefArrangement,
          currentValue: widget.vm.settings.standardClefArrangement!,
        ),
        Container(
          padding: const EdgeInsets.fromLTRB(10, 10, 0, 0),
          child: TextButton(
            style: TextButton.styleFrom(
              backgroundColor: customColors.uebungsmodusLightColor,
            ),
            onPressed: () => setState(() {
              widget.vm.settings.updateOtherSettings(widget.vm.settings.otherMode);
            }),
            child: Text(
              widget.vm.settings.saveButtonText,
              style: Theme.of(context).textTheme.labelMedium,
            ),
          ),
        ),
        ...additionalDrawerElements,
        Divider(
          color: customColors.uebungsmodusLightColor,
          thickness: 4,
          height: 10,
        ),
        Container(
          padding: const EdgeInsets.fromLTRB(10, 10, 0, 0),
          child: TextButton(
            style: TextButton.styleFrom(
              backgroundColor: customColors.uebungsmodusLightColor,
            ),
            onPressed: () => setState(() => widget.vm.settings.restoreDefault()),
            child: Text(
              'Einstellungen zurücksetzen',
              style: Theme.of(context).textTheme.labelMedium,
            ),
          ),
        ),
      ]

          //-------------------------- MODE-SPECIFIC SETTINGS

          //-------------------------- RESET BUTTON
          ;
      return drawerElements;
    }

    /////////////////////////////////////////////////////////
    //----------------------- RETURN VALUE (THE ACTUAL QUIZVIEW WIDGET)
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: ViewConstants.appBarHeight,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () {
            //we are in "freies Training",
            // I. e. this button is (currently?) the only option to finish this mode.
            if (widget.numOfTasks == double.infinity) {
              widget.lessonCompleted.shownTasks = shownTasks;
              widget.lessonCompleted.endTime = DateTime.now();
              Navigator.push(
                context,
                MaterialPageRoute<dynamic>(
                  builder: (context) => widget.lessonCompleted,
                ),
              );
            }
            // We are in a "Lektionstraining"
            // The back button can function as a normal back button.
            else {
              Navigator.of(context).pop();
            }
          },
        ),
        title: Text(widget.modeName),
      ),

      body: OrientationBuilder(
        builder: (context, orientation) {
          final screenSize = MediaQuery.of(context).size;
          if (screenSize.width < 1.3 * screenSize.height) {
            final portraitCoordinates = PositionService.initPortrait(screenSize);
            return portraitMode(context, portraitCoordinates);
          } else {
            final landscapeCoordinates = PositionService.initLandscape(screenSize);
            return landscapeMode(context, landscapeCoordinates);
          }
        },
      ),

      ////////////////////////////////////////////////// SETTINGS

      endDrawer: widget.showSettingsDrawer
          ? Drawer(
              child: ListView(
                padding: EdgeInsets.zero,
                children: composeDrawer(),
              ),
            )
          : null,
      onEndDrawerChanged: (isOpen) {
        if (!isOpen) {
          widget.vm.settings.updateSettings();
        }
      },
    );
  }
}
