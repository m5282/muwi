import 'package:flutter/material.dart';
import 'package:muwi/tonfall_scroll_behavior.dart';
import 'package:muwi/view/start_menu.dart';
import 'package:muwi/view/theme/custom_theme.dart';

void main() {
  runApp(const StartView(version: 'unknown'));
}

class StartView extends StatelessWidget {
  const StartView({required this.version, super.key});
  final String version;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      scrollBehavior: TonfallScrollBehavior(),
      theme: lightTheme.copyWith(
        extensions: <ThemeExtension<dynamic>>[
          CustomColors.light,
        ],
      ),
      home: StartMenu(version: version),
    );
  }
}
