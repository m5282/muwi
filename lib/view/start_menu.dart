import 'package:flutter/material.dart';
import 'package:muwi/model/settings/interval_settings.dart';
import 'package:muwi/model/settings/triad_settings.dart';
import 'package:muwi/model/tonfall_shared_preferences.dart';
import 'package:muwi/view/about_view.dart';
import 'package:muwi/view/badges_view.dart';
import 'package:muwi/view/choose_level_interval.dart';
import 'package:muwi/view/choose_level_triad.dart';
import 'package:muwi/view/lessons_training.dart';
import 'package:muwi/view/onboarding_view.dart';
import 'package:muwi/view/subwidgets/card_button_composer.dart';
import 'package:muwi/view/subwidgets/menu_button.dart';
import 'package:muwi/view/theme/custom_theme.dart';

/// This Widget is the first widget that appears to the user in normal scenario.
/// It's the main menu.
class StartMenu extends StatelessWidget {
  const StartMenu({required this.version, super.key});

  final String version;

  //TODO: Get rid of redundancy.

  /// Method loading the current Settings.
  Future<TriadSettings> loadTriadSettings(String mode) async {
    try {
      final maybeSettings = TriadSettings.fromJson(
        await TonfallSharedPreferences.getSettings('${mode}settings'),
      );
      return maybeSettings;
    } on Exception {
      throw Exception('No settings found.');
    }
  }

  /// Method loading the current Settings.
  Future<IntervalSettings> loadIntervalSettings(String mode) async {
    try {
      final maybeSettings = IntervalSettings.fromJson(
        await TonfallSharedPreferences.getSettings('${mode}settings'),
      );
      return maybeSettings;
    } on Exception {
      throw Exception('No settings found.');
    }
  }

  @override
  Widget build(BuildContext context) {
    final customColors = Theme.of(context).extension<CustomColors>()!;

    return Scaffold(
      appBar: AppBar(
        title: const Text('Tonfall'),
      ),
      //meu button
      drawer: Drawer(
        child: ListView(
          children: [
            DrawerHeader(
              child: Image.asset(
                'lib/assets/Tonfall-Logo.png',
              ),
            ),
            ListTile(
              title: Text(
                'Tutorial',
                style: Theme.of(context).textTheme.labelMedium,
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute<dynamic>(
                    builder: (context) => OnboardingView(
                      updateScreen: false,
                      version: version,
                    ),
                  ),
                );
              },
            ),
            ListTile(
              title: Text(
                'Abzeichen',
                style: Theme.of(context).textTheme.labelMedium,
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute<dynamic>(
                    builder: (context) => const BadgesView(),
                  ),
                );
              },
            ),
            ListTile(
              title: Text(
                'About',
                style: Theme.of(context).textTheme.labelMedium,
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute<dynamic>(
                    builder: (context) => AboutView(version: version),
                  ),
                );
              },
            ),
          ],
        ),
      ),
      body: LayoutBuilder(
        builder: (context, constraints) {
          return SingleChildScrollView(
            child: ConstrainedBox(
              //height: height,
              // space (of the buttons) to the left and right border of the screen
              //margin:
              constraints: BoxConstraints(
                minWidth: constraints.maxWidth,
                minHeight: constraints.maxHeight,
              ),
              child: Container(
                //Space to the left and right
                margin: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    // group buttons together
                    Column(
                      // button width takes full width
                      crossAxisAlignment: CrossAxisAlignment.stretch,

                      children: [
                        // whitespace above first button
                        const SizedBox(height: 40),
                        // Group everything, that belongs to "Dreiklänge"
                        CardButtonComposer(
                          backgroundColor: customColors.uebungsmodusColor,
                          headerText: 'DREIKLÄNGE',
                          children: [
                            MenuButton('freies Training', customColors.notSelectedColor, () {
                              Navigator.push(
                                context,
                                MaterialPageRoute<dynamic>(
                                  builder: (context) => const ChooseLevelTriad(),
                                ),
                              );
                            }),
                            MenuButton('Lektionstraining', customColors.notSelectedColor, () {
                              Navigator.push(
                                context,
                                MaterialPageRoute<dynamic>(
                                  builder: (context) => LessonTraining(
                                    version: version,
                                  ),
                                ),
                              );
                            }),
                          ],
                        ),

                        //Space between "DREIKLÄNGE" und "INVERVALL" boxes.
                        const SizedBox(height: 40),
                        CardButtonComposer(
                          backgroundColor: customColors.klausurmodusColor,
                          headerText: 'INTERVALLE',
                          children: [
                            MenuButton('freies Training', customColors.notSelectedColor, () {
                              Navigator.push(
                                context,
                                MaterialPageRoute<dynamic>(
                                  builder: (context) => const ChooseLevelInterval(),
                                ),
                              );
                            }),
                          ],
                        ),

//Todo: hier einfügen!
                      ],
                    ),
                    const SizedBox(height: 40),
                    // App logo at the button
                    Container(
                      margin: const EdgeInsets.fromLTRB(0, 0, 0, 20),
                      child: const Image(
                        image: AssetImage(
                          'lib/assets/Tonfall-Logo.png',
                        ),
                        height: 75,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
