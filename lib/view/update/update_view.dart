import 'package:flutter/material.dart';
import 'package:muwi/model/tonfall_shared_preferences.dart';
import 'package:muwi/view/start_view.dart';

// This file shows the onboarding.
// I. e. a short tutorial.
// Currently, you have to point your run configurations to this file
// to see the onboarding.

class UpdateView extends StatelessWidget {
  const UpdateView({required this.version, super.key});

  final String version;

  @override
  Widget build(BuildContext context) {
    //TODO: Can we get themes here?
    const preliminaryHeadingStyle = TextStyle(fontSize: 17, fontWeight: FontWeight.bold);

    return Scaffold(
      body: Stack(
        children: <Widget>[
          const Image(
            image: AssetImage('lib/assets/OnboardingBackground03.png'),
            // display the image full screen (no borders)
            fit: BoxFit.cover,
            height: double.infinity,
            width: double.infinity,
          ),
          ListView(
            padding: const EdgeInsets.fromLTRB(50, 50, 50, 50),
            children: [
              Center(
                  child: Text(
                'Neuerungen in Version $version:\n',
                style: Theme.of(context).textTheme.headlineMedium,
              ),),
              Text(
                'Version 3.1.0 \n'
                '- Abzeichen für das tägliche Nutzen von Tonfall \n'
                '- Aufgabennummer im Lektionstraining sichtbar \n'
                '- Bug Fixes \n'
                '\n'
                'Version 3.0.0 \n'
                '- Vordefinierte Schwierigkeitsstufen \n'
                '- Lektionstraining\n'
                '- Feedback nach einem abgeschlossenem Aufgabenset\n'
                '- Abzeichen\n'
                '- Querformat',
                style: Theme.of(context).textTheme.bodyLarge,
              ),
              Container(
                padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                child: ElevatedButton(
                  onPressed: () {
                    TonfallSharedPreferences.setVersion(
                      version,
                    ).then((value) =>
                        Navigator.push(
                          context,
                          MaterialPageRoute<dynamic>(builder: (context) => StartView(version: version)),
                        ),
                    );
                  },
                  style: ElevatedButton.styleFrom(
                    foregroundColor: Colors.black,
                    backgroundColor: Colors.white,
                  ),
                  child: const Text(
                    'Loslegen',
                    style: preliminaryHeadingStyle,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
