import 'package:flutter/material.dart';
import 'package:muwi/enums/interval_enum.dart';
import 'package:muwi/enums/interval_quality.dart';
import 'package:muwi/interfaces/i_answer.dart';
import 'package:muwi/model/settings/interval_settings.dart';
import 'package:muwi/view/quiz_view.dart';
import 'package:muwi/view/subwidgets/keyboard/answer_key.dart';
import 'package:muwi/view/subwidgets/usersettings/menu_option.dart';
import 'package:muwi/view/theme/custom_theme.dart';
import 'package:muwi/view_model/keyboard_vm/interval_keyboard_vm.dart';
import 'package:muwi/view_model/quiz_page_view_model.dart';
import 'package:muwi/view_model/real_coordinates.dart';

class IntervalQuizView extends QuizView {
  const IntervalQuizView({
    required QuizPageViewModel intervalVM,
    required super.lessonCompleted,
    super.key,
    super.numOfTasks = double.infinity,
    super.showSettingsDrawer,
  }) : super(vm: intervalVM, modeName: 'Intervalle');

  @override
  IntervalQuizViewState createState() => IntervalQuizViewState();
}

/// This class calls the build-method of the parent class.
/// Before doing so, it contributes all the widgets specific to the interval mode.
class IntervalQuizViewState extends QuizViewState<IntervalQuizView> {
  @override
  Widget build(BuildContext context) {
    //TODO: We need to redefine this because of the Divider-Widget. super.build contains the exact same line. Can we find another solution?
    final customColors = Theme.of(context).extension<CustomColors>()!;

    // We currently need these casts because the QuizView does not know that an IntervalQPVM is always supplied with an IntervalKeyboardVM and IntervalSettings.
    // There might be another way to let it know automatically.
    final keyboardVM = widget.vm.keyboardVM as IntervalKeyboardVM;
    final settings = widget.vm.settings as IntervalSettings;

    //-------------------------------- CALLBACK FUNCTIONS FOR THE KEYBOARD:

    /// INTERVAL
    void selectInterval(IntervalEnum interval) {
      setState(() {
        // Changing the selection is only possible if the finish button wasn't hit yet.
        if (!widget.vm.keyboardVM.finished) {
          // If this accidental is already selected, unselect it.
          if (keyboardVM.intervalEnum == interval) {
            keyboardVM.unselectInterval(interval);
            //widget.vm.keyboardVM.unselectInterval(interval);
          } else {
            // First, cancel previous selection.
            keyboardVM
              ..unselectAllIntervals()
              ..selectInterval(interval);
          }
        }
      });
    }

    /// INTERVAL MODE
    void selectIntervalQuality(IntervalQuality mode) {
      setState(() {
        // Changing the selection is only possible if the finish button wasn't hit yet.
        if (!widget.vm.keyboardVM.finished) {
          // If this accidental is already selected, unselect it.
          if (keyboardVM.intervalQuality == mode) {
            keyboardVM.unselectQuality(mode);
          } else {
            // First, cancel previous selection.
            keyboardVM
              ..unselectAllQualities()
              ..selectQuality(mode);
          }
        }
      });
    }

    //--------- Filling the rows of buttons

    rowsOfButtons = (RealCoordinates coordinates) {
      Widget Function(double) createTextLabel(String label) {
        return (double height) => Text(style: TextStyle(fontSize: height), label);
      }

      Widget createAnswerKey<T extends IAnswer>(
        T answer,
        void Function(T) changeState,
        Size buttonSize,
        Widget Function(double) createKeyboardLabel,
      ) {
        return AnswerKey<T>(
          answer: answer,
          finished: keyboardVM.finished,
          buttonStatusCode: keyboardVM.keyboardColorCodes[answer]!,
          buttonSize: buttonSize,
          changeState: changeState,
          createButtonLabel: createKeyboardLabel,
        );
      }

      final buttonsFirstRow = <Widget>[];
      final buttonsSecondRow = <Widget>[];
      final buttonsThirdRow = <Widget>[];

      // First row of buttons
      for (final interval in [
        IntervalEnum.unison,
        IntervalEnum.second,
        IntervalEnum.third,
        IntervalEnum.fourth,
        IntervalEnum.fifth,
      ]) {
        buttonsFirstRow.add(
          createAnswerKey(
            interval,
            selectInterval,
            Size(coordinates.intervalButtonWidth, coordinates.buttonHeight),
            createTextLabel(interval.keyLabel),
          ),
        );
      }

      //--------- Second row of buttons.
      for (final interval in [
        IntervalEnum.sixth,
        IntervalEnum.seventh,
        IntervalEnum.octave,
        IntervalEnum.ninth,
        IntervalEnum.tenth,
      ]) {
        buttonsSecondRow.add(
          createAnswerKey(
            interval,
            selectInterval,
            Size(coordinates.intervalButtonWidth, coordinates.buttonHeight),
            createTextLabel(interval.keyLabel),
          ),
        );
      }

      //--------- Third row of buttons.
      for (final quality in IntervalQuality.values) {
        buttonsThirdRow.add(
          createAnswerKey(
            quality,
            selectIntervalQuality,
            Size(coordinates.intervalButtonWidth, coordinates.buttonHeight),
            createTextLabel(quality.keyLabel),
          ),
        );
      }
      return [buttonsFirstRow, buttonsSecondRow, buttonsThirdRow];
    };

    //--------- Bar displaying user input or correct answer.
    resultBarWidgets.clear();
    // List of Text Widgets showing the user input or the correct result:
    var resultBarText = '';
    // Show the correct result when the finish button was hit.
    if (widget.vm.keyboardVM.finished) {
      resultBarText = widget.vm.currentDisplayableTask!.task.toString();
      resultBarWidgets
          .add((double height) => Text(style: TextStyle(fontSize: height), resultBarText));
    } else {
      // Before the finish button was hit, show the user input.
      if (keyboardVM.intervalQuality != null) {
        resultBarText += keyboardVM.intervalQuality!.keyLabel;
      }
      if (keyboardVM.intervalEnum != null) {
        resultBarText += keyboardVM.intervalEnum!.keyLabel;
      }
      resultBarWidgets
          .add((double height) => Text(style: TextStyle(fontSize: height), resultBarText));
    }

    //----------------------- Drawer elements for specific settings
    additionalDrawerElements = [
      Divider(color: customColors.uebungsmodusLightColor, thickness: 4, height: 10),
      Row(
        children: [
          Expanded(
            child: Text(
              'Intervallspezifische Einstellungen:',
              style: Theme.of(context).textTheme.titleMedium,
            ),
          ),
        ],
      ),
      MenuOption(
        // This option disables augmented and diminished perfect intervals.
        title: Text(
          'Noten haben maximal zwei Versetzungszeichen und ein Auflösungszeichen.',
          style: Theme.of(context).textTheme.labelMedium,
        ),
        toggleValue: () => setState(() {
          settings.maxTwoAccidentals = !settings.maxTwoAccidentals;
        }),
        currentValue: settings.maxTwoAccidentals,
      ),
    ];

    return super.build(context);
  }
}
