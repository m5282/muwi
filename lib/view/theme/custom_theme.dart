import 'package:flutter/material.dart';

//All colors we ever use as constants
//Colors we have in a light and dark variant.
const colorBlue = Color(0xFF7FBBD4); // selected
const colorBlueLight = Color(0xFFBBDDF0); //Testmodus
const colorPurple = Color(0xFFB08BBD); //Klausurmodus
const colorPurpleLight = Color(0xFFCEAED9); //Klausurmodus
const colorGreenLight = Color(0xFFADCEC2);
const colorGreen = Color(0xFFB9DDA9); //correct and selected
const colorYellow = Color(0xFFF2CA5F);
const colorYellowLight = Color(0xFFF8E49E); //correctAndNotSelected
const colorRed = Color(0xFFFB9A7B); //incorrectAndSelected
const colorRedLight = Color(0xFFFFC9BD);
//colors, we only have in one variant
const colorWhite = Color(0xFFFFFFFF); // notSelected
const colorBlack = Color(0xFF000000);
const colorDeepPurple = Color(0xFFB30047); // Light theme, fifth note.
const colorOceanBlue = Color(0xFF006699); // Light theme, root note.
const colorOrange = Color(0xFFFF6600); // Light theme, fifth note.
const colorGrey = Color(0xFFAAA9AD);
const colorBrown = Color(0xFF977547);

//This class extends the general theme class with the specific color properties
//we need for this app.
class CustomColors extends ThemeExtension<CustomColors> {
  const CustomColors({
    required this.lernmodusColor,
    required this.lernmodusLightColor,
    required this.uebungsmodusColor,
    required this.uebungsmodusLightColor,
    required this.klausurmodusColor,
    required this.klausurmodusLightColor,
    required this.neutralResultBarColor,
    required this.correctAndSelectedColor,
    required this.correctAndSelectedLightColor,
    required this.correctAndNotSelectedColor,
    required this.incorrectAndSelectedColor,
    required this.incorrectAndSelectedLightColor,
    required this.selectedColor,
    required this.notSelectedColor,
    required this.rootNoteColor,
    required this.thirdNoteColor,
    required this.fifthNoteColor,
    required this.neutralNoteColor,
    required this.bronzeColor,
    required this.silverColor,
    required this.goldColor,
  });

  final Color lernmodusColor;
  final Color lernmodusLightColor;
  final Color uebungsmodusColor;
  final Color uebungsmodusLightColor;
  final Color klausurmodusColor;
  final Color klausurmodusLightColor;
  final Color neutralResultBarColor;
  final Color correctAndSelectedColor;
  final Color correctAndSelectedLightColor;
  final Color correctAndNotSelectedColor;
  final Color incorrectAndSelectedColor;
  final Color incorrectAndSelectedLightColor;
  final Color selectedColor;
  final Color notSelectedColor;
  final Color rootNoteColor;
  final Color thirdNoteColor;
  final Color fifthNoteColor;
  final Color neutralNoteColor;
  final Color goldColor;
  final Color silverColor;
  final Color bronzeColor;

  @override
  ThemeExtension<CustomColors> copyWith({
    Color lernmodusColor = colorYellow,
    Color lernmodusLightColor = colorYellowLight,
    Color uebungsmodusColor = colorBlue,
    Color uebungsmodusLightColor = colorBlueLight,
    Color klausurmodusColor = colorPurple,
    Color klausurmodusLightColor = colorPurpleLight,
    Color neutralResultBarColor =  colorBlue,
    Color correctAndSelectedColor = colorGreen,
    Color correctAndSelectedLightColor = colorGreenLight,
    Color correctAndNotSelectedColor = colorYellowLight,
    Color incorrectAndSelectedColor = colorRed,
    Color incorrectAndSelectedLightColor = colorRedLight,
    Color selectedColor = colorBlue,
    Color notSelectedColor = colorWhite,
    Color rootNoteColor = colorOceanBlue,
    Color thirdNoteColor = colorDeepPurple,
    Color fifthNoteColor = colorOrange,
    Color neutralNoteColor = colorBlack,
    Color goldColor = colorYellow,
    Color silverColor = colorGrey,
    Color bronzeColor = colorBrown,

  }) {
    return CustomColors(
      lernmodusColor: this.lernmodusColor,
      lernmodusLightColor: this.lernmodusLightColor,
      uebungsmodusColor: this.uebungsmodusColor,
      uebungsmodusLightColor: this.uebungsmodusLightColor,
      klausurmodusColor: this.klausurmodusColor,
      klausurmodusLightColor: this.klausurmodusLightColor,
      neutralResultBarColor: this.neutralResultBarColor,
      correctAndSelectedColor: this.correctAndSelectedColor,
      correctAndSelectedLightColor: this.correctAndSelectedLightColor,
      correctAndNotSelectedColor: this.correctAndNotSelectedColor,
      incorrectAndSelectedColor: this.incorrectAndSelectedColor,
      incorrectAndSelectedLightColor: this.incorrectAndSelectedLightColor,
      selectedColor: this.selectedColor,
      notSelectedColor: this.notSelectedColor,
      rootNoteColor: this.rootNoteColor,
      thirdNoteColor: this.thirdNoteColor,
      fifthNoteColor: this.fifthNoteColor,
      neutralNoteColor: this.neutralNoteColor,
      bronzeColor: this.bronzeColor,
      silverColor: this.silverColor,
      goldColor: this.goldColor,
    );
  }

  @override
  ThemeExtension<CustomColors> lerp(
    ThemeExtension<CustomColors>? other,
    double t,
  ) {
    if (other is! CustomColors) {
      return this;
    }
    return CustomColors(
      lernmodusColor: Color.lerp(lernmodusColor, other.lernmodusColor, t) ?? colorYellow,
      lernmodusLightColor: Color.lerp(lernmodusLightColor, other.lernmodusLightColor, t) ?? colorYellowLight,
      uebungsmodusColor: Color.lerp(uebungsmodusColor, other.uebungsmodusColor, t) ?? colorBlue,
      uebungsmodusLightColor: Color.lerp(uebungsmodusLightColor, other.uebungsmodusLightColor, t) ?? colorBlueLight,
      klausurmodusColor: Color.lerp(klausurmodusColor, other.klausurmodusColor, t) ?? colorPurple,
      klausurmodusLightColor: Color.lerp(klausurmodusLightColor, other.klausurmodusLightColor, t) ?? colorPurpleLight,
      neutralResultBarColor: Color.lerp(neutralResultBarColor, other.neutralResultBarColor, t) ?? colorBlue,
      correctAndSelectedColor:
          Color.lerp(correctAndSelectedColor, other.correctAndSelectedColor, t) ?? colorGreen,
      correctAndSelectedLightColor: Color.lerp(
        correctAndSelectedLightColor,
        other.correctAndSelectedLightColor,
        t,
      ) ?? colorGreenLight,
      correctAndNotSelectedColor: Color.lerp(
        correctAndNotSelectedColor,
        other.correctAndNotSelectedColor,
        t,
      ) ?? colorYellowLight,
      incorrectAndSelectedColor: Color.lerp(
        incorrectAndSelectedColor,
        other.incorrectAndSelectedColor,
        t,
      ) ?? colorRed,
      incorrectAndSelectedLightColor: Color.lerp(
        incorrectAndSelectedLightColor,
        other.incorrectAndSelectedLightColor,
        t,
      ) ?? colorRedLight,
      selectedColor: Color.lerp(selectedColor, other.selectedColor, t) ?? colorBlue,
      notSelectedColor: Color.lerp(notSelectedColor, other.notSelectedColor, t) ?? colorWhite,
      rootNoteColor: Color.lerp(rootNoteColor, other.rootNoteColor, t) ?? colorOceanBlue,
      thirdNoteColor: Color.lerp(thirdNoteColor, other.thirdNoteColor, t) ?? colorDeepPurple,
      fifthNoteColor: Color.lerp(fifthNoteColor, other.fifthNoteColor, t) ?? colorOrange,
      neutralNoteColor: Color.lerp(neutralNoteColor, other.neutralNoteColor, t) ?? colorBlack,
      bronzeColor: Color.lerp(bronzeColor, other.bronzeColor, t) ?? colorBrown,
      silverColor: Color.lerp(silverColor, other.silverColor, t) ?? colorGrey,
      goldColor: Color.lerp(goldColor, other.goldColor, t) ?? colorYellow,
    );
  }

  @override
  String toString() => 'CustomColors('
      'lernmodusColor: $lernmodusColor, '
      'lernmodusLightColor: $lernmodusLightColor, '
      'uebungsmodusColor: $uebungsmodusColor, '
      'uebungsmodusLightColor: $uebungsmodusLightColor, '
      'klausurmodusColor: $klausurmodusColor, '
      'klausurmodusColor: $klausurmodusLightColor, '
      'neutralResultBar: $neutralResultBarColor, '
      'correctAndSelected: $correctAndSelectedColor, '
      'correctAndSelectedLightColor: $correctAndSelectedLightColor, '
      'correctAndNotSelected: $correctAndNotSelectedColor, '
      'incorrectAndSelected: $incorrectAndSelectedColor, '
      'incorrectAndSelectedLightColor: $incorrectAndSelectedLightColor, '
      'selected: $selectedColor, '
      'notSelected: $notSelectedColor, '
      'rootNote: $rootNoteColor, '
      'thirdNote: $thirdNoteColor, '
      'fifthNote: $fifthNoteColor, '
      'neutralNote: $neutralNoteColor, '
      'goldColor: $goldColor'
      'silverColor: $silverColor, '
      'bronzeColor: $bronzeColor, '
      ')';

  //the light theme
  //Set the concrete colors for the custom properties for the light theme.
  static const light = CustomColors(
    lernmodusColor: colorYellow,
    lernmodusLightColor: colorYellowLight,
    uebungsmodusColor: colorBlue,
    uebungsmodusLightColor: colorBlueLight,
    klausurmodusColor: colorPurple,
    klausurmodusLightColor: colorPurpleLight,
    neutralResultBarColor: colorBlue,
    correctAndSelectedColor: colorGreen,
    correctAndSelectedLightColor: colorGreenLight,
    correctAndNotSelectedColor: colorYellowLight,
    incorrectAndSelectedColor: colorRed,
    incorrectAndSelectedLightColor: colorRedLight,
    selectedColor: colorBlue,
    notSelectedColor: colorWhite,
    rootNoteColor: colorOceanBlue,
    thirdNoteColor: colorDeepPurple,
    fifthNoteColor: colorOrange,
    neutralNoteColor: colorBlack,
    goldColor: colorYellow,
    silverColor: colorGrey,
    bronzeColor: colorBrown,
  );
}

ThemeData lightTheme = ThemeData(
  brightness: Brightness.light,
  appBarTheme: const AppBarTheme(
    color: colorBlueLight,
    titleTextStyle: TextStyle(fontSize: 20, color: colorWhite),
  ),
  // The NEW (2021) theme is the one with written names, like bodyMedium
  // The OLD one is the one with letters like body1.
  // We can NOT mix them, otherwise the app breaks.
  textTheme: const TextTheme(
    //Label: You can click, swipe on (beside) the text
    // drawer title (e.g. "Schwierigkeit einstellen")
    // Style used for some of the headings in the settings.
    headlineLarge: TextStyle(fontSize: 35, color: colorWhite, fontWeight: FontWeight.bold),
    titleMedium: TextStyle(fontSize: 17, color: colorBlack, fontWeight: FontWeight.bold),
    labelLarge: TextStyle(fontSize: 17, color: colorBlack),
    // standard text. Used for text in drawers like settings and difficulty
    labelMedium: TextStyle(fontSize: 15, color: colorBlack),
    // continous text like in about
    bodyMedium: TextStyle(color: Colors.black, fontSize: 18, letterSpacing: 2),
  ),

  elevatedButtonTheme: ElevatedButtonThemeData(
    style: ElevatedButton.styleFrom(
      foregroundColor: colorBlack,
      backgroundColor: colorBlue,
      textStyle: const TextStyle(fontSize: 25),
      // set minimum height greater than text size
      minimumSize: const Size(0, 40),
      elevation: 0,
    ),
  ),

  textButtonTheme: TextButtonThemeData(
    style: TextButton.styleFrom(
      elevation: 0,
    ),
  ),
);
