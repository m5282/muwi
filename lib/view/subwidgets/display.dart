import 'package:analyzer_plugin/utilities/pair.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:muwi/model/clef.dart';
import 'package:muwi/view/subwidgets/subdisplay/show_chord.dart';
import 'package:muwi/view/subwidgets/subdisplay/staff_display.dart';
import 'package:muwi/view_model/drawable_muwi.dart';
import 'package:muwi/view_model/real_coordinates.dart';

class Display extends StatelessWidget {
  const Display({
    required this.lowerDrawables,
    required this.upperDrawables,
    required this.lowerClefCoordinate,
    required this.upperClefCoordinate,
    required this.realCoordinates,
    required this.lowerDisplayLedgerLines,
    required this.upperDisplayLedgerLines,
    super.key,
  });

  /// All Drawables, which shall be shown in the upper accolade.
  final List<DrawableMuwi> upperDrawables;

  /// All Drawables, which shall be shown in the lower accolade.
  final List<DrawableMuwi> lowerDrawables;

  /// The y-coordinate and the kind of the lower clef.
  final Pair<Clef, int> lowerClefCoordinate;

  /// The y-coordinate and the kind of the upper clef.
  final Pair<Clef, int> upperClefCoordinate;

  /// The current instance of the position service, containing the real positions on the screen.
  final RealCoordinates realCoordinates;

  /// The coordinates for the ledger lines
  final List<Pair<int, int>> lowerDisplayLedgerLines;
  final List<Pair<int, int>> upperDisplayLedgerLines;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        StaffDisplay(
          realCoordinates: realCoordinates,
          lowerClefCoordinate: lowerClefCoordinate,
          upperClefCoordinate: upperClefCoordinate,
          lowerDisplayLedgerLines: lowerDisplayLedgerLines,
          upperDisplayLedgerLines: upperDisplayLedgerLines,
        ),
        ShowChord(
          realCoordinates: realCoordinates,
          lowerDrawables: lowerDrawables,
          upperDrawables: upperDrawables,
        ),
      ],
    );
  }
}
