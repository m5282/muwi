import 'package:flutter/material.dart';
import 'package:muwi/constants/view_constants.dart';
import 'package:muwi/enums/badge_medal_color.dart';
import 'package:muwi/enums/level_names.dart';

import 'package:muwi/model/badge_model.dart';
import 'package:muwi/view/subwidgets/badge_symbol.dart';
import 'package:muwi/view/theme/custom_theme.dart';

class BadgeCard extends StatelessWidget {
  const BadgeCard({
    required this.badgeDescription,
    required this.assetPath,
    required this.badge1Accomplished,
    required this.badge2Accomplished,
    this.asset1Color,
    this.asset2Color,
    this.badge1Difficulty,
    this.badge2Difficulty,
    this.medalColor,
    this.badgeValue1,
    this.badgeValue2,
    this.backgroundColor,
    super.key,
  });

  factory BadgeCard.streakBadge({
    required BadgeMedalColor medalColor,
    required String badgeDescription,
    required String assetPath,
    required DateTime now,
    required DateTime? lastGame,
    required int currentValue,
    required int allTimeBestValue,
}){
    final currentStreakLength = BadgeModel.newStreakLength(
        now: now,
        nowIsNewStreakTime: false,
        lastGame: lastGame,
        currentValue: currentValue,
    );
   final accomplishedBadgeColorCurrentValue = BadgeModel.getStreakMedalColor(streakLength: currentStreakLength)?.value ?? -1;
   final accomplishedBadgeColorAllTimeBestValue = BadgeModel.getStreakMedalColor(streakLength: allTimeBestValue)?.value ?? -1;
   return BadgeCard(
       medalColor: medalColor,
       badgeDescription: badgeDescription,
       assetPath: assetPath,
       badge1Accomplished: accomplishedBadgeColorCurrentValue >= medalColor.value,
       badge2Accomplished: accomplishedBadgeColorAllTimeBestValue >= medalColor.value,
       badgeValue1: '$currentValue',
       badgeValue2: '$allTimeBestValue',
   );
}

  factory BadgeCard.composeAllDifficulties({
    required DateTime? badgeEasyTime,
    required DateTime? badgeMediumTime,
    required DateTime? badgeHardTime,
    required BadgeModel oneConcreteBadge,
    required Color backgroundColor,
    required Color badgeColor,
  }) {
    final now = DateTime.now().toUtc();
    final badgeEasyTimeDT = badgeEasyTime;
    final badgeMediumTimeDT = badgeMediumTime;
    final badgeHardTimeDT = badgeHardTime;
    const twoWeeks = Duration(days: 14);

    final durationSinceEasyAccomplished = badgeEasyTimeDT != null ? now.difference(badgeEasyTimeDT) : null;
    final durationSinceMediumAccomplished = badgeMediumTimeDT != null ? now.difference(badgeMediumTimeDT) : null;
    final durationSinceHardAccomplished = badgeHardTimeDT != null ? now.difference(badgeHardTimeDT) : null;


    if (badgeHardTime != null) {
      if (durationSinceHardAccomplished != null && durationSinceHardAccomplished <= twoWeeks) {
        return BadgeCard(
          backgroundColor: backgroundColor,
          asset1Color: badgeColor,
          asset2Color: badgeColor,
          badge1Difficulty: Difficulty.hard,
          badge2Difficulty: Difficulty.hard,
          badgeDescription: oneConcreteBadge.germanDescription,
          assetPath: oneConcreteBadge.picturePath,
          badge1Accomplished: true,
          badge2Accomplished: true,
        );
      } else if (durationSinceMediumAccomplished != null && durationSinceMediumAccomplished <= twoWeeks) {
        return BadgeCard(
          backgroundColor: backgroundColor,
          asset1Color: badgeColor,
          asset2Color: badgeColor,
          badge1Difficulty: Difficulty.medium,
          badge2Difficulty: Difficulty.hard,
          badgeDescription: oneConcreteBadge.germanDescription,
          assetPath: oneConcreteBadge.picturePath,
          badge1Accomplished: true,
          badge2Accomplished: true,
        );
      } else if (durationSinceEasyAccomplished != null && durationSinceEasyAccomplished <= twoWeeks) {
        return BadgeCard(
          backgroundColor: backgroundColor,
          asset1Color: badgeColor,
          asset2Color: badgeColor,
          badge1Difficulty: Difficulty.easy,
          badge2Difficulty: Difficulty.hard,
          badgeDescription: oneConcreteBadge.germanDescription,
          assetPath: oneConcreteBadge.picturePath,
          badge1Accomplished: true,
          badge2Accomplished: true,
        );
      } else {
        return BadgeCard(
          backgroundColor: backgroundColor,
          asset1Color: badgeColor,
          asset2Color: badgeColor,
          badge2Difficulty: Difficulty.hard,
          badgeDescription: oneConcreteBadge.germanDescription,
          assetPath: oneConcreteBadge.picturePath,
          badge1Accomplished: false,
          badge2Accomplished: true,
        );
      }
    } else if (badgeMediumTime != null) {
      if (durationSinceMediumAccomplished != null && durationSinceMediumAccomplished <= twoWeeks) {
        return BadgeCard(
          backgroundColor: backgroundColor,
          asset1Color: badgeColor,
          asset2Color: badgeColor,
          badge1Difficulty: Difficulty.medium,
          badge2Difficulty: Difficulty.medium,
          badgeDescription: oneConcreteBadge.germanDescription,
          assetPath: oneConcreteBadge.picturePath,
          badge1Accomplished: true,
          badge2Accomplished: true,
        );
      } else if (durationSinceEasyAccomplished != null && durationSinceEasyAccomplished <= twoWeeks) {
        return BadgeCard(
          backgroundColor: backgroundColor,
          asset1Color: badgeColor,
          asset2Color: badgeColor,
          badge1Difficulty: Difficulty.easy,
          badge2Difficulty: Difficulty.medium,
          badgeDescription: oneConcreteBadge.germanDescription,
          assetPath: oneConcreteBadge.picturePath,
          badge1Accomplished: true,
          badge2Accomplished: true,
        );
    }else {
        return BadgeCard(
          backgroundColor: backgroundColor,
          asset1Color: badgeColor,
          asset2Color: badgeColor,
          badge2Difficulty: Difficulty.medium,
          badgeDescription: oneConcreteBadge.germanDescription,
          assetPath: oneConcreteBadge.picturePath,
          badge1Accomplished: false,
          badge2Accomplished: true,
        );
      }
    } else if (badgeEasyTime != null) {
      if (durationSinceEasyAccomplished != null && durationSinceEasyAccomplished <= twoWeeks) {
        return BadgeCard(
          backgroundColor: backgroundColor,
          asset1Color: badgeColor,
          asset2Color: badgeColor,
          badge1Difficulty: Difficulty.easy,
          badge2Difficulty: Difficulty.easy,
          badgeDescription: oneConcreteBadge.germanDescription,
          assetPath: oneConcreteBadge.picturePath,
          badge1Accomplished: true,
          badge2Accomplished: true,
        );
      } else {
        return BadgeCard(
          backgroundColor: backgroundColor,
          asset1Color: badgeColor,
          asset2Color: badgeColor,
          badge2Difficulty: Difficulty.easy,
          badgeDescription: oneConcreteBadge.germanDescription,
          assetPath: oneConcreteBadge.picturePath,
          badge1Accomplished: false,
          badge2Accomplished: true,
        );
      }
    }

    //Nothing accomplished yet.
    //Do not show any badge or stars, just show the description,
    //so the user knows, what his challenge is.
    return BadgeCard(
      backgroundColor: backgroundColor,
      asset1Color: badgeColor,
      asset2Color: backgroundColor,
      badgeDescription: oneConcreteBadge.germanDescription,
      assetPath: oneConcreteBadge.picturePath,
      badge1Accomplished: false,
      badge2Accomplished: false,
    );
  }

  final Color? backgroundColor;
  final Color? asset1Color;
  final Color? asset2Color;
  final BadgeMedalColor? medalColor;
  final String badgeDescription;
  final String assetPath;
  final bool badge1Accomplished;
  final bool badge2Accomplished;
  final Difficulty? badge1Difficulty;
  final Difficulty? badge2Difficulty;
  final String? badgeValue1;
  final String? badgeValue2;

  @override
  Widget build(BuildContext context) {
    final customColors = Theme.of(context).extension<CustomColors>()!;
    return Container(
      decoration: BoxDecoration(
        color: backgroundColor ?? customColors.notSelectedColor,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Row(
        children: [
          BadgeSymbol(
            accomplished: badge1Accomplished,
            assetPath: assetPath,
            badgeDifficulty: badge1Difficulty,
            badgeValue: badgeValue1,
            assetColor: (asset1Color != null) ? asset1Color! : medalColor == BadgeMedalColor.bronze ? customColors.bronzeColor : medalColor == BadgeMedalColor.silver ? customColors.silverColor : customColors.goldColor,
          ),
          const SizedBox(width: ViewConstants.sizeBetweenRows),
          BadgeSymbol(
            accomplished: badge2Accomplished,
            assetPath: assetPath,
            assetColor: (asset2Color != null) ? asset2Color! : medalColor == BadgeMedalColor.bronze ? customColors.bronzeColor : medalColor == BadgeMedalColor.silver ? customColors.silverColor : customColors.goldColor,
            badgeDifficulty: badge2Difficulty,
            badgeValue: badgeValue2,
          ),
          const SizedBox(width: ViewConstants.sizeBetweenRows),
          Expanded(
            child: Text(
              badgeDescription,
              style: Theme
                  .of(context)
                  .textTheme
                  .bodyMedium,
            ),
          ),
        ],
      ),
    );
  }
}
