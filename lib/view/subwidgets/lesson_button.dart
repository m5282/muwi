import 'package:flutter/material.dart';
import 'package:muwi/constants/runtime_constants.dart';
import 'package:muwi/enums/lesson_training_names.dart';
import 'package:muwi/model/settings/triad_settings.dart';
import 'package:muwi/view/lesson_completed.dart';
import 'package:muwi/view/subwidgets/menu_button.dart';
import 'package:muwi/view/triad_quiz_view.dart';
import 'package:muwi/view_model/keyboard_vm/triad_keyboard_vm.dart';
import 'package:muwi/view_model/taskproviders/triad_provider.dart';
import 'package:muwi/view_model/triad_quiz_page_view_model.dart';

class LessonButton extends StatefulWidget {
  const LessonButton({required this.progressBarColor, required this.backgroundColor, required this.lessonIDName, required this.numberOfTasks, required this.initSettings, required this.progressInPercent, required this.showStopwatch, super.key,
    this.stopwatchColor = Colors.black,
  });

  final Color progressBarColor;
  final Color backgroundColor;
  final LessonIDName lessonIDName;
  final num numberOfTasks;
  final TriadSettings Function() initSettings;
  final double progressInPercent;
  final Color stopwatchColor;
  final bool showStopwatch;

  @override
  State<LessonButton> createState() => _LessonButtonState();
}

class _LessonButtonState extends State<LessonButton> {
  late double _progressInPercent;
  late bool _showStopwatch;

  @override
  void initState() {
    super.initState();
    _progressInPercent = widget.progressInPercent;
    _showStopwatch = widget.showStopwatch;
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      fit: StackFit.passthrough,
      alignment: Alignment.center,
      children: [
        LinearProgressIndicator(
          //the percentage
          value: _progressInPercent,
          color: widget.progressBarColor,
          backgroundColor: widget.backgroundColor,
          minHeight: 40,
        ),
        if (_showStopwatch)
          Positioned(
            right: 10, //10 pixels from the right
            child: Image(
              image: const AssetImage(
                'lib/assets/stopwatch-with-hand.png',
              ),
              color: widget.stopwatchColor,
              height: 30,
            ),
          ),
        MenuButton(
          widget.lessonIDName.displayableGermanName,
          Colors.transparent,
          () async {
            final lessonCompleted = LessonCompleted(
              lessonNameID: widget.lessonIDName,
              version: RuntimeConstants.version,
              currentQPVM: TriadQuizPageViewModel(
                settings: widget.initSettings(),
                triadProvider: TriadProvider(),
                keyboardVM: TriadKeyboardVM(),
              ),
              startTime: DateTime.now(),
              pointsAchieved: 0,
              numberOfTasks: widget.numberOfTasks,
            );
            final res = await Navigator.push(
              context,
              MaterialPageRoute<dynamic>(
                builder: (context) => TriadQuizView(
                  triadVM: TriadQuizPageViewModel(
                    settings: widget.initSettings(),
                    triadProvider: TriadProvider(),
                    keyboardVM: TriadKeyboardVM(),
                  ),
                  numOfTasks: widget.numberOfTasks,
                  lessonCompleted: lessonCompleted,
                  showSettingsDrawer: false,
                ),
              ),
            );
            if (res != null) {
              res as (double, bool);
              setState(() {
                _progressInPercent = res.$1;
                _showStopwatch = res.$2;
              });
            }
          },
        ),
      ],
    );
  }
}
