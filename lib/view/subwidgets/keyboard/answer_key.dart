import 'dart:math';

import 'package:flutter/material.dart';
import 'package:muwi/constants/view_constants.dart';
import 'package:muwi/enums/color_code.dart';
import 'package:muwi/interfaces/i_answer.dart';
import 'package:muwi/view/theme/custom_theme.dart';

class AnswerKey<T extends IAnswer> extends StatelessWidget {
  const AnswerKey({
    required this.answer,
    required this.createButtonLabel,
    required this.finished,
    required this.buttonStatusCode,
    required this.buttonSize,
    required this.changeState,
    super.key,
  });

  final Size buttonSize;
  final void Function(T) changeState;
  final bool finished;
  final ColorCode buttonStatusCode;
  final Widget Function(double) createButtonLabel;
  final T answer;

  @override
  Widget build(BuildContext context) {
    // Firstly, fill the list of widgets, which are currently needed:
    // Add the container for displaying the button itself.
    final outputChildren = <Widget>[
      Container(
        //Todo: Chrome needs vertical margin
        margin: const EdgeInsets.fromLTRB(
          ViewConstants.horizontalPaddingOfOneButton,
          0,
          ViewConstants.horizontalPaddingOfOneButton,
          0,
        ),
        width: buttonSize.width,
        height: buttonSize.height,
        child: TextButton(
          onPressed: () {
            changeState(answer);
          },
          style: TextButton.styleFrom(
            backgroundColor: buttonStatusCode.getColor(context),
            foregroundColor: Colors.black,
            // Delete space between buttons, so this can be handled by the parent widgets.
            tapTargetSize: MaterialTapTargetSize.shrinkWrap,
            alignment: Alignment.center,
            padding: EdgeInsets.zero,
          ),
          child: createButtonLabel(
            max(
              ViewConstants.minHeightOfButtonText,
              min(
                buttonSize.height * 2 / 3,
                ViewConstants.maxHeightOfButtonText,
              ),
            ),
          ),
        ),
      ),
    ];

    // If the user is already finished, show a little circle on top of the button:
    if (finished && buttonStatusCode != ColorCode.notSelected) {
      final currentColour = buttonStatusCode == ColorCode.correctAndNotSelected
          ? Theme.of(context).extension<CustomColors>()!.incorrectAndSelectedColor
          : buttonStatusCode.getColor(context)!;

      outputChildren.add(
        Positioned(
          left: buttonSize.width / 2 -
              ViewConstants.iconWidth / 2 +
              ViewConstants.horizontalPaddingOfOneButton,
          top: ViewConstants.iconOffset,
          child: Container(
            width: ViewConstants.iconWidth,
            height: ViewConstants.iconHeight,
            decoration: BoxDecoration(
              color: Colors.white,
              shape: BoxShape.circle,
              border: Border.all(color: buttonStatusCode.getColor(context)!),
            ),
            child: Center(
              child: Icon(
                buttonStatusCode.getSymbol(context),
                size: ViewConstants.iconSymbolSize,
                color: currentColour,
              ),
            ),
          ),
        ),
      );
    }

    return Stack(clipBehavior: Clip.none, children: outputChildren);
  }
}
