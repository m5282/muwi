import 'dart:math';

import 'package:flutter/material.dart';
import 'package:muwi/constants/view_constants.dart';
import 'package:muwi/enums/color_code.dart';
import 'package:muwi/enums/operations.dart';

class OperationsKey extends StatelessWidget {
  const OperationsKey({
    required this.operation,
    required this.finished,
    required this.colorCode,
    required this.buttonSize,
    required this.changeState,
    required this.createButtonLabel,
    super.key,
  });

  final Size buttonSize;
  final void Function() changeState;
  final bool finished;
  final Operations operation;
  final ColorCode colorCode;
  final Widget Function(double) createButtonLabel;

  @override
  Widget build(BuildContext context) {
    return Container(
      //Todo: Chrome needs vertical margin
      margin: const EdgeInsets.fromLTRB(
        ViewConstants.horizontalPaddingOfOneButton,
        0,
        ViewConstants.horizontalPaddingOfOneButton,
        0,
      ),
      width: buttonSize.width,
      height: buttonSize.height,
      child: TextButton(
        onPressed: changeState,
        style: TextButton.styleFrom(
          foregroundColor: Colors.black,
          //backgroundColor: answerStatusCode.color, //text color
          backgroundColor: colorCode.getColor(context),

          // Delete space between buttons, so this can be handled by the parent widgets.
          tapTargetSize: MaterialTapTargetSize.shrinkWrap,
          alignment: Alignment.center,
          padding: EdgeInsets.zero,
        ),
        child: createButtonLabel(
          max(
            ViewConstants.minHeightOfButtonText,
            min(
              buttonSize.height * 2 / 3,
              ViewConstants.maxHeightOfButtonText,
            ),
          ),
        ),
      ),
    );
  }
}
