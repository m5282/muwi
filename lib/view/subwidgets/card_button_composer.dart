import 'package:flutter/material.dart';

/// We use this widget to compose buttons visually together.
/// E. g. in the start_menu to group the buttons "freies Training" and
/// "Leveltraining" under the header "DREIKLÄNGE" together.
class CardButtonComposer extends StatelessWidget {
  const CardButtonComposer({
    required this.backgroundColor,
    super.key,
    this.headerText = '',
    this.children,
  });

  final Color backgroundColor;
  final String headerText;
  final List<Widget>? children;

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 10,
      color: backgroundColor,
      borderRadius: const BorderRadius.all(Radius.circular(10)),
      child: Container(
        // Add some space to the left and right of the child widgets (buttons).
        padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
        child: Column(
          // The children (Buttons) should fill the horizontal space.
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              headerText,
              style: Theme.of(context).textTheme.headlineLarge,
              textAlign: TextAlign.center,
            ),
            for (final Widget widget in children ?? []) widget,
          ],
        ),
      ),
    );
  }
}
