import 'package:flutter/cupertino.dart';

import 'package:muwi/constants/view_constants.dart';

class BadgeHeadRow extends StatelessWidget {
  const BadgeHeadRow({
    required this.backgroundColor,
    super.key,
    this.text1 = '',
    this.text2 = '',
    this.text3 = '',
  });

  final Color backgroundColor;
  final String text1;
  final String text2;
  final String text3;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: backgroundColor,
        borderRadius: BorderRadius.circular(5),
      ),
      child: Row(
        children: [
          SizedBox(
            width: 3 * ViewConstants.sizeOfStars,
            child: Text(
              text1,
              textAlign: TextAlign.center,
            ),
          ),
          const SizedBox(width: ViewConstants.sizeBetweenRows),
          SizedBox(
            width: 3 * ViewConstants.sizeOfStars,
            child: Text(
              text2,
              textAlign: TextAlign.center,
            ),
          ),
          const SizedBox(width: ViewConstants.sizeBetweenRows),
          Expanded(
            child: Text(
              text3,
              textAlign: TextAlign.left,
            ),
          ),
        ],
      ),
    );
  }
}
