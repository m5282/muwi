import 'package:flutter/material.dart';

/// The widget to create the three big buttons in the middle of the screen in the start menu.
class MenuButton extends StatelessWidget {
  /// [text]: text to display on the button
  /// [backgroundColor]: color of the button
  /// [callback]: route to new widget
  const MenuButton(
    this.text,
    this.backgroundColor,
    this.callback, {
    super.key,
  });
  final String text;
  final Color backgroundColor;
  final VoidCallback? callback;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: callback,
      style: ElevatedButton.styleFrom(
        // The rest of the style is in the theme class defined.
        // We only define background Color here, because it is individual
        // for every button.
        backgroundColor: backgroundColor,
      ),
      child: Text(text),
    );
  }
}
