import 'package:flutter/material.dart';
import 'package:muwi/constants/view_constants.dart';
import 'package:muwi/enums/level_names.dart';

///Displays the badge picture and its caption.
///The caption is either the level displayed as stars or a value displayed as a string.
class BadgeSymbol extends StatelessWidget {

  const BadgeSymbol({
    required this.accomplished,
    required this.assetPath,
    required this.assetColor,
    this.badgeDifficulty,
    this.badgeValue,
    super.key,
  });
  final String assetPath;
  final Color assetColor;
  final Difficulty? badgeDifficulty;
  final bool accomplished;
  final String? badgeValue;

  @override
  Widget build(BuildContext context) {
    // If both badgeDifficulty and badgeValue are not set, than the badge is not accomplished
    //          (badgeDifficulty == null  &&      badgeValue == null) =>   not accomplished
    // <=>  not (badgeDifficulty == null  &&      badgeValue == null)  or (not accomplished)
    // <=> (not (badgeDifficulty == null) or not (badgeValue == null)) or (not accomplished)
    // <=>      (badgeDifficulty != null  or      badgeValue != null)  or (not accomplished)
    assert((badgeDifficulty != null || badgeValue != null) || !accomplished,
    'If badgeDifficulty and badgeValue are both null, than accomplished should be false. '
    'If there is nothing to display, that means for the user, that he has not accomplished the badge yet. '
    'badgeDifficulty: $badgeDifficulty, badgeValue: $badgeValue, accomplished: $accomplished');

    if (accomplished) {
      assert((badgeDifficulty != null && badgeValue == null) || badgeDifficulty == null && badgeValue != null,
      'Can only display stars for difficulty or a value, not both for a accomplished badge '
          'accomplished: $accomplished, badgeDifficulty: $badgeDifficulty, badgeValue: $badgeValue',);
      return Column(
        children: [
          SizedBox(
            height: 60,
            width: 60 + ViewConstants.sizeOfStars,
            child: Image(
              image: AssetImage(assetPath),
              color: assetColor,
            ),
          ),
          if (badgeDifficulty != null)
            starRow(badgeDifficulty!)
          else if(badgeValue != null)
            Text(badgeValue!,
              style: Theme
                .of(context)
                .textTheme
                .bodyMedium,),
        ],
      );
    } else {
      return const SizedBox(
        height: 60 + ViewConstants.sizeOfStars,
        width: 3 * ViewConstants.sizeOfStars,
      );
    }
  }


  Widget starRow(Difficulty badgeDifficulty){
    return Row(
      children: [
        for (int i = 0; i < badgeDifficulty.toInt(); i++)
          Icon(
            Icons.star,
            color: assetColor,
            size: ViewConstants.sizeOfStars,
          ),
        for (int i = 0; i < 3 - badgeDifficulty.toInt(); i++)
          Icon(
            Icons.star_border,
            color: assetColor,
            size: ViewConstants.sizeOfStars,
          ),
      ],
    );
  }
}
