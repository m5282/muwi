import 'package:flutter/cupertino.dart';
import 'package:numberpicker/numberpicker.dart';

class CustomNumberPicker extends StatelessWidget {
  const CustomNumberPicker(
    this.title,
    this.currentValue,
    this.minValue,
    this.maxValue,
    this.setValue, {
    super.key,
  });

  /// This callback function changes the state in some parent node.
  final void Function(int) setValue;

  /// The current state.
  final int currentValue;

  /// The minimum selectable value.
  final int minValue;

  /// The maximum selectable value.
  final int maxValue;

  /// Some description of the setting.
  final Text title;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.fromLTRB(10, 10, 0, 0),
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: title,
              ),
            ],
          ),
          Row(
            children: [
              NumberPicker(
                axis: Axis.horizontal,
                value: currentValue,
                itemWidth: 40,
                minValue: minValue,
                maxValue: maxValue,
                onChanged: setValue,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
