import 'package:flutter/material.dart';

/// This widget consists of a row with a checkbox and a description next to it.
class MenuOption extends StatelessWidget {
  const MenuOption({
    required this.title,
    required this.toggleValue,
    required this.currentValue,
    super.key,
  });

  /// This callback function toggles the current selection.
  final void Function() toggleValue;

  /// This variable determines whether the box is currently checked or not.
  final bool currentValue;

  /// The description of the option.
  final Text title;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Checkbox(
          value: currentValue,
          onChanged: (b) {
            toggleValue();
          },
        ),
        Expanded(
          child: title,
        ),
      ],
    );
  }
}
