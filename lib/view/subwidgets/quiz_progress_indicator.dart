import 'package:flutter/material.dart';


///Show a simple shownTasks / numberOfTasks in the top right corner or nothing if numberOfTasks is not finite,
///i. e. there will be an infinite number of tasks.
class QuizProgressIndicator extends StatelessWidget{

  const QuizProgressIndicator({required this.numberOfTasks, required this.shownTasks, required this.height, super.key});

  final num? numberOfTasks;
  final int shownTasks;
  final double height;

  @override
  Widget build(BuildContext context) {
    if (numberOfTasks != null) {
      if(numberOfTasks! < double.infinity) {
        return Align(
          alignment: Alignment.topRight,
          child: Container(margin: const EdgeInsets.all(10),
            child: Text(
                '$shownTasks / $numberOfTasks',
                style: Theme.of(context).textTheme.bodyMedium,
            ),
          ),
        );
      }
    }
    return const SizedBox();
  }
}
