import 'package:analyzer_plugin/utilities/pair.dart';
import 'package:flutter/material.dart';
import 'package:muwi/enums/color_code.dart';
import 'package:muwi/view_model/drawable_muwi.dart';
import 'package:muwi/view_model/real_coordinates.dart';

/// This class takes care of the display of the notes and accidentals.
class ShowChord extends StatelessWidget {
  const ShowChord({
    required this.upperDrawables,
    required this.lowerDrawables,
    required this.realCoordinates,
    super.key,
  });

  final List<DrawableMuwi> upperDrawables;
  final List<DrawableMuwi> lowerDrawables;

  /// Service providing the real coordinates of the x- and y-coordinates on the screen.
  final RealCoordinates realCoordinates;

  @override
  Widget build(BuildContext context) {
    final children = <Widget>[];

    // Draw the notes and accidentals of the upper system
    draw(context, lowerDrawables, children, realCoordinates.heightOfANote, 0);
    // Draw the notes and accidentals of the lower system
    draw(context, upperDrawables, children, realCoordinates.heightOfANote, 1);

    if (children.isEmpty) {
      return throw UnsupportedError(
        'Die Liste der Widgets für die Noten ist leer! D:',
      );
    }
    return Stack(children: children);
  }

  /// Method to actually set every note and accidental.
  void draw(
    BuildContext context,
    List<DrawableMuwi> drawableList,
    List<Widget> children,
    double height,
    int staffID,
  ) {
    for (final drawable in drawableList) {
      children.add(
        image(
          realCoordinates.verticalPositions[Pair(staffID, drawable.yCoordinate)]! +
              drawable.verticalOffset * realCoordinates.spaceBetweenTwoYCoordinates,
          realCoordinates.horizontalPositions[drawable.xCoordinate]!,
          height * drawable.scalingFactor,
          drawable.path,
          drawable.colorCode.getColor(context)!,
        ),
      );
    }
  }

  /// The widget for one single note or accidental.
  Widget image(
    double bottom,
    double left,
    double height,
    String path,
    Color color,
  ) {
    return Positioned(
      bottom: bottom,
      left: left,
      child: Image.asset(
        path,
        height: height,
        color: color,
      ),
    );
  }
}
