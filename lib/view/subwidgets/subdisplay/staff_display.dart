import 'package:analyzer_plugin/utilities/pair.dart';
import 'package:flutter/material.dart';
import 'package:muwi/constants/view_constants.dart';
import 'package:muwi/model/clef.dart';
import 'package:muwi/view_model/real_coordinates.dart';

// Adding some abstraction to the numbers
int topDisplayID = 1;
int bottomDisplayID = 0;

class StaffDisplay extends StatelessWidget {
  const StaffDisplay({
    required this.lowerClefCoordinate,
    required this.upperClefCoordinate,
    required this.lowerDisplayLedgerLines,
    required this.upperDisplayLedgerLines,
    required this.realCoordinates,
    super.key,
  });

  /// The Position Service: Providing access to a matrix (two hashmaps) containing the actual values of the (x,y)-positions on the used screen.
  final RealCoordinates realCoordinates;

  /// Bottom of each clef in y-coordinates for the lower staff.
  /// Will be -6 for the g clef and -3 for the f clef.
  final Pair<Clef, int> lowerClefCoordinate;

  /// Bottom of each clef in y-coordinates for the upper staff.
  /// Will be -6 for the g clef and -3 for the f clef.
  final Pair<Clef, int> upperClefCoordinate;

  //---------------- The coordinates for the ledger lines
  /// The coordinates for the ledger lines
  final List<Pair<int, int>> lowerDisplayLedgerLines;
  final List<Pair<int, int>> upperDisplayLedgerLines;

  @override
  Widget build(BuildContext context) {
    final children = <Widget>[];

    // ----------------------------------------------------------------------------

    //---------------------- DRAWING THE LINES

    // ---------- Regular lines
    for (final yCoordinate in [-4, -2, 0, 2, 4]) {
      children
        ..add(
          horizontalLine(
            realCoordinates.verticalPositions[Pair(bottomDisplayID, yCoordinate)]!,
            ViewConstants.xZeroCoordinate,
            realCoordinates.widthOfRegularLine,
          ),
        )
        ..add(
          horizontalLine(
            realCoordinates.verticalPositions[Pair(topDisplayID, yCoordinate)]!,
            ViewConstants.xZeroCoordinate,
            realCoordinates.widthOfRegularLine,
          ),
        );
    }

    // ---------- Ledger lines
    for (final line in lowerDisplayLedgerLines) {
      children.add(
        horizontalLine(
          realCoordinates.verticalPositions[Pair(bottomDisplayID, line.last)]!,
          realCoordinates.horizontalPositions[line.first]! -
              realCoordinates.widthOfLedgerLine / 2 +
              realCoordinates.widthOfANote / 2,
          realCoordinates.widthOfLedgerLine,
        ),
      );
    }
    for (final line in upperDisplayLedgerLines) {
      children.add(
        horizontalLine(
          realCoordinates.verticalPositions[Pair(topDisplayID, line.last)]!,
          realCoordinates.horizontalPositions[line.first]! -
              realCoordinates.widthOfLedgerLine / 2 +
              realCoordinates.widthOfANote / 2,
          realCoordinates.widthOfLedgerLine,
        ),
      );
    }

    // The vertical line and the brace extend from the lowest regular line of the bottom staff to the highest regular line of the top staff.
    children
      ..add(verticalLine())
      ..add(brace())
      // Add clefs to both displays
      ..add(drawClef(lowerClefCoordinate, bottomDisplayID))
      ..add(drawClef(upperClefCoordinate, topDisplayID));

    // Stack the widgets as transparent layers
    return Stack(children: children);
  }

  /// Widget for a single horizontal line.
  Widget horizontalLine(double bottom, double left, double width) {
    return Positioned(
      bottom: bottom,
      left: left,
      child: Container(
        height: 1,
        width: width,
        color: Colors.black,
      ),
    );
  }

  /// Widget for the vertical line.
  Widget verticalLine() {
    return Positioned(
      left: realCoordinates.horizontalPositions[0],
      bottom: realCoordinates.bottomOfBrace,
      child: Container(
        color: Colors.black,
        height: realCoordinates.heightOfBrace,
        width: 2,
      ),
    );
  }

  /// Staff Widget.
  Widget brace() {
    return Positioned(
      left: realCoordinates.leftBeginOfBrace,
      bottom: realCoordinates.bottomOfBrace,
      height: realCoordinates.heightOfBrace,
      child: Image.asset(ViewConstants.braceImage),
    );
  }

  /// Clef Widget
  Widget drawClef(Pair<Clef, int> clefPair, int staffID) {
    var picClef = '';
    // The clefHeight will depend on the clef (the f clef shorter than the g clef).
    var clefHeight = 0.0;

    switch (clefPair.first) {
      case Clef.bass:
        {
          picClef = ViewConstants.fClefImage;
          // We use the ID of the bottom display for retrieving the distance between the bottom and the top position of the clef. It does not matter which display ID we use as long as we do not use both IDs in one line.
          clefHeight =
              (realCoordinates.verticalPositions[Pair(bottomDisplayID, ViewConstants.topOfFClef)]! -
                      realCoordinates
                          .verticalPositions[Pair(bottomDisplayID, ViewConstants.bottomOfFClef)]!)
                  .abs();
        }

      case Clef.treble:
        {
          picClef = ViewConstants.gClefImage;
          clefHeight =
              (realCoordinates.verticalPositions[Pair(bottomDisplayID, ViewConstants.topOfGClef)]! -
                      realCoordinates
                          .verticalPositions[Pair(bottomDisplayID, ViewConstants.bottomOfGClef)]!)
                  .abs();
        }

      case Clef.tenor:
        {
          picClef = ViewConstants.cClefImage;
          clefHeight = (realCoordinates
                      .verticalPositions[Pair(bottomDisplayID, ViewConstants.topOfTenorClef)]! -
                  realCoordinates
                      .verticalPositions[Pair(bottomDisplayID, ViewConstants.bottomOfTenorClef)]!)
              .abs();
        }

      case Clef.alto:
        {
          picClef = ViewConstants.cClefImage;
          clefHeight = (realCoordinates
                      .verticalPositions[Pair(bottomDisplayID, ViewConstants.topOfAltoClef)]! -
                  realCoordinates
                      .verticalPositions[Pair(bottomDisplayID, ViewConstants.bottomOfAltoClef)]!)
              .abs();
        }
    }

    return Positioned(
      bottom: realCoordinates.verticalPositions[Pair(staffID, clefPair.last)],
      left: realCoordinates.horizontalPositions[ViewConstants.xCoordinateOfClefs],
      height: clefHeight,
      child: Image.asset(picClef),
    );
  }
}
