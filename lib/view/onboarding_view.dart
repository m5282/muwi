import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:muwi/model/tonfall_shared_preferences.dart';
import 'package:muwi/view/start_view.dart';
import 'package:muwi/view/update/update_view.dart';
import 'package:muwi/view_model/onboarding_texts_view_model.dart';

// This file shows the onboarding.
// I. e. a short tutorial.
// Currently, you have to point your run configurations to this file
// to see the onboarding.

class OnboardingView extends StatefulWidget {
  const OnboardingView({
    required this.updateScreen,
    required this.version,
    super.key,
  });

  final bool updateScreen;
  final String version;

  @override
  OnboardingViewState createState() => OnboardingViewState();
}

class OnboardingViewState extends State<OnboardingView> {
  final PageController _controller = PageController();

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return PageView(
      controller: _controller,
      children: [
        OnboardingPage(
          updateScreen: widget.updateScreen,
          title: 'Hallo!',
          text: IntroTexts.text1(),
          buttonText: 'Loslegen!',
          imageLocation: 'lib/assets/OnboardingBackground01.png',
          version: widget.version,
        ),
        OnboardingPage(
          updateScreen: widget.updateScreen,
          title: 'Dokumentation',
          text: IntroTexts.text2(),
          buttonText: 'Loslegen!',
          imageLocation: 'lib/assets/OnboardingBackground02.png',
          version: widget.version,
        ),
        /* OnboardingView('Übungsmodus', IntroTexts.text3(), 'Überspringen',
            'lib/assets/OnboardingBackground03.png'),
        OnboardingView('Klausurmodus', IntroTexts.text4(), 'Loslegen!',
            'lib/assets/OnboardingBackground04.png'), */
      ],
    );
  }
}

class OnboardingPage extends StatelessWidget {
  const OnboardingPage({
    required this.updateScreen,
    required this.title,
    required this.text,
    required this.buttonText,
    required this.imageLocation,
    required this.version,
    super.key,
  });

  //Caption of the Screen
  final String title;
  //The explanation text in the middle of the screen
  final TextSpan text;
  //Button to end the onboarding
  final String buttonText;
  // path to background image
  final String imageLocation;
  // If this flag is set, the update view is displayed at the end.
  final bool updateScreen;
  // The current version of the app.
  final String version;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Image(
            image: AssetImage(imageLocation),
            // display the image full screen (no borders)
            fit: BoxFit.cover,
            height: double.infinity,
            width: double.infinity,
          ),
          Center(
            // Group title and text together
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      margin: const EdgeInsets.fromLTRB(0, 50, 0, 50),
                      child: AutoSizeText(
                        title,
                        minFontSize: 5,
                        maxFontSize: 50,
                        maxLines: 1,
                        style: const TextStyle(
                          fontSize: 50,
                          color: Color(0xff56656d),
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.fromLTRB(50, 0, 50, 0),
                      child: AutoSizeText.rich(
                        text,
                        minFontSize: 5,
                        maxFontSize: 50, //28
                        maxLines: 5,
                        textAlign: TextAlign.center,
                        style: const TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 30,
                          color: Color(0xff56656d),
                        ),
                      ),
                    ),
                  ],
                ),
                // group ... and Skip Button together
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    const Text(
                      '..', //one dot for each page
                      style: TextStyle(
                        fontSize: 30,
                        letterSpacing: 5,
                      ),
                    ),
                    ElevatedButton(
                      onPressed: () {
                        // the user is done with the onboarding,
                        // so we don't show it again.
                        TonfallSharedPreferences.setOnboarding(value: false);
                        if (updateScreen) {
                          Navigator.push(
                            context,
                            MaterialPageRoute<dynamic>(
                              builder: (
                                context,
                              ) =>
                                  UpdateView(version: version),
                            ),
                          );
                        } else {
                          Navigator.push(
                            context,
                            MaterialPageRoute<dynamic>(
                              builder: (
                                context,
                              ) =>
                                  StartView(version: version),
                            ),
                          );
                        }
                      },
                      style: ElevatedButton.styleFrom(
                        foregroundColor: Colors.black,
                        backgroundColor: Colors.white,
                      ),
                      child: Text(buttonText),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
