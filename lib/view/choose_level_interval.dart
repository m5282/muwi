import 'package:flutter/material.dart';
import 'package:muwi/constants/global_constants.dart';
import 'package:muwi/constants/runtime_constants.dart';
import 'package:muwi/enums/lesson_training_names.dart';
import 'package:muwi/enums/level_names.dart';
import 'package:muwi/enums/task_mode.dart';
import 'package:muwi/model/clef.dart';
import 'package:muwi/model/settings/interval_settings.dart';
import 'package:muwi/model/tonfall_shared_preferences.dart';
import 'package:muwi/view/interval_quiz_view.dart';
import 'package:muwi/view/lesson_completed.dart';
import 'package:muwi/view/subwidgets/card_button_composer.dart';
import 'package:muwi/view/subwidgets/menu_button.dart';
import 'package:muwi/view/theme/custom_theme.dart';
import 'package:muwi/view_model/keyboard_vm/interval_keyboard_vm.dart';
import 'package:muwi/view_model/quiz_page_view_model.dart';
import 'package:muwi/view_model/taskproviders/interval_provider.dart';

// Todo: Here's a lot of redundancy with "Lektionstraining".
// The levels are like the last one from the Lektionstraining, but with an infinite number of tasks.
// The view is the same as in choose_level_triad.

class ChooseLevelInterval extends StatelessWidget {
  const ChooseLevelInterval({super.key});

  /// Method loading the current Settings.
  Future<IntervalSettings> loadIntervalSettings(String mode) async {
    try {
      final maybeSettings =
          IntervalSettings.fromJson(await TonfallSharedPreferences.getSettings('${mode}settings'));
      return maybeSettings;
    } on Exception {
      throw Exception('No settings found.');
    }
  }

  @override
  Widget build(BuildContext context) {
    final customColors = Theme.of(context).extension<CustomColors>()!;

    IntervalSettings easyIntervalSettings() {
      final settings = IntervalSettings()
        ..maxKeySignatureAccidentals = 0
        ..maxTwoAccidentals = true
        ..upperClef = Clef.treble
        ..lowerClef = Clef.bass;
      return settings;
    }

    IntervalSettings mediumIntervalSettings() {
      final settings = IntervalSettings()
        ..maxTwoAccidentals = true
        ..upperClef = Clef.treble
        ..lowerClef = Clef.bass;
      return settings;
    }

    IntervalSettings hardIntervalSettings() {
      final settings = IntervalSettings()
        ..maxTwoAccidentals = false
        ..clefPreferences = {
          Clef.treble: true,
          Clef.alto: true,
          Clef.tenor: true,
          Clef.bass: true,
        };
      return settings;
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text('Levelauswahl: Intervalle'),
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            const SizedBox(
              height: 40,
            ), //some space to the top
            Container(
              margin: const EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: CardButtonComposer(
                backgroundColor: customColors.uebungsmodusLightColor,
                children: [
                  MenuButton(LessonIDName.freeIntervalTrainingEasy.displayableGermanName,
                      customColors.correctAndSelectedLightColor, () {
                    final easyQPVM = QuizPageViewModel(
                      settings: easyIntervalSettings(),
                      taskProvider: IntervalProvider(),
                      keyboardVM: IntervalKeyboardVM(),
                    );

                    final lessonCompleted = LessonCompleted(
                      lessonNameID: LessonIDName.freeIntervalTrainingEasy,
                      version: RuntimeConstants.version,
                      currentQPVM: easyQPVM,
                      startTime: DateTime.now(),
                      pointsAchieved: 0,
                      numberOfTasks: double.infinity,
                      taskMode: TaskMode.interval,
                      pointsPerCorrectTask: 1,
                      difficulty: Difficulty.easy,
                    );
                    Navigator.push(
                      context,
                      MaterialPageRoute<dynamic>(
                        builder: (context) => IntervalQuizView(
                          intervalVM: easyQPVM,
                          lessonCompleted: lessonCompleted,
                          showSettingsDrawer: false,
                        ),
                      ),
                    );
                  }),
                  MenuButton(LessonIDName.freeIntervalTrainingMedium.displayableGermanName,
                      customColors.correctAndNotSelectedColor, () {
                    final mediumQPVM = QuizPageViewModel(
                      settings: mediumIntervalSettings(),
                      taskProvider: IntervalProvider(),
                      keyboardVM: IntervalKeyboardVM(),
                    );

                    final lessonCompleted = LessonCompleted(
                      lessonNameID: LessonIDName.freeIntervalTrainingMedium,
                      version: RuntimeConstants.version,
                      currentQPVM: mediumQPVM,
                      startTime: DateTime.now(),
                      pointsAchieved: 0,
                      pointsPerCorrectTask: 1,
                      numberOfTasks: double.infinity,
                      difficulty: Difficulty.medium,
                      taskMode: TaskMode.interval,
                    );
                    Navigator.push(
                      context,
                      MaterialPageRoute<dynamic>(
                        builder: (context) => IntervalQuizView(
                          intervalVM: mediumQPVM,
                          lessonCompleted: lessonCompleted,
                          showSettingsDrawer: false,
                        ),
                      ),
                    );
                  }),
                  MenuButton(LessonIDName.freeIntervalTrainingHard.displayableGermanName,
                      customColors.klausurmodusLightColor, () {
                    final hardQPVM = QuizPageViewModel(
                      settings: hardIntervalSettings(),
                      taskProvider: IntervalProvider(),
                      keyboardVM: IntervalKeyboardVM(),
                    );
                    final lessonCompleted = LessonCompleted(
                      lessonNameID: LessonIDName.freeIntervalTrainingHard,
                      version: RuntimeConstants.version,
                      currentQPVM: hardQPVM,
                      startTime: DateTime.now(),
                      pointsAchieved: 0,
                      pointsPerCorrectTask: 1,
                      numberOfTasks: double.infinity,
                      difficulty: Difficulty.hard,
                      taskMode: TaskMode.interval,
                    );
                    Navigator.push(
                      context,
                      MaterialPageRoute<dynamic>(
                        builder: (context) => IntervalQuizView(
                          intervalVM: hardQPVM,
                          lessonCompleted: lessonCompleted,
                          showSettingsDrawer: false,
                        ),
                      ),
                    );
                  }),
                ],
              ),
            ),
            const SizedBox(
              height: 40,
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: CardButtonComposer(
                backgroundColor: customColors.uebungsmodusLightColor,
                children: [
                  MenuButton(LessonIDName.userAdjustableIntervalTraining.displayableGermanName,
                      customColors.notSelectedColor, () {
                    Navigator.push(
                      context,
                      MaterialPageRoute<dynamic>(
                        builder: (context) => FutureBuilder<IntervalSettings>(
                          // Fetching the settings from SharedPreferences. This is the asynchronous operation.
                          future: loadIntervalSettings(GlobalConstants.intervalmodus),
                          builder: (
                            BuildContext context,
                            AsyncSnapshot<IntervalSettings> snapshot,
                          ) {
                            QuizPageViewModel userAdjustableQPVM;

                            Widget child;
                            if (snapshot.hasData) {
                              // Extract the settings from the snapshot.
                              final settings = snapshot.data!;
                              userAdjustableQPVM = QuizPageViewModel(
                                settings: settings,
                                taskProvider: IntervalProvider(),
                                keyboardVM: IntervalKeyboardVM(),
                              );
                              final lessonCompleted = LessonCompleted(
                                lessonNameID: LessonIDName.userAdjustableIntervalTraining,
                                version: RuntimeConstants.version,
                                currentQPVM: userAdjustableQPVM,
                                startTime: DateTime.now(),
                                pointsPerCorrectTask: 1,
                                pointsAchieved: 0,
                                numberOfTasks: double.infinity,
                                taskMode: TaskMode.interval,
                              );

                              child = IntervalQuizView(
                                intervalVM: userAdjustableQPVM,
                                lessonCompleted: lessonCompleted,
                              );
                            } else if (snapshot.hasError) {
                              // Provide default settings in case of an error.
                              final qpvm = QuizPageViewModel(
                                settings: IntervalSettings(),
                                taskProvider: IntervalProvider(),
                                keyboardVM: IntervalKeyboardVM(),
                              );

                              // Extract the settings from the snapshot. => Not possible, no data! Hence, the error!
                              //IntervalSettings settings = snapshot.data!;
                              final settings = IntervalSettings();
                              userAdjustableQPVM = QuizPageViewModel(
                                settings: settings,
                                taskProvider: IntervalProvider(),
                                keyboardVM: IntervalKeyboardVM(),
                              );
                              final lessonCompleted = LessonCompleted(
                                lessonNameID: LessonIDName.userAdjustableIntervalTraining,
                                version: RuntimeConstants.version,
                                currentQPVM: userAdjustableQPVM,
                                startTime: DateTime.now(),
                                pointsPerCorrectTask: 1,
                                pointsAchieved: 0,
                                numberOfTasks: double.infinity,
                                taskMode: TaskMode.interval,
                              );
                              child = IntervalQuizView(
                                intervalVM: qpvm,
                                lessonCompleted: lessonCompleted,
                              );
                            } else {
                              // Show a circular progress indicator as long as the settings are loading.
                              child = const Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  SizedBox(
                                    width: 60,
                                    height: 60,
                                    child: CircularProgressIndicator(),
                                  ),
                                ],
                              );
                            }
                            return child;
                          },
                        ),
                      ),
                    );
                  }),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
