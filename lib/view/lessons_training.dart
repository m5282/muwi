import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:muwi/constants/musical_constants.dart';
import 'package:muwi/enums/lesson_training_names.dart';
import 'package:muwi/model/accidental.dart';
import 'package:muwi/model/clef.dart';
import 'package:muwi/model/settings/triad_settings.dart';
import 'package:muwi/model/tonfall_shared_preferences.dart';
import 'package:muwi/view/start_menu.dart';
import 'package:muwi/view/subwidgets/card_button_composer.dart';
import 'package:muwi/view/subwidgets/lesson_button.dart';
import 'package:muwi/view/theme/custom_theme.dart';

class LessonTraining extends StatelessWidget {
  const LessonTraining({required this.version, super.key});

  final String version;

  static const numberOfTasks = 12;

  @override
  Widget build(BuildContext context) {
    TriadSettings trebleClefSettings() {
      final settings = TriadSettings()
        ..upperClef = Clef.treble
        ..lowerClef = Clef.treble
        ..snowmenOnly = true
        ..numberOfLedgerLines = 3
        ..standardClefArrangement = true
        ..oneKeySignature = true
        ..numKeySignatureAccidentals = 0
        ..accidental = Accidental.none
        ..patternToChoseFrom = {
          for (int i = 0; i < 7; i += 1)
            MusicalConstants.cMajorScale[i]: MusicalConstants.majorScale[i],
        };
      return settings;
    }

    TriadSettings bassClefSettings() {
      final settings = TriadSettings()
        ..upperClef = Clef.bass
        ..lowerClef = Clef.bass
        ..snowmenOnly = true
        ..standardClefArrangement = true
        ..oneKeySignature = true
        ..numKeySignatureAccidentals = 0
        ..accidental = Accidental.none
        ..patternToChoseFrom = {
          for (int i = 0; i < 7; i += 1)
            MusicalConstants.cMajorScale[i]: MusicalConstants.majorScale[i],
        };
      return settings;
    }

    //C-Dur / a-Moll
    TriadSettings zeroSharpsSettings() {
      final settings = TriadSettings()
        ..upperClef = Clef.treble
        ..lowerClef = Clef.bass
        ..standardClefArrangement = true
        ..oneKeySignature = true
        ..numKeySignatureAccidentals = 0
        ..accidental = Accidental.none
        ..patternToChoseFrom = {
          for (int i = 0; i < 7; i += 1)
            MusicalConstants.cMajorScale[i]: MusicalConstants.majorScale[i],
        };
      return settings;
    }

    TriadSettings oneSharpSettings() {
      //settings.numberOfTasksLeft = 5;
      final settings = TriadSettings()
        ..oneKeySignature = true
        ..numKeySignatureAccidentals = 1
        ..accidental = Accidental.sharp
        ..upperClef = Clef.treble
        ..lowerClef = Clef.bass
        ..patternToChoseFrom = {
          for (int i = 0; i < 7; i += 1)
            MusicalConstants.gMajorScale[i]: MusicalConstants.majorScale[i],
        };
      return settings;
    }

    //F-Dur / d-Moll
    TriadSettings oneFlatSettings() {
      final settings = TriadSettings()
        ..oneKeySignature = true
        ..numKeySignatureAccidentals = 1
        ..accidental = Accidental.flat
        ..upperClef = Clef.treble
        ..lowerClef = Clef.bass
        ..patternToChoseFrom = {
          for (int i = 0; i < 7; i += 1)
            MusicalConstants.fMajorScale[i]: MusicalConstants.majorScale[i],
        };
      return settings;
    }

    // D-Dur / h-Moll
    TriadSettings twoSharpsSettings() {
      final settings = TriadSettings()
        ..oneKeySignature = true
        ..numKeySignatureAccidentals = 2
        ..accidental = Accidental.sharp
        ..upperClef = Clef.treble
        ..lowerClef = Clef.bass
        ..patternToChoseFrom = {
          for (int i = 0; i < 7; i += 1)
            MusicalConstants.dMajorScale[i]: MusicalConstants.majorScale[i],
        };
      return settings;
    }

    //B-Dur / g-Moll
    TriadSettings twoFlatSettings() {
      final settings = TriadSettings()
        ..oneKeySignature = true
        ..numKeySignatureAccidentals = 2
        ..accidental = Accidental.flat
        ..upperClef = Clef.treble
        ..lowerClef = Clef.bass
        ..patternToChoseFrom = {
          for (int i = 0; i < 7; i += 1)
            MusicalConstants.bFlatMajorScale[i]: MusicalConstants.majorScale[i],
        };
      return settings;
    }

    // A-Dur / fis-Moll
    TriadSettings threeSharpsSettings() {
      final settings = TriadSettings()
        ..oneKeySignature = true
        ..numKeySignatureAccidentals = 3
        ..accidental = Accidental.sharp
        ..upperClef = Clef.treble
        ..lowerClef = Clef.bass
        ..patternToChoseFrom = {
          for (int i = 0; i < 7; i += 1)
            MusicalConstants.aMajorScale[i]: MusicalConstants.majorScale[i],
        };
      return settings;
    }

    //Es-Dur / c-Moll
    TriadSettings threeFlatSettings() {
      final settings = TriadSettings()
        ..oneKeySignature = true
        ..numKeySignatureAccidentals = 3
        ..accidental = Accidental.flat
        ..upperClef = Clef.treble
        ..lowerClef = Clef.bass
        ..patternToChoseFrom = {
          for (int i = 0; i < 7; i += 1)
            MusicalConstants.eFlatMajorScale[i]: MusicalConstants.majorScale[i],
        };
      return settings;
    }

    //E-Dur / cis-Moll
    TriadSettings fourSharpsSettings() {
      final settings = TriadSettings()
        ..oneKeySignature = true
        ..numKeySignatureAccidentals = 4
        ..accidental = Accidental.sharp
        ..upperClef = Clef.treble
        ..lowerClef = Clef.bass
        ..patternToChoseFrom = {
          for (int i = 0; i < 7; i += 1)
            MusicalConstants.eMajorScale[i]: MusicalConstants.majorScale[i],
        };
      return settings;
    }

    //As-Dur / f-Moll
    TriadSettings fourFlatSettings() {
      final settings = TriadSettings()
        ..oneKeySignature = true
        ..numKeySignatureAccidentals = 4
        ..accidental = Accidental.flat
        ..upperClef = Clef.treble
        ..lowerClef = Clef.bass
        ..patternToChoseFrom = {
          for (int i = 0; i < 7; i += 1)
            MusicalConstants.aFlatMajorScale[i]: MusicalConstants.majorScale[i],
        };
      return settings;
    }

    // H-Dur / gis-Moll
    TriadSettings fiveSharpsSettings() {
      final settings = TriadSettings()
        ..oneKeySignature = true
        ..numKeySignatureAccidentals = 5
        ..accidental = Accidental.sharp
        ..upperClef = Clef.treble
        ..lowerClef = Clef.bass
        ..patternToChoseFrom = {
          for (int i = 0; i < 7; i += 1)
            MusicalConstants.bMajorScale[i]: MusicalConstants.majorScale[i],
        };
      return settings;
    }

    //Des-Dur / b-Moll
    TriadSettings fiveFlatSettings() {
      final settings = TriadSettings()
        ..oneKeySignature = true
        ..numKeySignatureAccidentals = 5
        ..accidental = Accidental.flat
        ..upperClef = Clef.treble
        ..lowerClef = Clef.bass
        ..patternToChoseFrom = {
          for (int i = 0; i < 7; i += 1)
            MusicalConstants.dFlatMajorScale[i]: MusicalConstants.majorScale[i],
        };
      return settings;
    }

    //Fis-Dur / dis-Moll
    TriadSettings sixSharpsSettings() {
      final settings = TriadSettings()
        ..oneKeySignature = true
        ..numKeySignatureAccidentals = 6
        ..accidental = Accidental.sharp
        ..upperClef = Clef.treble
        ..lowerClef = Clef.bass
        ..patternToChoseFrom = {
          for (int i = 0; i < 7; i += 1)
            MusicalConstants.fSharpMajorScale[i]: MusicalConstants.majorScale[i],
        };
      return settings;
    }

    //Ges-Dur / es-Moll
    TriadSettings sixFlatSettings() {
      final settings = TriadSettings()
        ..oneKeySignature = true
        ..numKeySignatureAccidentals = 6
        ..accidental = Accidental.flat
        ..upperClef = Clef.treble
        ..lowerClef = Clef.bass
        ..patternToChoseFrom = {
          for (int i = 0; i < 7; i += 1)
            MusicalConstants.gFlatMajorScale[i]: MusicalConstants.majorScale[i],
        };
      return settings;
    }

    //Übermäßig / Vermindert
    TriadSettings augAndDimSettings() {
      final settings = TriadSettings()
        ..upperClef = Clef.treble
        ..lowerClef = Clef.bass
        ..maxKeySignatureAccidentals = 6
        ..diminishedFrequency = 1
        ..augmentedFrequency = 1
        ..majorFrequency = 0
        ..minorFrequency = 0;
      return settings;
    }

    //Tonartfremde Klänge 1
    TriadSettings accidentalChordSettings1() {
      final settings = TriadSettings()
        ..upperClef = Clef.treble
        ..lowerClef = Clef.bass
        ..maxKeySignatureAccidentals = 3
        ..diminishedFrequency = 2
        ..augmentedFrequency = 2
        ..majorFrequency = 1
        ..minorFrequency = 1;
      return settings;
    }

    //Tonartfremde Klänge 2
    TriadSettings accidentalChordSettings2() {
      final settings = TriadSettings()
        ..upperClef = Clef.treble
        ..lowerClef = Clef.bass
        ..maxKeySignatureAccidentals = 6
        ..diminishedFrequency = 2
        ..augmentedFrequency = 2
        ..majorFrequency = 1
        ..minorFrequency = 1;

      return settings;
    }

    //Tenorschlüssel
    TriadSettings tenorClefSettings() {
      final settings = TriadSettings()
        ..upperClef = Clef.tenor
        ..lowerClef = Clef.tenor
        ..maxKeySignatureAccidentals = 6
        ..diminishedFrequency = 2
        ..augmentedFrequency = 2
        ..majorFrequency = 1
        ..minorFrequency = 1;
      return settings;
    }

    //Altschlüssel
    TriadSettings altoClefSettings() {
      final settings = TriadSettings()
        ..upperClef = Clef.alto
        ..lowerClef = Clef.alto
        ..maxKeySignatureAccidentals = 6
        ..diminishedFrequency = 2
        ..augmentedFrequency = 2
        ..majorFrequency = 1
        ..minorFrequency = 1;
      return settings;
    }

    //alle Schlüssel - Hohen über tiefen Schlüsseln
    TriadSettings allClefsStandardArrangementSettings() {
      final settings = TriadSettings()
        ..clefPreferences = {
          Clef.treble: true,
          Clef.alto: true,
          Clef.tenor: true,
          Clef.bass: true,
        }
        ..standardClefArrangement = true
        ..maxKeySignatureAccidentals = 6
        ..diminishedFrequency = 2
        ..augmentedFrequency = 2
        ..majorFrequency = 1
        ..minorFrequency = 1;
      return settings;
    }

    //alle Schlüssel - zufällige Sortierung
    TriadSettings allClefsNoOrderSettings() {
      final settings = TriadSettings()
        ..clefPreferences = {
          Clef.treble: true,
          Clef.alto: true,
          Clef.tenor: true,
          Clef.bass: true,
        }
        ..standardClefArrangement = false
        ..maxKeySignatureAccidentals = 6
        ..diminishedFrequency = 2
        ..augmentedFrequency = 2
        ..majorFrequency = 1
        ..minorFrequency = 1;
      return settings;
    }

    final customColors = Theme.of(context).extension<CustomColors>()!;

    return FutureBuilder<List<dynamic>>(
      future: Future.wait([
        TonfallSharedPreferences.getLessonTrainingPercent(LessonIDName.trebleClef.id),
        //0
        TonfallSharedPreferences.getLessonTrainingPercent(LessonIDName.bassClef.id),
        //1
        TonfallSharedPreferences.getLessonTrainingPercent(LessonIDName.zeroSharps.id),
        //2
        TonfallSharedPreferences.getLessonTrainingPercent(LessonIDName.oneSharp.id),
        //3
        TonfallSharedPreferences.getLessonTrainingPercent(LessonIDName.twoSharps.id),
        //4
        TonfallSharedPreferences.getLessonTrainingPercent(LessonIDName.threeSharps.id),
        //5
        TonfallSharedPreferences.getLessonTrainingPercent(LessonIDName.fourSharps.id),
        // 6
        TonfallSharedPreferences.getLessonTrainingPercent(LessonIDName.fiveSharps.id),
        //7
        TonfallSharedPreferences.getLessonTrainingPercent(LessonIDName.sixSharps.id),
        //8
        TonfallSharedPreferences.getLessonTrainingPercent(LessonIDName.oneFlat.id),
        //9
        TonfallSharedPreferences.getLessonTrainingPercent(LessonIDName.twoFlats.id),
        // 10
        TonfallSharedPreferences.getLessonTrainingPercent(LessonIDName.threeFlats.id),
        // 11
        TonfallSharedPreferences.getLessonTrainingPercent(LessonIDName.fourFlats.id),
        //12
        TonfallSharedPreferences.getLessonTrainingPercent(LessonIDName.fiveFlats.id),
        //13
        TonfallSharedPreferences.getLessonTrainingPercent(LessonIDName.sixFlats.id),
        //14
        TonfallSharedPreferences.getLessonTrainingPercent(LessonIDName.augmentedAndDiminished.id),
        //15
        TonfallSharedPreferences.getLessonTrainingPercent(LessonIDName.chordsWithAccidentals1.id),
        //16
        TonfallSharedPreferences.getLessonTrainingPercent(LessonIDName.chordsWithAccidentals2.id),
        //17
        TonfallSharedPreferences.getLessonTrainingPercent(LessonIDName.tenorClef.id),
        //18
        TonfallSharedPreferences.getLessonTrainingPercent(LessonIDName.altoClef.id),
        // 19
        TonfallSharedPreferences.getLessonTrainingPercent(LessonIDName.allClefs1.id),
        //20
        TonfallSharedPreferences.getLessonTrainingPercent(LessonIDName.allClefs2.id),
        //21

        TonfallSharedPreferences.getWatchBadge(LessonIDName.trebleClef.watchbadge),
        //22
        TonfallSharedPreferences.getWatchBadge(LessonIDName.bassClef.watchbadge),
        //23
        TonfallSharedPreferences.getWatchBadge(LessonIDName.zeroSharps.watchbadge),
        //24
        TonfallSharedPreferences.getWatchBadge(LessonIDName.oneSharp.watchbadge),
        //25
        TonfallSharedPreferences.getWatchBadge(LessonIDName.twoSharps.watchbadge),
        //26
        TonfallSharedPreferences.getWatchBadge(LessonIDName.threeSharps.watchbadge),
        // 27
        TonfallSharedPreferences.getWatchBadge(LessonIDName.fourSharps.watchbadge),
        //28
        TonfallSharedPreferences.getWatchBadge(LessonIDName.fiveSharps.watchbadge),
        //29
        TonfallSharedPreferences.getWatchBadge(LessonIDName.sixSharps.watchbadge),
        //30
        TonfallSharedPreferences.getWatchBadge(LessonIDName.oneFlat.watchbadge),
        //31
        TonfallSharedPreferences.getWatchBadge(LessonIDName.twoFlats.watchbadge),
        //32
        TonfallSharedPreferences.getWatchBadge(LessonIDName.threeFlats.watchbadge),
        //33
        TonfallSharedPreferences.getWatchBadge(LessonIDName.fourFlats.watchbadge),
        //34
        TonfallSharedPreferences.getWatchBadge(LessonIDName.fiveFlats.watchbadge),
        //35
        TonfallSharedPreferences.getWatchBadge(LessonIDName.sixFlats.watchbadge),
        // 36
        TonfallSharedPreferences.getWatchBadge(LessonIDName.augmentedAndDiminished.watchbadge),
        //37
        TonfallSharedPreferences.getWatchBadge(LessonIDName.chordsWithAccidentals1.watchbadge),
        //38
        TonfallSharedPreferences.getWatchBadge(LessonIDName.chordsWithAccidentals2.watchbadge),
        //39
        TonfallSharedPreferences.getWatchBadge(LessonIDName.tenorClef.watchbadge),
        //40
        TonfallSharedPreferences.getWatchBadge(LessonIDName.altoClef.watchbadge),
        //41
        TonfallSharedPreferences.getWatchBadge(LessonIDName.allClefs1.watchbadge),
        //42
        TonfallSharedPreferences.getWatchBadge(LessonIDName.allClefs2.watchbadge),
        //43
      ]),
      builder: (context, AsyncSnapshot<List<dynamic>> snapshot) {
        if (snapshot.hasData) {
          final trebleClefPercent = snapshot.data?[0] as double? ?? 0;
          final bassClefPercent = snapshot.data?[1] as double? ?? 0;
          final zeroSharpsPercent = snapshot.data?[2] as double? ?? 0;
          final oneSharpPercent = snapshot.data?[3] as double? ?? 0;
          final twoSharpsPercent = snapshot.data?[4] as double? ?? 0;
          final threeSharpsPercent = snapshot.data?[5] as double? ?? 0;
          final fourSharpsPercent = snapshot.data?[6] as double? ?? 0;
          final fiveSharpsPercent = snapshot.data?[7] as double? ?? 0;
          final sixSharpsPercent = snapshot.data?[8] as double? ?? 0;
          final oneFlatPercent = snapshot.data?[9] as double? ?? 0;
          final twoFlatsPercent = snapshot.data?[10] as double? ?? 0;
          final threeFlatsPercent = snapshot.data?[11] as double? ?? 0;
          final fourFlatsPercent = snapshot.data?[12] as double? ?? 0;
          final fiveFlatsPercent = snapshot.data?[13] as double? ?? 0;
          final sixFlatsPercent = snapshot.data?[14] as double? ?? 0;
          final augmentedAndDiminishedPercent = snapshot.data?[15] as double? ?? 0;
          final chordsWithAccidentals1Percent = snapshot.data?[16] as double? ?? 0;
          final chordsWithAccidentals2Percent = snapshot.data?[17] as double? ?? 0;
          final tenorClefPercent = snapshot.data?[18] as double? ?? 0;
          final altoClefPercent = snapshot.data?[19] as double? ?? 0;
          final allClefs1Percent = snapshot.data?[20] as double? ?? 0;
          final allClefs2Percent = snapshot.data?[21] as double? ?? 0;

          final trebleClefWatchBadge = snapshot.data?[22] as bool? ?? false;
          final bassClefWatchBadge = snapshot.data?[23] as bool? ?? false;
          final zeroSharpsWatchBadge = snapshot.data?[24] as bool? ?? false;
          final oneSharpWatchBadge = snapshot.data?[25] as bool? ?? false;
          final twoSharpsWatchBadge = snapshot.data?[26] as bool? ?? false;
          final threeSharpsWatchBadge = snapshot.data?[27] as bool? ?? false;
          final fourSharpsWatchBadge = snapshot.data?[28] as bool? ?? false;
          final fiveSharpsWatchBadge = snapshot.data?[29] as bool? ?? false;
          final sixSharpsWatchBadge = snapshot.data?[30] as bool? ?? false;
          final oneFlatWatchBadge = snapshot.data?[31] as bool? ?? false;
          final twoFlatsWatchBadge = snapshot.data?[32] as bool? ?? false;
          final threeFlatsWatchBadge = snapshot.data?[33] as bool? ?? false;
          final fourFlatsWatchBadge = snapshot.data?[34] as bool? ?? false;
          final fiveFlatsWatchBadge = snapshot.data?[35] as bool? ?? false;
          final sixFlatsWatchBadge = snapshot.data?[36] as bool? ?? false;
          final augmentedAndDiminishedWatchBadge = snapshot.data?[37] as bool? ?? false;
          final chordsWithAccidentals1WatchBadge = snapshot.data?[38] as bool? ?? false;
          final chordsWithAccidentals2WatchBadge = snapshot.data?[39] as bool? ?? false;
          final tenorClefWatchBadge = snapshot.data?[40] as bool? ?? false;
          final altoClefWatchBadge = snapshot.data?[41] as bool? ?? false;
          final allClefs1WatchBadge = snapshot.data?[42] as bool? ?? false;
          final allClefs2WatchBadge = snapshot.data?[43] as bool? ?? false;

          if (kDebugMode) {
            print('altoClefPercent: $altoClefPercent');
          }
          if (kDebugMode) {
            print('chordsWithAccidentals2Percent: $chordsWithAccidentals2Percent');
          }
          //TonfallSharedPreferences.setLessonTrainingPercent(LessonIDName.chordsWithAccidentals2.id, 0);
          //TonfallSharedPreferences.setLessonTrainingPercent(LessonIDName.tenorClef.id, 0);

          return Scaffold(
            appBar: AppBar(
              title: const Text('Lektionstraining'),
              leading: IconButton(
                icon: const Icon(
                  Icons.arrow_back,
                ),
                onPressed: () => Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute<dynamic>(builder: (context) => StartMenu(version: version)),
                  (route) => false,
                ),
              ),
            ),
            body: SingleChildScrollView(
              child: Container(
                margin: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: Column(
                  children: [
                    const SizedBox(height: 40),
                    CardButtonComposer(
                      backgroundColor: customColors.correctAndSelectedColor,
                      headerText: 'Leicht',
                      children: [
                        LessonButton(
                          progressBarColor: customColors.correctAndSelectedLightColor,
                          backgroundColor: customColors.notSelectedColor,
                          lessonIDName: LessonIDName.trebleClef,
                          numberOfTasks: numberOfTasks,
                          initSettings: trebleClefSettings,
                          progressInPercent: trebleClefPercent,
                          showStopwatch: trebleClefWatchBadge,
                        ),
                        LessonButton(
                          progressBarColor: customColors.correctAndSelectedLightColor,
                          backgroundColor: customColors.notSelectedColor,
                          lessonIDName: LessonIDName.bassClef,
                          numberOfTasks: numberOfTasks,
                          initSettings: bassClefSettings,
                          progressInPercent: bassClefPercent,
                          showStopwatch: bassClefWatchBadge,
                        ),
                        LessonButton(
                          progressBarColor: customColors.correctAndSelectedLightColor,
                          backgroundColor: customColors.notSelectedColor,
                          lessonIDName: LessonIDName.zeroSharps,
                          numberOfTasks: numberOfTasks,
                          initSettings: zeroSharpsSettings,
                          progressInPercent: zeroSharpsPercent,
                          showStopwatch: zeroSharpsWatchBadge,
                        ),
                      ],
                    ),
                    const SizedBox(height: 40),
                    CardButtonComposer(
                      backgroundColor: customColors.lernmodusLightColor,
                      headerText: 'Mittel',
                      children: [
                        // 1 sharp
                        LessonButton(
                          lessonIDName: LessonIDName.oneSharp,
                          progressInPercent: oneSharpPercent,
                          initSettings: oneSharpSettings,
                          showStopwatch: oneSharpWatchBadge,
                          progressBarColor: customColors.lernmodusColor,
                          backgroundColor: customColors.notSelectedColor,
                          numberOfTasks: numberOfTasks,
                        ),
                        // 1 flat
                        LessonButton(
                          lessonIDName: LessonIDName.oneFlat,
                          progressInPercent: oneFlatPercent,
                          initSettings: oneFlatSettings,
                          showStopwatch: oneFlatWatchBadge,
                          progressBarColor: customColors.lernmodusColor,
                          backgroundColor: customColors.notSelectedColor,
                          numberOfTasks: numberOfTasks,
                        ),
                        // 2 sharps
                        LessonButton(
                          lessonIDName: LessonIDName.twoSharps,
                          progressInPercent: twoSharpsPercent,
                          initSettings: twoSharpsSettings,
                          showStopwatch: twoSharpsWatchBadge,
                          progressBarColor: customColors.lernmodusColor,
                          backgroundColor: customColors.notSelectedColor,
                          numberOfTasks: numberOfTasks,
                        ),
                        //2 flats
                        LessonButton(
                          lessonIDName: LessonIDName.twoFlats,
                          progressInPercent: twoFlatsPercent,
                          initSettings: twoFlatSettings,
                          showStopwatch: twoFlatsWatchBadge,
                          progressBarColor: customColors.lernmodusColor,
                          backgroundColor: customColors.notSelectedColor,
                          numberOfTasks: numberOfTasks,
                        ),
                        // 3 sharps
                        LessonButton(
                          lessonIDName: LessonIDName.threeSharps,
                          progressInPercent: threeSharpsPercent,
                          initSettings: threeSharpsSettings,
                          showStopwatch: threeSharpsWatchBadge,
                          progressBarColor: customColors.lernmodusColor,
                          backgroundColor: customColors.notSelectedColor,
                          numberOfTasks: numberOfTasks,
                        ),
                        //3 flats
                        LessonButton(
                          lessonIDName: LessonIDName.threeFlats,
                          progressInPercent: threeFlatsPercent,
                          initSettings: threeFlatSettings,
                          showStopwatch: threeFlatsWatchBadge,
                          progressBarColor: customColors.lernmodusColor,
                          backgroundColor: customColors.notSelectedColor,
                          numberOfTasks: numberOfTasks,
                        ),
                        // 4 sharps
                        LessonButton(
                          lessonIDName: LessonIDName.fourSharps,
                          progressInPercent: fourSharpsPercent,
                          initSettings: fourSharpsSettings,
                          showStopwatch: fourSharpsWatchBadge,
                          progressBarColor: customColors.lernmodusColor,
                          backgroundColor: customColors.notSelectedColor,
                          numberOfTasks: numberOfTasks,
                        ),
                        // 4 flats
                        LessonButton(
                          lessonIDName: LessonIDName.fourFlats,
                          progressInPercent: fourFlatsPercent,
                          initSettings: fourFlatSettings,
                          showStopwatch: fourFlatsWatchBadge,
                          progressBarColor: customColors.lernmodusColor,
                          backgroundColor: customColors.notSelectedColor,
                          numberOfTasks: numberOfTasks,
                        ),
                        // 5 sharps
                        LessonButton(
                          lessonIDName: LessonIDName.fiveSharps,
                          progressInPercent: fiveSharpsPercent,
                          initSettings: fiveSharpsSettings,
                          showStopwatch: fiveSharpsWatchBadge,
                          progressBarColor: customColors.lernmodusColor,
                          backgroundColor: customColors.notSelectedColor,
                          numberOfTasks: numberOfTasks,
                        ),
                        // 5 flats
                        LessonButton(
                          lessonIDName: LessonIDName.fiveFlats,
                          progressInPercent: fiveFlatsPercent,
                          initSettings: fiveFlatSettings,
                          showStopwatch: fiveFlatsWatchBadge,
                          progressBarColor: customColors.lernmodusColor,
                          backgroundColor: customColors.notSelectedColor,
                          numberOfTasks: numberOfTasks,
                        ),

                        // 6 sharps
                        LessonButton(
                          lessonIDName: LessonIDName.sixSharps,
                          progressInPercent: sixSharpsPercent,
                          initSettings: sixSharpsSettings,
                          showStopwatch: sixSharpsWatchBadge,
                          progressBarColor: customColors.lernmodusColor,
                          backgroundColor: customColors.notSelectedColor,
                          numberOfTasks: numberOfTasks,
                        ),
                        // 6 flats
                        LessonButton(
                          lessonIDName: LessonIDName.sixFlats,
                          progressInPercent: sixFlatsPercent,
                          initSettings: sixFlatSettings,
                          showStopwatch: sixFlatsWatchBadge,
                          progressBarColor: customColors.lernmodusColor,
                          backgroundColor: customColors.notSelectedColor,
                          numberOfTasks: numberOfTasks,
                        ),
                        //Übermäßig und Vermindert
                        LessonButton(
                          lessonIDName: LessonIDName.augmentedAndDiminished,
                          progressInPercent: augmentedAndDiminishedPercent,
                          initSettings: augAndDimSettings,
                          showStopwatch: augmentedAndDiminishedWatchBadge,
                          progressBarColor: customColors.lernmodusColor,
                          backgroundColor: customColors.notSelectedColor,
                          numberOfTasks: numberOfTasks,
                        ),
                        //Klänge mit Akzidentien 1
                        LessonButton(
                          lessonIDName: LessonIDName.chordsWithAccidentals1,
                          progressInPercent: chordsWithAccidentals1Percent,
                          initSettings: accidentalChordSettings1,
                          showStopwatch: chordsWithAccidentals1WatchBadge,
                          progressBarColor: customColors.lernmodusColor,
                          backgroundColor: customColors.notSelectedColor,
                          numberOfTasks: numberOfTasks,
                        ),
                        //Klänge mit Akzidentien 2
                        LessonButton(
                          lessonIDName: LessonIDName.chordsWithAccidentals2,
                          progressInPercent: chordsWithAccidentals2Percent,
                          initSettings: accidentalChordSettings2,
                          showStopwatch: chordsWithAccidentals2WatchBadge,
                          progressBarColor: customColors.lernmodusColor,
                          backgroundColor: customColors.notSelectedColor,
                          numberOfTasks: numberOfTasks,
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 40,
                    ),
                    CardButtonComposer(
                      backgroundColor: customColors.incorrectAndSelectedLightColor,
                      headerText: 'SCHWER',
                      children: [
                        LessonButton(
                          lessonIDName: LessonIDName.tenorClef,
                          progressInPercent: tenorClefPercent,
                          initSettings: tenorClefSettings,
                          showStopwatch: tenorClefWatchBadge,
                          progressBarColor: customColors.incorrectAndSelectedColor,
                          backgroundColor: customColors.notSelectedColor,
                          numberOfTasks: numberOfTasks,
                        ),
                        LessonButton(
                          lessonIDName: LessonIDName.altoClef,
                          progressInPercent: altoClefPercent,
                          initSettings: altoClefSettings,
                          showStopwatch: altoClefWatchBadge,
                          progressBarColor: customColors.incorrectAndSelectedColor,
                          backgroundColor: customColors.notSelectedColor,
                          numberOfTasks: numberOfTasks,
                        ),
                        LessonButton(
                          lessonIDName: LessonIDName.allClefs1,
                          progressInPercent: allClefs1Percent,
                          initSettings: allClefsStandardArrangementSettings,
                          showStopwatch: allClefs1WatchBadge,
                          progressBarColor: customColors.incorrectAndSelectedColor,
                          backgroundColor: customColors.notSelectedColor,
                          numberOfTasks: numberOfTasks,
                        ),
                        LessonButton(
                          lessonIDName: LessonIDName.allClefs2,
                          progressInPercent: allClefs2Percent,
                          initSettings: allClefsNoOrderSettings,
                          showStopwatch: allClefs2WatchBadge,
                          progressBarColor: customColors.incorrectAndSelectedColor,
                          backgroundColor: customColors.notSelectedColor,
                          numberOfTasks: numberOfTasks,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          );
        } else {
          return const CircularProgressIndicator();
        }
      },
    );
  }
}
