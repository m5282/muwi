import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:muwi/enums/badge_medal_color.dart';
import 'package:muwi/enums/badge_period_under_consideration.dart';
import 'package:muwi/enums/lesson_training_names.dart';
import 'package:muwi/enums/level_names.dart';
import 'package:muwi/enums/task_mode.dart';
import 'package:muwi/model/badge_model.dart';
import 'package:muwi/model/tonfall_shared_preferences.dart';
import 'package:muwi/view/subwidgets/menu_button.dart';
import 'package:muwi/view/theme/custom_theme.dart';
import 'package:muwi/view_model/quiz_page_view_model.dart';

//Todo: Badges next to each other. Problem: Images then get no width.
//Better state management. We assume that all nullable variables are
//are set when this class is shown. They just can't be set at the
//Moment this class is created.
//This problem is also indicated by the lint warning "This class is marked as @immutable ....".
//Ternary operators that extend over several lines are confusing.

class LessonCompleted extends StatefulWidget {
  //Todo: When creating a lesson, a lot has to be set manually.
  // Create a better constructor

  LessonCompleted({
    required this.lessonNameID,
    required this.version,
    required this.currentQPVM,
    super.key,
    this.taskMode = TaskMode.notExplicitSet,
    this.startTime,
    this.endTime,
    this.pointsAchieved,
    this.numberOfTasks,
    this.shownTasks,
    this.difficulty = Difficulty.other,
    this.pointsPerCorrectTask = 2,
    this.correctlySolvedInRowBest = 0,
  });

  DateTime? startTime;
  DateTime? endTime;
  int? pointsAchieved;
  final num? numberOfTasks;
  int? shownTasks;
  final Difficulty difficulty;

  //For a correct triad, the user gets 2 Points.
  // For a correct interval, the user gets 1 Point.
  final int pointsPerCorrectTask;
  final LessonIDName lessonNameID;
  final String version;
  final TaskMode taskMode;
  final QuizPageViewModel currentQPVM;
  int correctlySolvedInRowCurrent = 0;
  int correctlySolvedInRowBest;
  final int x = 0;

  @override
  State<StatefulWidget> createState() {
    return _LessonCompletedState();
  }
}

class _LessonCompletedState extends State<LessonCompleted> {
  @override
  Widget build(BuildContext context) {
    final customColors = Theme.of(context).extension<CustomColors>() ?? CustomColors.light;

    //When the user starts a quiz again,
    //he should begin with a task.
    //Especially, he should not see the last task, that he already answered.
    widget.currentQPVM.nextChord();

    ///Set this boolean true, if at least half of the points are achieved.
    ///That's the limit to pass the test.
    var examPassed = false;
    if (widget.pointsAchieved! >= widget.shownTasks! * widget.pointsPerCorrectTask / 2) {
      examPassed = true;
    }

    final differenceInSeconds = widget.endTime!.difference(widget.startTime!).inSeconds;
    final newPercentage =
        (1 / (widget.shownTasks! * widget.pointsPerCorrectTask)) * widget.pointsAchieved!;

    //In the end, there are two badges: Speed and CorrectInARow
    var goldSpeedBadge = false;
    var silverSpeedBadge = false;
    var bronzeSpeedBadge = false;
    var lessonTrainingSpeedBadge = false;

    var goldCorrectInRowBadge = false;
    var silverCorrectInRowBadge = false;
    var bronzeCorrectInRowBadge = false;

    var streakBadge = false;
    BadgeMedalColor? streakColor;
    var currentStreakLength = 0;

    //Speed badge: Difficulty - Easy
    if (BadgeModel.triadSpeedBronzeEasy().accomplished(
      difficulty: widget.difficulty,
      shownTasks: widget.shownTasks,
      pointsAchieved: widget.pointsAchieved,
      pointsPerCorrectTask: widget.pointsPerCorrectTask,
      differenceInSeconds: differenceInSeconds,
      taskMode: widget.taskMode,
    ) as bool) {
      TonfallSharedPreferences.setAccomplishedDateTimeForBadge(
        BadgeModel.triadSpeedBronzeEasy(),
        widget.endTime,
      );
      bronzeSpeedBadge = true;
    }
    if (BadgeModel.triadSpeedSilverEasy().accomplished(
      difficulty: widget.difficulty,
      shownTasks: widget.shownTasks,
      pointsAchieved: widget.pointsAchieved,
      pointsPerCorrectTask: widget.pointsPerCorrectTask,
      differenceInSeconds: differenceInSeconds,
      taskMode: widget.taskMode,
    ) as bool) {
      TonfallSharedPreferences.setAccomplishedDateTimeForBadge(
        BadgeModel.triadSpeedSilverEasy(),
        widget.endTime,
      );
      silverSpeedBadge = true;
    }
    //
    if (BadgeModel.triadSpeedGoldEasy().accomplished(
      difficulty: widget.difficulty,
      shownTasks: widget.shownTasks,
      pointsAchieved: widget.pointsAchieved,
      pointsPerCorrectTask: widget.pointsPerCorrectTask,
      differenceInSeconds: differenceInSeconds,
      taskMode: widget.taskMode,
    ) as bool) {
      TonfallSharedPreferences.setAccomplishedDateTimeForBadge(
        BadgeModel.triadSpeedGoldEasy(),
        widget.endTime,
      );
      goldSpeedBadge = true;
    }
    //Speed badge: Difficulty - Medium
    if (BadgeModel.triadSpeedBronzeMedium().accomplished(
      difficulty: widget.difficulty,
      shownTasks: widget.shownTasks,
      pointsAchieved: widget.pointsAchieved,
      pointsPerCorrectTask: widget.pointsPerCorrectTask,
      differenceInSeconds: differenceInSeconds,
      taskMode: widget.taskMode,
    ) as bool) {
      TonfallSharedPreferences.setAccomplishedDateTimeForBadge(
        BadgeModel.triadSpeedBronzeMedium(),
        widget.endTime,
      );
      bronzeSpeedBadge = true;
    }
    if (BadgeModel.triadSpeedSilverMedium().accomplished(
      difficulty: widget.difficulty,
      shownTasks: widget.shownTasks,
      pointsAchieved: widget.pointsAchieved,
      pointsPerCorrectTask: widget.pointsPerCorrectTask,
      differenceInSeconds: differenceInSeconds,
      taskMode: widget.taskMode,
    ) as bool) {
      TonfallSharedPreferences.setAccomplishedDateTimeForBadge(
        BadgeModel.triadSpeedSilverMedium(),
        widget.endTime,
      );
      silverSpeedBadge = true;
    }
    //
    if (BadgeModel.triadSpeedGoldMedium().accomplished(
      difficulty: widget.difficulty,
      shownTasks: widget.shownTasks,
      pointsAchieved: widget.pointsAchieved,
      pointsPerCorrectTask: widget.pointsPerCorrectTask,
      differenceInSeconds: differenceInSeconds,
      taskMode: widget.taskMode,
    ) as bool) {
      TonfallSharedPreferences.setAccomplishedDateTimeForBadge(
        BadgeModel.triadSpeedGoldMedium(),
        widget.endTime,
      );
      goldSpeedBadge = true;
    }
    //Speed badge: Difficulty - Hard
    if (BadgeModel.triadSpeedBronzeHard().accomplished(
      difficulty: widget.difficulty,
      shownTasks: widget.shownTasks,
      pointsAchieved: widget.pointsAchieved,
      pointsPerCorrectTask: widget.pointsPerCorrectTask,
      differenceInSeconds: differenceInSeconds,
      taskMode: widget.taskMode,
    ) as bool) {
      TonfallSharedPreferences.setAccomplishedDateTimeForBadge(
        BadgeModel.triadSpeedBronzeHard(),
        widget.endTime,
      );
      bronzeSpeedBadge = true;
    }
    if (BadgeModel.triadSpeedSilverHard().accomplished(
      difficulty: widget.difficulty,
      shownTasks: widget.shownTasks,
      pointsAchieved: widget.pointsAchieved,
      pointsPerCorrectTask: widget.pointsPerCorrectTask,
      differenceInSeconds: differenceInSeconds,
      taskMode: widget.taskMode,
    ) as bool) {
      TonfallSharedPreferences.setAccomplishedDateTimeForBadge(
        BadgeModel.triadSpeedSilverHard(),
        widget.endTime,
      );
      silverSpeedBadge = true;
    }
    //
    if (BadgeModel.triadSpeedGoldHard().accomplished(
      difficulty: widget.difficulty,
      shownTasks: widget.shownTasks,
      pointsAchieved: widget.pointsAchieved,
      pointsPerCorrectTask: widget.pointsPerCorrectTask,
      differenceInSeconds: differenceInSeconds,
      taskMode: widget.taskMode,
    ) as bool) {
      TonfallSharedPreferences.setAccomplishedDateTimeForBadge(
        BadgeModel.triadSpeedGoldHard(),
        widget.endTime,
      );
      goldSpeedBadge = true;
    }
    //Correct in a row
    if (BadgeModel.triadsCorrectlySolvedInARowBronzeEasy().accomplished(
      difficulty: widget.difficulty,
      shownTasks: widget.shownTasks,
      pointsAchieved: widget.pointsAchieved,
      pointsPerCorrectTask: widget.pointsPerCorrectTask,
      correctlySolvedInRowBest: widget.correctlySolvedInRowBest,
      taskMode: widget.taskMode,
    ) as bool) {
      TonfallSharedPreferences.setAccomplishedDateTimeForBadge(
        BadgeModel.triadsCorrectlySolvedInARowBronzeEasy(),
        widget.endTime,
      );
      bronzeCorrectInRowBadge = true;
    }
    if (BadgeModel.triadsCorrectlySolvedInARowSilverEasy().accomplished(
      difficulty: widget.difficulty,
      shownTasks: widget.shownTasks,
      pointsAchieved: widget.pointsAchieved,
      pointsPerCorrectTask: widget.pointsPerCorrectTask,
      correctlySolvedInRowBest: widget.correctlySolvedInRowBest,
      taskMode: widget.taskMode,
    ) as bool) {
      TonfallSharedPreferences.setAccomplishedDateTimeForBadge(
        BadgeModel.triadsCorrectlySolvedInARowSilverEasy(),
        widget.endTime,
      );
      silverCorrectInRowBadge = true;
    }
    if (BadgeModel.triadsCorrectlySolvedInARowGoldEasy().accomplished(
      difficulty: widget.difficulty,
      shownTasks: widget.shownTasks,
      pointsAchieved: widget.pointsAchieved,
      pointsPerCorrectTask: widget.pointsPerCorrectTask,
      correctlySolvedInRowBest: widget.correctlySolvedInRowBest,
      taskMode: widget.taskMode,
    ) as bool) {
      TonfallSharedPreferences.setAccomplishedDateTimeForBadge(
        BadgeModel.triadsCorrectlySolvedInARowGoldEasy(),
        widget.endTime,
      );
      goldCorrectInRowBadge = true;
    }
    if (BadgeModel.triadsCorrectlySolvedInARowBronzeMedium().accomplished(
      difficulty: widget.difficulty,
      shownTasks: widget.shownTasks,
      pointsAchieved: widget.pointsAchieved,
      pointsPerCorrectTask: widget.pointsPerCorrectTask,
      correctlySolvedInRowBest: widget.correctlySolvedInRowBest,
      taskMode: widget.taskMode,
    ) as bool) {
      TonfallSharedPreferences.setAccomplishedDateTimeForBadge(
        BadgeModel.triadsCorrectlySolvedInARowBronzeMedium(),
        widget.endTime,
      );
      bronzeCorrectInRowBadge = true;
    }
    if (BadgeModel.triadsCorrectlySolvedInARowSilverMedium().accomplished(
      difficulty: widget.difficulty,
      shownTasks: widget.shownTasks,
      pointsAchieved: widget.pointsAchieved,
      pointsPerCorrectTask: widget.pointsPerCorrectTask,
      correctlySolvedInRowBest: widget.correctlySolvedInRowBest,
      taskMode: widget.taskMode,
    ) as bool) {
      TonfallSharedPreferences.setAccomplishedDateTimeForBadge(
        BadgeModel.triadsCorrectlySolvedInARowSilverMedium(),
        widget.endTime,
      );
      silverCorrectInRowBadge = true;
    }
    if (BadgeModel.triadsCorrectlySolvedInARowGoldMedium().accomplished(
      difficulty: widget.difficulty,
      shownTasks: widget.shownTasks,
      pointsAchieved: widget.pointsAchieved,
      pointsPerCorrectTask: widget.pointsPerCorrectTask,
      correctlySolvedInRowBest: widget.correctlySolvedInRowBest,
      taskMode: widget.taskMode,
    ) as bool) {
      TonfallSharedPreferences.setAccomplishedDateTimeForBadge(
        BadgeModel.triadsCorrectlySolvedInARowGoldMedium(),
        widget.endTime,
      );
      goldCorrectInRowBadge = true;
    }
    if (BadgeModel.triadsCorrectlySolvedInARowBronzeHard().accomplished(
      difficulty: widget.difficulty,
      shownTasks: widget.shownTasks,
      pointsAchieved: widget.pointsAchieved,
      pointsPerCorrectTask: widget.pointsPerCorrectTask,
      correctlySolvedInRowBest: widget.correctlySolvedInRowBest,
      taskMode: widget.taskMode,
    ) as bool) {
      TonfallSharedPreferences.setAccomplishedDateTimeForBadge(
        BadgeModel.triadsCorrectlySolvedInARowBronzeHard(),
        widget.endTime,
      );
      bronzeCorrectInRowBadge = true;
    }
    if (BadgeModel.triadsCorrectlySolvedInARowSilverHard().accomplished(
      difficulty: widget.difficulty,
      shownTasks: widget.shownTasks,
      pointsAchieved: widget.pointsAchieved,
      pointsPerCorrectTask: widget.pointsPerCorrectTask,
      correctlySolvedInRowBest: widget.correctlySolvedInRowBest,
      taskMode: widget.taskMode,
    ) as bool) {
      TonfallSharedPreferences.setAccomplishedDateTimeForBadge(
        BadgeModel.triadsCorrectlySolvedInARowSilverHard(),
        widget.endTime,
      );
      silverCorrectInRowBadge = true;
    }
    if (BadgeModel.triadsCorrectlySolvedInARowGoldHard().accomplished(
      difficulty: widget.difficulty,
      shownTasks: widget.shownTasks,
      pointsAchieved: widget.pointsAchieved,
      pointsPerCorrectTask: widget.pointsPerCorrectTask,
      correctlySolvedInRowBest: widget.correctlySolvedInRowBest,
      taskMode: widget.taskMode,
    ) as bool) {
      TonfallSharedPreferences.setAccomplishedDateTimeForBadge(
        BadgeModel.triadsCorrectlySolvedInARowGoldHard(),
        widget.endTime,
      );
      goldCorrectInRowBadge = true;
    }

    //Interval Badges
    //Speed badge: Difficulty - Easy
    if (BadgeModel.intervalSpeedBronzeEasy().accomplished(
      difficulty: widget.difficulty,
      shownTasks: widget.shownTasks,
      pointsAchieved: widget.pointsAchieved,
      pointsPerCorrectTask: widget.pointsPerCorrectTask,
      differenceInSeconds: differenceInSeconds,
      taskMode: widget.taskMode,
    ) as bool) {
      TonfallSharedPreferences.setAccomplishedDateTimeForBadge(
        BadgeModel.intervalSpeedBronzeEasy(),
        widget.endTime,
      );
      bronzeSpeedBadge = true;
    }
    if (BadgeModel.intervalSpeedSilverEasy().accomplished(
      difficulty: widget.difficulty,
      shownTasks: widget.shownTasks,
      pointsAchieved: widget.pointsAchieved,
      pointsPerCorrectTask: widget.pointsPerCorrectTask,
      differenceInSeconds: differenceInSeconds,
      taskMode: widget.taskMode,
    ) as bool) {
      TonfallSharedPreferences.setAccomplishedDateTimeForBadge(
        BadgeModel.intervalSpeedSilverEasy(),
        widget.endTime,
      );
      silverSpeedBadge = true;
    }
    //
    if (BadgeModel.intervalSpeedGoldEasy().accomplished(
      difficulty: widget.difficulty,
      shownTasks: widget.shownTasks,
      pointsAchieved: widget.pointsAchieved,
      pointsPerCorrectTask: widget.pointsPerCorrectTask,
      differenceInSeconds: differenceInSeconds,
      taskMode: widget.taskMode,
    ) as bool) {
      TonfallSharedPreferences.setAccomplishedDateTimeForBadge(
        BadgeModel.intervalSpeedGoldEasy(),
        widget.endTime,
      );
      goldSpeedBadge = true;
    }
    //Speed badge: Difficulty - Medium
    if (BadgeModel.intervalSpeedBronzeMedium().accomplished(
      difficulty: widget.difficulty,
      shownTasks: widget.shownTasks,
      pointsAchieved: widget.pointsAchieved,
      pointsPerCorrectTask: widget.pointsPerCorrectTask,
      differenceInSeconds: differenceInSeconds,
      taskMode: widget.taskMode,
    ) as bool) {
      TonfallSharedPreferences.setAccomplishedDateTimeForBadge(
        BadgeModel.intervalSpeedBronzeMedium(),
        widget.endTime,
      );
      bronzeSpeedBadge = true;
    }
    if (BadgeModel.intervalSpeedSilverMedium().accomplished(
      difficulty: widget.difficulty,
      shownTasks: widget.shownTasks,
      pointsAchieved: widget.pointsAchieved,
      pointsPerCorrectTask: widget.pointsPerCorrectTask,
      differenceInSeconds: differenceInSeconds,
      taskMode: widget.taskMode,
    ) as bool) {
      TonfallSharedPreferences.setAccomplishedDateTimeForBadge(
        BadgeModel.intervalSpeedSilverMedium(),
        widget.endTime,
      );
      silverSpeedBadge = true;
    }
    //
    if (BadgeModel.intervalSpeedGoldMedium().accomplished(
      difficulty: widget.difficulty,
      shownTasks: widget.shownTasks,
      pointsAchieved: widget.pointsAchieved,
      pointsPerCorrectTask: widget.pointsPerCorrectTask,
      differenceInSeconds: differenceInSeconds,
      taskMode: widget.taskMode,
    ) as bool) {
      TonfallSharedPreferences.setAccomplishedDateTimeForBadge(
        BadgeModel.intervalSpeedGoldMedium(),
        widget.endTime,
      );
      goldSpeedBadge = true;
    }
    //Speed badge: Difficulty - Hard
    if (BadgeModel.intervalSpeedBronzeHard().accomplished(
      difficulty: widget.difficulty,
      shownTasks: widget.shownTasks,
      pointsAchieved: widget.pointsAchieved,
      pointsPerCorrectTask: widget.pointsPerCorrectTask,
      differenceInSeconds: differenceInSeconds,
      taskMode: widget.taskMode,
    ) as bool) {
      TonfallSharedPreferences.setAccomplishedDateTimeForBadge(
        BadgeModel.intervalSpeedBronzeHard(),
        widget.endTime,
      );
      bronzeSpeedBadge = true;
    }
    if (BadgeModel.intervalSpeedSilverHard().accomplished(
      difficulty: widget.difficulty,
      shownTasks: widget.shownTasks,
      pointsAchieved: widget.pointsAchieved,
      pointsPerCorrectTask: widget.pointsPerCorrectTask,
      differenceInSeconds: differenceInSeconds,
      taskMode: widget.taskMode,
    ) as bool) {
      TonfallSharedPreferences.setAccomplishedDateTimeForBadge(
        BadgeModel.intervalSpeedSilverHard(),
        widget.endTime,
      );
      silverSpeedBadge = true;
    }
    //
    if (BadgeModel.intervalSpeedGoldHard().accomplished(
      difficulty: widget.difficulty,
      shownTasks: widget.shownTasks,
      pointsAchieved: widget.pointsAchieved,
      pointsPerCorrectTask: widget.pointsPerCorrectTask,
      differenceInSeconds: differenceInSeconds,
      taskMode: widget.taskMode,
    ) as bool) {
      TonfallSharedPreferences.setAccomplishedDateTimeForBadge(
        BadgeModel.intervalSpeedGoldHard(),
        widget.endTime,
      );
      goldSpeedBadge = true;
    }
    //Correct in a row
    if (BadgeModel.intervalCorrectlySolvedInARowBronzeEasy().accomplished(
      difficulty: widget.difficulty,
      shownTasks: widget.shownTasks,
      pointsAchieved: widget.pointsAchieved,
      pointsPerCorrectTask: widget.pointsPerCorrectTask,
      correctlySolvedInRowBest: widget.correctlySolvedInRowBest,
      taskMode: widget.taskMode,
    ) as bool) {
      TonfallSharedPreferences.setAccomplishedDateTimeForBadge(
        BadgeModel.intervalCorrectlySolvedInARowBronzeEasy(),
        widget.endTime,
      );
      bronzeCorrectInRowBadge = true;
    }
    if (BadgeModel.intervalCorrectlySolvedInARowSilverEasy().accomplished(
      difficulty: widget.difficulty,
      shownTasks: widget.shownTasks,
      pointsAchieved: widget.pointsAchieved,
      pointsPerCorrectTask: widget.pointsPerCorrectTask,
      correctlySolvedInRowBest: widget.correctlySolvedInRowBest,
      taskMode: widget.taskMode,
    ) as bool) {
      TonfallSharedPreferences.setAccomplishedDateTimeForBadge(
        BadgeModel.intervalCorrectlySolvedInARowSilverEasy(),
        widget.endTime,
      );
      silverCorrectInRowBadge = true;
    }
    if (BadgeModel.intervalCorrectlySolvedInARowGoldEasy().accomplished(
      difficulty: widget.difficulty,
      shownTasks: widget.shownTasks,
      pointsAchieved: widget.pointsAchieved,
      pointsPerCorrectTask: widget.pointsPerCorrectTask,
      correctlySolvedInRowBest: widget.correctlySolvedInRowBest,
      taskMode: widget.taskMode,
    ) as bool) {
      TonfallSharedPreferences.setAccomplishedDateTimeForBadge(
        BadgeModel.intervalCorrectlySolvedInARowGoldEasy(),
        widget.endTime,
      );
      goldCorrectInRowBadge = true;
    }
    if (BadgeModel.intervalCorrectlySolvedInARowBronzeMedium().accomplished(
      difficulty: widget.difficulty,
      shownTasks: widget.shownTasks,
      pointsAchieved: widget.pointsAchieved,
      pointsPerCorrectTask: widget.pointsPerCorrectTask,
      correctlySolvedInRowBest: widget.correctlySolvedInRowBest,
      taskMode: widget.taskMode,
    ) as bool) {
      TonfallSharedPreferences.setAccomplishedDateTimeForBadge(
        BadgeModel.intervalCorrectlySolvedInARowBronzeMedium(),
        widget.endTime,
      );
      bronzeCorrectInRowBadge = true;
    }
    if (BadgeModel.intervalCorrectlySolvedInARowSilverMedium().accomplished(
      difficulty: widget.difficulty,
      shownTasks: widget.shownTasks,
      pointsAchieved: widget.pointsAchieved,
      pointsPerCorrectTask: widget.pointsPerCorrectTask,
      correctlySolvedInRowBest: widget.correctlySolvedInRowBest,
      taskMode: widget.taskMode,
    ) as bool) {
      TonfallSharedPreferences.setAccomplishedDateTimeForBadge(
        BadgeModel.intervalCorrectlySolvedInARowSilverMedium(),
        widget.endTime,
      );
      silverCorrectInRowBadge = true;
    }
    if (BadgeModel.intervalCorrectlySolvedInARowGoldMedium().accomplished(
      difficulty: widget.difficulty,
      shownTasks: widget.shownTasks,
      pointsAchieved: widget.pointsAchieved,
      pointsPerCorrectTask: widget.pointsPerCorrectTask,
      correctlySolvedInRowBest: widget.correctlySolvedInRowBest,
      taskMode: widget.taskMode,
    ) as bool) {
      TonfallSharedPreferences.setAccomplishedDateTimeForBadge(
        BadgeModel.intervalCorrectlySolvedInARowGoldMedium(),
        widget.endTime,
      );
      goldCorrectInRowBadge = true;
    }
    if (BadgeModel.intervalCorrectlySolvedInARowBronzeHard().accomplished(
      difficulty: widget.difficulty,
      shownTasks: widget.shownTasks,
      pointsAchieved: widget.pointsAchieved,
      pointsPerCorrectTask: widget.pointsPerCorrectTask,
      correctlySolvedInRowBest: widget.correctlySolvedInRowBest,
      taskMode: widget.taskMode,
    ) as bool) {
      TonfallSharedPreferences.setAccomplishedDateTimeForBadge(
        BadgeModel.intervalCorrectlySolvedInARowBronzeHard(),
        widget.endTime,
      );
      bronzeCorrectInRowBadge = true;
    }
    if (BadgeModel.intervalCorrectlySolvedInARowSilverHard().accomplished(
      difficulty: widget.difficulty,
      shownTasks: widget.shownTasks,
      pointsAchieved: widget.pointsAchieved,
      pointsPerCorrectTask: widget.pointsPerCorrectTask,
      correctlySolvedInRowBest: widget.correctlySolvedInRowBest,
      taskMode: widget.taskMode,
    ) as bool) {
      TonfallSharedPreferences.setAccomplishedDateTimeForBadge(
        BadgeModel.intervalCorrectlySolvedInARowSilverHard(),
        widget.endTime,
      );
      silverCorrectInRowBadge = true;
    }
    if (BadgeModel.intervalCorrectlySolvedInARowGoldHard().accomplished(
      difficulty: widget.difficulty,
      shownTasks: widget.shownTasks,
      pointsAchieved: widget.pointsAchieved,
      pointsPerCorrectTask: widget.pointsPerCorrectTask,
      correctlySolvedInRowBest: widget.correctlySolvedInRowBest,
      taskMode: widget.taskMode,
    ) as bool) {
      TonfallSharedPreferences.setAccomplishedDateTimeForBadge(
        BadgeModel.intervalCorrectlySolvedInARowGoldHard(),
        widget.endTime,
      );
      goldCorrectInRowBadge = true;
    }

    TonfallSharedPreferences.getAccomplishedDateTimeForBadge(BadgeModel.streak()).then((lastGameDayTime) async {
      if (BadgeModel.streak().accomplished(
        pointsAchieved: widget.pointsAchieved,
      ) as bool) {
        final bestStreakLengthFromDisk = await TonfallSharedPreferences.getValueFromBadge(
          BadgeModel.streak(), BadgePeriodUnderConsideration.allTime,);
        final currentStreakLengthFromDisk = await TonfallSharedPreferences.getValueFromBadge(
          BadgeModel.streak(), BadgePeriodUnderConsideration.twoWeeks,);
        currentStreakLength = BadgeModel.newStreakLength(
          now: widget.endTime!,
          nowIsNewStreakTime: true,
          lastGame: lastGameDayTime,
          currentValue: currentStreakLengthFromDisk ?? 0,
        );
        if(currentStreakLengthFromDisk != currentStreakLength){
          streakColor = BadgeModel.getStreakMedalColor(streakLength: currentStreakLength);
          streakBadge = streakColor != null;
          await TonfallSharedPreferences.setValueFromBadge(BadgeModel.streak(), BadgePeriodUnderConsideration.twoWeeks, currentStreakLength);
          if(bestStreakLengthFromDisk == null || currentStreakLength > bestStreakLengthFromDisk){
            await TonfallSharedPreferences.setValueFromBadge(BadgeModel.streak(), BadgePeriodUnderConsideration.allTime, currentStreakLength);
          }
        }
        await TonfallSharedPreferences.setAccomplishedDateTimeForBadge(BadgeModel.streak(), widget.endTime);
      }
    });

    lessonTrainingSpeedBadge = widget.numberOfTasks! < double.infinity &&
        differenceInSeconds <= widget.shownTasks! * 20 &&
        examPassed;
    if (lessonTrainingSpeedBadge) {
      TonfallSharedPreferences.setWatchBadge(widget.lessonNameID.watchbadge, received: true);
    }

    return FutureBuilder<double>(
      future: TonfallSharedPreferences.getLessonTrainingPercent(widget.lessonNameID.id),
      builder: (context, AsyncSnapshot<double> snapshot) {
        final oldPercentage = snapshot.data ?? 0;
        if (newPercentage > oldPercentage) {
          TonfallSharedPreferences.setLessonTrainingPercent(
            widget.lessonNameID.id,
            newPercentage,
          );
        }
        if (snapshot.hasData) {
          return Scaffold(
            appBar: AppBar(
              title: const Text('Lektion abgeschlossen'),
              //it does not make sense to go back to the previous screen, the user should use the "Fertig" button
              automaticallyImplyLeading: false,
            ),
            body: SingleChildScrollView(
              child: Container(
                // space (of the buttons) to the left and right border of the screen
                margin: const EdgeInsets.fromLTRB(20, 20, 20, 20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Column(
                      children: [
                        LinearProgressIndicator(
                          //the percentage
                          value: newPercentage,
                          color: customColors.correctAndSelectedColor,
                          backgroundColor: customColors.incorrectAndSelectedColor,
                          minHeight: 40,
                        ),
                        Text(
                          // widget.numberOfTasks != double.infinity ?
                          // "${widget.pointsAchieved} / ${widget.numberOfTasks! * 2}"
                          // :
                          '${widget.pointsAchieved} / ${widget.shownTasks! * widget.pointsPerCorrectTask}',
                          textAlign: TextAlign.center,
                          style: Theme.of(context)
                              .textTheme
                              .headlineLarge
                              ?.copyWith(color: customColors.neutralNoteColor),
                        ),
                        Text(
                          'Punkten',
                          textAlign: TextAlign.center,
                          style: Theme.of(context).textTheme.bodyMedium,
                        ),
                        const SizedBox(
                          height: 40,
                        ),
                        if (goldSpeedBadge ||
                            silverSpeedBadge ||
                            bronzeSpeedBadge ||
                            lessonTrainingSpeedBadge)
                          Container(
                            margin: const EdgeInsets.fromLTRB(50, 20, 50, 20),
                            child: Stack(
                              alignment: Alignment.center,
                              children: [
                                Image(
                                  image: const AssetImage('lib/assets/stopwatch.png'),
                                  color: goldSpeedBadge
                                      ? customColors.goldColor
                                      : silverSpeedBadge
                                          ? customColors.silverColor
                                          : bronzeSpeedBadge
                                              ? customColors.bronzeColor
                                              : customColors.neutralNoteColor,
                                ),
                                Container(
                                  margin: const EdgeInsets.fromLTRB(0, 40, 0, 0),
                                  child: Column(
                                    children: [
                                      Text(
                                        "${differenceInSeconds ~/ 60}:${(differenceInSeconds % 60).toString().padLeft(2, '0')}",
                                        style: Theme.of(context).textTheme.headlineLarge?.copyWith(
                                              color: customColors.neutralNoteColor,
                                            ),
                                      ),
                                      Text(
                                        'Minuten',
                                        style: Theme.of(context).textTheme.bodyMedium,
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          )
                        else
                          Text(
                            "Zeit: ${differenceInSeconds ~/ 60}:${(differenceInSeconds % 60).toString().padLeft(2, '0')} Minuten",
                            style: Theme.of(context)
                                .textTheme
                                .headlineLarge
                                ?.copyWith(color: customColors.neutralNoteColor),
                          ),
                        if (bronzeCorrectInRowBadge ||
                            silverCorrectInRowBadge ||
                            goldCorrectInRowBadge)
                          Container(
                            margin: const EdgeInsets.fromLTRB(50, 20, 50, 20),
                            child: Image(
                              image: const AssetImage('lib/assets/arrow-up.png'),
                              color: goldCorrectInRowBadge
                                  ? customColors.goldColor
                                  : silverCorrectInRowBadge
                                      ? customColors.silverColor
                                      : bronzeCorrectInRowBadge
                                          ? customColors.bronzeColor
                                          : customColors.neutralNoteColor,
                            ),
                          )
                        else
                          const SizedBox(),
                        if (goldCorrectInRowBadge ||
                            silverCorrectInRowBadge ||
                            bronzeCorrectInRowBadge)
                          Text(
                            '${widget.correctlySolvedInRowBest} in Folge richtig',
                            style: Theme.of(context)
                                .textTheme
                                .headlineLarge
                                ?.copyWith(color: customColors.neutralNoteColor),
                          )
                        else
                          const SizedBox(),
                        if (bronzeSpeedBadge ||
                            silverSpeedBadge ||
                            goldSpeedBadge ||
                            bronzeCorrectInRowBadge ||
                            silverCorrectInRowBadge ||
                            goldCorrectInRowBadge)
                          widget.difficulty == Difficulty.easy
                              ? Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Column(
                                      children: [
                                        Icon(
                                          Icons.star,
                                          size: Theme.of(context).textTheme.headlineLarge?.fontSize,
                                        ),
                                        Text(
                                          'einfach',
                                          style: Theme.of(context).textTheme.bodyMedium,
                                        ),
                                      ],
                                    ),
                                    const SizedBox(
                                      width: 10,
                                    ),
                                    Column(
                                      children: [
                                        Icon(
                                          Icons.star_border,
                                          size: Theme.of(context).textTheme.headlineLarge?.fontSize,
                                        ),
                                        Text(
                                          'mittel',
                                          style: Theme.of(context).textTheme.bodyMedium,
                                        ),
                                      ],
                                    ),
                                    const SizedBox(
                                      width: 10,
                                    ),
                                    Column(
                                      children: [
                                        Icon(
                                          Icons.star_border,
                                          size: Theme.of(context).textTheme.headlineLarge?.fontSize,
                                        ),
                                        Text(
                                          'schwer',
                                          style: Theme.of(context).textTheme.bodyMedium,
                                        ),
                                      ],
                                    ),
                                  ],
                                )
                              : widget.difficulty == Difficulty.medium
                                  ? Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Column(
                                          children: [
                                            Icon(
                                              Icons.star,
                                              size: Theme.of(context)
                                                  .textTheme
                                                  .headlineLarge
                                                  ?.fontSize,
                                            ),
                                            Text(
                                              'einfach',
                                              style: Theme.of(context).textTheme.bodyMedium,
                                            ),
                                          ],
                                        ),
                                        const SizedBox(
                                          width: 10,
                                        ),
                                        Column(
                                          children: [
                                            Icon(
                                              Icons.star,
                                              size: Theme.of(context)
                                                  .textTheme
                                                  .headlineLarge
                                                  ?.fontSize,
                                            ),
                                            Text(
                                              'mittel',
                                              style: Theme.of(context).textTheme.bodyMedium,
                                            ),
                                          ],
                                        ),
                                        const SizedBox(
                                          width: 10,
                                        ),
                                        Column(
                                          children: [
                                            Icon(
                                              Icons.star_border,
                                              size: Theme.of(context)
                                                  .textTheme
                                                  .headlineLarge
                                                  ?.fontSize,
                                            ),
                                            Text(
                                              'schwer',
                                              style: Theme.of(context).textTheme.bodyMedium,
                                            ),
                                          ],
                                        ),
                                      ],
                                    )
                                  : Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Column(
                                          children: [
                                            Icon(
                                              Icons.star,
                                              size: Theme.of(context)
                                                  .textTheme
                                                  .headlineLarge
                                                  ?.fontSize,
                                            ),
                                            Text(
                                              'einfach',
                                              style: Theme.of(context).textTheme.bodyMedium,
                                            ),
                                          ],
                                        ),
                                        const SizedBox(
                                          width: 10,
                                        ),
                                        Column(
                                          children: [
                                            Icon(
                                              Icons.star,
                                              size: Theme.of(context)
                                                  .textTheme
                                                  .headlineLarge
                                                  ?.fontSize,
                                            ),
                                            Text(
                                              'mittel',
                                              style: Theme.of(context).textTheme.bodyMedium,
                                            ),
                                          ],
                                        ),
                                        const SizedBox(
                                          width: 10,
                                        ),
                                        Column(
                                          children: [
                                            Icon(
                                              Icons.star,
                                              size: Theme.of(context)
                                                  .textTheme
                                                  .headlineLarge
                                                  ?.fontSize,
                                            ),
                                            Text(
                                              'schwer',
                                              style: Theme.of(context).textTheme.bodyMedium,
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                        const SizedBox(
                          height: 20,
                        ),
                        if (!goldCorrectInRowBadge &&
                            !silverCorrectInRowBadge &&
                            !bronzeCorrectInRowBadge)
                          Text(
                            '${widget.correctlySolvedInRowBest} in Folge richtig',
                            style: Theme.of(context)
                                .textTheme
                                .headlineLarge
                                ?.copyWith(color: customColors.neutralNoteColor),
                          )
                        else
                          const SizedBox(),

                        const SizedBox(
                          height: 40,
                        ),

                        //Streak badge
                        if (streakBadge)
                          Container(
                            margin: const EdgeInsets.fromLTRB(50, 20, 50, 20),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Stack(
                                  alignment: Alignment.bottomCenter,
                                  children: [
                                    Image(
                                      image: const AssetImage(BadgeModel.streakPath),
                                      color: streakColor == BadgeMedalColor.gold
                                          ? customColors.goldColor
                                          : streakColor == BadgeMedalColor.silver
                                          ? customColors.silverColor
                                          : streakColor == BadgeMedalColor.bronze
                                          ? customColors.bronzeColor
                                          : customColors.neutralNoteColor,
                                    ),
                                    Text('$currentStreakLength',
                                      style: Theme.of(context)
                                          .textTheme
                                          .headlineLarge
                                          ?.copyWith(color: customColors.neutralNoteColor),
                                    ),
                                  ],
                                ),
                                Text('Tage in Folge benutzt',
                                  style: Theme.of(context)
                                      .textTheme
                                      .headlineLarge
                                      ?.copyWith(color: customColors.neutralNoteColor),
                                ),
                              ],
                            ),
                          )
                        else
                          Text('$currentStreakLength Tag${currentStreakLength != 1 ? "e" : ""} in Folge benutzt',
                            style: Theme.of(context)
                                .textTheme
                                .headlineLarge
                                ?.copyWith(color: customColors.neutralNoteColor),
                          ),
                      ],
                    ),
                    MenuButton(
                      'Fertig',
                      customColors.uebungsmodusLightColor,
                      () {
                        //Go two screens back.
                        // The first one is the QuizView,
                        // the second one is the one from where the quiz was started
                        Navigator.pop(context);
                        Navigator.pop(
                          context,
                          (math.max(oldPercentage, newPercentage), lessonTrainingSpeedBadge),
                        );
                      },
                    ),
                  ],
                ),
              ),
            ),
          );
        } else {
          return const CircularProgressIndicator();
        }
      },
    );
  }
}
