import 'package:flutter/material.dart';
import 'package:muwi/constants/global_constants.dart';
import 'package:muwi/constants/musical_constants.dart';
import 'package:muwi/constants/runtime_constants.dart';
import 'package:muwi/enums/lesson_training_names.dart';
import 'package:muwi/enums/level_names.dart';
import 'package:muwi/enums/task_mode.dart';
import 'package:muwi/model/accidental.dart';
import 'package:muwi/model/clef.dart';
import 'package:muwi/model/settings/triad_settings.dart';
import 'package:muwi/model/tonfall_shared_preferences.dart';
import 'package:muwi/view/lesson_completed.dart';
import 'package:muwi/view/subwidgets/card_button_composer.dart';
import 'package:muwi/view/subwidgets/menu_button.dart';
import 'package:muwi/view/theme/custom_theme.dart';
import 'package:muwi/view/triad_quiz_view.dart';
import 'package:muwi/view_model/keyboard_vm/triad_keyboard_vm.dart';
import 'package:muwi/view_model/taskproviders/triad_provider.dart';
import 'package:muwi/view_model/triad_quiz_page_view_model.dart';

// Todo: Heres a lot of redundancy with "Lektionstraining".
// The levels are like the last one from the Lektionstraining, but with an infinite number of tasks.

class ChooseLevelTriad extends StatelessWidget {
  const ChooseLevelTriad({super.key});

  /// Method loading the current Settings.
  Future<TriadSettings> loadTriadSettings(String mode) async {
    try {
      final maybeSettings = TriadSettings.fromJson(
        await TonfallSharedPreferences.getSettings('${mode}settings'),
      );
      return maybeSettings;
    } on Exception {
      throw Exception('No settings found.');
    }
  }

  @override
  Widget build(BuildContext context) {
    TriadSettings easySettings() {
      final settings = TriadSettings()
        ..snowmenOnly = false
        ..upperClef = Clef.treble
        ..lowerClef = Clef.bass
        ..standardClefArrangement = true
        ..oneKeySignature = true
        ..numKeySignatureAccidentals = 0
        ..accidental = Accidental.none
        ..patternToChoseFrom = {
          for (int i = 0; i < 7; i += 1)
            MusicalConstants.cMajorScale[i]: MusicalConstants.majorScale[i],
        };
      return settings;
    }

    TriadSettings mediumTriadSettings() {
      final settings = TriadSettings()
        ..upperClef = Clef.treble
        ..lowerClef = Clef.bass
        ..snowmenOnly = false
        ..maxKeySignatureAccidentals = 6
        ..diminishedFrequency = 2
        ..augmentedFrequency = 2
        ..majorFrequency = 1
        ..minorFrequency = 1;
      return settings;
    }

    TriadSettings hardTriadSettings() {
      final settings = TriadSettings()
        ..clefPreferences = {
          Clef.treble: true,
          Clef.alto: true,
          Clef.tenor: true,
          Clef.bass: true,
        }
        ..standardClefArrangement = false
        ..maxKeySignatureAccidentals = 6
        ..diminishedFrequency = 2
        ..augmentedFrequency = 2
        ..majorFrequency = 1
        ..minorFrequency = 1
        ..snowmenOnly = false;
      return settings;
    }

    final customColors = Theme.of(context).extension<CustomColors>()!;

    return Scaffold(
      appBar: AppBar(
        title: const Text('Levelauswahl: Dreiklänge'),
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            const SizedBox(
              height: 40,
            ), //some space to the top
            Container(
              margin: const EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: CardButtonComposer(
                backgroundColor: customColors.uebungsmodusLightColor,
                children: [
                  MenuButton(
                    LessonIDName.freeTriadTrainingEasy.displayableGermanName,
                    customColors.correctAndSelectedLightColor,
                    () {
                      final easyQPVM = TriadQuizPageViewModel(
                        settings: easySettings(),
                        triadProvider: TriadProvider(),
                        keyboardVM: TriadKeyboardVM(),
                      );

                      final lessonCompleted = LessonCompleted(
                        lessonNameID: LessonIDName.freeTriadTrainingEasy,
                        version: RuntimeConstants.version,
                        currentQPVM: easyQPVM,
                        startTime: DateTime.now(),
                        pointsAchieved: 0,
                        numberOfTasks: double.infinity,
                        difficulty: Difficulty.easy,
                        taskMode: TaskMode.triad,
                      );

                      Navigator.push(
                        context,
                        MaterialPageRoute<dynamic>(
                          builder: (context) => TriadQuizView(
                            triadVM: TriadQuizPageViewModel(
                              settings: easySettings(),
                              triadProvider: TriadProvider(),
                              keyboardVM: TriadKeyboardVM(),
                            ),
                            lessonCompleted: lessonCompleted,
                            showSettingsDrawer: false,
                          ),
                        ),
                      );
                    },
                  ),
                  MenuButton(
                    LessonIDName.freeTriadTrainingMedium.displayableGermanName,
                    customColors.correctAndNotSelectedColor,
                    () {
                      final mediumQPVM = TriadQuizPageViewModel(
                        settings: mediumTriadSettings(),
                        triadProvider: TriadProvider(),
                        keyboardVM: TriadKeyboardVM(),
                      );

                      final lessonCompleted = LessonCompleted(
                        lessonNameID: LessonIDName.freeTriadTrainingMedium,
                        version: RuntimeConstants.version,
                        currentQPVM: mediumQPVM,
                        startTime: DateTime.now(),
                        pointsAchieved: 0,
                        numberOfTasks: double.infinity,
                        difficulty: Difficulty.medium,
                        taskMode: TaskMode.triad,
                      );
                      Navigator.push(
                        context,
                        MaterialPageRoute<dynamic>(
                          builder: (context) => TriadQuizView(
                            triadVM: TriadQuizPageViewModel(
                              settings: mediumTriadSettings(),
                              triadProvider: TriadProvider(),
                              keyboardVM: TriadKeyboardVM(),
                            ),
                            lessonCompleted: lessonCompleted,
                            showSettingsDrawer: false,
                          ),
                        ),
                      );
                    },
                  ),
                  MenuButton(
                    LessonIDName.freeTriadTrainingHard.displayableGermanName,
                    customColors.klausurmodusLightColor,
                    () {
                      final hardQPVM = TriadQuizPageViewModel(
                        settings: hardTriadSettings(),
                        triadProvider: TriadProvider(),
                        keyboardVM: TriadKeyboardVM(),
                      );

                      final lessonCompleted = LessonCompleted(
                        lessonNameID: LessonIDName.freeTriadTrainingMedium,
                        version: RuntimeConstants.version,
                        currentQPVM: hardQPVM,
                        startTime: DateTime.now(),
                        pointsAchieved: 0,
                        numberOfTasks: double.infinity,
                        difficulty: Difficulty.hard,
                        taskMode: TaskMode.triad,
                      );

                      Navigator.push(
                        context,
                        MaterialPageRoute<dynamic>(
                          builder: (context) => TriadQuizView(
                            triadVM: TriadQuizPageViewModel(
                              settings: hardTriadSettings(),
                              triadProvider: TriadProvider(),
                              keyboardVM: TriadKeyboardVM(),
                            ),
                            lessonCompleted: lessonCompleted,
                            showSettingsDrawer: false,
                          ),
                        ),
                      );
                    },
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 40,
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: CardButtonComposer(
                backgroundColor: customColors.uebungsmodusLightColor,
                children: [
                  MenuButton(LessonIDName.userAdjustableTriadTraining.displayableGermanName,
                      customColors.notSelectedColor, () {
                    Navigator.push(
                      context,
                      MaterialPageRoute<dynamic>(
                        builder: (context) => FutureBuilder<TriadSettings>(
                          // Fetching the settings from SharedPreferences. This is the asynchronous operation.
                          future: loadTriadSettings(GlobalConstants.uebungsmodus),
                          builder: (
                            BuildContext context,
                            AsyncSnapshot<TriadSettings> snapshot,
                          ) {
                            TriadQuizPageViewModel userAdjustableQPVM;

                            Widget child;
                            if (snapshot.hasData) {
                              // Extract the settings from the snapshot.
                              final settings = snapshot.data!;
                              final userAdjustableQPVM = TriadQuizPageViewModel(
                                settings: settings,
                                triadProvider: TriadProvider(),
                                keyboardVM: TriadKeyboardVM(),
                              );

                              final lessonCompleted = LessonCompleted(
                                lessonNameID: LessonIDName.userAdjustableTriadTraining,
                                version: RuntimeConstants.version,
                                currentQPVM: userAdjustableQPVM,
                                startTime: DateTime.now(),
                                pointsAchieved: 0,
                                numberOfTasks: double.infinity,
                                taskMode: TaskMode.triad,
                              );

                              child = TriadQuizView(
                                triadVM: userAdjustableQPVM,
                                lessonCompleted: lessonCompleted,
                              );
                            } else if (snapshot.hasError) {
                              // Provide default settings in case of an error.
                              userAdjustableQPVM = TriadQuizPageViewModel(
                                settings: TriadSettings(),
                                triadProvider: TriadProvider(),
                                keyboardVM: TriadKeyboardVM(),
                              );
                              final lessonCompleted = LessonCompleted(
                                lessonNameID: LessonIDName.userAdjustableTriadTraining,
                                version: RuntimeConstants.version,
                                currentQPVM: userAdjustableQPVM,
                                startTime: DateTime.now(),
                                pointsAchieved: 0,
                                numberOfTasks: double.infinity,
                                taskMode: TaskMode.triad,
                              );

                              child = TriadQuizView(
                                triadVM: userAdjustableQPVM,
                                lessonCompleted: lessonCompleted,
                              );
                            } else {
                              // Show a circular progress indicator as long as the settings are loading.
                              child = const Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  SizedBox(
                                    width: 60,
                                    height: 60,
                                    child: CircularProgressIndicator(),
                                  ),
                                ],
                              );
                            }
                            return child;
                          },
                        ),
                      ),
                    );
                  }),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
