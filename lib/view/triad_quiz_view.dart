import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:muwi/enums/accidental_enum.dart';
import 'package:muwi/enums/bass.dart';
import 'package:muwi/interfaces/i_answer.dart';
import 'package:muwi/model/accidental.dart';
import 'package:muwi/model/chord.dart';
import 'package:muwi/model/chord_quality.dart';
import 'package:muwi/model/note.dart';
import 'package:muwi/model/note_class.dart';
import 'package:muwi/model/note_letter.dart';
import 'package:muwi/model/settings/triad_settings.dart';
import 'package:muwi/model/triad_task.dart';
import 'package:muwi/view/quiz_view.dart';
import 'package:muwi/view/subwidgets/keyboard/answer_key.dart';
import 'package:muwi/view/subwidgets/subdisplay/show_chord.dart';
import 'package:muwi/view/subwidgets/usersettings/custom_number_picker.dart';
import 'package:muwi/view/subwidgets/usersettings/menu_option.dart';
import 'package:muwi/view/theme/custom_theme.dart';
import 'package:muwi/view_model/keyboard_vm/triad_keyboard_vm.dart';
import 'package:muwi/view_model/real_coordinates.dart';
import 'package:muwi/view_model/triad_quiz_page_view_model.dart';

class TriadQuizView extends QuizView {
  const TriadQuizView({
    required this.triadVM,
    required super.lessonCompleted,
    super.key,
    //required super.ps,
    super.numOfTasks = double.infinity,
    super.showSettingsDrawer,
  }) : super(
          vm: triadVM,
          modeName: 'Dreiklänge',
        );

  final TriadQuizPageViewModel triadVM;

  @override
  TriadQuizViewState createState() => TriadQuizViewState();
}

/// This class calls the build-method of the parent class.
/// Before doing so, it contributes all the widgets specific to the triad mode.
class TriadQuizViewState extends QuizViewState<TriadQuizView> {
  //-------------------------------------------------------------------
  @override
  Widget build(BuildContext context) {
    //TODO: We need to redefine this because of the Divider-Widget. super.build contains the exact same line. Can we find another solution?
    final customColors = Theme.of(context).extension<CustomColors>()!;

    // We currently need these casts because the QuizView does not know that an TriadQPVM is always supplied with specific derived classes.
    // There might be another way to let it know automatically.
    final keyboardVM = widget.vm.keyboardVM as TriadKeyboardVM;
    final settings = widget.vm.settings as TriadSettings;

    //-------------------------------- CALLBACK FUNCTIONS FOR THE KEYBOARD:

    /// ACCIDENTAL
    void selectAcc(Accidental acc) {
      setState(() {
        // Changing the selection is only possible if the finish button wasn't hit yet.
        if (!keyboardVM.finished) {
          // If this accidental is already selected, unselect it.
          if (keyboardVM.accidental == acc) {
            keyboardVM.unselectAccidental(acc);
          } else {
            // First, cancel previous selection.
            keyboardVM
              ..unselectAllAccidentals()
              // Always unselect these, because they cannot be selected together with letters, accidentals or bass.
              ..unselectAugDim()
              ..selectAccidental(acc);
          }
        }
      });
    }

    /// NOTELETTER
    void selectLetter(NoteLetter letter) {
      setState(() {
        // Changing the selection is only possible if the finish button wasn't hit yet.
        if (!keyboardVM.finished) {
          // If this letter is already selected, unselect it.
          if (keyboardVM.letter == letter) {
            keyboardVM.unselectLetter(letter);
          } else {
            // First, cancel previous selection and make sure augmented/diminished is not selected.
            keyboardVM
              ..unselectAllLetters()
              ..unselectAugDim()
              // Then select letter.
              ..selectLetter(letter);
          }
        }
      });
    }

    /// BASS
    void selectBass(Bass bass) {
      setState(() {
        // Changing the selection is only possible if the finish button wasn't hit yet.
        if (!keyboardVM.finished) {
          // If this bass value is already selected, unselect it.
          if (keyboardVM.bass == bass) {
            keyboardVM.unselectBass(bass);
          } else {
            // first, cancel previous selection and make sure augmented/diminished is not selected.
            keyboardVM
              ..unselectAllBass()
              ..unselectAugDim()
              // Then select bass.
              ..selectBass(bass);
          }
        }
      });
    }

    /// CHORDQUALITY
    void selectQuality(ChordQuality quality) {
      setState(() {
        if (!widget.vm.keyboardVM.finished) {
          // Unselect, if selected.
          if (keyboardVM.quality == quality) {
            keyboardVM.unselectChordQuality(quality);
          } else {
            // First, cancel previous selection.
            keyboardVM.unselectAllChordQuality();
            // Unselect all the letters, accidentals and bass values because they cannot be selected together with augmented or diminished.
            if (quality == ChordQuality.diminished || quality == ChordQuality.augmented) {
              keyboardVM
                ..unselectAllLetters()
                ..unselectAllAccidentals()
                ..unselectAllBass();
            }
            keyboardVM.selectChordQuality(quality);
          }
        }
      });
    }

    rowsOfButtons = (RealCoordinates coordinates) {
      final buttonsFirstRow = <Widget>[];
      final buttonsSecondRow = <Widget>[];
      final buttonsThirdRow = <Widget>[];

      Widget Function(double) createImageLabel(String path) {
        return (double height) => Image.asset(path, height: height);
      }

      Widget Function(double) createTextLabel(String label) {
        return (double height) => AutoSizeText(
              maxLines: 1,
              style: TextStyle(fontSize: height),
              label,
            );
      }

      Widget createAnswerKey<T extends IAnswer>(
        T answer,
        void Function(T) changeState,
        Size buttonSize,
        Widget Function(double) createKeyboardLabel,
      ) {
        return AnswerKey(
          answer: answer,
          finished: keyboardVM.finished,
          buttonStatusCode: keyboardVM.keyboardColorCodes[answer]!,
          buttonSize: buttonSize,
          changeState: changeState,
          createButtonLabel: createKeyboardLabel,
        );
      }

      //--------- Filling the first row of buttons.

      for (final letter in NoteLetter.values) {
        buttonsFirstRow.add(
          createAnswerKey(
            letter,
            selectLetter,
            Size(coordinates.smallButtonWidth, coordinates.buttonHeight),
            createTextLabel(letter.keyLabel),
          ),
        );
      }

      //--------- Second row of buttons.
      buttonsSecondRow.addAll([
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            createAnswerKey(
              Accidental.sharp,
              selectAcc,
              Size(
                coordinates.mediumSizedButtonWidth,
                coordinates.buttonHeight,
              ),
              createImageLabel(AccidentalEnum.sharp.path),
            ),
            createAnswerKey(
              Accidental.flat,
              selectAcc,
              Size(
                coordinates.mediumSizedButtonWidth,
                coordinates.buttonHeight,
              ),
              createImageLabel(AccidentalEnum.flat.path),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            createAnswerKey(
              ChordQuality.major,
              selectQuality,
              Size(
                coordinates.doubledSmallButtonWidth,
                coordinates.buttonHeight,
              ),
              createTextLabel(ChordQuality.major.keyLabel),
            ),
            createAnswerKey(
              ChordQuality.minor,
              selectQuality,
              Size(
                coordinates.doubledSmallButtonWidth,
                coordinates.buttonHeight,
              ),
              createTextLabel(ChordQuality.minor.keyLabel),
            ),
          ],
        ),
      ]);

      //--------- Third row of buttons.
      buttonsThirdRow.addAll([
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            createAnswerKey(
              Bass.third,
              selectBass,
              Size(
                coordinates.mediumSizedButtonWidth,
                coordinates.buttonHeight,
              ),
              createTextLabel(Bass.third.keyLabel),
            ),
            createAnswerKey(
              Bass.fifth,
              selectBass,
              Size(
                coordinates.mediumSizedButtonWidth,
                coordinates.buttonHeight,
              ),
              createTextLabel(Bass.fifth.keyLabel),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            createAnswerKey(
              ChordQuality.augmented,
              selectQuality,
              Size(
                coordinates.doubledSmallButtonWidth,
                coordinates.buttonHeight,
              ),
              createTextLabel(ChordQuality.augmented.keyLabel),
            ),
            createAnswerKey(
              ChordQuality.diminished,
              selectQuality,
              Size(
                coordinates.doubledSmallButtonWidth,
                coordinates.buttonHeight,
              ),
              createTextLabel(ChordQuality.diminished.keyLabel),
            ),
          ],
        ),
      ]);

      return [buttonsFirstRow, buttonsSecondRow, buttonsThirdRow];
    };

    //--------- Bar displaying user input or correct answer.
    resultBarWidgets.clear();
    // Line one contains letter, accidental and mode. Examples: Cis, b, vermindert
    var chordLineOne = '';
    // Line two displays only the role of the lowest note, i.e. 3, 5 or nothing.
    var chordLineTwo = '';

    // Show the correct result when the finish button was hit.
    if (keyboardVM.finished) {
      chordLineOne = keyboardVM.currentTask.toString();
      resultBarWidgets.add(
        (double height) => Text(style: TextStyle(fontSize: height), chordLineOne),
      );
      final task = keyboardVM.currentTask! as TriadTask;
      if (task.quality != ChordQuality.augmented && task.quality != ChordQuality.diminished) {
        chordLineTwo += task.bass.name;
        resultBarWidgets.add(
          (double height) => Text(style: TextStyle(fontSize: height), chordLineTwo),
        );
      }
      // Before the finish button was hit, show the user input.
    } else {
      // Augmented and diminished do not have a letter.
      if (keyboardVM.quality == ChordQuality.diminished ||
          keyboardVM.quality == ChordQuality.augmented) {
        chordLineOne = keyboardVM.quality!.longGermanName;
        resultBarWidgets.add(
          (double height) => Text(style: TextStyle(fontSize: height), chordLineOne),
        );

        // Do not display anything when no letter is selected.
      } else if (keyboardVM.letter != null) {
        // The role of the lowest note (third or fifth)
        if (keyboardVM.bass != Bass.root) {
          chordLineTwo += keyboardVM.bass.name;
        }

        // If neither major nor minor is selected, use minor as the default and display the name in lowercase.
        if (keyboardVM.quality == null) {
          chordLineOne = Chord(
            baseNote: Note(
              NoteClass(keyboardVM.letter!, keyboardVM.accidental!),
              0,
            ),
            quality: ChordQuality.minor,
          ).toString();
        } else {
          chordLineOne = Chord(
            baseNote: Note(
              NoteClass(keyboardVM.letter!, keyboardVM.accidental!),
              0,
            ),
            quality: keyboardVM.quality!,
          ).toString();
        }
        resultBarWidgets
          ..add(
            (double height) => Text(style: TextStyle(fontSize: height), chordLineOne),
          )
          ..add(
            (double height) => Text(style: TextStyle(fontSize: height), chordLineTwo),
          );
      }
    }

    //----------------------- Drawer elements for specific settings
    additionalDrawerElements = [
      Divider(
        color: customColors.uebungsmodusLightColor,
        thickness: 4,
        height: 10,
      ),
      Row(
        children: [
          Expanded(
            child: Text(
              'Dreiklangspezifische Einstellungen:',
              style: Theme.of(context).textTheme.titleMedium,
            ),
          ),
        ],
      ),
      Divider(
        color: customColors.uebungsmodusLightColor,
        thickness: 4,
        height: 10,
      ),
      MenuOption(
        title: Text(
          'Nur 3 Noten. Keine Oktavierungen.',
          style: Theme.of(context).textTheme.labelMedium,
        ),
        toggleValue: () => setState(() {
          settings.snowmenOnly = !settings.snowmenOnly;
        }),
        currentValue: settings.snowmenOnly,
      ),
      Divider(
        color: customColors.uebungsmodusLightColor,
        thickness: 4,
        height: 10,
      ),
      Row(
        children: [
          Expanded(
            child: Text(
              'Verhältnis von Dur, Moll, Übermäßig und Vermindert:',
              style: Theme.of(context).textTheme.titleMedium,
            ),
          ),
        ],
      ),
      Divider(
        color: customColors.uebungsmodusLightColor,
        thickness: 4,
        height: 10,
      ),
      CustomNumberPicker(
        Text(
          'Dur:',
          style: Theme.of(context).textTheme.labelMedium,
        ),
        settings.majorFrequency,
        0,
        10,
        (n) => setState(() => settings.updateMajor(n)),
      ),
      CustomNumberPicker(
        Text(
          'Moll:',
          style: Theme.of(context).textTheme.labelMedium,
        ),
        settings.minorFrequency,
        0,
        10,
        (n) => setState(() => settings.updateMinor(n)),
      ),
      CustomNumberPicker(
        Text(
          'Übermäßig:',
          style: Theme.of(context).textTheme.labelMedium,
        ),
        settings.augmentedFrequency,
        0,
        10,
        (n) => setState(() => settings.updateAugmented(n)),
      ),
      CustomNumberPicker(
        Text(
          'Vermindert:',
          style: Theme.of(context).textTheme.labelMedium,
        ),
        settings.diminishedFrequency,
        0,
        10,
        (n) => setState(() => settings.updateDiminished(n)),
      ),
    ];

    //-------------------- Snowman animation
    additionalDisplayElements = [
      (RealCoordinates coordinates) => AnimatedOpacity(
            // If the widget is visible, animate to 0.0 (invisible).
            // If the widget is hidden, animate to 1.0 (fully visible).
            opacity: keyboardVM.finished ? 1.0 : 0.0,
            duration: const Duration(milliseconds: 1000),
            onEnd: () => setState(() {
              if (keyboardVM.finished) {
                widget.triadVM.colorDrawables();
              }
            }),
            // Do not show the fade-out animation
            child: Visibility(
              visible: keyboardVM.finished,
              child: ShowChord(
                upperDrawables: widget.triadVM.upperSnowmanDrawables,
                lowerDrawables: widget.triadVM.lowerSnowmanDrawables,
                realCoordinates: coordinates,
              ),
            ),
      ),
    ];

    return super.build(context);
  }
}
