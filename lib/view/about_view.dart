import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:muwi/constants/runtime_constants.dart';
import 'package:url_launcher/url_launcher.dart';

///This class shows the about screen.
///Here we show all the legal information about our app.
class AboutView extends StatelessWidget {
  const AboutView({required this.version, super.key});
  final String version;

  static final Uri _emailLaunchUri = Uri(
    scheme: 'mailto',
    path: 'tonfall@musik.uni-kiel.de',
    query: encodeQueryParameters(<String, String>{
      'subject': 'Feedback zu Version ${RuntimeConstants.version}',
    }),
  );

  static final Uri _gitlabUrl = Uri.parse(
  'https://gitlab.com/m5282/muwi/',);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('About'),
        leading: BackButton(
          onPressed: () {
            Navigator.pop(context);
            // If we want to close the drawer instant, use this line instead
            //Navigator.push(context, MaterialPageRoute(builder: (context) => tonfallRoot()));
          },
        ),
      ),
      body: Container(
        padding: const EdgeInsets.all(20),
        child: SingleChildScrollView(
          child: RichText(
            text: TextSpan(
              style: Theme.of(context).textTheme.bodyMedium,
              children: <TextSpan>[
                const TextSpan(
                  text: 'Was ist Tonfall? \n',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                const TextSpan(
                  text:
                      'Tonfall ist ein plattformübergreifendes Open-Source-Projekt, welches derzeit von fünf Studierenden, drei aus der Informatik und zwei aus der Musikwissenschaft, entwickelt wird. Entstanden ist das Projekt Ende 2021 in einem fachübergreifenden Seminar. Die App war ursprünglich für Studierende der Musikwissenschaft an der CAU konzipiert. Seit Ende 2022 ist Tonfall für alle Musiktheorie-Interessierten erhältlich.\n',
                ),
                const TextSpan(
                  text:
                      'Es sind noch viele Erweiterungen geplant, aber gelegentlich geht es nur langsam voran, da sich das Studium vordrängt. Falls Ihr Wünsche oder Feedback habt, meldet Euch gerne unter ',
                ),
                TextSpan(
                    text: 'tonfall@musik.uni-kiel.de\n',
                    style: const TextStyle(
                        decoration: TextDecoration.underline,
                    ),
                    recognizer: TapGestureRecognizer()
                      ..onTap = () {
                        launchUrl(_emailLaunchUri);
                      },
                ),
                const TextSpan(
                  text: ' oder besucht das Gitlab-Repository unter ',
                ),
                TextSpan(
                  text: 'https://gitlab.com/m5282/muwi.\n',
                    style: const TextStyle(
                        decoration: TextDecoration.underline,
                    ),
                    recognizer: TapGestureRecognizer()
                      ..onTap = () {
                        launchUrl(
                          _gitlabUrl,
                          mode: LaunchMode.externalApplication,
                        );
                      },
                ),
                const TextSpan(
                  text: 'Mitwirkende: \n',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                const TextSpan(
                  text:
                      'Alexander Hofmann (Design), Natalie Kaufhold (Entwicklung), Hannah Metz (Design), Ansgar von Velsen (Entwicklung), Esther Zwanger (Entwicklung)\n',
                ),
                const TextSpan(
                  text: 'Ehemalige Mitwirkende: \n',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                const TextSpan(
                  text:
                      'Josephina Dibbern, Lukas Fellensiek, Luise Hamann, Lisa Kuchenbrandt, Christopher Kunze, Fredrik Wieczerkowski\n',
                ),
                const TextSpan(
                  text: 'Rechtliches: \n',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                const TextSpan(
                  text:
                      'Das Vorhaben wurde durch den Digitalisierungsfonds 2020 und den Fonds für  Lehrinnovation 2022 der Christian-Albrechts-Universität zu Kiel gefördert. Die Verantwortung für den Inhalt dieser Veröffentlichung liegt bei den Autor*innen, mit Support durch Dataport Softwareentwicklung DS6.\n',
                ),
                const TextSpan(text: 'Genutzte Packages: package_info_plus.\n'),
                const TextSpan(
                  text: 'Kontakt:\n',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                TextSpan(
                    text: 'tonfall@musik.uni-kiel.de\n',
                    style: const TextStyle(
                        decoration: TextDecoration.underline,
                    ),
                    recognizer: TapGestureRecognizer()
                      ..onTap = () {
                        launchUrl(_emailLaunchUri);
                      },
                ),
                const TextSpan(
                  text: 'Gitlab:\n',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                TextSpan(
                    text: 'https://gitlab.com/m5282/muwi\n',
                    style: const TextStyle(
                        decoration: TextDecoration.underline,
                    ),
                    recognizer: TapGestureRecognizer()
                      ..onTap = () {
                        launchUrl(
                          _gitlabUrl,
                          mode: LaunchMode.externalApplication,
                        );
                      },
                ),
                const TextSpan(
                  text: 'Version: ',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                TextSpan(text: version),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

String? encodeQueryParameters(Map<String, String> params) {
  return params.entries
      .map((MapEntry<String, String> e) =>
  '${Uri.encodeComponent(e.key)}=${Uri.encodeComponent(e.value)}',
  ).join('&');
}
