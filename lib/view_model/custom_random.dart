import 'dart:math';
import 'package:analyzer_plugin/utilities/pair.dart';

/// This custom random generator contains the method frequencies, which resembles the haskell function frequencies of the Arbitrary type class.
/// The method takes a list of pairs of values and their desired frequency in relation to the other values.
/// Example: frequencies([Pair("FortyTwo",2),Pair("Zero",1)]) will generate the value "FortyTwo" twice as often as the value "Zero".
class CustomRandom<T> {
  Random rand = Random();

  /// First parameter of one tuple: The value.
  /// Second parameter of one tuple: The desired frequency in relation to other frequencies.
  /// Example: frequencies([Pair("FortyTwo",2),Pair("Zero",1)]) will generate the value "FortyTwo" twice as often as the value "Zero".
  T frequencies(List<Pair<T, int>> frequencies) {
    // Frequency values are summed up here. For the example above, the sum is 3.
    var sum = 0;

    // This list will be filled with the given objects. Objects with higher frequency values will be overrepresented in this list.
    final ranges = <T>[];

    // Adding as many objects to the list as specified by the frequency parameter. This overrepresents objects with higher frequency values:
    // For the example above, the list ist ["FortyTwo", "FortyTwo", "Zero"]
    for (final pair in frequencies) {
      sum += pair.last;
      ranges.addAll(List.filled(pair.last, pair.first));
    }

    // Instead of choosing a value from the number of objects, we choose from the number of objects times 10. This might or might not add more entropy.
    // We can change this back to a simpler mechanism if there are doubts of the benefit og this.
    final pool = sum * 10;
    final intermediateRandInt = rand.nextInt(pool);
    return ranges[intermediateRandInt % sum];
  }
}
