import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:url_launcher/url_launcher.dart';

/// This class contains the explanation texts for the onboarding screens
class IntroTexts {

  static TextSpan text1() {
    return const TextSpan(
        text: 'Willkommen in der Lernapp Tonfall.',
    );
    // "Es folgt eine kleine Führung.";
  }

  static TextSpan text2() {
    return TextSpan(
      children: [
        const TextSpan(
        text: 'Eine Erklärung zu Tonfall ist auf unserer gitlab Seite hinterlegt:\n',
        ),
        TextSpan(
          text: 'https://gitlab.com/m5282/muwi/',
        style: const TextStyle(
            decoration: TextDecoration.underline,
        ),
        recognizer: TapGestureRecognizer()
        ..onTap = () {
          launchUrl(
            Uri.parse('https://gitlab.com/m5282/muwi/-/blob/development/documentation/Tonfall-Anleitung.pdf'),
          mode: LaunchMode.externalApplication,
          );
        },
        ),
  ],
    );
    /*"Trainiere hier deine grundlegenden Fähigkeiten der Akkordbestimmung "
        "und den Umgang mit der App. Du erhältst Schritt für Schritt "
        "Feedback bei deiner Eingabe.";*/
  }

  static TextSpan text3() {
    return const TextSpan(
        text: 'Verbessere deine Fähigkeiten in der Akkordbestimmung '
        'und überprüfe nach der Eingabe die Lösung. '
        'Erkenne deine Fortschritte anhand der Auswertung.',
    );
  }

  static TextSpan text4() {
    return const TextSpan(
        text: 'Simuliere eine Prüfungssituation und arbeite unter Zeitdruck. '
        'Nach Beenden des Aufgabensatzes erhältst du deine Auswertung.',
    );
  }
}
