import 'package:flutter/foundation.dart';
import 'package:muwi/enums/color_code.dart';
import 'package:muwi/interfaces/i_drawable.dart';

/// An object of this class stores all the data needed by the view for the display of one single staff object like a note or an accidental.
/// See ViewModelConstants for a detailed documentation on the coordinate grid.
@immutable
class DrawableMuwi implements IDrawable {
  DrawableMuwi(
    this.xCoordinate,
    this.yCoordinate,
    this.scalingFactor,
    this.verticalOffset,
    this.path, {
    this.colorCode = ColorCode.neutralNote,
    this.isNote = false,
  });

  @override
  int xCoordinate;
  @override
  int yCoordinate;

  /// The path to the image file.
  @override
  String path;

  /// Example: Accidentals should be twice as big as notes. Notes have currently a scaling factor of one.
  @override
  double scalingFactor;

  /// The root of a picture is at the bottom. This variable handles the offset between the bottom of the picture and the position where we want the center to be.
  @override
  double verticalOffset;

  /// Set to true if this drawable is a note.
  @override
  bool isNote = false;

  @override
  ColorCode colorCode = ColorCode.neutralNote;

  @override
  bool operator ==(Object other) =>
      other is DrawableMuwi &&
      xCoordinate == other.xCoordinate &&
      yCoordinate == other.yCoordinate &&
      path == other.path &&
      scalingFactor == other.scalingFactor &&
      verticalOffset == other.verticalOffset;

  @override
  int get hashCode => Object.hash(
        xCoordinate,
        yCoordinate,
        path,
        scalingFactor,
        verticalOffset,
      );

  @override
  String toString() {
    return 'xCoordinate: $xCoordinate, '
        'yCoordinate: $yCoordinate, '
        'path: $path, '
        'scalingFactor: $scalingFactor, '
        'verticalOffset: $verticalOffset, '
        'isNote: $isNote, '
        'colorCode: $colorCode';
  }
}
