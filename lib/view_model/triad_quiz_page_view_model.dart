import 'package:muwi/constants/viewmodel_constants.dart';
import 'package:muwi/model/chord.dart';
import 'package:muwi/model/note.dart';
import 'package:muwi/model/note_class.dart';
import 'package:muwi/model/settings/triad_settings.dart';
import 'package:muwi/model/triad_task.dart';
import 'package:muwi/view_model/drawable_creator.dart';
import 'package:muwi/view_model/drawable_muwi.dart';
import 'package:muwi/view_model/keyboard_vm/triad_keyboard_vm.dart';
import 'package:muwi/view_model/position.dart';
import 'package:muwi/view_model/quiz_page_view_model.dart';
import 'package:muwi/view_model/taskproviders/triad_provider.dart';

/// The view model for a quiz page where the
/// user repeatedly guesses (e.g. randomly generated)
/// chords.
class TriadQuizPageViewModel extends QuizPageViewModel {
  TriadQuizPageViewModel({
    required TriadSettings settings,
    required TriadProvider triadProvider,
    required TriadKeyboardVM keyboardVM,
  }) : super(
          settings: settings,
          taskProvider: triadProvider,
          keyboardVM: keyboardVM,
        );

  /// The snowman as a list of drawables for the upper display.
  List<DrawableMuwi> upperSnowmanDrawables = [];

  /// The snowman as a list of drawables for the lower display.
  List<DrawableMuwi> lowerSnowmanDrawables = [];

  /// The notes of the snowman in the upper system.
  List<Note> upperSnowman = [];

  /// The notes of the snowman in the lower system.
  List<Note> lowerSnowman = [];

  //--------------------------------- Statistics:

  //TODO: Statistics...
  // /// The data on the learning progress of the user. This object will be replaced by an object retrieved from sharedPreferences in most cases. When this is not possible, e.g. in the beginning, this fresh Statistics object will be used instead.
  // Statistics statistics = Statistics();
  //
  // /// Set this flag to collect and display data on the learning progress.
  // bool collectStatistics = false;
  //
  // /// Method to load the current Statistics.
  // loadStatistics() async {
  //   try {
  //     Statistics maybeStatistics =
  //     Statistics.fromJson(await TonfallSharedPreferences.getStatistics("${modeName}statistics"));
  //       statistics = maybeStatistics;
  //   } on Exception {
  //     // This happens at least once, since there is no Statistics object stored in sharedPreferences when the App is loaded for the first time. In this case, the fresh Statistics object initialised above is used.
  //   }
  // }
  //
  // /// This method just delegates the update mechanism to the instance of the statistics.
  // updateStatistics(bool result) {
  //   statistics.updateStatistics(result);
  //   TonfallSharedPreferences.setStatistics("${modeName}statistics", statistics);
  // }
  //

  /////////////// NEXTCHORD /////////////

  /// This method computes a new random chord using the current settings.
  @override
  void nextChord() {
    super.nextChord();
    // Fill the list of Drawables for the Snowman.
    computeSnowmanDrawables();
  }

  /// Select a suitable octave for the snowman and fill the list of drawables.
  void computeSnowmanDrawables() {
    final currentTriad = currentDisplayableTask!.task as TriadTask;

    // Select a suitable octave for the root note. This is the first octave where the snowman will not be below the lowest permitted position.
    var rootPosition = Position(currentTriad.letter, 0);
    var lowestPermittedSnowmanRoot = Position.positionOfLine(
      ViewModelConstants.lowestRegularLineIndex - 1,
      upperClef!,
    );
    // Note that the div operator in flutter works as follows: -6 ~/ 7 = 0 and -8 ~/ 7 = 1. This is not wanted here.
    // If the lowest permitted position is 17 higher than the root note, we want to add 3 octaves octaves. (17 / 7).ceil() = 3.
    // If the lowest permitted position is 17 lower than the root note, we want to subtract 2 octaves. (- 17 / 7).ceil() = -2.
    var octavesToAdd = (lowestPermittedSnowmanRoot.getOffset(rootPosition) / 7).ceil();

    upperSnowman = Chord(
      baseNote: Note(
        NoteClass(currentTriad.letter, currentTriad.accidental),
        octavesToAdd,
      ),
      quality: currentTriad.quality,
    ).notes;
    upperSnowmanDrawables = DrawableCreator.createSnowmanDrawables(
      settings,
      keySignature!,
      upperClef!,
      upperSnowman,
      upperDisplayNotes,
    );

    //////////////////// Same thing for the snowman in the lower display.
    rootPosition = Position(currentTriad.letter, 0);
    lowestPermittedSnowmanRoot = Position.positionOfLine(
      ViewModelConstants.lowestRegularLineIndex - 1,
      lowerClef!,
    );
    octavesToAdd = (lowestPermittedSnowmanRoot.getOffset(rootPosition) / 7).ceil();

    lowerSnowman = Chord(
      baseNote: Note(
        NoteClass(currentTriad.letter, currentTriad.accidental),
        octavesToAdd,
      ),
      quality: currentTriad.quality,
    ).notes;
    lowerSnowmanDrawables = DrawableCreator.createSnowmanDrawables(
      settings,
      keySignature!,
      lowerClef!,
      lowerSnowman,
      lowerDisplayNotes,
    );
  }

  /// Color the original drawables in both displays. Color the snowman.
  /// Use the colors of the upper snowman as a reference.
  void colorDrawables() {
    upperSnowmanDrawables = DrawableCreator.coloredDrawables(
      upperSnowmanDrawables,
      upperSnowman,
      upperClef!,
    );
    lowerSnowmanDrawables = DrawableCreator.coloredDrawables(
      lowerSnowmanDrawables,
      upperSnowman,
      lowerClef!,
    );
    upperDrawables = DrawableCreator.coloredDrawables(
      upperDrawables,
      upperSnowman,
      upperClef!,
    );
    lowerDrawables = DrawableCreator.coloredDrawables(
      lowerDrawables,
      upperSnowman,
      lowerClef!,
    );
  }
}
