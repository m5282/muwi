import 'package:muwi/enums/color_code.dart';
import 'package:muwi/enums/interval_enum.dart';
import 'package:muwi/enums/interval_quality.dart';
import 'package:muwi/model/interval.dart';
import 'package:muwi/view_model/keyboard_vm/keyboard_vm.dart';

class IntervalKeyboardVM extends KeyboardVM {
  //------------------------------- User input:
  /// The IntervalEnum component to be determined by the user. Example: third, fifth, seventh.
  IntervalEnum? intervalEnum;

  /// The mode to be determined by the user. Examples: perfect, minor, augmented.
  IntervalQuality? intervalQuality;

  //////////// EVALUATION

  @override
  int eval() {
    final currentInterval = currentTask! as Interval;

    //////////// INTERVAL QUALITY
    /// Compare the selected interval mode to the correct interval mode.
    if (intervalQuality != currentInterval.intervalQuality) {
      // Wrong answer:
      if (intervalQuality != null) {
        keyboardColorCodes[intervalQuality!] =
            ColorCode.incorrectAndSelected; // Incorrect. Will turn red.
        finalStatusCode = ColorCode.incorrectAndSelected;
      }
      keyboardColorCodes[currentInterval.intervalQuality] =
          ColorCode.correctAndNotSelected; // Will turn yellow.
    } else {
      /// Correct answer:
      assert(
        intervalQuality != null, 'IntervalQuality is null',
      ); // In this case, interval!.intervalMode would be null. This should not happen.
      keyboardColorCodes[intervalQuality!] = ColorCode.correctAndSelected; // will turn green.
    }

    //////////// INTERVAL ENUM
    /// Compare the selected interval mode to the correct interval mode.
    if (intervalEnum != currentInterval.intervalEnum) {
      // Wrong answer:
      if (intervalEnum != null) {
        keyboardColorCodes[intervalEnum!] =
            ColorCode.incorrectAndSelected; // Incorrect. Will turn red.
        finalStatusCode = ColorCode.incorrectAndSelected;
      }
      keyboardColorCodes[currentInterval.intervalEnum] =
          ColorCode.correctAndNotSelected; // Will turn yellow.
    } else {
      /// Correct answer:
      assert(
        intervalEnum != null, 'IntervalEnum is null',
      ); // In this case, interval!.interval would be null. This should not happen.
      keyboardColorCodes[intervalEnum!] = ColorCode.correctAndSelected; // will turn green.
    }

    /// Update final result.
    finalStatusCode =
        finalStatusCode.index > keyboardColorCodes[currentInterval.intervalQuality]!.index
            ? finalStatusCode
            : keyboardColorCodes[currentInterval.intervalQuality]!;

    super.eval();
    if (super.finalStatusCode == ColorCode.correctAndSelected) {
      return 1;
    } else {
      return 0;
    }
  }

  /////////////// SETTERS FOR THE ANSWER STATUS CODE //////////////////

  @override
  void unselectAll() {
    unselectAllIntervals();
    unselectAllQualities();
    resetOperationsStatusCodes();
    super.unselectAll();
  }

  void unselectAllIntervals() {
    intervalEnum = null;
    for (final i in IntervalEnum.values) {
      keyboardColorCodes[i] = ColorCode.notSelected;
    }
  }

  void selectInterval(IntervalEnum i) {
    keyboardColorCodes[i] = ColorCode.selected;
    intervalEnum = i;
  }

  void selectQuality(IntervalQuality q) {
    keyboardColorCodes[q] = ColorCode.selected;
    intervalQuality = q;
  }

  void unselectInterval(IntervalEnum i) {
    keyboardColorCodes[i] = ColorCode.notSelected;
    intervalEnum = null;
  }

  void unselectQuality(IntervalQuality q) {
    keyboardColorCodes[q] = ColorCode.notSelected;
    intervalQuality = null;
  }

  void unselectAllQualities() {
    intervalQuality = null;
    for (final q in IntervalQuality.values) {
      keyboardColorCodes[q] = ColorCode.notSelected;
    }
  }
}
