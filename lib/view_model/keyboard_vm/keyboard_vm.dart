import 'package:flutter/foundation.dart';

import 'package:muwi/enums/color_code.dart';
import 'package:muwi/enums/operations.dart';
import 'package:muwi/interfaces/i_answer.dart';
import 'package:muwi/interfaces/i_task.dart';

abstract class KeyboardVM {
  /// The current task. The user has to determine the IntervalEnum component and the mode.
  ITask? currentTask;

  /// This flag is set if the user hits next without finishing the task.
  bool finished = false;

  /// The final answer is correct unless is turns out to be incorrect.
  /// This variable determines the color of the finish button.
  ColorCode finalStatusCode = ColorCode.correctAndSelected; // Will turn green if not changed.

  /// This variable determines the color of the result bar.
  ColorCode resultBarStatusCode = ColorCode.neutralResultBar;

  Map<Operations, ColorCode> answerCodesForOperations = {};

  /// The map containing the current state of the keyboard (for each button: some value out of {selected,correctAndSelected,notSelected, etc.}).
  Map<IAnswer, ColorCode> keyboardColorCodes = {};

  /// This method evaluates the answer which is currently chosen by the user.
  /// It updates the maps for letters, modes, bass, accidentals and operations. These contain the status codes which are needed for the colours of the buttons.
  int eval() {
    assert(finished, 'Finished equals false. This should not be the case when eval() is called.');
    if (kDebugMode) {
      print('finalStatusCode: $finalStatusCode');
    }

    /// Update the status for the result bar.
    /// The bar becomes red whenever the answer is not a 100% correct.
    resultBarStatus =
      finalStatusCode == ColorCode.correctAndSelected
          ? finalStatusCode
          : ColorCode.incorrectAndSelected;
    ///////// FINAL RESULT

    /// The final result should now be the worst of all scores obtained:
    /// Red if any of the answers was wrong, yellow if a least one correct answer was missing, but no wrong answers were selected, else green.
    /// Now adapt the color of the finish button:
    setFinalAnswerStatus(finalStatusCode);

    // If the final status code is correct and selected, only then we will return true.
    // return finalStatusCode == ColorCode.correctAndSelected;
    return -1;
  }

  set resultBarStatus(ColorCode code) {
    resultBarStatusCode = code;
  }

  ColorCode get resultBarStatus {
    return resultBarStatusCode;
  }

  void setFinalAnswerStatus(ColorCode code) {
    answerCodesForOperations[Operations.finish] = code;
  }

  void resetOperationsStatusCodes() {
    for (final op in Operations.values) {
      answerCodesForOperations[op] = ColorCode.notSelected;
    }
  }

  void unselectAll() {
    finalStatusCode = ColorCode.correctAndSelected;
    resultBarStatusCode = ColorCode.neutralResultBar;
  }
}
