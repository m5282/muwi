import 'package:muwi/enums/bass.dart';
import 'package:muwi/enums/color_code.dart';
import 'package:muwi/model/accidental.dart';
import 'package:muwi/model/chord_quality.dart';
import 'package:muwi/model/note_letter.dart';
import 'package:muwi/model/triad_task.dart';
import 'package:muwi/view_model/keyboard_vm/keyboard_vm.dart';

class TriadKeyboardVM extends KeyboardVM {
  //------------------------------- User input:
  /// The note letter as entered by the user.
  NoteLetter? letter;

  /// The quality as entered by the user.
  ChordQuality? quality;

  /// The role of the lowest note as entered by the user.
  Bass bass = Bass.root;

  /// The accidental as entered by the user.
  Accidental? accidental = Accidental.none;

  ////////////// EVALUATION ///////////////

  /// This method evaluates the answer which is currently chosen by the user.
  /// It updates the maps for letters, modes, bass, accidentals and operations. These contain the status codes which are needed for the colours of the buttons.
  @override
  int eval() {
    //int points = 0;
    var correctLetter = false;
    var correctAccidental = false;
    //bool correctChordQuality = false;
    var correctMajorOrMinor = false;
    var correctDimOrAug = false;
    var correctBass = false;

    final currentTriad = currentTask! as TriadTask;
    assert(finished == true, 'Finished equals false. This is not expected here.');

    /////////////// SPECIAL CASE: AUGMENTED OR DIMINISHED

    /// If the chord quality is either augmented or diminished, then no letter, no accidental and no bass should be selected.
    /// If any of these is selected anyway, the answer is downright wrong and the finish button will turn red when pressed.
    if (currentTriad.quality == ChordQuality.augmented ||
        currentTriad.quality == ChordQuality.diminished) {
      if (letter != null) {
        keyboardColorCodes[letter!] = ColorCode.incorrectAndSelected;
        finalStatusCode = ColorCode.incorrectAndSelected;
      }
      if (accidental != Accidental.none) {
        keyboardColorCodes[accidental!] = ColorCode.incorrectAndSelected;
        finalStatusCode = ColorCode.incorrectAndSelected;
      }
      if (bass != Bass.root) {
        keyboardColorCodes[bass] = ColorCode.incorrectAndSelected;
        finalStatusCode = ColorCode.incorrectAndSelected;
      }

      /// In all these cases, there are no yellow buttons for letter, accidental or bass. Just red or nothing.
    } else {
      //////////// LETTERS

      /// If the answers match, the pressed button will turn green.
      if (letter == currentTriad.letter) {
        /// Correct answer:
        assert(letter != null, 'Letter is null');
        keyboardColorCodes[letter!] = ColorCode.correctAndSelected; // will turn green.
        correctLetter = true;
      } else {
        /// Wrong answer.
        if (letter != null) {
          keyboardColorCodes[letter!] = ColorCode.incorrectAndSelected; // will turn red.
          finalStatusCode = ColorCode.incorrectAndSelected;
        }

        /// If nothing was selected, the correct button will still turn yellow.
        keyboardColorCodes[currentTriad.letter] =
            ColorCode.correctAndNotSelected; // will turn yellow.

        /// Adapt the final result. Choose the one with the higher value for Red > Yellow > Green.
        finalStatusCode = finalStatusCode.index > keyboardColorCodes[currentTriad.letter]!.index
            ? finalStatusCode
            : keyboardColorCodes[currentTriad.letter]!;
      }

      ////////// ACCIDENTALS

      /// Same for accidentals: Green for correct choice.
      /// Caution: There exists no button for Accidental.none!
      if (accidental == currentTriad.accidental) {
        /// Correct answer:
        correctAccidental = true;
        if (accidental != Accidental.none) {
          /// Color the button if there exists a button for the answer (if the accidental is not none).
          keyboardColorCodes[accidental!] = ColorCode.correctAndSelected; // will turn green.
        }
      } else {
        /// Wrong answer.
        /// Incorrect choice red, correct but not selected answer yellow.
        /// Note that Accidental.none behaves like the Bass.root-case.
        if (accidental != Accidental.none) {
          keyboardColorCodes[accidental!] = ColorCode.incorrectAndSelected; // will turn red.
          finalStatusCode = ColorCode.incorrectAndSelected;
        }
        if (currentTriad.accidental != Accidental.none) {
          keyboardColorCodes[currentTriad.accidental] =
              ColorCode.correctAndNotSelected; // will turn yellow.
          finalStatusCode =
              finalStatusCode.index > keyboardColorCodes[currentTriad.accidental]!.index
                  ? finalStatusCode
                  : keyboardColorCodes[currentTriad.accidental]!;
        }
      }

      ///////////// BASS
      /// Same thing for bass: root does not appear as an answer.
      /// If the root is correctly chosen by not pressing 3 or 5, nothing happens. If third or fifth is correctly chosen, the button turns green.
      if (bass == currentTriad.bass) {
        /// Correct answer.
        correctBass = true;
        if (bass != Bass.root) {
          /// If there exists a button to color in green (if the user correctly chose 3 or 5), then color it in green.
          keyboardColorCodes[bass] = ColorCode.correctAndSelected;
        }
      } else {
        /// Wrong answer.
        /// Same thing as before.
        if (bass != Bass.root) {
          /// In this case, a wrong answer was chosen.
          /// Incorrect 3 or 5 turns red.
          keyboardColorCodes[bass] = ColorCode.incorrectAndSelected;
          finalStatusCode = ColorCode.incorrectAndSelected;
        }
        if (currentTriad.bass != Bass.root) {
          /// Correct but not selected 3 or 5 turns yellow.
          keyboardColorCodes[currentTriad.bass] = ColorCode.correctAndNotSelected;

          /// Update final result. Best case: Yellow.
          finalStatusCode = finalStatusCode.index > keyboardColorCodes[currentTriad.bass]!.index
              ? finalStatusCode
              : keyboardColorCodes[currentTriad.bass]!;
        }
      }
    }

    //////////// CHORDQUALITY
    /// Finally, because the correct chord quality is never null:
    /// Compare the selected chord quality to the correct chord quality.
    if (quality != currentTriad.quality) {
      // Wrong answer:
      if (quality != null) {
        keyboardColorCodes[quality!] = ColorCode.incorrectAndSelected; // Incorrect. Will turn red.
        finalStatusCode = ColorCode.incorrectAndSelected;
      }
      keyboardColorCodes[currentTriad.quality] =
          ColorCode.correctAndNotSelected; // Will turn yellow.
    } else {
      /// Correct answer:
      //correctChordQuality = true;
      if (currentTriad.quality == ChordQuality.major ||
          currentTriad.quality == ChordQuality.minor) {
        correctMajorOrMinor = true;
      } else {
        correctDimOrAug = true;
      }
      assert(
        quality != null, 'Quality is null',
      ); // In this case, currentChord!.quality would be null. This should not happen.
      keyboardColorCodes[quality!] = ColorCode.correctAndSelected; // will turn green.
    }

    /// Update final result.
    finalStatusCode = finalStatusCode.index > keyboardColorCodes[currentTriad.quality]!.index
        ? finalStatusCode
        : keyboardColorCodes[currentTriad.quality]!;

    super.eval();

    if (correctDimOrAug) {
      return 2;
    }

    if (correctMajorOrMinor && correctLetter && correctAccidental && !correctBass) {
      return 1;
    }

    if (correctMajorOrMinor && correctLetter && correctAccidental && correctBass) {
      return 2;
    }

    return 0;
  }

  /////////////// SETTERS FOR THE ANSWER STATUS CODE //////////////////

  @override
  void unselectAll() {
    unselectAllLetters();
    unselectAllAccidentals();
    unselectAllBass();
    unselectAllChordQuality();
    resetOperationsStatusCodes();
    super.unselectAll();
  }

  void unselectAllLetters() {
    letter = null;
    for (final l in NoteLetter.values) {
      keyboardColorCodes[l] = ColorCode.notSelected;
    }
  }

  void unselectAllAccidentals() {
    accidental = Accidental.none;
    for (final acc in {Accidental.sharp, Accidental.flat}) {
      keyboardColorCodes[acc] = ColorCode.notSelected;
    }
  }

  void unselectAllBass() {
    bass = Bass.root;
    for (final b in {Bass.third, Bass.fifth}) {
      keyboardColorCodes[b] = ColorCode.notSelected;
    }
  }

  void unselectAllChordQuality() {
    quality = null;
    for (final q in ChordQuality.values) {
      keyboardColorCodes[q] = ColorCode.notSelected;
    }
  }

  void selectBass(Bass b) {
    keyboardColorCodes[b] = ColorCode.selected;
    bass = b;
  }

  void selectAccidental(Accidental acc) {
    keyboardColorCodes[acc] = ColorCode.selected;
    accidental = acc;
  }

  void selectLetter(NoteLetter l) {
    letter = l;
    keyboardColorCodes[l] = ColorCode.selected;
  }

  void selectChordQuality(ChordQuality q) {
    quality = q;
    keyboardColorCodes[q] = ColorCode.selected;
  }

  void unselectBass(Bass b) {
    keyboardColorCodes[bass] = ColorCode.notSelected;
    bass = Bass.root;
  }

  void unselectAccidental(Accidental acc) {
    keyboardColorCodes[acc] = ColorCode.notSelected;
    accidental = Accidental.none;
  }

  void unselectLetter(NoteLetter l) {
    letter = null;
    keyboardColorCodes[l] = ColorCode.notSelected;
  }

  void unselectChordQuality(ChordQuality q) {
    quality = null;
    keyboardColorCodes[q] = ColorCode.notSelected;
  }

  /// Helper function needed whenever a letter, bass or accidental is selected.
  /// Resets augmented/diminished because they cannot be selected together with letters, accidentals or bass.
  void unselectAugDim() {
    if (quality == ChordQuality.diminished || quality == ChordQuality.augmented) {
      quality = null;
      for (final q in {ChordQuality.augmented, ChordQuality.diminished}) {
        keyboardColorCodes[q] = ColorCode.notSelected;
      }
    }
  }
}
