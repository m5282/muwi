import 'dart:math';

import 'package:analyzer_plugin/utilities/pair.dart';
import 'package:muwi/constants/viewmodel_constants.dart';
import 'package:muwi/enums/accidental_enum.dart';
import 'package:muwi/enums/color_code.dart';
import 'package:muwi/model/accidental.dart';
import 'package:muwi/model/clef.dart';
import 'package:muwi/model/key_signature.dart';
import 'package:muwi/model/note.dart';
import 'package:muwi/model/note_class.dart';
import 'package:muwi/model/note_letter.dart';
import 'package:muwi/model/settings/settings.dart';
import 'package:muwi/view_model/drawable_muwi.dart';
import 'package:muwi/view_model/position.dart';

/// The positions of the accidentals for each clef, accidental type and letter.
int getKeySignaturePosition(Clef clef, NoteLetter letter, Accidental acc) {
  var octavesToSubtract = 0;
  if (clef == Clef.bass) {
    octavesToSubtract = 2;
  }
  if (clef == Clef.tenor || clef == Clef.alto) {
    octavesToSubtract = 1;
  }

  switch (letter) {
    case NoteLetter.c:
      return 5 - octavesToSubtract;
    case NoteLetter.d:
      return 5 - octavesToSubtract;
    case NoteLetter.e:
      return 5 - octavesToSubtract;
    case NoteLetter.f:
      // Note that key signature position for f flat is not the same as for f sharp.
      if (acc == Accidental.flat) {
        return 4 - octavesToSubtract;
      } else {
        return 5 - octavesToSubtract;
      }
    case NoteLetter.g:
      // Same as for f.
      if (acc == Accidental.flat) {
        return 4 - octavesToSubtract;
      } else {
        return 5 - octavesToSubtract;
      }
    case NoteLetter.a:
      return 4 - octavesToSubtract;
    case NoteLetter.b:
      return 4 - octavesToSubtract;
  }
}

/// This class provides the methods for transforming chord rearrangements and key signatures into lists of objects containing all the parameters needed by the view.
class DrawableCreator {
  /// This method handles the key signature. For a given key signature and a given clef, it returns a list of drawables. This is the first part of the final list. The second part consists of the drawables for the chord arrangement.
  static List<DrawableMuwi> createKeySignatureDrawables(
    KeySignature ks,
    Clef clef,
  ) {
    if (ks.alteredNoteClasses.isEmpty) {
      return [];
    } else {
      // This list keeps only the notes whose NoteClass coincides with one from the KeySignature.
      final filtered = <Note>[];

      // Extract accidental (flat or sharp). This works because there are currently no mixtures of flats and sharps in one KeySignature.
      //TODO: Refine this so it works for mixed KeySignatures as well.
      final accidental = ks.alteredNoteClasses.first.accidental;

      // This offset is needed because the origin of an inserted picture is its bottom, not its center.
      final verticalOffset = accidental.toSymbol().verticalOffset;

      // Only the letters matter. If the KeySignature contains f# and the chord contains f♭♭, the accidentals must be merged regardless of the octave.
      final letters = ks.alteredNoteClasses.map((nc) => nc.letter).toList();

      // Construct notes from the KeySignature lying in the range where we want to display the KeySignature accidentals.
      // We need to supply clef, letter and accidental here, because the exact position of a key signature accidental depends on all three.
      for (final l in letters) {
        filtered.add(
          Note(
            NoteClass(l, accidental),
            getKeySignaturePosition(clef, l, accidental),
          ),
        );
      }

      // Then, assuming that the KeySignature does not contain mixtures of flats and sharps, create display objects for the KeySignature accidentals.
      final drawables = <DrawableMuwi>[];
      var x = ViewModelConstants.beginOfKeySignatureKeys;
      for (final note in filtered) {
        final y = Position(note.noteClass.letter, note.octave).getDisplayIndex(clef);
        drawables.add(
          DrawableMuwi(
            x,
            y,
            ViewModelConstants.accScalingFactor,
            verticalOffset,
            accidental.toSymbol().path,
          ),
        );
        x++;
      }
      return drawables;
    }
  }

  static List<DrawableMuwi> createChordDrawables(
    Settings settings,
    KeySignature keySignature,
    Clef clef,
    List<Note> notes,
  ) {
    final mergedChord = mergeNotesAndKeySignature(settings, keySignature, notes);
    final drawables = drawablesFromRawData(mergedChord, clef);
    return drawables;
  }

  /// Merge the accidentals of the current chord arrangement with those of the KeySignature. Return a list of Drawable objects. This is the second part of the final list containing the drawables for the key signature and the chord arrangement.
  /// This method also takes care of adding more space between accidentals which are overlapping too much on the display.
  /// Example of one merge of chord notes with key signature accidentals:
  /// Clef: Treble
  /// KeySignature: D mayor
  /// Chord: No clue what chord this is :D
  ///
  /// -------------           ---------------           ---------------
  ///
  /// -------------           ---------------           ---------------
  ///       #                       ♭ o                     ♮ ♭ o
  /// -------------     +     ---------------     =     ---------------
  ///                               ♭ o                       ♭ o
  /// -------------           ---------------           ---------------
  ///     #                         ♭ o                     ♮ ♭ o
  /// -------------           ---------------           ---------------
  ///
  static List<Pair<Note, List<AccidentalEnum>>> mergeNotesAndKeySignature(
    Settings settings,
    KeySignature ks,
    List<Note> notes,
  ) {
    final mergedChord = <Pair<Note, List<AccidentalEnum>>>[];

    if (notes.isEmpty) {
      return mergedChord;
    } else {
      // The altered NoteClasses in this KeySignature.
      final alteredNotes = ks.alteredNoteClasses;

      // ----------------------------- Computing the list of accidentals for a note, depending on the key signature and the settings.

      for (final note in notes) {
        // For each note, a list of accidentals merged with the KeySignature is created. Example: [natural, flat]
        final symbols = <AccidentalEnum>[];

        // These are the accidentals eliminated by the KeySignature. If the KeySignature contains f#, then f## will be made into f#. Therefore, one # is removed.
        var accidentalsToRemove = 0;

        for (final altered in alteredNotes) {
          if (note.noteClass.letter == altered.letter) {
            if (note.accidental.semitones >= 0 && altered.accidental.semitones <= 0 ||
                note.accidental.semitones <= 0 && altered.accidental.semitones >= 0) {
              /// Clash between KeySignature symbol and chord accidental.
              /// Example: KeySignature contains f flat, chord contains f##.
              /// Final list will be [natural,sharp,sharp]
              /// Admittedly, handling the case of multiple KeySignature accidentals in one line might be exaggerated. Adding one single natural should be sufficient here.
              symbols.addAll(
                List.filled(
                  altered.accidental.semitones.abs(),
                  AccidentalEnum.natural,
                ),
              );
              break;
            } else {
              /// Same type of Accidental found in both lists.
              /// Example f# present in KeySignature and f## in chord.
              /// Result will be f# (2 # - 1 #).
              accidentalsToRemove = altered.accidental.semitones.abs();

              /// This is an option which was requested by some of the students:
              /// If the flag cumulativeAccidentals is set to false and a note has more than one accidental, all of these accidentals are displayed even if there is an accidental in the key signature which would normally cancel out one of the accidentals.
              if (!settings.cumulativeAccidentals) {
                if (note.accidental.semitones.abs() > 1) {
                  accidentalsToRemove = 0;
                }
              }

              break;
            }
          }
        }
        symbols.addAll(
          List.filled(
            note.accidental.semitones.abs() - accidentalsToRemove,
            note.accidental.toSymbol(),
          ),
        );
        mergedChord.add(Pair(note, symbols));
      }
    }
    return mergedChord;
  }

  // ------------------------------------ Creating the Drawable.

  /// This method takes a list of pairs where each pair contains a note and some list of the form [AccidentalEnum.natural, AccidentalEnum.sharp].
  /// From this list, it creates the Drawables for the display. It also scatters the accidentals in case they collide.
  static List<DrawableMuwi> drawablesFromRawData(
    List<Pair<Note, List<AccidentalEnum>>> mergedChord,
    Clef clef,
  ) {
    // The return value
    var drawableChord = <DrawableMuwi>[];

    // The shifting logic is different for unison and second. We deal with them separately.
    var conflictingUnison = false;
    var conflictingSecond = false;

    // We need these variables in order to make sure accidentals get enough space.
    // For efficiency reasons, track these two separately:
    // -20 is unreachable. We do not want an immediate collision here.
    var previousYCoordinate = -20;
    var previousLineWithAccidentals = false;
    // We need this one only for the last line.
    var conflictingThird = false;
    // This is the list where the y coordinates together with the accidentals for each y coordinate are added. Non conflicting lines are not kept.
    final linesWithAccidentalsToScatter = <Pair<int, int>>[Pair(previousYCoordinate, 0)];

    for (final pair in mergedChord) {
      final note = pair.first;
      final symbols = pair.last;

      // This offset is the position of the notes minus one free space minus the number of accidentals. This is where the accidentals start.
      var offset = symbols.length + 1;

      final notePos = Position(note.letter, note.octave);
      final y = notePos.getDisplayIndex(clef);

      if (y - 2 != previousYCoordinate || symbols.isEmpty || !previousLineWithAccidentals) {
        // If there is currently no conflict and if the previous line was not conflicting with its previous line, delete the previous line from the list.
        if (!conflictingThird) {
          linesWithAccidentalsToScatter.removeLast();
        }
        conflictingThird = false;
      } else {
        conflictingThird = true;
      }

      // Handle conflicting second. The logic is different from the logic for the thirds, because we move notes and accidentals here and we move them regardless of whether the note carries accidentals or not.
      if (y - 1 == previousYCoordinate) {
        conflictingSecond = true;
      }

      // Handle conflicting unison. The logic is different from the logic for the thirds or seconds, because the notes and their accidentals are placed next to each other.
      if (y == previousYCoordinate) {
        conflictingUnison = true;
      }

      // Always temporarily add the current line. It needs to be compared to the next line.
      linesWithAccidentalsToScatter.add(Pair(y, symbols.length));

      // Prepare the variables for the next check
      previousLineWithAccidentals = symbols.isNotEmpty;

      previousYCoordinate = y;

      // Now create the drawables for the accidentals in the correct order
      for (final symbol in symbols) {
        final x = ViewModelConstants.xCoordinateOfNotes - offset;
        drawableChord.add(
          DrawableMuwi(
            x,
            y,
            ViewModelConstants.accScalingFactor,
            symbol.verticalOffset,
            symbol.path,
          ),
        );
        offset--;
      }

      // Finally, add the note.
      drawableChord.add(
        DrawableMuwi(
          ViewModelConstants.xCoordinateOfNotes,
          y,
          ViewModelConstants.noteScalingFactor,
          ViewModelConstants.verticalOffsetForNotes,
          ViewModelConstants.notePath,
          isNote: true,
        ),
      );
    }

    // If the last line had no conflict, remove it from the list.
    if (!conflictingThird) {
      linesWithAccidentalsToScatter.removeLast();
    }

    // ---------------------------- Dealing with conflicts, shifting around overlapping accidentals.

    // Deal with the conflicting lines if necessary
    if (linesWithAccidentalsToScatter.isNotEmpty) {
      drawableChord = handleConflictingThirds(drawableChord, linesWithAccidentalsToScatter);
    }

    // If there are conflicting seconds, deal with them:
    if (conflictingSecond) {
      drawableChord = handleConflictingSeconds(drawableChord);
    }

    // If there are conflicting seconds, deal with them:
    if (conflictingUnison) {
      drawableChord = handleConflictingUnisons(drawableChord);
    }

    return drawableChord;
  }

  /// This method scatters and shifts accidentals in lines of conflicting thirds, such that accidentals do not overlap too much.
  /// The first parameter drawableChord is some list of Drawables.
  /// If there are lines of conflicting thirds, the parameter linesWithAccidentalsToScatter contains the y-coordinates of conflicting lines and the number of accidentals which have to be scattered.
  /// The accidentals in every second conflicting line are not only spaced, but also shifted by one.
  /// EXAMPLE:
  ///    ---------------           ---------------
  ///
  ///    ---------------           ---------------
  ///       ♮ ♭ ♭  o                 ♮  ♭  ♭   o
  ///    ---------------  becomes  ---------------
  ///       ♮ ♭ ♭  o               ♮  ♭  ♭     o
  ///    ---------------           ---------------
  ///         ♮ ♭  o                    ♮  ♭   o
  ///    ---------------           ---------------
  static List<DrawableMuwi> handleConflictingThirds(
    List<DrawableMuwi> drawableChord,
    List<Pair<int, int>> linesWithAccidentalsToScatter,
  ) {
    // If we are dealing with a third, we can have up to 4 accidentals, but only the innermost two accidentals have to be spaced.
    // In case of a triad, we need to space three accidentals in some cases, but we will never have four of them.
    var maxScatteredAccidentals = 0;
    if (linesWithAccidentalsToScatter.length > 2) {
      maxScatteredAccidentals = 3;
    } else {
      maxScatteredAccidentals = 2;
    }

    // Every second conflicting line is shifted to the left. Between two accidentals, one free space is inserted.
    var shiftToTheLeft = false;
    // We need this variable in order to keep track of whether we are in a shifting process or not.
    var accidentalCounter = -1;

    for (final drawable in drawableChord) {
      // The accidentalCounter is negative when we start a new line. During the shifting process, do not enter this block.
      if (accidentalCounter < 0) {
        if (linesWithAccidentalsToScatter.isNotEmpty) {
          final yCoordinateOfConflictingLine = linesWithAccidentalsToScatter.first.first;
          final numberOfAccidentalsInConflictingLine = linesWithAccidentalsToScatter.first.last;
          // Check whether the current line is one of the conflicting lines.
          if (drawable.yCoordinate == yCoordinateOfConflictingLine) {
            // Extract data and pop conflicting item.
            accidentalCounter = numberOfAccidentalsInConflictingLine;
            linesWithAccidentalsToScatter.removeAt(0);
          }
        }
      }

      // This variable might have changed in the previous block. This is NOT an if .. else.
      if (accidentalCounter >= 0) {
        if (accidentalCounter == 0) {
          // Shifting ends here. Prepare parameter for next line.
          shiftToTheLeft = !shiftToTheLeft;
        } else {
          // If we are in this block, there are still accidentals to shift.
          // Note that an accidental has to be shifted by at least 2 from the note position anyway. This cancels out the fact that the first accidental should not be shifted by the accidentalCounter.

          // The two innermost accidentals are spaced, the others are tight. The following two lines determine the number of spaced / tight accidentals.
          final int spacedAccidentals = min(maxScatteredAccidentals, accidentalCounter);
          final int tightAccidentals = max(0, accidentalCounter - spacedAccidentals);
          // Now we compute the new position for the current accidental.
          drawable.xCoordinate =
              ViewModelConstants.xCoordinateOfNotes - 2 * spacedAccidentals - tightAccidentals;
          if (shiftToTheLeft) {
            // In addition to the spacing, shift every second line by 1.
            drawable.xCoordinate = drawable.xCoordinate - 1;
          }
        }
        accidentalCounter--;
      }
    }

    return drawableChord;
  }

  /// This method puts unison notes and their accidentals next to each other, such that the Drawables do not overlap.
  /// The parameter drawableChord represents one single single unison! Caution: THIS METHOD IS ONLY SUITABLE FOR INTERVALS!
  /// EXAMPLE:
  ///    ------------------------------
  ///
  ///    ------------------------------
  ///       ♮ ♭ o   ♮  ♭  ♭   o
  ///    ------------------------------
  ///
  ///    ------------------------------
  ///
  ///    ------------------------------
  static List<DrawableMuwi> handleConflictingUnisons(List<DrawableMuwi> drawableInterval) {
    // As long ad we iterate through the drawables of the first note, we simply count drawables.
    // When we reach the drawables for the second note, we start shifting.
    var secondLine = false;
    // The number of drawables for the second note equals the number of drawables minus the number of drawables for the first note.
    // Therefore, we keep decreasing this number until we reach the drawables of the second note.
    var drawableCounter = drawableInterval.length;

    for (final drawable in drawableInterval) {
      if (secondLine) {
        // This is the number of indices we need to add if we want two empty spaces between the first note and the accidentals of the second note.
        //       1 2 3 4 5 6
        // ♮ ♭ o     ♮ ♭ ♭ o
        final offset = drawableCounter + 3;
        drawable.xCoordinate = drawable.xCoordinate + offset;
      } else {
        // The drawables of the first note end when we reach a note.
        if (drawable.isNote) {
          secondLine = true;
        }
        drawableCounter--;
      }
    }

    return drawableInterval;
  }

  /// This method scatters and shifts Drawables in lines of conflicting seconds, such that the Drawables do not overlap too much.
  /// The parameter drawableChord represents one single single second! Caution: THIS METHOD IS ONLY SUITABLE FOR INTERVALS! It does not work with more than two notes!
  /// The Drawables are not only spaced, but also shifted in the upper line.
  /// EXAMPLE:
  ///    ---------------           ---------------
  ///
  ///    ---------------           ---------------
  ///     ♮ ♭ ♭ ♭  o                ♮ ♭ ♭  ♭  o
  ///    -----♮-♭--o----  becomes  -------♮--♭--o-
  ///
  ///    ---------------           ---------------
  ///
  ///    ---------------           ---------------
  static List<DrawableMuwi> handleConflictingSeconds(List<DrawableMuwi> drawableInterval) {
    // While we have to avoid collisions of accidentals lying on top of each other, we also have to prevent the accidentals from colliding with the key signature. The key signature collides with the accidentals when we space and shift a sequence of four accidentals. Since the lower note will never carry more than two accidentals, we only need to space the two innermost accidentals. See example above.

    var lowerLine = true;
    const maxSpacedAccidentals = 2;

    for (final drawable in drawableInterval) {
      if (!drawable.isNote) {
        // ------------------ Spacing accidentals:

        // First, find out if this is the first, second, third or fourth accidental by looking at its coordinate.
        // EXAMPLES:
        // First accidental: (19 - 17) - 1 = 1. This note stays in the same place unless it gets shifted by the mod 2 shifting rule further below.
        // Second accidental: (19 - 16) - 1 = 2. This note gets positioned 2 units before the first accidental.
        // Third accidental: (19 - 15) - 1 = 3. This note gets positioned 4 units before the first accidental.
        final accidentalIndex = (ViewModelConstants.xCoordinateOfNotes - drawable.xCoordinate) - 1;

        // The two innermost accidentals are spaced, the others are tight. The following two lines determine the number of spaced / tight accidentals.
        final int spacedAccidentals = min(maxSpacedAccidentals, accidentalIndex);
        final int tightAccidentals = max(0, accidentalIndex - spacedAccidentals);
        // Now we compute the new position for the current accidental.
        drawable.xCoordinate =
            ViewModelConstants.xCoordinateOfNotes - 2 * spacedAccidentals - tightAccidentals;
      }

      if (!lowerLine) {
        // mod 2 shifting rule:
        // In addition to the spacing, shift every element in the lower line by 1.
        drawable.xCoordinate = drawable.xCoordinate - 1;
      }

      if (drawable.isNote) {
        lowerLine = !lowerLine;
      }
    }

    return drawableInterval;
  }

  /// A drawable snowman is simpler than a drawable chord: It does not carry accidentals. Therefore, it does not depend on the key signature or on the settings for the accidentals.
  static List<DrawableMuwi> createSnowmanDrawables(
    Settings settings,
    KeySignature keySignature,
    Clef clef,
    List<Note> snowman,
    List<Note> chord,
  ) {
    // The return value
    var drawableSnowman = <DrawableMuwi>[];

    final snowmanRawData = mergeNotesAndKeySignature(settings, keySignature, snowman);
    final chordRawData = mergeNotesAndKeySignature(settings, keySignature, chord);

    // See details below.
    final newSnowman = <Pair<Note, List<AccidentalEnum>>>[];

    for (final snowmanItem in snowmanRawData) {
      // Replacing snowmanItem has no effect on the original object in the list.
      // snowmanItem seems to be only a copy of the list item.
      // Therefore, I replace the original list by newSnowman.
      // This does not seem very elegant. If anyone finds a better solution, go ahead!
      var newItem = snowmanItem;
      for (final chordItem in chordRawData) {
        if (chordItem.first == snowmanItem.first) {
          newItem = Pair(snowmanItem.first, []);
        }
      }
      newSnowman.add(newItem);
    }

    drawableSnowman = drawablesFromRawData(newSnowman, clef);
    for (final drawable in drawableSnowman) {
      drawable.xCoordinate = drawable.xCoordinate + ViewModelConstants.snowmanShift;
    }
    return drawableSnowman;
  }

  /// Color the drawables: All roots, thirds and fifths get their respective color.
  static List<DrawableMuwi> coloredDrawables(
    List<DrawableMuwi> drawables,
    List<Note> snowman,
    Clef clef,
  ) {
    // The return value
    final drawableSnowman = <DrawableMuwi>[];

    // Do not color the accidentals, only the notes.
    for (final drawable in drawables) {
      if (drawable.isNote) {
        if (Position.positionOfLine(drawable.yCoordinate, clef).letter == snowman[0].letter) {
          // Root notes:
          drawableSnowman.add(
            DrawableMuwi(
              drawable.xCoordinate,
              drawable.yCoordinate,
              drawable.scalingFactor,
              drawable.verticalOffset,
              ViewModelConstants.whiteNotePath,
              colorCode: ColorCode.rootNote,
              isNote: true,
            ),
          );
        } else if (Position.positionOfLine(drawable.yCoordinate, clef).letter ==
            snowman[1].letter) {
          // Third notes:
          drawableSnowman.add(
            DrawableMuwi(
              drawable.xCoordinate,
              drawable.yCoordinate,
              drawable.scalingFactor,
              drawable.verticalOffset,
              ViewModelConstants.whiteNotePath,
              colorCode: ColorCode.thirdNote,
              isNote: true,
            ),
          );
        } else {
          // Fifth notes:
          drawableSnowman.add(
            DrawableMuwi(
              drawable.xCoordinate,
              drawable.yCoordinate,
              drawable.scalingFactor,
              drawable.verticalOffset,
              ViewModelConstants.whiteNotePath,
              colorCode: ColorCode.fifthNote,
              isNote: true,
            ),
          );
        }
      } else {
        drawableSnowman.add(drawable);
      }
    }

    return drawableSnowman;
  }
}
