import 'dart:math';

import 'package:analyzer_plugin/utilities/pair.dart';
import 'package:muwi/constants/viewmodel_constants.dart';
import 'package:muwi/interfaces/i_task_provider.dart';
import 'package:muwi/model/accidental.dart';
import 'package:muwi/model/chord.dart';
import 'package:muwi/model/chord_quality.dart';
import 'package:muwi/model/displayable_task.dart';
import 'package:muwi/model/note.dart';
import 'package:muwi/model/note_class.dart';
import 'package:muwi/model/note_letter.dart';
import 'package:muwi/model/settings/triad_settings.dart';
import 'package:muwi/model/triad_task.dart';
import 'package:muwi/services/triad_arrangement_service.dart';
import 'package:muwi/view_model/custom_random.dart';
import 'package:muwi/view_model/position.dart';

class TriadProvider implements ITaskProvider {
  factory TriadProvider() => _privateInstance;

  TriadProvider._internal();

  // Providing a singleton instance of the TriadProvider.
  static final TriadProvider _privateInstance = TriadProvider._internal();

  Random random = Random();
  CustomRandom<int> customRand = CustomRandom<int>();

  bool debug = false;

  // Method generating a Random Triad.
  @override
  DisplayableTask generateTask({
    required covariant TriadSettings settings,
    required Position upperBound,
    required Position lowerBound,
    Map<NoteClass, ChordQuality>? patternToChoseFrom,
  }) {
    // ------------------- Generate parameters for the root note

    // This will be the letter of the root note:
    NoteLetter letter;
    // We will pick random indices from the enum NoteLetter:
    final maxLetter = NoteLetter.values.length;

    // Subtract four from highest displayable note to get the highest admissible root.
    //      --- max, highest ledger line.
    //          max - 1
    //      --- max - 2
    //          max - 3
    //      --- max - 4, first ledger line, highest possible position for the root.
    final maxRoot = upperBound - 4;
    final maxOctave = maxRoot.octave;

    // EXAMPLE:
    // highestPossibleOctave: 6
    // lowestPossibleOctave: 3
    // Difference 6 - 3 = 3
    // random.nextInt(max) does not include the maximum value, therefore we have to add 1.
    // 3 + random.nextInt(3+1) will generate a number within {3,4,5,6}.
    final octave = lowerBound.octave + random.nextInt(maxOctave - lowerBound.octave + 1);

    // Restrict range for the lowest and highest octave.
    if (octave == lowerBound.octave) {
      // LOWEST OCTAVE
      final minLetter = lowerBound.letter.index;
      // EXAMPLE:
      // octave: 3
      // lowerBound: f3
      // Index of f: 3
      // maxLetter (length of enum): 7
      // difference: 3
      // 3 + random.nextInt(3+4) will generate an index within {3,4,5,6}
      letter = NoteLetter.values[minLetter + random.nextInt(maxLetter - minLetter)];
    } else if (octave == maxOctave) {
      // HIGHEST OCTAVE:
      // If e6 is the highest displayable note:
      // maxRoot returns a5 with index 5.
      // nextInt does not include the last value, therefore we have to add 1 to include the highest possible index.
      letter = NoteLetter.values[random.nextInt(maxRoot.letter.index + 1)];
    } else {
      // Recall that maxOctave is the octave of the highest possible root note, not of the highest possible note.
      // Therefore, we are free to choose any letter here.
      letter = NoteLetter.values[random.nextInt(maxLetter)];
    }

    Chord newTriad;

    // Shortcut when the choice of base notes is restricted by the level.
    if (patternToChoseFrom != null) {
      final chosenPattern =
          patternToChoseFrom.entries.toList()[Random().nextInt(patternToChoseFrom.length)];
      var root = Note(chosenPattern.key, octave);
      // Edge case 1: Letter outside the displayable range within the highest octave.
      if (Position(root.letter, root.octave) > maxRoot) {
        root = Note(chosenPattern.key, octave - 1);
      }
      // Edge case 2: Letter outside the displayable range within the lowest octave.
      if (Position(root.letter, root.octave) < lowerBound) {
        root = Note(chosenPattern.key, octave + 1);
      }
      newTriad = Chord(baseNote: root, quality: patternToChoseFrom[chosenPattern.key]!);
    } else {
      // Build root note
      /// Choose accidental. A sharp, E sharp and B sharp.
      Accidental accidental;

      ///  Exclude F flat, C flat.
      if (letter == NoteLetter.f || letter == NoteLetter.c) {
        accidental = Accidental(
          // Recall that sharps are positive integers. Only sharp or none allowed here.
          // nextInt excludes maximum value.
          random.nextInt(ViewModelConstants.maxSharps) + 1,
        );

        /// Exclude A sharp, E sharp and H sharp.
      } else if (letter == NoteLetter.a || letter == NoteLetter.e || letter == NoteLetter.b) {
        accidental = Accidental(
          // Recall that flats are negative integers. Only flat or none allowed here.
          ViewModelConstants.maxFlats + random.nextInt(ViewModelConstants.maxFlats.abs() + 1),
        );
      } else {
        accidental = Accidental(
          ViewModelConstants.maxFlats +
              random.nextInt(ViewModelConstants.maxSharps - ViewModelConstants.maxFlats + 1),
        );
      }
      final noteClass = NoteClass(letter, accidental);
      // If the constructor was provided with a root letter, take that one.
      // Otherwise, use the calculated one.
      final root = Note(noteClass, octave);

      /// Generate chord quality using the frequencies chosen by the user.
      final customRandInt = customRand.frequencies([
        Pair(ChordQuality.major.index, settings.majorFrequency),
        Pair(ChordQuality.minor.index, settings.minorFrequency),
        Pair(ChordQuality.augmented.index, settings.augmentedFrequency),
        Pair(ChordQuality.diminished.index, settings.diminishedFrequency),
      ]);
      final quality = ChordQuality.values[customRandInt];

      /// Build and return random triad
      newTriad = Chord(baseNote: root, quality: quality);
    }

    /// Create an arrangement for the triad. The default is a snowman (the root position of the triad).
    /// TriadArrangement contains the method rearrange() which creates a random arrangement of 2-6 notes of the triad within the displayable range.
    final triadArrangementService = TriadArrangementService(
      rootPosition: newTriad,
      upperBound: upperBound,
      lowerBound: lowerBound,
      snowmenOnly: settings.snowmenOnly,
    )..rearrange();

    // TODO: Restructure TriadArrangementService
    final triadArrangement = TriadTask(
      bass: triadArrangementService.bass,
      letter: triadArrangementService.rootPosition.baseNote.letter,
      accidental: triadArrangementService.rootPosition.baseNote.accidental,
      quality: triadArrangementService.rootPosition.quality,
    );

    return DisplayableTask(
      task: triadArrangement,
      listOfNotes: triadArrangementService.choiceOfNotes,
    );
  }
}
