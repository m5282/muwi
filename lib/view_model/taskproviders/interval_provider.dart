import 'dart:math';

import 'package:muwi/constants/viewmodel_constants.dart';
import 'package:muwi/enums/interval_enum.dart';
import 'package:muwi/enums/interval_quality.dart';
import 'package:muwi/interfaces/i_task_provider.dart';
import 'package:muwi/model/accidental.dart';
import 'package:muwi/model/chord_quality.dart';
import 'package:muwi/model/displayable_task.dart';
import 'package:muwi/model/interval.dart';
import 'package:muwi/model/note.dart';
import 'package:muwi/model/note_class.dart';
import 'package:muwi/model/note_letter.dart';
import 'package:muwi/model/settings/interval_settings.dart';
import 'package:muwi/view_model/custom_random.dart';
import 'package:muwi/view_model/position.dart';

/// This service can generate valid random intervals with the method nextInterval() and choose suitable notes within a given range for a given interval with the method chooseNotesInRange().
class IntervalProvider implements ITaskProvider {
  Random random = Random();
  CustomRandom<int> customRandom = CustomRandom();

  /// This method creates a random valid interval.
  @override
  DisplayableTask generateTask({
    required covariant IntervalSettings settings,
    required Position upperBound,
    required Position lowerBound,
    Map<NoteClass, ChordQuality>? patternToChoseFrom,
  }) {
    final genericInterval = IntervalEnum.values[random.nextInt(IntervalEnum.values.length)];
    final allowedQualities = genericInterval.getAllowedQualities();

    // In "Imperfect" minor or major intervals such as seconds, thirds or sevenths, the upper note can have up to two accidentals if the base note carries an accidental.
    // If we allow augmented and diminished imperfect intervals, these might carry up to three accidentals in addition to a natural.
    // We do not want such intervals by default, but students can opt them in.
    if (settings.maxTwoAccidentals && !genericInterval.isPerfect()) {
      allowedQualities
        ..remove(IntervalQuality.diminished)
        ..remove(IntervalQuality.augmented);
    }

    final quality = allowedQualities[random.nextInt(allowedQualities.length)];
    final concreteInterval = Interval.forQuality(genericInterval, quality);
    return DisplayableTask(
      task: concreteInterval,
      listOfNotes: chooseNotesInRange(upperBound, lowerBound, concreteInterval, settings),
    );
  }

  /// This method chooses two suitable notes for a given interval within a given range.
  List<Note> chooseNotesInRange(
    Position upperbound,
    Position lowerBound,
    Interval interval,
    IntervalSettings settings,
  ) {
    // EXAMPLE:
    // highestPossiblePosition: e6
    // Interval: third, diatonic steps 2
    // e6 - 2 = c6
    final highestPossibleBaseNote = upperbound - interval.diatonicSteps;
    final highestPossibleOctave = highestPossibleBaseNote.octave;
    final lowestPossibleOctave = lowerBound.octave;

    // EXAMPLE:
    // highestPossibleOctave: 6
    // lowestPossibleOctave: 3
    // Difference 6 - 3 = 3
    // random.nextInt(max) does not include the maximum value, therefore we have to add 1.
    // 3 + random.nextInt(3+1) will generate a number within {3,4,5,6}.
    final octave =
        lowestPossibleOctave + random.nextInt(highestPossibleOctave - lowestPossibleOctave + 1);
    NoteLetter baseNoteLetter;
    if (octave == lowestPossibleOctave) {
      // EXAMPLE:
      // octave: 3
      // lowerBound: f3
      // Index of f: 3
      // Length of NoteLetter.values: 7
      // difference: 4
      // 3 + random.nextInt(4) will generate an index within {3,4,5,6}
      final minLetterIndex = lowerBound.letter.index;
      baseNoteLetter = NoteLetter
          .values[minLetterIndex + random.nextInt(NoteLetter.values.length - minLetterIndex)];
    } else if (octave == highestPossibleOctave) {
      // EXAMPLE:
      // octave: 6
      // highestPossibleBaseNote: c6
      // Index of c: 0
      // random.nextInt(0+1) can only generate 0.
      final maxLetterIndex = highestPossibleBaseNote.letter.index;
      baseNoteLetter = NoteLetter.values[random.nextInt(maxLetterIndex + 1)];
    } else {
      baseNoteLetter = NoteLetter.values[random.nextInt(NoteLetter.values.length)];
    }

    Accidental accidental;

    // Some root notes have to be excluded if we want to exclude scenarios with more than 3 accidentals.
    if (settings.maxTwoAccidentals && baseNoteLetter == NoteLetter.b) {
      accidental = Accidental(
        // Recall that flats are negative integers. Only flat or none allowed here.
        ViewModelConstants.maxFlats + random.nextInt(ViewModelConstants.maxFlats.abs() + 1),
      );
    } else {
      // EXAMPLE:
      // maxFlats = -1, maxSharps = 1
      // -1 + random.nextInt(2 + 1) will generate a value between -1 and (including) 1.
      accidental = Accidental(
        ViewModelConstants.maxFlats +
            random.nextInt(ViewModelConstants.maxSharps - ViewModelConstants.maxFlats + 1),
      );
    }

    final baseNote = Note(NoteClass(baseNoteLetter, accidental), octave);
    final secondNote = baseNote + interval;
    return [baseNote, secondNote];
  }
}
