import 'package:flutter/foundation.dart';
import 'package:muwi/model/clef.dart';
import 'package:muwi/model/note.dart';
import 'package:muwi/model/note_letter.dart';

/// This class renders the position of a note in the quizview.
/// Index 0 represents the middle line in both Clefs,
/// positive numbers stand for notes in the upper part,
/// negative numbers for notes in the lower part.
/// The distance between two lines is composed of two steps of 1.
///
///     -------------------  <-- 4
///                          <-- 3
///     -------------------  <-- 2
///                          <-- 1
///     -------------------  <-- 0
///                          <-- -1
///     -------------------  <-- -2
///                          <-- -3
///     -------------------  <-- -4
@immutable
class Position {
  /// Constructor for a Position.
  const Position(this.letter, this.octave);

  /// Returns the Position at index x with a given clef.
  factory Position.positionOfLine(int givenIndex, Clef clef) {
    final offset = givenIndex - clef.referencePosition;
    final clefPosition = Position(clef.note.letter, clef.note.octave);
    return clefPosition + offset;
  }

  /// Reference note positions for treble and bass.
  static final Position f3 = Position(Clef.bass.note.letter, Clef.bass.note.octave);
  static final Position g4 = Position(Clef.treble.note.letter, Clef.treble.note.octave);

  final int octave;
  final NoteLetter letter;

  /// Returns the display index of a given position for a given clef as used by the view.
  int getDisplayIndex(Clef clef) {
    final distanceFromReference = getOffset(Position(clef.note.letter, clef.note.octave));
    final tempViewIndex = clef.referencePosition + distanceFromReference;
    return tempViewIndex;
  }

  /// Computes the distance between two positions.
  int getOffset(Position other) {
    final scaleSize = NoteLetter.values.length;
    final octaveOffset = (octave - other.octave) * scaleSize;
    final letterOffset = letter.index - other.letter.index;
    final totalOffset = octaveOffset + letterOffset;
    return totalOffset;
  }

  /// Returns the y-coordinate for a note and a given clef.
  static int lineOfNoteWithClef(Note note, Clef clef) {
    return Position(note.letter, note.octave).getDisplayIndex(clef);
  }

  /// Adds an integer value to a position.
  Position operator +(int value) {
    final letterTotal = letter.index + value;
    // Note that the div operator in flutter works as follows: -6 ~/ 7 = 0 and -8 ~/ 7 = 1. This is not wanted here.
    final newOctave = octave + (letterTotal / 7).floor();
    // Example 1: Add 2 to h in octave 3.
    // The difference is 2, so we end up at index 8. If we floor this value divided by 7, we get 1. This is the number of octaves we want to add.
    // Example 2: Add 2 to f in octave 3.
    // The difference is 2, so we end up at index 5. If we floor this value divided by 7, we get 0. This is the number of octaves we want to add.
    final newLetter = NoteLetter.values[letterTotal % 7];
    return Position(newLetter, newOctave);
  }

  /// Subtracts an integer value from a position.
  /// Simply for convenience, since pos1 + (-x) returns the same result.
  Position operator -(int value) {
    // Note that the div operator in flutter works as follows: -6 ~/ 7 = 0 and -8 ~/ 7 = 1. This is not wanted here.
    final letterTotal = letter.index - value;
    // Example 1: Subtract 2 from d in octave 3.
    // The difference is 2, so we end up at index -1. If we floor this value divided by 7, we get -1. This is the number of octaves we want to subtract.
    // Example 2: Subtract 2 from f in octave 3.
    // The difference is 2, so we end up at index 1. If we floor this value divided by 7, we get 0. This is the number of octaves we want to subtract.
    final newOctave = octave + (letterTotal / 7).floor();
    final newLetter = NoteLetter.values[letterTotal % 7];
    return Position(newLetter, newOctave);
  }

  // --------------------- Comparison operators for positions.

  /// This position is lower than another position if the octave of this position is lower. If both are in the same octave, this position is lower if its letter has a lower index.
  bool operator <(Position other) {
    if (octave < other.octave) {
      return true;
    } else if (octave == other.octave) {
      if (letter.index < other.letter.index) {
        return true;
      } else {
        return false;
      }
    }
    return false;
  }

  /// This position is higher than the other position if the octave of this position is higher. If both are in the same octave, this position is higher if its letter has a higher index.
  bool operator >(Position other) {
    if (octave > other.octave) {
      return true;
    } else if (octave == other.octave) {
      if (letter.index > other.letter.index) {
        return true;
      } else {
        return false;
      }
    }
    return false;
  }

  /// This position is lower than or equal to the other position if the octave of this position is lower than or equal to the octave of the other position. If both are in the same octave, this position <= the other position if its letter has a lower index or if both letters are identical.
  bool operator <=(Position other) {
    return this < other || this == other;
  }

  /// This position is higher than or equal to the other position if the octave of this position is higher than or equal to the octave of the other position. If both are in the same octave, this position >= the other position if its letter has a higher index or if both letters are identical.
  bool operator >=(Position other) {
    return this > other || this == other;
  }

  /// This position is equal to the other position if both letters and octaves are identical.
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Position &&
          runtimeType == other.runtimeType &&
          octave == other.octave &&
          letter == other.letter;

  @override
  int get hashCode => octave.hashCode ^ letter.hashCode;
}
