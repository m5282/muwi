import 'dart:math';

import 'package:muwi/model/accidental.dart';
import 'package:muwi/model/clef.dart';
import 'package:muwi/model/key_signature.dart';
import 'package:muwi/model/mode.dart';
import 'package:muwi/model/note_class.dart';

/// This class generates certain random parameters for the ViewModel.
/// Outsourcing these functionalities make it easier to test the ViewModel.
class ParamGenerator {
  /// For debugging purposes
  static bool insane = false;

  /// This is the choice of root notes for major scales. Needed for the key signature generation.
  static const allowedBaseNotesForMajorScales = [
    // No accidental
    NoteClass.c, // none

    // One accidental
    NoteClass.g, // sharp
    NoteClass.f, // flat

    // Two accidentals
    NoteClass.d, // sharp
    NoteClass.bFlat, // flat

    // Three accidentals
    NoteClass.a, // sharp
    NoteClass.eFlat, // flat

    // Four accidentals
    NoteClass.e, // sharp
    NoteClass.aFlat, // flat

    // Five accidentals
    NoteClass.b, // sharp
    NoteClass.dFlat, // flat

    // Six accidentals
    NoteClass.fSharp, // sharp
    NoteClass.gFlat, // flat
  ];

  /// This is the choice of root notes for major scales. Needed for the key signature generation.
  static const allowedBaseNotesForMinorScales = [
    // No accidental
    NoteClass.a, // none

    // One Accidental
    NoteClass.e, // sharp
    NoteClass.d, // flat

    // Two accidentals
    NoteClass.b, // sharp
    NoteClass.g, // flat

    // Three accidentals
    NoteClass.fSharp, // sharp
    NoteClass.c, // flat

    // Four accidentals
    NoteClass.cSharp, // sharp
    NoteClass.f, // flat

    // Five accidentals
    NoteClass.gSharp, // sharp
    NoteClass.bFlat, // flat

    // Six accidentals
    NoteClass.dSharp, // sharp
    NoteClass.eFlat, // flat
  ];

  static const modes = [Mode.major, Mode.minor];

  /// Randomly generates a new clef for the top display.
  static Clef genClef(List<Clef> possibleClefs) {
    final clefIndex = Random().nextInt(possibleClefs.length);
    return possibleClefs[clefIndex];
  }

  /// return KeySignature customised by the user.
  static KeySignature customKeySignature(int maxAccidentals) {
    assert(maxAccidentals <= 6 && maxAccidentals >= 0, 'Max accidentals should be between 0 and 6, but was $maxAccidentals');

    /// Generate mode (major or minor).
    final randomMode = modes[Random().nextInt(modes.length)];

    /// The list we are allowed to choose from.
    var allowedBaseNotes = <NoteClass>[];

    /// Generate base note. Not all notes are allowed.
    if (randomMode == Mode.major) {
      allowedBaseNotes = allowedBaseNotesForMajorScales;
    } else {
      allowedBaseNotes = allowedBaseNotesForMinorScales;
    }

    final randomKey = allowedBaseNotes[Random().nextInt(1 + maxAccidentals * 2)];
    if (insane) {
      return KeySignature(key: NoteClass.bFlat, mode: Mode.minor);
    } else {
      return KeySignature(key: randomKey, mode: randomMode);
    }
  }

  ///
  /// Allowed Combinations:
  /// numAccidentials: 0, accidential: Accidential.none
  /// numAccidentials 1, accidential: Accidential.flat / Accidential.sharp
  /// ...
  /// numAccidentials 6, accidentials: Accidential.flat or Accidentials.sharp
  static KeySignature oneKeySignature(
    int numAccidentals,
    Accidental accidental,
  ) {
    if (accidental == Accidental.sharp) {
      if (numAccidentals == 1) {
        // 0 or 1?
        final rand = Random().nextInt(2);
        if (rand == 0) {
          return KeySignature(key: NoteClass.g, mode: Mode.major);
        } else {
          return KeySignature(key: NoteClass.e, mode: Mode.minor);
        }
      }

      if (numAccidentals == 2) {
        // 0 or 1?
        final rand0 = Random().nextInt(2);
        if (rand0 == 0) {
          return KeySignature(key: NoteClass.d, mode: Mode.major);
        } else {
          return KeySignature(key: NoteClass.b, mode: Mode.minor);
        }
      }

      if (numAccidentals == 3) {
        // 0 or 1?
        final rand1 = Random().nextInt(2);
        if (rand1 == 0) {
          return KeySignature(key: NoteClass.a, mode: Mode.major);
        } else {
          return KeySignature(key: NoteClass.fSharp, mode: Mode.minor);
        }
      }
      if (numAccidentals == 4) {
        // 0 or 1?
        final rand2 = Random().nextInt(2);
        if (rand2 == 0) {
          return KeySignature(key: NoteClass.e, mode: Mode.major);
        } else {
          return KeySignature(key: NoteClass.cSharp, mode: Mode.minor);
        }
      }
      if (numAccidentals == 5) {
        // 0 or 1?
        final rand3 = Random().nextInt(2);
        if (rand3 == 0) {
          return KeySignature(key: NoteClass.b, mode: Mode.major);
        } else {
          return KeySignature(key: NoteClass.gSharp, mode: Mode.minor);
        }
      }
      if (numAccidentals == 6) {
        // 0 or 1?
        final rand4 = Random().nextInt(2);
        if (rand4 == 0) {
          return KeySignature(key: NoteClass.fSharp, mode: Mode.major);
        } else {
          return KeySignature(key: NoteClass.dSharp, mode: Mode.minor);
        }
      }
    } else if (accidental == Accidental.flat) {
      if (numAccidentals == 1) {
        final rand5 = Random().nextInt(2);
        if (rand5 == 0) {
          return KeySignature(key: NoteClass.f, mode: Mode.major);
        } else {
          return KeySignature(key: NoteClass.d, mode: Mode.minor);
        }
      }

      if (numAccidentals == 2) {
        final rand6 = Random().nextInt(2);
        if (rand6 == 0) {
          return KeySignature(key: NoteClass.bFlat, mode: Mode.major);
        } else {
          return KeySignature(key: NoteClass.g, mode: Mode.minor);
        }
      }
      if (numAccidentals == 3) {
        final rand7 = Random().nextInt(2);
        if (rand7 == 0) {
          return KeySignature(key: NoteClass.eFlat, mode: Mode.major);
        } else {
          return KeySignature(key: NoteClass.c, mode: Mode.minor);
        }
      }

      if (numAccidentals == 4) {
        final rand8 = Random().nextInt(2);
        if (rand8 == 0) {
          return KeySignature(key: NoteClass.aFlat, mode: Mode.major);
        } else {
          return KeySignature(key: NoteClass.f, mode: Mode.minor);
        }
      }

      if (numAccidentals == 5) {
        final rand9 = Random().nextInt(2);
        if (rand9 == 0) {
          return KeySignature(key: NoteClass.dFlat, mode: Mode.major);
        } else {
          return KeySignature(key: NoteClass.bFlat, mode: Mode.minor);
        }
      }

      if (numAccidentals == 6) {
        final rand10 = Random().nextInt(2);
        if (rand10 == 0) {
          return KeySignature(key: NoteClass.gFlat, mode: Mode.major);
        } else {
          return KeySignature(key: NoteClass.eFlat, mode: Mode.minor);
        }
      }
    } else {
      assert(accidental == Accidental.none,'oneKeySignature() here: Accidental is none.');
      final rand11 = Random().nextInt(2);
      if (rand11 == 0) {
        return KeySignature(key: NoteClass.c, mode: Mode.major);
      } else {
        return KeySignature(key: NoteClass.a, mode: Mode.minor);
      }
    }
    throw Exception('numAccidentials was $numAccidentals but should be between 0 and 6');
  }
}
