import 'dart:collection';

import 'package:analyzer_plugin/utilities/pair.dart';

class RealCoordinates {
  /// The height reserved for display.
  double displayHeight = 0;

  /// The usable area does not include the space reserved for the toolbar.
  double keyboardWithResultBar = 0;

  /// This map contains the vertical positions in the view.
  HashMap<Pair<int, int>, double> verticalPositions = HashMap<Pair<int, int>, double>();

  /// This map contains the horizontal positions in the view.
  HashMap<int, double> horizontalPositions = HashMap();

// ------------------------------- The staff display

  /// The width of a regular line is a fraction of the screenSize, adjusted in ViewConstants.
  double widthOfRegularLine = 0;

  /// This variable will change if we want to draw ledger lines.
  double widthOfLedgerLine = 0;

  /// We need this variable for centering the note on the ledger line. The width of a note 1.4 is times its height. The height extends over two y-coordinates.
  double widthOfANote = 0;

  /// The height of a note.
  double heightOfANote = 0;

  /// Vertical space between single lines:
  double spaceBetweenTwoYCoordinates = 0;

  /// The height of the brace
  double heightOfBrace = 0;

  /// The width of the brace
  double widthOfBrace = 0;

  /// The bottom of the brace;
  double bottomOfBrace = 0;

  /// This is where the brace starts, seen from the left.
  double leftBeginOfBrace = 0;

  /// This is the vertical position of the middle line of one staff without the offset added for the lower display.
  double verticalCenterOfStaff = 0;

// ----------------------- The keyboard

  /// The height of the keyboard including the result bar.
  double widthOfKeyboardWithoutMargins = 0;
  double leftRightKeyboardMargin = 0;
  double bottomTopKeyboardMargin = 0;
  double keyboardWidth = 0;
  double resultBarHeight = 0;
  double keyboardHeight = 0;

  /// The sizes of the buttons.
  double smallButtonWidth = 0;
  double intervalButtonWidth = 0;
  double mediumSizedButtonWidth = 0;
  double doubledSmallButtonWidth = 0;
  double doubledMediumSizedButtonWidth = 0;
  double buttonHeight = 0;
}
