import 'dart:math';

import 'package:analyzer_plugin/utilities/pair.dart';
import 'package:muwi/constants/viewmodel_constants.dart';
import 'package:muwi/interfaces/i_drawable.dart';
import 'package:muwi/interfaces/i_task_provider.dart';
import 'package:muwi/model/clef.dart';
import 'package:muwi/model/displayable_task.dart';
import 'package:muwi/model/key_signature.dart';
import 'package:muwi/model/note.dart';
import 'package:muwi/model/settings/settings.dart';
import 'package:muwi/view_model/drawable_creator.dart';
import 'package:muwi/view_model/drawable_muwi.dart';
import 'package:muwi/view_model/keyboard_vm/keyboard_vm.dart';
import 'package:muwi/view_model/param_generator.dart';
import 'package:muwi/view_model/position.dart';

/// This class manages all the data needed for the task generation, task display and user input evaluation.
class QuizPageViewModel {
  QuizPageViewModel({
    required this.settings,
    required this.taskProvider,
    required this.keyboardVM,
  }) {
    settings.saveButtonText = settings.saveChanges;
    nextChord();
  }

  /// This ViewModel manages the state of the keyboard: User input, evaluation, etc.
  KeyboardVM keyboardVM;

  /// This service generates the task (currently either triads or intervals).
  ITaskProvider taskProvider;

  //------------------------- Environment: Ranges, Clefs, key signature, etc.

  /// This is the highest possible Position (letter, octave) for a note in the current setting. Its value depends on both clefs.
  Position? upperBound;

  /// This is the lowest possible Position (letter, octave) for a note in the current setting. Its value depends on both clefs.
  Position? lowerBound;

  /// The clef assigned to the top staff.
  Clef? upperClef;

  /// The clef assigned to the bottom staff.
  Clef? lowerClef;

  /// The KeySignature for both displays.
  KeySignature? keySignature;

  /// The coordinate of the lower Clef. Should be -6 for treble ans -3 for bass.
  Pair<Clef, int>? lowerClefCoordinate;

  /// The coordinate of the lower Clef. Should be -6 for treble ans -3 for bass.
  Pair<Clef, int>? upperClefCoordinate;

  /// The list of Drawables for the upper display.
  List<DrawableMuwi> upperDrawables = [];

  /// The list of Drawables for the lower display.
  List<DrawableMuwi> lowerDrawables = [];

  /// The notes in the upper display
  List<Note> upperDisplayNotes = [];

  /// The notes in the lower display
  List<Note> lowerDisplayNotes = [];

  /// The coordinates for the ledger lines in the upper display.
  List<Pair<int, int>> upperDisplayLedgerLines = [];

  /// The coordinates for the ledger lines in the lower display.
  List<Pair<int, int>> lowerDisplayLedgerLines = [];

  /// The notes to be displayed.
  List<Note> displayedNotes = [];

  /// The highest y-coordinate of a display. The value depends on the number of ledger lines.
  int highestIndex = 0;

  /// The lowest y-coordinate of a display. The value depends on the number of ledger lines.
  int lowestIndex = 0;

  //--------------------------------- Settings:

  /// In each one of the individual modes, this will be some class extending the general Settings class.
  Settings settings;

  //--------------------------------- Statistics:
  // TODO: Create some class with constructor Statistics(String modeName)?
  // /// The data on the learning progress of the user. This object will be replaced by an object retrieved from sharedPreferences in most cases. When this is not possible, e.g. in the beginning, this fresh Statistics object will be used instead.
  // Statistics statistics = Statistics();
  //
  // /// Set this flag to collect and display data on the learning progress.
  // bool collectStatistics = false;
  //
  // /// Method to load the current Statistics.
  // loadStatistics() async {
  //   try {
  //     Statistics maybeStatistics =
  //     Statistics.fromJson(await TonfallSharedPreferences.getStatistics("${modeName}statistics"));
  //     statistics = maybeStatistics;
  //   } on Exception {
  //     // This happens at least once, since there is no Statistics object stored in sharedPreferences when the App is loaded for the first time. In this case, the fresh Statistics object initialised above is used.
  //   }
  // }
  //
  // /// This method just delegates the update mechanism to the instance of the statistics.
  // updateStatistics(bool result) {
  //   statistics.updateStatistics(result);
  //   TonfallSharedPreferences.setStatistics("${modeName}statistics", statistics);
  // }

  ///////////// CURRENT TASK

  /// The task itself: Some triad or interval. A DisplayableTask contains all the data the user needs to provide plus the exact notes to be displayed.
  DisplayableTask? currentDisplayableTask;

  /////////////// NEXTCHORD /////////////

  /// This method computes a new random task using the current settings.
  void nextChord() {
    // // Load the current statistics:
    // if (collectStatistics) {
    //   loadStatistics();
    // }

    // Reset flags:
    keyboardVM
      ..finished = false

      // Wipe previous selections.
      ..unselectAll();

    // Clear lists of notes.
    upperDisplayNotes.clear();
    lowerDisplayNotes.clear();

    // Generate new clefs.
    final possibleClefs =
        settings.clefPreferences.keys.where((clef) => settings.clefPreferences[clef]!).toList();
    upperClef = settings.upperClef ?? ParamGenerator.genClef(possibleClefs);
    lowerClef = settings.lowerClef ?? ParamGenerator.genClef(possibleClefs);
    if (settings.standardClefArrangement!) {
      // If the user wanted a standard clef arrangement and the lower clef turns out to be the clef for the higher pitches, swap clefs.
      if (Position.positionOfLine(0, upperClef!) < Position.positionOfLine(0, lowerClef!)) {
        final temp = upperClef!;
        upperClef = lowerClef;
        lowerClef = temp;
      }
    }

    // Set the coordinates as used by the view.
    lowerClefCoordinate = setClefCoordinates(lowerClef!);
    upperClefCoordinate = setClefCoordinates(upperClef!);

    // Set the max indices of the notes according to the settings.
    setIndices();
    // Adapt range to new clefs.
    setRange();

    // Generate basic triad within displayable range.
    currentDisplayableTask = taskProvider.generateTask(
      settings: settings,
      upperBound: upperBound!,
      lowerBound: lowerBound!,
      patternToChoseFrom: settings.patternToChoseFrom,
    );

    // Extract the task containing only the data the user needs to know. This will be used for the evaluation of the user input. Therefore, it is passed on to the keyboardVM.
    keyboardVM.currentTask = currentDisplayableTask!.task;

    // Extract the notes to be displayed. These are only relevant for the display, not for the evaluation.
    displayedNotes = currentDisplayableTask!.listOfNotes!;

    // Generate KeySignature
    if (settings.oneKeySignature) {
      keySignature =
          ParamGenerator.oneKeySignature(settings.numKeySignatureAccidentals, settings.accidental);
    } else {
      keySignature = ParamGenerator.customKeySignature(settings.maxKeySignatureAccidentals);
    }

    // Assign the notes to the upper or lower display, depending on the ranges of the clefs.
    assignToDisplays();

    // Create Drawables for from KeySignature
    upperDrawables = DrawableCreator.createKeySignatureDrawables(keySignature!, upperClef!);
    lowerDrawables = DrawableCreator.createKeySignatureDrawables(keySignature!, lowerClef!);

    // Add Drawables for notes and their accidentals
    upperDrawables.addAll(
      DrawableCreator.createChordDrawables(
        settings,
        keySignature!,
        upperClef!,
        upperDisplayNotes,
      ),
    );
    lowerDrawables.addAll(
      DrawableCreator.createChordDrawables(
        settings,
        keySignature!,
        lowerClef!,
        lowerDisplayNotes,
      ),
    );

    // Set the coordinates of the ledger lines.
    lowerDisplayLedgerLines = getLedgerLinePositions(lowerDrawables);
    upperDisplayLedgerLines = getLedgerLinePositions(upperDrawables);
  }

  //////////////// ADAPTING RANGES, PLACING NOTES ////////////////

  /// Retrieve the coordinates for the ledger lines.
  List<Pair<int, int>> getLedgerLinePositions(List<DrawableMuwi> drawables) {
    final coordinates = <Pair<int, int>>{};

    for (final IDrawable drawable in drawables) {
      if (drawable.isNote) {
        final yCoordinate = drawable.yCoordinate;

        // Ledger line below the regular lines?
        if (yCoordinate < ViewModelConstants.lowestRegularLineIndex) {
          // Set pointer to highest ledger line
          var current = ViewModelConstants.lowestRegularLineIndex - 2;
          // Fill up ledger lines below the note.
          while (current >= yCoordinate) {
            coordinates.add(Pair(drawable.xCoordinate, current));
            current -= 2;
          }
        }

        // Ledger line above the regular lines?
        if (yCoordinate > ViewModelConstants.highestRegularLineIndex) {
          // Set pointer to lowest ledger line
          var current = ViewModelConstants.highestRegularLineIndex + 2;
          // Fill up ledger lines above the note.
          while (current <= yCoordinate) {
            coordinates.add(Pair(drawable.xCoordinate, current));
            current += 2;
          }
        }
      }
    }
    return coordinates.toList();
  }

  /// Set the needed indices, because the instance of settings is not accessible outside of a method and it's fault-prone to manually calculate them each time.
  void setIndices() {
    // The minimum and maximum indices depend on the number of ledger lines.
    // The topmost regular line is at index 4, the lowermost regular line at index -4.
    // With the default setting of 3 ledger lines, the extrema are at 10 and -10.
    highestIndex = ViewModelConstants.highestRegularLineIndex + settings.numberOfLedgerLines * 2;
    lowestIndex = ViewModelConstants.lowestRegularLineIndex - settings.numberOfLedgerLines * 2;
  }

  /// The range within which notes can be displayed.
  void setRange() {
    // This gets the Position (letter, octave) for the extrema and the given clefs.
    final lowestPositionOnLowerDisplay = Position.positionOfLine(lowestIndex, lowerClef!);
    final highestPositionOnLowerDisplay = Position.positionOfLine(highestIndex, lowerClef!);
    final lowestPositionOnUpperDisplay = Position.positionOfLine(lowestIndex, upperClef!);
    final highestPositionOnUpperDisplay = Position.positionOfLine(highestIndex, upperClef!);

    // We need these values in order to choose notes within the displayable range.
    upperBound = highestPositionOnLowerDisplay <= highestPositionOnUpperDisplay
        ? highestPositionOnUpperDisplay
        : highestPositionOnLowerDisplay;
    lowerBound = lowestPositionOnLowerDisplay <= lowestPositionOnUpperDisplay
        ? lowestPositionOnLowerDisplay
        : lowestPositionOnUpperDisplay;
  }

  /// Chooses the staff display in which a note appears.
  void assignToDisplays() {
    // If the clef is identical for both displays, notes can be assigned randomly.
    if (lowerClef == upperClef) {
      for (final note in displayedNotes) {
        assignRandomly(note);
      }
      // Otherwise the notes can be distributed randomly only in the overlapping range.
      // TriadProvider and chordRearrangement are responsible for generating only notes which lie in the range of at least one staff.
    } else {
      for (final note in displayedNotes) {
        final position = Position(note.letter, note.octave);
        // If the note is too low-pitched for the top staff, assign it to the bottom staff:
        if (position < lowestPosition(upperClef!)) {
          lowerDisplayNotes.add(note);
          // If the note is too low-pitched for the lower display, put it in the upper display:
        } else if (position < lowestPosition(lowerClef!)) {
          upperDisplayNotes.add(note);
        }
        // If the note is high-pitched for the bottom staff, assign it to the top staff:
        else if (highestPosition(lowerClef!) < position) {
          upperDisplayNotes.add(note);
        }
        // If the note is too high-pitched for the upper display, put it in the lower display:
        else if (highestPosition(upperClef!) < position) {
          lowerDisplayNotes.add(note);
        } else {
          // In this case, the note lies within the overlapping range and can be assigned to any staff.
          assignRandomly(note);
        }
      }
    }
  }

  /// This sets the bottom position of a clef image.
  /// The treble clef starts at the first ledger line (The C-line).
  /// The bass clef starts between the first and the second main line from the bottom up (The A-position).
  Pair<Clef, int> setClefCoordinates(Clef clef) {
    switch (clef) {
      case (Clef.treble):
        return Pair(clef, -6);
      case (Clef.bass):
        return Pair(clef, -3);
      case (Clef.tenor):
        return Pair(clef, -2);
      case (Clef.alto):
        return Pair(clef, -4);
      default:
        return Pair(clef, 0);
    }
  }

  /// Randomly assigns a note to a display if it lies within the overlapping range.
  void assignRandomly(Note note) {
    final upper = Random().nextBool();
    if (upper) {
      upperDisplayNotes.add(note);
    } else {
      lowerDisplayNotes.add(note);
    }
  }

  /// Returns the highest possible Position for a given Clef.
  Position highestPosition(Clef clef) {
    return Position.positionOfLine(highestIndex, clef);
  }

  /// Returns the lowest possible Position for a given Clef.
  Position lowestPosition(Clef clef) {
    return Position.positionOfLine(lowestIndex, clef);
  }
}
