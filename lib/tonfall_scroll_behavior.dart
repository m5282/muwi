import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

/// We need this class in order to allow swiping pages in the web app.
class TonfallScrollBehavior extends MaterialScrollBehavior {
  @override
  Set<PointerDeviceKind> get dragDevices => <PointerDeviceKind>{
        PointerDeviceKind.touch,
        PointerDeviceKind.mouse,
      };
}
