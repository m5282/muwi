/// These are the levels of difficulty a user can select from in the free training.
/// For easy middle and hard, he receives badges.
/// For the others not. That's why they are accumulated under others.
enum Difficulty {
  easy,
  medium,
  hard,
  other,
}

extension ToInt on Difficulty {
  int toInt() {
    switch (this) {
      case Difficulty.easy:
        return 1;
      case Difficulty.medium:
        return 2;
      case Difficulty.hard:
        return 3;
      case Difficulty.other:
        return 0;
      // ignore: no_default_cases
      default:
        return -1;
    }
  }
}
