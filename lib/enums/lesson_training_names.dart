enum LessonIDName {
  trebleClef,
  bassClef,
  zeroSharps,
  oneSharp,
  twoSharps,
  threeSharps,
  fourSharps,
  fiveSharps,
  sixSharps,
  oneFlat,
  twoFlats,
  threeFlats,
  fourFlats,
  fiveFlats,
  sixFlats,
  augmentedAndDiminished,
  chordsWithAccidentals1,
  chordsWithAccidentals2,
  tenorClef,
  altoClef,
  allClefs1,
  allClefs2,

  //freies Training Dreiklänge
  freeTriadTrainingEasy,
  freeTriadTrainingMedium,
  freeTriadTrainingHard,
  userAdjustableTriadTraining,

  //freies Training Intervalle
  freeIntervalTrainingEasy,
  freeIntervalTrainingMedium,
  freeIntervalTrainingHard,
  userAdjustableIntervalTraining,
}

extension DisplayableGermanName on LessonIDName {
  String get displayableGermanName {
    switch (this) {
      case LessonIDName.trebleClef:
        return 'G-Schlüssel';
      case LessonIDName.bassClef:
        return 'F-Schlüssel';
      case LessonIDName.zeroSharps:
        return 'C-Dur / a-Moll';
      case LessonIDName.oneSharp:
        return 'G-Dur / e-Moll';
      case LessonIDName.twoSharps:
        return 'D-Dur / h-Moll';
      case LessonIDName.threeSharps:
        return 'A-Dur / fis-Moll';
      case LessonIDName.fourSharps:
        return 'E-Dur / cis-Moll';
      case LessonIDName.fiveSharps:
        return 'H-Dur / gis-Moll';
      case LessonIDName.sixSharps:
        return 'Fis-Dur / dis-Moll';
      case LessonIDName.oneFlat:
        return 'F-Dur / d-Moll';
      case LessonIDName.twoFlats:
        return 'B-Dur / g-Moll';
      case LessonIDName.threeFlats:
        return 'Es-Dur / c-Moll';
      case LessonIDName.fourFlats:
        return 'As-Dur f-Moll';
      case LessonIDName.fiveFlats:
        return 'Des-Dur / b-Moll';
      case LessonIDName.sixFlats:
        return 'Ges-Dur / es-Moll';
      case LessonIDName.augmentedAndDiminished:
        return 'Übermäßig / Vermindert';
      case LessonIDName.chordsWithAccidentals1:
        return 'Klänge mit Akzidentien 1';
      case LessonIDName.chordsWithAccidentals2:
        return 'Klänge mit Akzidentien 2';
      case LessonIDName.tenorClef:
        return 'Tenorschlüssel';
      case LessonIDName.altoClef:
        return 'Altschlüssel';
      case LessonIDName.allClefs1:
        return 'alle Schlüssel 1';
      case LessonIDName.allClefs2:
        return 'alle Schlüssel 2';
      case LessonIDName.freeTriadTrainingEasy:
        return 'Einfach';
      case LessonIDName.freeTriadTrainingMedium:
        return 'Mittel';
      case LessonIDName.freeTriadTrainingHard:
        return 'Schwer';
      case LessonIDName.userAdjustableTriadTraining:
        return 'Feintuning';
      case LessonIDName.freeIntervalTrainingEasy:
        return 'Einfach';
      case LessonIDName.freeIntervalTrainingMedium:
        return 'Mittel';
      case LessonIDName.freeIntervalTrainingHard:
        return 'Schwer';
      case LessonIDName.userAdjustableIntervalTraining:
        return 'Feintuning';
      // ignore: no_default_cases
      default:
        //If we do't hava a translation, show the original name.
        return toString();
    }
  }
}

extension Watchbadge on LessonIDName {
  String get watchbadge {
    return '${toString()}-watchBadge';
  }
}

extension ID on LessonIDName {
  String get id {
    return toString();
  }
}
