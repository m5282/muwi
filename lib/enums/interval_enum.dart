import 'package:muwi/enums/interval_quality.dart';

import 'package:muwi/interfaces/i_answer.dart';

/// This enum stores which qualities each interval is allowed to have.
enum IntervalEnum implements IAnswer {
  unison,
  second,
  third,
  fourth,
  fifth,
  sixth,
  seventh,
  octave,
  ninth,
  tenth;

  /// An element of this enum is also part of the solution, i.e. it is a selectable answer. Therefore, there has to be a button with some label.
  @override
  String get keyLabel {
    return germanAbbreviation;
  }

  static IntervalEnum fromString(String string) {
    for (final interval in IntervalEnum.values) {
      if (string == interval.toString()) {
        return interval;
      }
    }
    throw UnsupportedError('Interval $string not exist');
  }
}

extension IntervalEnumExtensions on IntervalEnum {
  /// Returns whether an interval can be perfect or not.
  bool isPerfect() {
    switch (this) {
      case IntervalEnum.unison:
        return true;
      case IntervalEnum.second:
        return false;
      case IntervalEnum.third:
        return false;
      case IntervalEnum.fourth:
        return true;
      case IntervalEnum.fifth:
        return true;
      case IntervalEnum.sixth:
        return false;
      case IntervalEnum.seventh:
        return false;
      case IntervalEnum.octave:
        return true;
      case IntervalEnum.ninth:
        return false;
      case IntervalEnum.tenth:
        return false;
    }
  }

  /// For each interval, different qualities are allowed.
  /// Returns the allowed qualities.
  List<IntervalQuality> getAllowedQualities() {
    if (isPerfect()) {
      return <IntervalQuality>[
        IntervalQuality.diminished,
        IntervalQuality.augmented,
        IntervalQuality.perfect,
      ];
    }
    return <IntervalQuality>[
      IntervalQuality.diminished,
      IntervalQuality.augmented,
      IntervalQuality.major,
      IntervalQuality.minor,
    ];
  }

  /// Returns the offset for the chosen quality.
  int getOffset(IntervalQuality quality) {
    switch (quality) {
      case IntervalQuality.major:
        if (isPerfect()) {
          throw Exception('Not allowed quality!');
        }
        return 1;
      case IntervalQuality.augmented:
        if (isPerfect()) {
          return 1;
        }
        return 2;
      case IntervalQuality.minor:
        if (isPerfect()) {
          throw Exception('Not allowed quality!');
        }
        return 0;
      case IntervalQuality.diminished:
        return -1;
      case IntervalQuality.perfect:
        if (isPerfect()) {
          return 0;
        }
        throw Exception('Not allowed quality!');
    }
  }

  /// Returns the diatonic steps.
  int get diatonicSteps {
    switch (this) {
      // Perfect intervals:
      case IntervalEnum.unison:
        return 0;
      case IntervalEnum.fourth:
        return 3;
      case IntervalEnum.fifth:
        return 4;
      case IntervalEnum.octave:
        return 7;

      // Imperfect intervals:
      case IntervalEnum.second:
        return 1;
      case IntervalEnum.third:
        return 2;
      case IntervalEnum.sixth:
        return 5;
      case IntervalEnum.seventh:
        return 6;
      case IntervalEnum.ninth:
        return 8;
      case IntervalEnum.tenth:
        return 9;
    }
  }

  /// Returns the semitoneSteps + the offset of the chosen quality.
  int getSemitoneSteps(IntervalQuality quality) {
    switch (this) {
      // Perfect intervals:
      case IntervalEnum.unison:
        return getOffset(quality);
      case IntervalEnum.fourth:
        return 5 + getOffset(quality);
      case IntervalEnum.fifth:
        return 7 + getOffset(quality);
      case IntervalEnum.octave:
        return 12 + getOffset(quality);

      // Imperfect intervals:
      case IntervalEnum.second:
        return 1 + getOffset(quality);
      case IntervalEnum.third:
        return 3 + getOffset(quality);
      case IntervalEnum.sixth:
        return 8 + getOffset(quality);
      case IntervalEnum.seventh:
        return 10 + getOffset(quality);
      case IntervalEnum.ninth:
        return 13 + getOffset(quality);
      case IntervalEnum.tenth:
        return 15 + getOffset(quality);
    }
  }

  /// Returns the german name.
  String get germanName {
    switch (this) {
      // Perfect intervals:
      case IntervalEnum.unison:
        return 'Prime';
      case IntervalEnum.fourth:
        return 'Quarte';
      case IntervalEnum.fifth:
        return 'Quinte';
      case IntervalEnum.octave:
        return 'Oktave';

      // Imperfect intervals:
      case IntervalEnum.second:
        return 'Sekunde';
      case IntervalEnum.third:
        return 'Terz';
      case IntervalEnum.sixth:
        return 'Sexte';
      case IntervalEnum.seventh:
        return 'Septime';
      case IntervalEnum.ninth:
        return 'None';
      case IntervalEnum.tenth:
        return 'Dezime';
    }
  }

  /// Returns the german name.
  String get germanAbbreviation {
    switch (this) {
      // Perfect intervals:
      case IntervalEnum.unison:
        return '1';
      case IntervalEnum.fourth:
        return '4';
      case IntervalEnum.fifth:
        return '5';
      case IntervalEnum.octave:
        return '8';

      // Imperfect intervals:
      case IntervalEnum.second:
        return '2';
      case IntervalEnum.third:
        return '3';
      case IntervalEnum.sixth:
        return '6';
      case IntervalEnum.seventh:
        return '7';
      case IntervalEnum.ninth:
        return '9';
      case IntervalEnum.tenth:
        return '10';
    }
  }
}
