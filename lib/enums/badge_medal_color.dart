enum BadgeMedalColor {
  bronze(1),
  silver(2),
  gold(3);

  const BadgeMedalColor(this.value);
  final num value;

  static BadgeMedalColor getByValue(num i){
    return BadgeMedalColor.values.firstWhere((x) => x.value == i);
  }
}
