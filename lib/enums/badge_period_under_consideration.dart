///This enum makes a technical seperation between the badge for the two-week period
///and the all Time period. This is (at least) necessary for the streak badge.
enum BadgePeriodUnderConsideration {
  //We set the ints here explicit. So we can change the names in future without chaining the "id" of the enums.
  twoWeeks(0),
  allTime(1);

  const BadgePeriodUnderConsideration(this.value);
  final num value;

  static BadgePeriodUnderConsideration getByValue(num i){
    return BadgePeriodUnderConsideration.values.firstWhere((x) => x.value == i);
  }
}
