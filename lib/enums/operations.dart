import 'package:flutter/material.dart';

/// Possible Operations from the user.
enum Operations { next, abort, finish }

extension OperationsExtension on Operations {
  IconData get symbol {
    switch (this) {
      case Operations.abort:
        return Icons.close_outlined;
      case Operations.next:
        return Icons.arrow_right;
      case Operations.finish:
        return Icons.check;
    }
  }

  String get name {
    switch (this) {
      case Operations.abort:
        return '\u{2A09}';
      case Operations.next:
        return '\u{2192}';
      case Operations.finish:
        return 'Fertig';
    }
  }
}
