import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:muwi/view/theme/custom_theme.dart';

/// This class represents all the different states in which a keyboard button can be.
/// The colour of a button depends on the status code.
enum ColorCode {
  neutralResultBar, //
  notSelected, // before evaluation.
  selected, // before evaluation.
  correctAndSelected,
  correctAndNotSelected,
  incorrectAndSelected,
  rootNote,
  thirdNote,
  fifthNote,
  neutralNote
}

extension ColorCodeExtension on ColorCode {
  /// For the moment, this provides the colors for the buttons.
  Color? getColor(BuildContext context) {
    final customColors = Theme.of(context).extension<CustomColors>()!;
    switch (this) {
      case ColorCode.neutralResultBar:
        return customColors.neutralResultBarColor;
      case ColorCode.correctAndSelected:
        return customColors.correctAndSelectedColor;
      case ColorCode.correctAndNotSelected:
        return customColors.correctAndNotSelectedColor;
      case ColorCode.incorrectAndSelected:
        return customColors.incorrectAndSelectedColor;
      case ColorCode.selected:
        return customColors.selectedColor;
      case ColorCode.notSelected:
        return customColors.notSelectedColor;
      case ColorCode.rootNote:
        return customColors.rootNoteColor;
      case ColorCode.thirdNote:
        return customColors.thirdNoteColor;
      case ColorCode.fifthNote:
        return customColors.fifthNoteColor;
      case ColorCode.neutralNote:
        return customColors.neutralNoteColor;
    }
  }

  /// Providing the symbol of the evaluation:
  IconData getSymbol(BuildContext context) {
    switch (this) {
      case ColorCode.correctAndSelected:
        return CupertinoIcons.checkmark;
      case ColorCode.correctAndNotSelected:
        return CupertinoIcons.exclamationmark;
      case ColorCode.incorrectAndSelected:
        return CupertinoIcons.xmark;
      // The default icon is a question mark.
      // ignore: no_default_cases
      default:
        return CupertinoIcons.question;
    }
  }
}
