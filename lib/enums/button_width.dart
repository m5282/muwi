enum ButtonWidth {
  /// Size of a note button (7 in one row)
  small,

  /// Size of #, b, 3, 5
  medium,

  /// Size of 1, 2, 3, g, k, r, etc.
  interval,

  /// Size of 'weiter' 'Abbrechen', 'Dur', 'Moll', 'ü', 'v'
  smallTimesTwo,

  /// Size of 'Fertig'
  mediumTimesTwo
}
