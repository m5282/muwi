import 'package:muwi/interfaces/i_answer.dart';

/// The role of the note in the bass.
enum Bass implements IAnswer {
  root,
  third,
  fifth;

  /// This returns the label to be displayed on a key.
  @override
  String get keyLabel {
    return name;
  }
}

extension BassExtension on Bass {
  String get name {
    switch (this) {
      case Bass.root:
        return '';
      case Bass.fifth:
        return '5';
      case Bass.third:
        return '3';
    }
  }
}
