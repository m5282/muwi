import 'package:muwi/constants/viewmodel_constants.dart';

/// This enum is mainly used to efficiently generate buttons on the keyboard.
/// Probably also interesting for the evaluation?
enum AccidentalEnum { sharp, flat, natural, none }

extension AccidentalEnumExtension on AccidentalEnum {
  String get symbol {
    switch (this) {
      case AccidentalEnum.sharp:
        return '\u{266F}'; //  ♯
      case AccidentalEnum.flat:
        return '\u{266D}';
      case AccidentalEnum.natural:
        return '♮';
      case AccidentalEnum.none:
        return '';
    }
  }

  String get path {
    switch (this) {
      case AccidentalEnum.flat:
        return 'lib/assets/flat.png';
      case AccidentalEnum.sharp:
        return 'lib/assets/sharp.png';
      case AccidentalEnum.natural:
        return 'lib/assets/natural.png';
      case AccidentalEnum.none:
        return '';
    }
  }

  double get verticalOffset {
    switch (this) {
      case AccidentalEnum.flat:
        return ViewModelConstants.verticalOffsetForFlat;
      case AccidentalEnum.sharp:
        return ViewModelConstants.verticalOffsetForSharp;
      case AccidentalEnum.natural:
        return ViewModelConstants.verticalOffsetForNatural;
      case AccidentalEnum.none:
        return 0;
    }
  }
}
