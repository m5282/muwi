import 'package:muwi/interfaces/i_answer.dart';

/// This enum contains the qualities intervals can have.
enum IntervalQuality implements IAnswer {
  major,
  minor,
  augmented,
  diminished,
  perfect;

  /// An element of this enum is also part of the solution, i.e. it is a selectable answer. Therefore, there has to be a button with some label.
  @override
  String get keyLabel {
    return germanAbbreviation;
  }
}

extension QualityExtension on IntervalQuality {
  String get germanAbbreviation {
    switch (this) {
      case IntervalQuality.major:
        return 'g';
      case IntervalQuality.minor:
        return 'k';
      case IntervalQuality.augmented:
        return 'ü';
      case IntervalQuality.diminished:
        return 'v';
      case IntervalQuality.perfect:
        return 'r';
    }
  }

  String get longGermanName {
    switch (this) {
      case IntervalQuality.major:
        return 'groß';
      case IntervalQuality.minor:
        return 'klein';
      case IntervalQuality.augmented:
        return 'übermäßig';
      case IntervalQuality.diminished:
        return 'vermindert';
      case IntervalQuality.perfect:
        return 'rein';
    }
  }
}
