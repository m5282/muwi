import 'package:muwi/constants/musical_constants.dart';
import 'package:muwi/interfaces/i_answer.dart';
import 'package:muwi/model/interval.dart';

/// This class is only for the quality of chords, like triads.
enum ChordQuality implements IAnswer {
  major,
  minor,
  augmented,
  diminished;

  /// This returns the label to be displayed on a key.
  @override
  String get keyLabel {
    return shortGermanName;
  }
}

extension ChordQualityExtension on ChordQuality {
  /// A triad can't be perfect! Hence, we return a major third.
  Interval get third {
    switch (this) {
      case ChordQuality.major:
        return MusicalConstants.majorThird;
      case ChordQuality.minor:
        return MusicalConstants.minorThird;
      case ChordQuality.augmented:
        return MusicalConstants.majorThird;
      case ChordQuality.diminished:
        return MusicalConstants.minorThird;
    }
  }

  /// A fifth can't be major or minor. Hence, we return perfect fifths.
  Interval get fifth {
    switch (this) {
      case ChordQuality.major:
        return MusicalConstants.perfectFifth;
      case ChordQuality.minor:
        return MusicalConstants.perfectFifth;
      case ChordQuality.augmented:
        return MusicalConstants.augmentedFifth;
      case ChordQuality.diminished:
        return MusicalConstants.diminishedFifth;
    }
  }

  String get shortGermanName {
    switch (this) {
      case ChordQuality.major:
        return 'Dur';
      case ChordQuality.minor:
        return 'Moll';
      case ChordQuality.augmented:
        return 'ü';
      case ChordQuality.diminished:
        return 'v';
    }
  }

  String get longGermanName {
    switch (this) {
      case ChordQuality.major:
        return 'Dur';
      case ChordQuality.minor:
        return 'Moll';
      case ChordQuality.augmented:
        return 'übermäßig';
      case ChordQuality.diminished:
        return 'vermindert';
    }
  }

  String get shortEnglish {
    switch (this) {
      case ChordQuality.major:
        return '';
      case ChordQuality.minor:
        return 'm';
      case ChordQuality.augmented:
        return 'aug';
      case ChordQuality.diminished:
        return 'dim';
    }
  }
}
