import 'package:flutter/foundation.dart';
import 'package:muwi/model/accidental.dart';
import 'package:muwi/model/interval.dart';
import 'package:muwi/model/note_class.dart';
import 'package:muwi/model/note_letter.dart';

/// A labelled pitch, i.e. an octaved NoteClass.
@immutable
class Note {
  const Note(this.noteClass, this.octave);

  final NoteClass noteClass;
  final int octave;

  Accidental get accidental => noteClass.accidental;

  NoteLetter get letter => noteClass.letter;

  /// The absolute (octave-dependent) semitone of the note.
  int get semitone => octave * 12 + noteClass.semitone;

  /// The absolute (octave-dependent) semitone of the note **letter**.
  int get letterSemitone => octave * 12 + letter.semitone;

  /// The absolute (octave-dependent) diatonic step of the note.
  int get letterIndex => octave * 7 + letter.index;

  /// Fetches the enharmonically equivalent note with
  /// the specified number of diatonic steps above this
  /// note.
  Note enharmonicEquivalent(int diatonicSteps) {
    // First compute the note with diatonicSteps letters above this one
    final result = Note(
      NoteClass(letter + diatonicSteps, accidental),
      octave + ((letter.index + diatonicSteps) / 7).floor(),
    );

    // Now how many semitones we moved this way and need to subtract from our accidental
    final semitoneDiff = letterSemitone - result.letterSemitone;
    final newAccidental = result.accidental + semitoneDiff;

    return result.withAccidental(newAccidental);
  }

  Note operator +(Interval interval) =>
      Note(noteClass.withAccidental(accidental + interval.semitones), octave)
          .enharmonicEquivalent(interval.diatonicSteps);

  Note operator -(Interval interval) =>
      Note(noteClass.withAccidental(accidental - interval.semitones), octave)
          .enharmonicEquivalent(-interval.diatonicSteps);

  Note withAccidental(Accidental newAccidental) =>
      Note(noteClass.withAccidental(newAccidental), octave);

  Note withLetter(NoteLetter newLetter) => Note(noteClass.withLetter(newLetter), octave);

  Note withOctave(int newOctave) => Note(noteClass, newOctave);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Note &&
          runtimeType == other.runtimeType &&
          noteClass == other.noteClass &&
          octave == other.octave;

  @override
  int get hashCode => noteClass.hashCode ^ octave.hashCode;

  @override
  String toString() => '$noteClass$octave';
}
