import 'package:muwi/enums/bass.dart';
import 'package:muwi/interfaces/i_task.dart';
import 'package:muwi/model/accidental.dart';
import 'package:muwi/model/chord.dart';
import 'package:muwi/model/chord_quality.dart';
import 'package:muwi/model/note.dart';
import 'package:muwi/model/note_class.dart';
import 'package:muwi/model/note_letter.dart';

/// This class contains only the data the user needs to provide to complete a task in triad mode.
class TriadTask implements ITask {
  TriadTask({
    required this.letter,
    required this.accidental,
    required this.bass,
    required this.quality,
  });

  final ChordQuality quality;

  final NoteLetter letter;

  final Accidental accidental;

  /// The role of the lowest note in the arrangement.
  final Bass bass;

  @override
  String toString() {
    return Chord(
      baseNote: Note(NoteClass(letter, accidental), 0),
      quality: quality,
    ).toString();
  }
}
