import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:muwi/enums/badge_period_under_consideration.dart';
import 'package:muwi/model/badge_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

/// This class stores values persistent.
class TonfallSharedPreferences {
  //----------------------------------- Onboarding
  /// The value "onboarding" corresponds to a boolean.
  /// True: Show onboarding screens.
  static const String onboarding = 'onboarding';

  /// The version-key stores the current version.
  static const String version = 'version';

  /// The suffix of the settings key. Prefixes are, for example, the names of the modes.
  static const String settings = 'settings';


  static Future<void> setOnboarding({required bool value}) async {
    // Without this line, we get an
    // Unhandled Exception: Null check operator used on a null value
    // error.
    WidgetsFlutterBinding.ensureInitialized();
    final sp = await SharedPreferences.getInstance();
    await sp.setBool(onboarding, value);
  }

  static Future<bool> getOnboarding() async {
    //WidgetsFlutterBinding.ensureInitialized();
    WidgetsFlutterBinding.ensureInitialized();
    final sp = await SharedPreferences.getInstance();
    //if the variable onboarding is not set yet, we assume that the app
    // hasn't run yet. That means, this is the first time and hence we want to
    // show the onboarding screens.
    final a = sp.getBool(onboarding) ?? true;
    return a;
  }

  static Future<void> removeOnboarding() async {
    WidgetsFlutterBinding.ensureInitialized();
    final sp = await SharedPreferences.getInstance();
    await sp.remove(onboarding);
  }

  //----------------------------------- Version

  /// Returns the current version. Whenever an update is installed, the user gets shown an update screen exactly once and the new version number is stored.
  static Future<String> getVersion() async {
    WidgetsFlutterBinding.ensureInitialized();
    final sp = await SharedPreferences.getInstance();
    return sp.getString(version) ?? 'error';
  }

  /// When the update screen was displayed to the user, the version number is replaced.
  static Future<void> setVersion(String value) async {
    WidgetsFlutterBinding.ensureInitialized();
    final sp = await SharedPreferences.getInstance();
    await sp.setString(version, value);
  }

  /// When the update screen was displayed to the user, the version number is replaced.
  static Future<void> removeVersion() async {
    WidgetsFlutterBinding.ensureInitialized();
    final sp = await SharedPreferences.getInstance();
    await sp.remove(version);
  }

  //----------------------------------- Statistics

  /// Store the Statistics instance for the specific mode.
  static Future<void> setStatistics(String statistics, dynamic value) async {
    WidgetsFlutterBinding.ensureInitialized();
    final sp = await SharedPreferences.getInstance();
    // Each mode has its own Statistics instance.
    await sp.setString(statistics, json.encode(value));
  }

  /// Returns the Statistics instance for the specific mode.
  /// If the Statistics instance is null, "error" will be returned.
  static Future<Map<String, dynamic>> getStatistics(String statistics) async {
    WidgetsFlutterBinding.ensureInitialized();
    final sp = await SharedPreferences.getInstance();
    return json.decode(sp.getString(statistics) ?? 'error') as Map<String, dynamic>;
  }

  //----------------------------------- Settings

  /// Store the Settings instance for the specific mode.
  static Future<void> setSettings(String mode, dynamic value) async {
    WidgetsFlutterBinding.ensureInitialized();
    final sp = await SharedPreferences.getInstance();
    // Each mode has its own Statistics instance.
    await sp.setString(mode, json.encode(value));
  }

  /// Returns the Settings instance for the specific mode.
  /// If the Settings instance is null, "error" will be returned.
  static Future<Map<String, dynamic>> getSettings(String mode) async {
    WidgetsFlutterBinding.ensureInitialized();
    final sp = await SharedPreferences.getInstance();
    // TODO: error will throw casting error
    return json.decode(sp.getString(mode) ?? 'error') as Map<String, dynamic>;
  }

  /// This procedure entirely removes the settings from SharedPreferences.
  static Future<void> removeSettings(String mode) async {
    WidgetsFlutterBinding.ensureInitialized();
    final sp = await SharedPreferences.getInstance();
    await sp.remove(mode);
  }

  ///Set the percentage shown in the menu buttons of the level training
  static Future<void> setLessonTrainingPercent(String lesson, double percent) async {
    WidgetsFlutterBinding.ensureInitialized();
    final sp = await SharedPreferences.getInstance();
    await sp.setDouble(lesson, percent);
  }

  static Future<double> getLessonTrainingPercent(String lesson) async {
    //WidgetsFlutterBinding.ensureInitialized();
    WidgetsFlutterBinding.ensureInitialized();
    final sp = await SharedPreferences.getInstance();
    //if the variable onboarding is not set yet, we assume that the app
    // hasn't run yet. That means, this is the first time and hence we want to
    // show the onboarding screens.
    final percent = sp.getDouble(lesson) ?? 0;
    return percent;
  }

  ///Set the stopwatch badge for the given lesson
  static Future<void> setWatchBadge(String lesson, {required bool received}) async {
    WidgetsFlutterBinding.ensureInitialized();
    final sp = await SharedPreferences.getInstance();
    await sp.setBool(lesson, received);
  }

  ///stopwatch badge already recieved for given lesson
  static Future<bool> getWatchBadge(String lesson) async {
    //WidgetsFlutterBinding.ensureInitialized();
    WidgetsFlutterBinding.ensureInitialized();
    final sp = await SharedPreferences.getInstance();
    //if the variable onboarding is not set yet, we assume that the app
    // hasn't run yet. That means, this is the first time and hence we want to
    // show the onboarding screens.
    final recived = sp.getBool(lesson) ?? false;
    return recived;
  }


  static Future<void> setAccomplishedDateTimeForBadge(BadgeModel badge, DateTime? time) async {
    WidgetsFlutterBinding.ensureInitialized();
    final sp = await SharedPreferences.getInstance();
    WidgetsFlutterBinding.ensureInitialized();
    if (time != null) {
      await sp.setInt(badge.id.toString(), time.millisecondsSinceEpoch);
    }
  }


  ///Returns the DateTime, when [badge] was accomplished last, or null
  ///if the badge hasn't been accomplished ever.
  static Future<DateTime?> getAccomplishedDateTimeForBadge(BadgeModel badge) async {
    WidgetsFlutterBinding.ensureInitialized();
    final sp = await SharedPreferences.getInstance();
    final accomplishedInMillisecondsFromEpoch = sp.getInt(badge.id.toString());
    if (accomplishedInMillisecondsFromEpoch != null) {
      return DateTime.fromMillisecondsSinceEpoch(
          accomplishedInMillisecondsFromEpoch,);
    } else {
       return null;
    }
  }

  ///Returns the value associated with a badge.
  ///E.g. a streak badge returns the length of the streak.
  static Future<int?> getValueFromBadge(BadgeModel badge, BadgePeriodUnderConsideration periodUnderConsideration) async {
    assert(badge.id == 36,
    'Right now, the only badge that has a value is the streak badge all other badges are just accomplished or not. '
        'If this assertion fails on badges created after version 3, delete it or modify it.');
    WidgetsFlutterBinding.ensureInitialized();
    final sp = await SharedPreferences.getInstance();
    final value = sp.getInt(badge.id.toString() + periodUnderConsideration.value.toString());
    return value;
  }

  ///Returns the value associated with a badge.
  ///E.g. a streak badge returns the length of the streak.
  static Future<void> setValueFromBadge(BadgeModel badge, BadgePeriodUnderConsideration periodUnderConsideration, int value) async {
    assert(badge.id == 36,
    'Right now, the only badge that has a value is the streak badge all other badges are just accomplished or not. '
        'If this assertion fails on badges created after version 3, delete it or modify it.');
    WidgetsFlutterBinding.ensureInitialized();
    final sp = await SharedPreferences.getInstance();
    await sp.setInt(badge.id.toString() + periodUnderConsideration.value.toString(), value);
  }
}
