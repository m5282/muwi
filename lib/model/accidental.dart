import 'package:flutter/foundation.dart';
import 'package:muwi/enums/accidental_enum.dart';
import 'package:muwi/interfaces/i_answer.dart';

/// A number of semitones by which a note is
/// pitched up (may be negative).
///
///     ...
///     2 -> ##
///     1 -> #
///     0 -> none
///     1 -> b
///     2 -> bb
///     ...
///
@immutable
class Accidental implements Comparable<Accidental>, IAnswer {
  const Accidental(this.semitones);

  final int semitones;

  static const Accidental sharp = Accidental(1);
  static const Accidental none = Accidental(0);
  static const Accidental flat = Accidental(-1);

  Accidental operator +(int delta) => Accidental(semitones + delta);

  Accidental operator -(int delta) => Accidental(semitones - delta);

  AccidentalEnum toSymbol() {
    if (semitones > 0) {
      return AccidentalEnum.sharp;
    } else if (semitones < 0) {
      return AccidentalEnum.flat;
    } else {
      return AccidentalEnum.none;
    }
  }

  @override
  String toString() {
    if (semitones > 0) {
      return '♯' * semitones; // ♯
    } else if (semitones < 0) {
      return '♭' * -semitones; // ♭
    } else {
      return '';
    }
  }

  /// This returns the label to be displayed on a key.
  @override
  String get keyLabel {
    return toString();
  }

  //WARNING: Because this method doesn't know the note it's applied to, it
  //returns the name aes and ees, although the correct names are as and es.
  String get germanName {
    if (semitones > 0) {
      return 'is' * semitones;
    } else if (semitones < 0) {
      return 'es' * -semitones;
    } else {
      return '';
    }
  }

  @override
  int compareTo(Accidental other) {
    return semitones.compareTo(other.semitones);
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Accidental && runtimeType == other.runtimeType && semitones == other.semitones;

  @override
  int get hashCode => semitones.hashCode;
}
