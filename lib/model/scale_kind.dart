import 'package:muwi/enums/interval_enum.dart';
import 'package:muwi/enums/interval_quality.dart';
import 'package:muwi/model/interval.dart';
import 'package:muwi/model/mode.dart';

enum ScaleKind { diatonic }

extension ScaleKindExtension on ScaleKind {
  List<Interval> intervalsIn(Mode mode) {
    switch (this) {
      case ScaleKind.diatonic:
        return [
          Interval.forQuality(IntervalEnum.unison, IntervalQuality.perfect),
          Interval.forQuality(IntervalEnum.second, IntervalQuality.major),
          mode.third,
          Interval.forQuality(IntervalEnum.fourth, IntervalQuality.perfect),
          Interval.forQuality(IntervalEnum.fifth, IntervalQuality.perfect),
          mode.sixth,
          mode.seventh,
        ];
    }
  }
}
