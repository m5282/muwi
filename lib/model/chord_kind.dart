import 'package:muwi/constants/musical_constants.dart';
import 'package:muwi/model/chord_quality.dart';
import 'package:muwi/model/interval.dart';

enum ChordKind { triad }

extension ChordKindExtension on ChordKind {
  List<Interval> intervalsIn(ChordQuality quality) {
    switch (this) {
      case ChordKind.triad:
        //return [NoteInterval.unison, quality.third, quality.fifth];
        return [MusicalConstants.perfectUnison, quality.third, quality.fifth];
    }
  }

  String get short {
    switch (this) {
      case ChordKind.triad:
        return '';
    }
  }
}
