import 'package:flutter/foundation.dart';
import 'package:muwi/model/mode.dart';
import 'package:muwi/model/note.dart';
import 'package:muwi/model/scale_kind.dart';

/// A scale over a note in parameterized form.
@immutable
class Scale {
  const Scale({
    required this.baseNote,
    required this.mode,
    this.kind = ScaleKind.diatonic,
  });

  final Note baseNote;
  final Mode mode;
  final ScaleKind kind;

  List<Note> get notes =>
      kind.intervalsIn(mode).map((interval) => baseNote + interval).whereType<Note>().toList();

  Scale withBaseNote(Note newBaseNote) => Scale(baseNote: newBaseNote, mode: mode, kind: kind);

  Scale withMode(Mode newMode) => Scale(baseNote: baseNote, mode: newMode, kind: kind);

  Scale withKind(ScaleKind newKind) => Scale(baseNote: baseNote, mode: mode, kind: newKind);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Scale &&
          runtimeType == other.runtimeType &&
          baseNote == other.baseNote &&
          mode == other.mode &&
          kind == other.kind;

  @override
  int get hashCode => baseNote.hashCode ^ mode.hashCode ^ kind.hashCode;

  @override
  String toString() => '${baseNote.noteClass}${mode.short} ($kind)';
}
