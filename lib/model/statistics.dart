import 'dart:collection';
import 'dart:convert';

/// This class stores every piece of information needed for the statistics.
class Statistics {
  // This unnamed constructor is NOT implemented by default!
  Statistics();

  /// Constructor to decode a JSON-Object back to an instance of Statistics.
  Statistics.fromJson(Map<String, dynamic> jsonInput) {
    shownTasks = jsonInput['shownTasks'] as int;
    solvedTasks = jsonInput['solvedTasks'] as int;
    // Produces a List of "true"/"false" strings.
    final lastResultsJson = jsonInput['lastResults'] as String;
    final decodingStepOne = json.decode(lastResultsJson) as List<dynamic>;
    // Produces a list of booleans.
    final decodingStepTwo = decodingStepOne.map((e) => json.decode(e as String) as bool).toList();
    lastResults.addAll(decodingStepTwo);
  }

  /// Number of already seen tasks.
  int shownTasks = 0;

  /// Number of actually solved tasks.
  int solvedTasks = 0;

  /// Number of stored results.
  static const int numberOfResults = 100;

  /// The last x results.
  /// Index = shownTasks % numberOfResults
  /// true => The task was correctly solved.
  /// false => otherwise.

  /// This queue stores the last x results.
  DoubleLinkedQueue<bool> lastResults = DoubleLinkedQueue();

  void updateStatistics({required bool result}) {
    // First, enqueue the result.
    lastResults.addFirst(result);
    // We only keep the last x results and update the queue accordingly.
    if (lastResults.length > numberOfResults) {
      // First, retrieve the oldest item. We need to compare it to the incoming item.
      //bool leaving = lastResults.last;
      // If false is replaced by true, the number of correct results increases.
      if (!lastResults.last && result) {
        solvedTasks++;
      } else if (lastResults.last && !result) {
        // If true is replaced by false, the number of correct results decreases.
        solvedTasks--;
      } // Otherwise some result is replaced by the same result and nothing happens to solvedTasks.
      // If the queue is at its maximum length, the oldest item has to go.
      lastResults.removeLast();
    } else {
      // If the queue has not reached its maximum size yet, simply increase solvedTasks or do nothing.
      if (result) {
        solvedTasks++;
      }
    }
    shownTasks++;
  }

  /// Method to convert the object to a JSON-Object.
  /// We need a JSON-Object to store this object in the shared preference storage.
  Map<String, dynamic> toJson() => {
        'shownTasks': shownTasks,
        'solvedTasks': solvedTasks,
        'lastResults': json.encode(lastResults.map((e) => json.encode(e)).toList()),
      };
}
