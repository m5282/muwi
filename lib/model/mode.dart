import 'package:muwi/constants/musical_constants.dart';
import 'package:muwi/model/interval.dart';

enum Mode { major, minor }

// TODO: Redundanz entfernen: Geht das alles nicht auch in einer Methode?
extension ModeExtension on Mode {
  Interval get second {
    switch (this) {
      case Mode.minor:
        return MusicalConstants.minorSecond;
      case Mode.major:
        return MusicalConstants.minorSecond;
    }
  }

  Interval get third {
    switch (this) {
      case Mode.minor:
        return MusicalConstants.minorThird;
      case Mode.major:
        return MusicalConstants.majorThird;
    }
  }

  Interval get sixth {
    switch (this) {
      case Mode.minor:
        return MusicalConstants.minorSixth;
      case Mode.major:
        return MusicalConstants.majorSixth;
    }
  }

  Interval get seventh {
    switch (this) {
      case Mode.minor:
        return MusicalConstants.minorSeventh;
      case Mode.major:
        return MusicalConstants.majorSeventh;
    }
  }

  String get germanName {
    switch (this) {
      case Mode.major:
        return 'Dur';
      case Mode.minor:
        return 'Moll';
    }
  }

  String get short {
    switch (this) {
      case Mode.major:
        return '';
      case Mode.minor:
        return 'm';
    }
  }
}
