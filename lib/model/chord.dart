import 'package:flutter/foundation.dart';
import 'package:muwi/model/accidental.dart';
import 'package:muwi/model/chord_kind.dart';
import 'package:muwi/model/chord_quality.dart';
import 'package:muwi/model/note.dart';
import 'package:muwi/model/note_letter.dart';

/// Harmonic notes above a certain base note. This class
/// represents chords in their parameterized form, to
/// make it easy to e.g. randomly generate a chord.
/// The actual notes in the chord can be fetched via the
/// getter `notes`.
@immutable
class Chord {
  const Chord({
    required this.baseNote,
    required this.quality,
    this.kind = ChordKind.triad,
  });
  final Note baseNote;
  final ChordQuality quality;
  final ChordKind kind;

  /// Returns the notes of the chord.
  List<Note> get notes {
    final intervals = kind.intervalsIn(quality);
    return intervals.map((interval) => baseNote + interval).whereType<Note>().toList();
  }

  Chord withBaseNote(Note newBaseNote) => Chord(
        baseNote: newBaseNote,
        quality: quality,
        kind: kind,
      );

  Chord withQuality(ChordQuality newQuality) => Chord(
        baseNote: baseNote,
        quality: newQuality,
        kind: kind,
      );

  Chord withKind(ChordKind newKind) => Chord(
        baseNote: baseNote,
        quality: quality,
        kind: newKind,
      );

  @override
  bool operator ==(Object other) =>
      other is Chord &&
      baseNote == other.baseNote &&
      quality == other.quality &&
      kind == other.kind;

  @override
  int get hashCode => Object.hash(baseNote, quality, kind);

  /// Example outputs: F♭, c#, übermäßig, vermindert
  @override
  String toString() {
    if (quality == ChordQuality.diminished || quality == ChordQuality.augmented) {
      return '${quality.longGermanName}${kind.short}';
    } else {
      String name;

      /// Lower case letters for major chord, lower case letters for minor chord.
      if (quality == ChordQuality.major) {
        name = baseNote.letter.germanName.toUpperCase();
      } else {
        name = baseNote.letter.germanName;
      }
      if (baseNote.accidental == Accidental.flat &&
          (baseNote.letter == NoteLetter.e || baseNote.letter == NoteLetter.a)) {
        name += 's';
      } else if (baseNote.accidental == Accidental.flat && baseNote.letter == NoteLetter.b) {
        if (quality == ChordQuality.major) {
          name = 'B';
        } else {
          name = 'b';
        }
      } else {
        name += baseNote.accidental.germanName;
      }
      return name;
    }
  }
}
