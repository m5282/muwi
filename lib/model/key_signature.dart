import 'package:flutter/foundation.dart';
import 'package:muwi/model/mode.dart';
import 'package:muwi/model/note.dart';
import 'package:muwi/model/note_class.dart';
import 'package:muwi/model/scale.dart';

/// Flattened and sharpened note classes ordered by the circle of fifths.
const flatsAndSharps = [
  // Flats
  NoteClass.bFlat,
  NoteClass.eFlat,
  NoteClass.aFlat,
  NoteClass.dFlat,
  NoteClass.gFlat,
  NoteClass.cFlat,
  // Sharps
  NoteClass.fSharp,
  NoteClass.cSharp,
  NoteClass.gSharp,
  NoteClass.dSharp,
  NoteClass.aSharp,
  NoteClass.eSharp,
  NoteClass.bSharp,
];

/// A key signature is a collection of sharpened or flattened
/// note classes, which e.g. could be rendered by the UI as a
/// set of flats/sharps in the beginning of a staff as common
/// in musical notation.
@immutable
class KeySignature {
  /// Constructs a key signature from the given key.
  factory KeySignature({required NoteClass key, required Mode mode}) {
    final notes = Scale(baseNote: Note(key, 0), mode: mode).notes;
    final noteClasses = notes.map((n) => n.noteClass).toSet();
    final alteredNoteClasses = flatsAndSharps.where(noteClasses.contains).toList();
    return KeySignature._(alteredNoteClasses);
  }

  const KeySignature._(this.alteredNoteClasses);

  /// The note classes of the altered notes. Ordered by circle
  /// of fifths for convenience (i.e. the UI can just render these
  /// from left to right).
  final List<NoteClass> alteredNoteClasses;

  @override
  String toString() => alteredNoteClasses.toString();

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is KeySignature &&
          runtimeType == other.runtimeType &&
          alteredNoteClasses == other.alteredNoteClasses;

  @override
  int get hashCode => alteredNoteClasses.hashCode;
}
