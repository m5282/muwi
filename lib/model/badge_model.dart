import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:muwi/enums/badge_category.dart';
import 'package:muwi/enums/badge_medal_color.dart';
import 'package:muwi/enums/level_names.dart';
import 'package:muwi/enums/task_mode.dart';

@immutable
class BadgeModel extends Equatable {

  const BadgeModel({
    required this.id,
    required this.badgeCategory,
    required this.medalColor,
    required this.shortName,
    required this.fullName,
    required this.accomplished,
    required this.picturePath,
    required this.difficulty,
    this.germanDescription = '',
    this.hasValue = false,
  });

  factory BadgeModel.errorBadge() =>
      BadgeModel(
        id: -1,
        badgeCategory: BadgeCategory.error,
        medalColor: BadgeMedalColor.bronze,
        shortName: 'error_badge',
        fullName: 'error_badge',
        germanDescription:
        'Dieser Badge sollte nicht erscheinen. Er kann als Fallback einer eventuellen null Instanz zurückgegeben werden. ',
        picturePath: stopWatchPath,
        difficulty: Difficulty.other,
        accomplished: () => false,
      );

  //Easy
  factory BadgeModel.triadSpeedBronzeEasy() =>
      BadgeModel(
        id: 0,
        badgeCategory: BadgeCategory.speed,
        medalColor: BadgeMedalColor.bronze,
        shortName: 'triad_speed_bronze_easy',
        fullName: 'triad_speed_bronze_easy',
        germanDescription: '20 Sekunden pro Dreiklang',
        picturePath: stopWatchPath,
        difficulty: Difficulty.easy,
        accomplished: ({
          required Difficulty difficulty,
          required int shownTasks,
          required int pointsAchieved,
          required int pointsPerCorrectTask,
          required int differenceInSeconds,
          required TaskMode taskMode,
        }) {
          if (standardRequirements(
            pointsAchieved: pointsAchieved,
            shownTask: shownTasks,
            pointsPerCorrectTask: pointsPerCorrectTask,
          )) {
            if (difficulty == Difficulty.easy &&
                taskMode == TaskMode.triad &&
                differenceInSeconds / shownTasks <= 20) {
              return true;
            }
          }
          return false;
        },
      );

  //BADGE: SPEED
  factory BadgeModel.triadSpeedSilverEasy() =>
      BadgeModel(
        id: 3,
        badgeCategory: BadgeCategory.speed,
        medalColor: BadgeMedalColor.silver,
        shortName: 'triad_speed_Silver_easy',
        fullName: 'triad_speed_Silver_easy',
        germanDescription: '10 Sekunden pro Dreiklang',
        picturePath: stopWatchPath,
        difficulty: Difficulty.easy,
        accomplished: ({
          required Difficulty difficulty,
          required int shownTasks,
          required int pointsAchieved,
          required int pointsPerCorrectTask,
          required int differenceInSeconds,
          required TaskMode taskMode,
        }) {
          if (standardRequirements(
            pointsAchieved: pointsAchieved,
            shownTask: shownTasks,
            pointsPerCorrectTask: pointsPerCorrectTask,
          )) {
            if (difficulty == Difficulty.easy &&
                taskMode == TaskMode.triad &&
                differenceInSeconds / shownTasks <= 10) {
              return true;
            }
          }
          return false;
        },
      );

  factory BadgeModel.triadSpeedGoldEasy() =>
      BadgeModel(
        id: 6,
        badgeCategory: BadgeCategory.speed,
        medalColor: BadgeMedalColor.gold,
        shortName: 'triad_speed_gold_easy',
        fullName: 'triad_speed_gold_easy',
        germanDescription: '5 Sekunden pro Dreiklang',
        picturePath: stopWatchPath,
        difficulty: Difficulty.easy,
        accomplished: ({
          required Difficulty difficulty,
          required int shownTasks,
          required int pointsAchieved,
          required int pointsPerCorrectTask,
          required int differenceInSeconds,
          required TaskMode taskMode,
        }) {
          if (standardRequirements(
            pointsAchieved: pointsAchieved,
            shownTask: shownTasks,
            pointsPerCorrectTask: pointsPerCorrectTask,
          )) {
            if (difficulty == Difficulty.easy &&
                taskMode == TaskMode.triad &&
                differenceInSeconds / shownTasks <= 5) {
              return true;
            }
          }
          return false;
        },
      );

  //Middle
  factory BadgeModel.triadSpeedBronzeMedium() =>
      BadgeModel(
        id: 1,
        badgeCategory: BadgeCategory.speed,
        medalColor: BadgeMedalColor.bronze,
        shortName: 'triad_speed_bronze_medium',
        fullName: 'triad_speed_bronze_medium',
        germanDescription: '20 Sekunden pro Dreiklang',
        picturePath: stopWatchPath,
        difficulty: Difficulty.medium,
        accomplished: ({
          required Difficulty difficulty,
          required int shownTasks,
          required int pointsAchieved,
          required int pointsPerCorrectTask,
          required int differenceInSeconds,
          required TaskMode taskMode,
        }) {
          if (standardRequirements(
            pointsAchieved: pointsAchieved,
            shownTask: shownTasks,
            pointsPerCorrectTask: pointsPerCorrectTask,
          )) {
            if (difficulty == Difficulty.medium &&
                taskMode == TaskMode.triad &&
                differenceInSeconds / shownTasks <= 20) {
              return true;
            }
          }
          return false;
        },
      );

  factory BadgeModel.triadSpeedSilverMedium() =>
      BadgeModel(
        id: 4,
        badgeCategory: BadgeCategory.speed,
        medalColor: BadgeMedalColor.silver,
        shortName: 'triad_speed_Silver_medium',
        fullName: 'triad_speed_Silver_medium',
        germanDescription: '10 Sekunden pro Dreiklang',
        picturePath: stopWatchPath,
        difficulty: Difficulty.medium,
        accomplished: ({
          required Difficulty difficulty,
          required int shownTasks,
          required int pointsAchieved,
          required int pointsPerCorrectTask,
          required int differenceInSeconds,
          required TaskMode taskMode,
        }) {
          if (standardRequirements(
            pointsAchieved: pointsAchieved,
            shownTask: shownTasks,
            pointsPerCorrectTask: pointsPerCorrectTask,
          )) {
            if (difficulty == Difficulty.medium &&
                taskMode == TaskMode.triad &&
                differenceInSeconds / shownTasks <= 10) {
              return true;
            }
          }
          return false;
        },
      );

  factory BadgeModel.triadSpeedGoldMedium() =>
      BadgeModel(
        id: 7,
        badgeCategory: BadgeCategory.speed,
        medalColor: BadgeMedalColor.gold,
        shortName: 'triad_speed_gold_medium',
        fullName: 'triad_speed_gold_medium',
        germanDescription: '5 Sekunden pro Dreiklang',
        picturePath: stopWatchPath,
        difficulty: Difficulty.medium,
        accomplished: ({
          required Difficulty difficulty,
          required int shownTasks,
          required int pointsAchieved,
          required int pointsPerCorrectTask,
          required int differenceInSeconds,
          required TaskMode taskMode,
        }) {
          if (standardRequirements(
            pointsAchieved: pointsAchieved,
            shownTask: shownTasks,
            pointsPerCorrectTask: pointsPerCorrectTask,
          )) {
            if (difficulty == Difficulty.medium &&
                taskMode == TaskMode.triad &&
                differenceInSeconds / shownTasks <= 5) {
              return true;
            }
          }
          return false;
        },
      );

  //Hard
  factory BadgeModel.triadSpeedBronzeHard() =>
      BadgeModel(
        id: 2,
        badgeCategory: BadgeCategory.speed,
        medalColor: BadgeMedalColor.bronze,
        shortName: 'triad_speed_bronze_hard',
        fullName: 'triad_speed_bronze_hard',
        germanDescription: '20 Sekunden pro Dreiklang',
        picturePath: stopWatchPath,
        difficulty: Difficulty.hard,
        accomplished: ({
          required Difficulty difficulty,
          required int shownTasks,
          required int pointsAchieved,
          required int pointsPerCorrectTask,
          required int differenceInSeconds,
          required TaskMode taskMode,
        }) {
          if (standardRequirements(
            pointsAchieved: pointsAchieved,
            shownTask: shownTasks,
            pointsPerCorrectTask: pointsPerCorrectTask,
          )) {
            if (difficulty == Difficulty.hard &&
                taskMode == TaskMode.triad &&
                differenceInSeconds / shownTasks <= 20) {
              return true;
            }
          }
          return false;
        },
      );

  factory BadgeModel.triadSpeedSilverHard() =>
      BadgeModel(
        id: 5,
        badgeCategory: BadgeCategory.speed,
        medalColor: BadgeMedalColor.silver,
        shortName: 'triad_speed_Silver_hard',
        fullName: 'triad_speed_Silver_hard',
        germanDescription: '10 Sekunden pro Dreiklang',
        picturePath: stopWatchPath,
        difficulty: Difficulty.hard,
        accomplished: ({
          required Difficulty difficulty,
          required int shownTasks,
          required int pointsAchieved,
          required int pointsPerCorrectTask,
          required int differenceInSeconds,
          required TaskMode taskMode,
        }) {
          if (standardRequirements(
            pointsAchieved: pointsAchieved,
            shownTask: shownTasks,
            pointsPerCorrectTask: pointsPerCorrectTask,
          )) {
            if (difficulty == Difficulty.hard &&
                taskMode == TaskMode.triad &&
                differenceInSeconds / shownTasks <= 10) {
              return true;
            }
          }
          return false;
        },
      );

  factory BadgeModel.triadSpeedGoldHard() =>
      BadgeModel(
        id: 8,
        badgeCategory: BadgeCategory.speed,
        medalColor: BadgeMedalColor.gold,
        shortName: 'triad_speed_gold_hard',
        fullName: 'triad_speed_gold_hard',
        germanDescription: '5 Sekunden pro Dreiklang',
        picturePath: stopWatchPath,
        difficulty: Difficulty.hard,
        accomplished: ({
          required Difficulty difficulty,
          required int shownTasks,
          required int pointsAchieved,
          required int pointsPerCorrectTask,
          required int differenceInSeconds,
          required TaskMode taskMode,
        }) {
          if (standardRequirements(
            pointsAchieved: pointsAchieved,
            shownTask: shownTasks,
            pointsPerCorrectTask: pointsPerCorrectTask,
          )) {
            if (difficulty == Difficulty.hard &&
                taskMode == TaskMode.triad &&
                differenceInSeconds / shownTasks <= 5) {
              return true;
            }
          }
          return false;
        },
      );

  //BADGE: ROW
  //Easy
  factory BadgeModel.triadsCorrectlySolvedInARowBronzeEasy() =>
      BadgeModel(
        id: 9,
        badgeCategory: BadgeCategory.correctlySolvedInARow,
        medalColor: BadgeMedalColor.bronze,
        shortName: 'triad_correct_in_a_row_bronze_easy',
        fullName: 'triad_correct_in_a_row_bronze_easy',
        germanDescription: '12 Dreiklänge in Folge richtig',
        picturePath: correctInARowPath,
        difficulty: Difficulty.easy,
        accomplished: ({
          required Difficulty difficulty,
          required int shownTasks,
          required int pointsAchieved,
          required int pointsPerCorrectTask,
          required int correctlySolvedInRowBest,
          required TaskMode taskMode,
        }) {
          if (standardRequirements(
            pointsAchieved: pointsAchieved,
            shownTask: shownTasks,
            pointsPerCorrectTask: pointsPerCorrectTask,
          )) {
            if (difficulty == Difficulty.easy &&
                taskMode == TaskMode.triad &&
                correctlySolvedInRowBest >= 12) {
              return true;
            }
          }
          return false;
        },
      );

  factory BadgeModel.triadsCorrectlySolvedInARowSilverEasy() =>
      BadgeModel(
        id: 12,
        badgeCategory: BadgeCategory.correctlySolvedInARow,
        medalColor: BadgeMedalColor.silver,
        shortName: 'triad_correct_in_a_row_Silver_easy',
        fullName: 'triad_correct_in_a_row_Silver_easy',
        germanDescription: '24 Dreiklänge in Folge richtig',
        picturePath: correctInARowPath,
        difficulty: Difficulty.easy,
        accomplished: ({
          required Difficulty difficulty,
          required int shownTasks,
          required int pointsAchieved,
          required int pointsPerCorrectTask,
          required int correctlySolvedInRowBest,
          required TaskMode taskMode,
        }) {
          if (standardRequirements(
            pointsAchieved: pointsAchieved,
            shownTask: shownTasks,
            pointsPerCorrectTask: pointsPerCorrectTask,
          )) {
            if (difficulty == Difficulty.easy &&
                taskMode == TaskMode.triad &&
                correctlySolvedInRowBest >= 24) {
              return true;
            }
          }
          return false;
        },
      );

  factory BadgeModel.triadsCorrectlySolvedInARowGoldEasy() =>
      BadgeModel(
        id: 15,
        badgeCategory: BadgeCategory.correctlySolvedInARow,
        medalColor: BadgeMedalColor.gold,
        shortName: 'triad_correct_in_a_row_gold_easy',
        fullName: 'triad_correct_in_a_row_gold_easy',
        germanDescription: '36 Dreiklänge in Folge richtig',
        picturePath: correctInARowPath,
        difficulty: Difficulty.easy,
        accomplished: ({
          required Difficulty difficulty,
          required int shownTasks,
          required int pointsAchieved,
          required int pointsPerCorrectTask,
          required int correctlySolvedInRowBest,
          required TaskMode taskMode,
        }) {
          if (standardRequirements(
            pointsAchieved: pointsAchieved,
            shownTask: shownTasks,
            pointsPerCorrectTask: pointsPerCorrectTask,
          )) {
            if (difficulty == Difficulty.easy &&
                taskMode == TaskMode.triad &&
                correctlySolvedInRowBest >= 36) {
              return true;
            }
          }
          return false;
        },
      );

  //Middle
  factory BadgeModel.triadsCorrectlySolvedInARowBronzeMedium() =>
      BadgeModel(
        id: 10,
        badgeCategory: BadgeCategory.correctlySolvedInARow,
        medalColor: BadgeMedalColor.bronze,
        shortName: 'triad_correct_solved_in_a_row_bronze_medium',
        fullName: 'triad_correct_solved_in_a_row_bronze_medium',
        germanDescription: '12 Dreiklänge in Folge richtig.',
        picturePath: correctInARowPath,
        difficulty: Difficulty.medium,
        accomplished: ({
          required Difficulty difficulty,
          required int shownTasks,
          required int pointsAchieved,
          required int pointsPerCorrectTask,
          required int correctlySolvedInRowBest,
          required TaskMode taskMode,
        }) {
          if (standardRequirements(
            pointsAchieved: pointsAchieved,
            shownTask: shownTasks,
            pointsPerCorrectTask: pointsPerCorrectTask,
          )) {
            if (difficulty == Difficulty.medium &&
                taskMode == TaskMode.triad &&
                correctlySolvedInRowBest >= 12) {
              return true;
            }
          }
          return false;
        },
      );

  factory BadgeModel.triadsCorrectlySolvedInARowSilverMedium() =>
      BadgeModel(
        id: 13,
        badgeCategory: BadgeCategory.correctlySolvedInARow,
        medalColor: BadgeMedalColor.silver,
        shortName: 'triad_correct_solved_in_a_row_Silver_medium',
        fullName: 'triad_correct_solved_in_a_row_Silver_medium',
        germanDescription: '24 Dreiklänge in Folge richtig',
        picturePath: correctInARowPath,
        difficulty: Difficulty.medium,
        accomplished: ({
          required Difficulty difficulty,
          required int shownTasks,
          required int pointsAchieved,
          required int pointsPerCorrectTask,
          required int correctlySolvedInRowBest,
          required TaskMode taskMode,
        }) {
          if (standardRequirements(
            pointsAchieved: pointsAchieved,
            shownTask: shownTasks,
            pointsPerCorrectTask: pointsPerCorrectTask,
          )) {
            if (difficulty == Difficulty.medium &&
                taskMode == TaskMode.triad &&
                correctlySolvedInRowBest >= 24) {
              return true;
            }
          }
          return false;
        },
      );

  factory BadgeModel.triadsCorrectlySolvedInARowGoldMedium() =>
      BadgeModel(
        id: 16,
        badgeCategory: BadgeCategory.correctlySolvedInARow,
        medalColor: BadgeMedalColor.gold,
        shortName: 'triad_correct_solved_in_a_row_gold_medium',
        fullName: 'triad_correct_solved_in_a_row_gold_medium',
        germanDescription: '36 Dreiklänge in Folge richtig',
        picturePath: correctInARowPath,
        difficulty: Difficulty.medium,
        accomplished: ({
          required Difficulty difficulty,
          required int shownTasks,
          required int pointsAchieved,
          required int pointsPerCorrectTask,
          required int correctlySolvedInRowBest,
          required TaskMode taskMode,
        }) {
          if (standardRequirements(
            pointsAchieved: pointsAchieved,
            shownTask: shownTasks,
            pointsPerCorrectTask: pointsPerCorrectTask,
          )) {
            if (difficulty == Difficulty.medium &&
                taskMode == TaskMode.triad &&
                correctlySolvedInRowBest >= 36) {
              return true;
            }
          }
          return false;
        },
      );

  //Hard
  factory BadgeModel.triadsCorrectlySolvedInARowBronzeHard() =>
      BadgeModel(
        id: 11,
        badgeCategory: BadgeCategory.correctlySolvedInARow,
        medalColor: BadgeMedalColor.bronze,
        shortName: 'triad_correct_solved_in_a_row_bronze_hard',
        fullName: 'triad_correct_solved_in_a_row_bronze_hard',
        germanDescription: '12 Dreiklänge in Folge richtig',
        picturePath: correctInARowPath,
        difficulty: Difficulty.hard,
        accomplished: ({
          required Difficulty difficulty,
          required int shownTasks,
          required int pointsAchieved,
          required int pointsPerCorrectTask,
          required int correctlySolvedInRowBest,
          required TaskMode taskMode,
        }) {
          if (standardRequirements(
            pointsAchieved: pointsAchieved,
            shownTask: shownTasks,
            pointsPerCorrectTask: pointsPerCorrectTask,
          )) {
            if (difficulty == Difficulty.hard &&
                taskMode == TaskMode.triad &&
                correctlySolvedInRowBest >= 12) {
              return true;
            }
          }
          return false;
        },
      );

  factory BadgeModel.triadsCorrectlySolvedInARowSilverHard() =>
      BadgeModel(
        id: 14,
        badgeCategory: BadgeCategory.correctlySolvedInARow,
        medalColor: BadgeMedalColor.silver,
        shortName: 'triad_correct_solved_in_a_row_Silver_hard',
        fullName: 'triad_correct_solved_in_a_row_Silver_hard',
        germanDescription: '24 Dreiklänge in Folge richtig',
        picturePath: correctInARowPath,
        difficulty: Difficulty.hard,
        accomplished: ({
          required Difficulty difficulty,
          required int shownTasks,
          required int pointsAchieved,
          required int pointsPerCorrectTask,
          required int correctlySolvedInRowBest,
          required TaskMode taskMode,
        }) {
          if (standardRequirements(
            pointsAchieved: pointsAchieved,
            shownTask: shownTasks,
            pointsPerCorrectTask: pointsPerCorrectTask,
          )) {
            if (difficulty == Difficulty.hard &&
                taskMode == TaskMode.triad &&
                correctlySolvedInRowBest >= 24) {
              return true;
            }
          }
          return false;
        },
      );

  factory BadgeModel.triadsCorrectlySolvedInARowGoldHard() =>
      BadgeModel(
        id: 17,
        badgeCategory: BadgeCategory.correctlySolvedInARow,
        medalColor: BadgeMedalColor.gold,
        shortName: 'triad_correct_solved_in_a_row_gold_hard',
        fullName: 'triad_correct_solved_in_a_row_gold_hard',
        germanDescription: '36 Dreiklänge in Folge richtig',
        picturePath: correctInARowPath,
        difficulty: Difficulty.hard,
        accomplished: ({
          required Difficulty difficulty,
          required int shownTasks,
          required int pointsAchieved,
          required int pointsPerCorrectTask,
          required int correctlySolvedInRowBest,
          required TaskMode taskMode,
        }) {
          if (standardRequirements(
            pointsAchieved: pointsAchieved,
            shownTask: shownTasks,
            pointsPerCorrectTask: pointsPerCorrectTask,
          )) {
            if (difficulty == Difficulty.hard &&
                taskMode == TaskMode.triad &&
                correctlySolvedInRowBest >= 36) {
              return true;
            }
          }
          return false;
        },
      );

  //INTERVALLE
  //Easy
  factory BadgeModel.intervalSpeedBronzeEasy() =>
      BadgeModel(
        id: 18,
        badgeCategory: BadgeCategory.speed,
        medalColor: BadgeMedalColor.bronze,
        shortName: 'interval_speed_bronze_easy',
        fullName: 'interval_speed_bronze_easy',
        germanDescription: '20 Sekunden pro Intervall',
        picturePath: stopWatchPath,
        difficulty: Difficulty.easy,
        accomplished: ({
          required Difficulty difficulty,
          required int shownTasks,
          required int pointsAchieved,
          required int pointsPerCorrectTask,
          required int differenceInSeconds,
          required TaskMode taskMode,
        }) {
          if (standardRequirements(
            pointsAchieved: pointsAchieved,
            shownTask: shownTasks,
            pointsPerCorrectTask: pointsPerCorrectTask,
          )) {
            if (difficulty == Difficulty.easy &&
                taskMode == TaskMode.interval &&
                differenceInSeconds / shownTasks <= 20) {
              return true;
            }
          }
          return false;
        },
      );

  //BADGE: SPEED
  factory BadgeModel.intervalSpeedSilverEasy() =>
      BadgeModel(
        id: 21,
        badgeCategory: BadgeCategory.speed,
        medalColor: BadgeMedalColor.silver,
        shortName: 'interval_speed_silver_easy',
        fullName: 'interval_speed_silver_easy',
        germanDescription: '10 Sekunden pro Intervall',
        picturePath: stopWatchPath,
        difficulty: Difficulty.easy,
        accomplished: ({
          required Difficulty difficulty,
          required int shownTasks,
          required int pointsAchieved,
          required int pointsPerCorrectTask,
          required int differenceInSeconds,
          required TaskMode taskMode,
        }) {
          if (standardRequirements(
            pointsAchieved: pointsAchieved,
            shownTask: shownTasks,
            pointsPerCorrectTask: pointsPerCorrectTask,
          )) {
            if (difficulty == Difficulty.easy &&
                taskMode == TaskMode.interval &&
                differenceInSeconds / shownTasks <= 10) {
              return true;
            }
          }
          return false;
        },
      );

  factory BadgeModel.intervalSpeedGoldEasy() =>
      BadgeModel(
        id: 24,
        badgeCategory: BadgeCategory.speed,
        medalColor: BadgeMedalColor.gold,
        shortName: 'interval_speed_gold_easy',
        fullName: 'interval_speed_gold_easy',
        germanDescription: '5 Sekunden pro Intervall',
        picturePath: stopWatchPath,
        difficulty: Difficulty.easy,
        accomplished: ({
          required Difficulty difficulty,
          required int shownTasks,
          required int pointsAchieved,
          required int pointsPerCorrectTask,
          required int differenceInSeconds,
          required TaskMode taskMode,
        }) {
          if (standardRequirements(
            pointsAchieved: pointsAchieved,
            shownTask: shownTasks,
            pointsPerCorrectTask: pointsPerCorrectTask,
          )) {
            if (difficulty == Difficulty.easy &&
                taskMode == TaskMode.interval &&
                differenceInSeconds / shownTasks <= 5) {
              return true;
            }
          }
          return false;
        },
      );

  //Middle
  factory BadgeModel.intervalSpeedBronzeMedium() =>
      BadgeModel(
        id: 19,
        badgeCategory: BadgeCategory.speed,
        medalColor: BadgeMedalColor.bronze,
        shortName: 'interval_speed_bronze_medium',
        fullName: 'interval_speed_bronze_medium',
        germanDescription: '20 Sekunden pro Intervall',
        picturePath: stopWatchPath,
        difficulty: Difficulty.medium,
        accomplished: ({
          required Difficulty difficulty,
          required int shownTasks,
          required int pointsAchieved,
          required int pointsPerCorrectTask,
          required int differenceInSeconds,
          required TaskMode taskMode,
        }) {
          if (standardRequirements(
            pointsAchieved: pointsAchieved,
            shownTask: shownTasks,
            pointsPerCorrectTask: pointsPerCorrectTask,
          )) {
            if (difficulty == Difficulty.medium &&
                taskMode == TaskMode.interval &&
                differenceInSeconds / shownTasks <= 20) {
              return true;
            }
          }
          return false;
        },
      );

  factory BadgeModel.intervalSpeedSilverMedium() =>
      BadgeModel(
        id: 22,
        badgeCategory: BadgeCategory.speed,
        medalColor: BadgeMedalColor.silver,
        shortName: 'interval_speed_silver_medium',
        fullName: 'interval_speed_silver_medium',
        germanDescription: '10 Sekunden pro Intervall',
        picturePath: stopWatchPath,
        difficulty: Difficulty.medium,
        accomplished: ({
          required Difficulty difficulty,
          required int shownTasks,
          required int pointsAchieved,
          required int pointsPerCorrectTask,
          required int differenceInSeconds,
          required TaskMode taskMode,
        }) {
          if (standardRequirements(
            pointsAchieved: pointsAchieved,
            shownTask: shownTasks,
            pointsPerCorrectTask: pointsPerCorrectTask,
          )) {
            if (difficulty == Difficulty.medium &&
                taskMode == TaskMode.interval &&
                differenceInSeconds / shownTasks <= 10) {
              return true;
            }
          }
          return false;
        },
      );

  factory BadgeModel.intervalSpeedGoldMedium() =>
      BadgeModel(
        id: 25,
        badgeCategory: BadgeCategory.speed,
        medalColor: BadgeMedalColor.gold,
        shortName: 'interval_speed_gold_medium',
        fullName: 'interval_speed_gold_medium',
        germanDescription: '5 Sekunden pro Intervall',
        picturePath: stopWatchPath,
        difficulty: Difficulty.medium,
        accomplished: ({
          required Difficulty difficulty,
          required int shownTasks,
          required int pointsAchieved,
          required int pointsPerCorrectTask,
          required int differenceInSeconds,
          required TaskMode taskMode,
        }) {
          if (standardRequirements(
            pointsAchieved: pointsAchieved,
            shownTask: shownTasks,
            pointsPerCorrectTask: pointsPerCorrectTask,
          )) {
            if (difficulty == Difficulty.medium &&
                taskMode == TaskMode.interval &&
                differenceInSeconds / shownTasks <= 5) {
              return true;
            }
          }
          return false;
        },
      );

  //Hard
  factory BadgeModel.intervalSpeedBronzeHard() =>
      BadgeModel(
        id: 20,
        badgeCategory: BadgeCategory.speed,
        medalColor: BadgeMedalColor.bronze,
        shortName: 'interval_speed_bronze_hard',
        fullName: 'interval_speed_bronze_hard',
        germanDescription: '20 Sekunden pro Intervall',
        picturePath: stopWatchPath,
        difficulty: Difficulty.hard,
        accomplished: ({
          required Difficulty difficulty,
          required int shownTasks,
          required int pointsAchieved,
          required int pointsPerCorrectTask,
          required int differenceInSeconds,
          required TaskMode taskMode,
        }) {
          if (standardRequirements(
            pointsAchieved: pointsAchieved,
            shownTask: shownTasks,
            pointsPerCorrectTask: pointsPerCorrectTask,
          )) {
            if (difficulty == Difficulty.hard &&
                taskMode == TaskMode.interval &&
                differenceInSeconds / shownTasks <= 20) {
              return true;
            }
          }
          return false;
        },
      );

  factory BadgeModel.intervalSpeedSilverHard() =>
      BadgeModel(
        id: 23,
        badgeCategory: BadgeCategory.speed,
        medalColor: BadgeMedalColor.silver,
        shortName: 'interval_speed_Silver_hard',
        fullName: 'interval_speed_Silver_hard',
        germanDescription: '10 Sekunden pro Intervall',
        picturePath: stopWatchPath,
        difficulty: Difficulty.hard,
        accomplished: ({
          required Difficulty difficulty,
          required int shownTasks,
          required int pointsAchieved,
          required int pointsPerCorrectTask,
          required int differenceInSeconds,
          required TaskMode taskMode,
        }) {
          if (standardRequirements(
            pointsAchieved: pointsAchieved,
            shownTask: shownTasks,
            pointsPerCorrectTask: pointsPerCorrectTask,
          )) {
            if (difficulty == Difficulty.hard &&
                taskMode == TaskMode.interval &&
                differenceInSeconds / shownTasks <= 10) {
              return true;
            }
          }
          return false;
        },
      );

  factory BadgeModel.intervalSpeedGoldHard() =>
      BadgeModel(
        id: 26,
        badgeCategory: BadgeCategory.speed,
        medalColor: BadgeMedalColor.gold,
        shortName: 'interval_speed_gold_hard',
        fullName: 'interval_speed_gold_hard',
        germanDescription: '5 Sekunden pro Intervall',
        difficulty: Difficulty.hard,
        picturePath: stopWatchPath,
        accomplished: ({
          required Difficulty difficulty,
          required int shownTasks,
          required int pointsAchieved,
          required int pointsPerCorrectTask,
          required int differenceInSeconds,
          required TaskMode taskMode,
        }) {
          if (standardRequirements(
            pointsAchieved: pointsAchieved,
            shownTask: shownTasks,
            pointsPerCorrectTask: pointsPerCorrectTask,
          )) {
            if (difficulty == Difficulty.hard &&
                taskMode == TaskMode.interval &&
                differenceInSeconds / shownTasks <= 5) {
              return true;
            }
          }
          return false;
        },
      );

  //BADGE: ROW
  //Easy
  factory BadgeModel.intervalCorrectlySolvedInARowBronzeEasy() =>
      BadgeModel(
        id: 27,
        badgeCategory: BadgeCategory.correctlySolvedInARow,
        medalColor: BadgeMedalColor.bronze,
        shortName: 'interval_correct_in_a_row_bronze_easy',
        fullName: 'interval_correct_in_a_row_bronze_easy',
        germanDescription: '12 Intervalle in Folge richtig',
        picturePath: correctInARowPath,
        difficulty: Difficulty.easy,
        accomplished: ({
          required Difficulty difficulty,
          required int shownTasks,
          required int pointsAchieved,
          required int pointsPerCorrectTask,
          required int correctlySolvedInRowBest,
          required TaskMode taskMode,
        }) {
          if (standardRequirements(
            pointsAchieved: pointsAchieved,
            shownTask: shownTasks,
            pointsPerCorrectTask: pointsPerCorrectTask,
          )) {
            if (difficulty == Difficulty.easy &&
                taskMode == TaskMode.interval &&
                correctlySolvedInRowBest >= 12) {
              return true;
            }
          }
          return false;
        },
      );

  factory BadgeModel.intervalCorrectlySolvedInARowSilverEasy() =>
      BadgeModel(
        id: 30,
        badgeCategory: BadgeCategory.correctlySolvedInARow,
        medalColor: BadgeMedalColor.silver,
        shortName: 'interval_correct_in_a_row_Silver_easy',
        fullName: 'interval_correct_in_a_row_Silver_easy',
        germanDescription: '24 Intervalle in Folge richtig',
        picturePath: correctInARowPath,
        difficulty: Difficulty.easy,
        accomplished: ({
          required Difficulty difficulty,
          required int shownTasks,
          required int pointsAchieved,
          required int pointsPerCorrectTask,
          required int correctlySolvedInRowBest,
          required TaskMode taskMode,
        }) {
          if (standardRequirements(
            pointsAchieved: pointsAchieved,
            shownTask: shownTasks,
            pointsPerCorrectTask: pointsPerCorrectTask,
          )) {
            if (difficulty == Difficulty.easy &&
                taskMode == TaskMode.interval &&
                correctlySolvedInRowBest >= 24) {
              return true;
            }
          }
          return false;
        },
      );

  factory BadgeModel.intervalCorrectlySolvedInARowGoldEasy() =>
      BadgeModel(
        id: 33,
        badgeCategory: BadgeCategory.correctlySolvedInARow,
        medalColor: BadgeMedalColor.gold,
        shortName: 'interval_correct_in_a_row_gold_easy',
        fullName: 'interval_correct_in_a_row_gold_easy',
        germanDescription: '36 Intervalle in Folge richtig',
        picturePath: correctInARowPath,
        difficulty: Difficulty.easy,
        accomplished: ({
          required Difficulty difficulty,
          required int shownTasks,
          required int pointsAchieved,
          required int pointsPerCorrectTask,
          required int correctlySolvedInRowBest,
          required TaskMode taskMode,
        }) {
          if (standardRequirements(
            pointsAchieved: pointsAchieved,
            shownTask: shownTasks,
            pointsPerCorrectTask: pointsPerCorrectTask,
          )) {
            if (difficulty == Difficulty.easy &&
                taskMode == TaskMode.interval &&
                correctlySolvedInRowBest >= 36) {
              return true;
            }
          }
          return false;
        },
      );

  //Middle
  factory BadgeModel.intervalCorrectlySolvedInARowBronzeMedium() =>
      BadgeModel(
        id: 28,
        badgeCategory: BadgeCategory.correctlySolvedInARow,
        medalColor: BadgeMedalColor.bronze,
        shortName: 'interval_correct_solved_in_a_row_bronze_medium',
        fullName: 'interval_correct_solved_in_a_row_bronze_medium',
        germanDescription: '12 Intervalle in Folge richtig.',
        picturePath: correctInARowPath,
        difficulty: Difficulty.medium,
        accomplished: ({
          required Difficulty difficulty,
          required int shownTasks,
          required int pointsAchieved,
          required int pointsPerCorrectTask,
          required int correctlySolvedInRowBest,
          required TaskMode taskMode,
        }) {
          if (standardRequirements(
            pointsAchieved: pointsAchieved,
            shownTask: shownTasks,
            pointsPerCorrectTask: pointsPerCorrectTask,
          )) {
            if (difficulty == Difficulty.medium &&
                taskMode == TaskMode.interval &&
                correctlySolvedInRowBest >= 12) {
              return true;
            }
          }
          return false;
        },
      );

  factory BadgeModel.intervalCorrectlySolvedInARowSilverMedium() =>
      BadgeModel(
        id: 31,
        badgeCategory: BadgeCategory.correctlySolvedInARow,
        medalColor: BadgeMedalColor.silver,
        shortName: 'interval_correct_solved_in_a_row_Silver_medium',
        fullName: 'interval_correct_solved_in_a_row_Silver_medium',
        germanDescription: '24 Intervalle in Folge richtig',
        picturePath: correctInARowPath,
        difficulty: Difficulty.medium,
        accomplished: ({
          required Difficulty difficulty,
          required int shownTasks,
          required int pointsAchieved,
          required int pointsPerCorrectTask,
          required int correctlySolvedInRowBest,
          required TaskMode taskMode,
        }) {
          if (standardRequirements(
            pointsAchieved: pointsAchieved,
            shownTask: shownTasks,
            pointsPerCorrectTask: pointsPerCorrectTask,
          )) {
            if (difficulty == Difficulty.medium &&
                taskMode == TaskMode.interval &&
                correctlySolvedInRowBest >= 24) {
              return true;
            }
          }
          return false;
        },
      );

  factory BadgeModel.intervalCorrectlySolvedInARowGoldMedium() =>
      BadgeModel(
        id: 34,
        badgeCategory: BadgeCategory.correctlySolvedInARow,
        medalColor: BadgeMedalColor.gold,
        shortName: 'interval_correct_solved_in_a_row_gold_medium',
        fullName: 'interval_correct_solved_in_a_row_gold_medium',
        germanDescription: '36 Intervalle in Folge richtig',
        picturePath: correctInARowPath,
        difficulty: Difficulty.medium,
        accomplished: ({
          required Difficulty difficulty,
          required int shownTasks,
          required int pointsAchieved,
          required int pointsPerCorrectTask,
          required int correctlySolvedInRowBest,
          required TaskMode taskMode,
        }) {
          if (standardRequirements(
            pointsAchieved: pointsAchieved,
            shownTask: shownTasks,
            pointsPerCorrectTask: pointsPerCorrectTask,
          )) {
            if (difficulty == Difficulty.medium &&
                taskMode == TaskMode.interval &&
                correctlySolvedInRowBest >= 36) {
              return true;
            }
          }
          return false;
        },
      );

  //Hard
  factory BadgeModel.intervalCorrectlySolvedInARowBronzeHard() =>
      BadgeModel(
        id: 29,
        badgeCategory: BadgeCategory.correctlySolvedInARow,
        medalColor: BadgeMedalColor.bronze,
        shortName: 'interval_correct_solved_in_a_row_bronze_hard',
        fullName: 'interval_correct_solved_in_a_row_bronze_hard',
        germanDescription: '12 Dreiklänge in Folge richtig',
        picturePath: correctInARowPath,
        difficulty: Difficulty.hard,
        accomplished: ({
          required Difficulty difficulty,
          required int shownTasks,
          required int pointsAchieved,
          required int pointsPerCorrectTask,
          required int correctlySolvedInRowBest,
          required TaskMode taskMode,
        }) {
          if (standardRequirements(
            pointsAchieved: pointsAchieved,
            shownTask: shownTasks,
            pointsPerCorrectTask: pointsPerCorrectTask,
          )) {
            if (difficulty == Difficulty.hard &&
                taskMode == TaskMode.interval &&
                correctlySolvedInRowBest >= 12) {
              return true;
            }
          }
          return false;
        },
      );

  factory BadgeModel.intervalCorrectlySolvedInARowSilverHard() =>
      BadgeModel(
        id: 32,
        badgeCategory: BadgeCategory.correctlySolvedInARow,
        medalColor: BadgeMedalColor.silver,
        shortName: 'interval_correct_solved_in_a_row_Silver_hard',
        fullName: 'interval_correct_solved_in_a_row_Silver_hard',
        germanDescription: '24 Intervalle in Folge richtig',
        picturePath: correctInARowPath,
        difficulty: Difficulty.hard,
        accomplished: ({
          required Difficulty difficulty,
          required int shownTasks,
          required int pointsAchieved,
          required int pointsPerCorrectTask,
          required int correctlySolvedInRowBest,
          required TaskMode taskMode,
        }) {
          if (standardRequirements(
            pointsAchieved: pointsAchieved,
            shownTask: shownTasks,
            pointsPerCorrectTask: pointsPerCorrectTask,
          )) {
            if (difficulty == Difficulty.hard &&
                taskMode == TaskMode.interval &&
                correctlySolvedInRowBest >= 24) {
              return true;
            }
          }
          return false;
        },
      );

  factory BadgeModel.intervalCorrectlySolvedInARowGoldHard() =>
      BadgeModel(
        id: 35,
        badgeCategory: BadgeCategory.correctlySolvedInARow,
        medalColor: BadgeMedalColor.gold,
        shortName: 'interval_correct_solved_in_a_row_gold_hard',
        fullName: 'interval_correct_solved_in_a_row_gold_hard',
        germanDescription: '36 Intervalle in Folge richtig',
        picturePath: correctInARowPath,
        difficulty: Difficulty.hard,
        accomplished: ({
          required Difficulty difficulty,
          required int shownTasks,
          required int pointsAchieved,
          required int pointsPerCorrectTask,
          required int correctlySolvedInRowBest,
          required TaskMode taskMode,
        }) {
          if (standardRequirements(
            pointsAchieved: pointsAchieved,
            shownTask: shownTasks,
            pointsPerCorrectTask: pointsPerCorrectTask,
          )) {
            if (difficulty == Difficulty.hard &&
                taskMode == TaskMode.interval &&
                correctlySolvedInRowBest >= 36) {
              return true;
            }
          }
          return false;
        },
      );

  ///The streak-badge is a bit special compared to the other badges.
  ///Accomplished for the streak badge means: The streak got one longer, i. e
  ///tonfall was used another day.
  factory BadgeModel.streak() =>
      BadgeModel(
        id: 36,
        badgeCategory: BadgeCategory.streak,
        //Wird von badge_view gesetzt und nicht hier
        medalColor: BadgeMedalColor.gold,
        shortName: 'streak',
        fullName: 'streak',
        germanDescription: 'Tage in Folge benutzt',
        picturePath: streakPath,
        difficulty: Difficulty.other,
        hasValue: true,
        accomplished: ({
          required int pointsAchieved,
        }) {
          if (pointsAchieved >= 1) {
            return true;
          } else {
            return false;
          }
        },
      );

  ///There are 2 categories * 3 levels * 3 medalColors = 18 Triad items
  ///counted from zero = 17
  static const int highestTriadID = 17;

  /// highestTriadID + (2 categories * 3 levels * 3 medalColors)
  /// = 17 + 18 = 35
  static const int highestIntervalID = 35;

  /// highest highestIntervalID + streakBadge
  /// = 35 + 1 = 36
  static const int highestID = 36;

  static const String stopWatchPath = 'lib/assets/stopwatch.png';
  static const String correctInARowPath = 'lib/assets/arrow-up.png';
  static const String streakPath = 'lib/assets/flame.png';

  final int id;
  final BadgeCategory badgeCategory;
  final BadgeMedalColor medalColor;
  final String shortName;
  final String fullName;
  final String germanDescription;
  final Function accomplished;
  final String picturePath;
  final Difficulty difficulty;
  final bool hasValue;

  static bool standardRequirements({
    required int pointsAchieved,
    required int shownTask,
    required int pointsPerCorrectTask,
  }) {
    // more than half of the points have been achieved
    final examPassed = pointsAchieved >= shownTask * pointsPerCorrectTask / 2;
    final enoughTasks = shownTask >= 12;
    return examPassed && enoughTasks;
  }

  static Map<int, BadgeModel> idToBadgeModel = <int, BadgeModel>{
    BadgeModel
        .triadSpeedBronzeEasy()
        .id: BadgeModel.triadSpeedBronzeEasy(),
    BadgeModel
        .triadSpeedSilverEasy()
        .id: BadgeModel.triadSpeedSilverEasy(),
    BadgeModel
        .triadSpeedGoldEasy()
        .id: BadgeModel.triadSpeedGoldEasy(),
    BadgeModel
        .triadSpeedBronzeMedium()
        .id: BadgeModel.triadSpeedBronzeMedium(),
    BadgeModel
        .triadSpeedSilverMedium()
        .id: BadgeModel.triadSpeedSilverMedium(),
    BadgeModel
        .triadSpeedGoldMedium()
        .id: BadgeModel.triadSpeedGoldMedium(),
    BadgeModel
        .triadSpeedBronzeHard()
        .id: BadgeModel.triadSpeedBronzeHard(),
    BadgeModel
        .triadSpeedSilverHard()
        .id: BadgeModel.triadSpeedSilverHard(),
    BadgeModel
        .triadSpeedGoldHard()
        .id: BadgeModel.triadSpeedGoldHard(),
    BadgeModel
        .triadsCorrectlySolvedInARowBronzeEasy()
        .id:
    BadgeModel.triadsCorrectlySolvedInARowBronzeEasy(),
    BadgeModel
        .triadsCorrectlySolvedInARowSilverEasy()
        .id:
    BadgeModel.triadsCorrectlySolvedInARowSilverEasy(),
    BadgeModel
        .triadsCorrectlySolvedInARowGoldEasy()
        .id:
    BadgeModel.triadsCorrectlySolvedInARowGoldEasy(),
    BadgeModel
        .triadsCorrectlySolvedInARowBronzeMedium()
        .id:
    BadgeModel.triadsCorrectlySolvedInARowBronzeMedium(),
    BadgeModel
        .triadsCorrectlySolvedInARowSilverMedium()
        .id:
    BadgeModel.triadsCorrectlySolvedInARowSilverMedium(),
    BadgeModel
        .triadsCorrectlySolvedInARowGoldMedium()
        .id:
    BadgeModel.triadsCorrectlySolvedInARowGoldMedium(),
    BadgeModel
        .triadsCorrectlySolvedInARowBronzeHard()
        .id:
    BadgeModel.triadsCorrectlySolvedInARowBronzeHard(),
    BadgeModel
        .triadsCorrectlySolvedInARowSilverHard()
        .id:
    BadgeModel.triadsCorrectlySolvedInARowSilverHard(),
    BadgeModel
        .triadsCorrectlySolvedInARowGoldHard()
        .id:
    BadgeModel.triadsCorrectlySolvedInARowGoldHard(),
    BadgeModel
        .intervalSpeedBronzeEasy()
        .id: BadgeModel.intervalSpeedBronzeEasy(),
    BadgeModel
        .intervalSpeedSilverEasy()
        .id: BadgeModel.intervalSpeedSilverEasy(),
    BadgeModel
        .intervalSpeedGoldEasy()
        .id: BadgeModel.intervalSpeedGoldEasy(),
    BadgeModel
        .intervalSpeedBronzeMedium()
        .id: BadgeModel.intervalSpeedBronzeMedium(),
    BadgeModel
        .intervalSpeedSilverMedium()
        .id: BadgeModel.intervalSpeedSilverMedium(),
    BadgeModel
        .intervalSpeedGoldMedium()
        .id: BadgeModel.intervalSpeedGoldMedium(),
    BadgeModel
        .intervalSpeedBronzeHard()
        .id: BadgeModel.intervalSpeedBronzeHard(),
    BadgeModel
        .intervalSpeedSilverHard()
        .id: BadgeModel.intervalSpeedSilverHard(),
    BadgeModel
        .intervalSpeedGoldHard()
        .id: BadgeModel.intervalSpeedGoldHard(),
    BadgeModel
        .intervalCorrectlySolvedInARowBronzeEasy()
        .id:
    BadgeModel.intervalCorrectlySolvedInARowBronzeEasy(),
    BadgeModel
        .intervalCorrectlySolvedInARowSilverEasy()
        .id:
    BadgeModel.intervalCorrectlySolvedInARowSilverEasy(),
    BadgeModel
        .intervalCorrectlySolvedInARowGoldEasy()
        .id:
    BadgeModel.intervalCorrectlySolvedInARowGoldEasy(),
    BadgeModel
        .intervalCorrectlySolvedInARowBronzeMedium()
        .id:
    BadgeModel.intervalCorrectlySolvedInARowBronzeMedium(),
    BadgeModel
        .intervalCorrectlySolvedInARowSilverMedium()
        .id:
    BadgeModel.intervalCorrectlySolvedInARowSilverMedium(),
    BadgeModel
        .intervalCorrectlySolvedInARowGoldMedium()
        .id:
    BadgeModel.intervalCorrectlySolvedInARowGoldMedium(),
    BadgeModel
        .intervalCorrectlySolvedInARowBronzeHard()
        .id:
    BadgeModel.intervalCorrectlySolvedInARowBronzeHard(),
    BadgeModel
        .intervalCorrectlySolvedInARowSilverHard()
        .id:
    BadgeModel.intervalCorrectlySolvedInARowSilverHard(),
    BadgeModel
        .intervalCorrectlySolvedInARowGoldHard()
        .id:
    BadgeModel.intervalCorrectlySolvedInARowGoldHard(),
    BadgeModel
        .streak()
        .id: BadgeModel.streak(),
  };

  @override
  //two badges are the same, if they have the same id
  List<Object?> get props => [id];



  ///Calculates newCurrentStreakLength
  ///If [nowIsNewStreakTime] = true, then the return value will be [currentValue] + 1,
  ///if [lastGame] was yesterday.
  ///
  /// Regardless of [nowIsNewStreakTime] this method returns
  /// - [currentValue], if [lastGame] was today.
  /// - 0, if [lastGame] was before yesterday.
  static int newStreakLength({
    required DateTime now,
    required bool nowIsNewStreakTime,
    required DateTime? lastGame,
    required int currentValue,
  }) {
    assert(lastGame == null || lastGame.isBefore(now), 'last Game should be before now (or null if first game). '
        'lastGame: $lastGame, now: $now');
    final yesterday = now.subtract(const Duration(hours: 24));
    if (lastGame != null) {
      //Played yesterday and today: streak gets one longer
      if (lastGame.year == yesterday.year
          && lastGame.month == yesterday.month
          && lastGame.day == yesterday.day) {
        assert(lastGame.difference(now) < const Duration(hours: 48),
        "We assume, that the streak got one longer, so the last game can't be longer than 48h ago."
            ' last: $lastGame, current end time: $now');
        return nowIsNewStreakTime ? currentValue + 1 : currentValue;
        //Already played today
      } else if (lastGame.year == now.year
          && lastGame.month == now.month
          && lastGame.day == now.day) {
        assert(lastGame.difference(now) < const Duration(hours: 24),
        'We assume, that we already played today, so duration must be less than 24h, last: $lastGame, endTime of quiz: $now',);
        return currentValue;
      }
    }
    //First time ever played or last game is to long ago
    return 0;
  }

  static BadgeMedalColor? getStreakMedalColor({required int streakLength}){
    if(streakLength >= 21){
      return BadgeMedalColor.gold;
    } else if (streakLength >= 14){
      return BadgeMedalColor.silver;
    } else if (streakLength >= 7) {
      return BadgeMedalColor.bronze;
    }
    return null;
  }

}
