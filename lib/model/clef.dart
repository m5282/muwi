import 'package:muwi/model/note.dart';
import 'package:muwi/model/note_class.dart';

/// An indicator for where notes are to be rendered
/// in the staff.
class Clef {
  const Clef({required this.note, required this.referencePosition});

  /// The note which the clef indicates (e.g. G for violin clef, F for bass clef, ...).
  /// Also used by the UI to determine the icon to draw.
  final Note note;

  /// On which half-line of the staff the clef's pitch is.
  /// These are mapped as follows:
  ///
  ///     -------------------  <-- 0
  ///                          <-- 1
  ///     -------------------  <-- 2
  ///                              .
  ///     -------------------      .
  ///                              .
  ///
  final int referencePosition;

  String get germanName {
    switch (this) {
      case treble:
        return 'Violinschlüssel';
      case bass:
        return 'Bassschlüssel';
      case tenor:
        return 'Tenorschlüssel';
      case alto:
        return 'Altschlüssel';
      default:
        throw UnsupportedError('Kein Schlüssel!');
    }
  }

  String get shortName {
    switch (this) {
      case treble:
        return 'treble';
      case bass:
        return 'bass';
      case tenor:
        return 'tenor';
      case alto:
        return 'alto';
      default:
        throw UnsupportedError('');
    }
  }

  static Clef fromString(String string) {
    switch (string) {
      case 'treble':
        return treble;
      case 'bass':
        return bass;
      case 'tenor':
        return tenor;
      case 'alto':
        return alto;
      default:
        throw UnsupportedError('');
    }
  }

  static const Clef treble = Clef(note: Note(NoteClass.g, 4), referencePosition: -2);
  static const Clef bass = Clef(note: Note(NoteClass.f, 3), referencePosition: 2);
  static const Clef tenor = Clef(note: Note(NoteClass.c, 4), referencePosition: 2);
  static const Clef alto = Clef(note: Note(NoteClass.c, 4), referencePosition: 0);

  static final List<Clef> values = [treble, bass, tenor, alto];
}
