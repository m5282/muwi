import 'dart:convert';

import 'package:muwi/constants/global_constants.dart';
import 'package:muwi/constants/viewmodel_constants.dart';
import 'package:muwi/enums/interval_enum.dart';
import 'package:muwi/model/settings/settings.dart';
import 'package:muwi/model/tonfall_shared_preferences.dart';

/// This class contains every information about the settings.
class IntervalSettings extends Settings {
  // This unnamed constructor is NOT implemented by default!
  IntervalSettings() {
    initFrequencies();
    maxTwoAccidentals = true;
  }

  /// Constructor to decode a JSON-Object back to an instance of Settings.
  IntervalSettings.fromJson(Map<String, dynamic>? jsonInput) {
    if (jsonInput != null) {
      copySettings(jsonInput);

      // Maps:
      final intervalFrequenciesJson = jsonInput['intervalFrequencies'] as String?;
      if (intervalFrequenciesJson != null) {
        final stepOne = json.decode(intervalFrequenciesJson) as Map<String, dynamic>;
        final stepTwo = stepOne.map(
          (key, value) => MapEntry(
            IntervalEnum.fromString(key),
            json.decode(value as String) as int,
          ),
        );

        // The Map storing the <IntervalEnum,bool> mappings.
        intervalFrequencies = stepTwo;
      } else {
        initFrequencies();
      }

      // Booleans:
      if (jsonInput['maxTwoAccidentals'] != null) {
        maxTwoAccidentals = jsonInput['maxTwoAccidentals'] as bool;
      } else {
        maxTwoAccidentals = ViewModelConstants.maxTwoAccidentals;
      }
    } else {
      // If no value was given, the default-values are used.
      restoreDefault();
    }
  }

  @override
  String modeName = GlobalConstants.intervalmodus;
  @override
  String otherMode = GlobalConstants.uebungsmodus;

  //---------------- VALUES ADJUSTABLE FOR INTERVALS

  /// If this flag is set to true, a note will not have more than two accidentals + one natural.
  bool maxTwoAccidentals = true;

  //------------------- Changing settings

  /// This text is displayed when the general settings contain unsaved changes
  @override
  String saveChanges = 'Diese Einstellungen auch für Dreiklänge übernehmen';

  /// This text is displayed after the changes to the general settings have been copied to the other mode.
  @override
  String changesSaved = 'Einstellungen für Dreiklänge übernommen';

  /// The text which is currently displayed on the button for copying the general settings to the other mode.
  @override
  String saveButtonText = '';

  /// Restores default settings
  @override
  void restoreDefault() {
    super.restoreDefault();
    initFrequencies();
  }

  //-------------------- Updating settings

  /// Method updating the settings.
  @override
  void updateSettings() {
    TonfallSharedPreferences.setSettings('${modeName}settings', this);
  }

  //------------ Serialization:

  @override
  Map<String, dynamic> toJson() {
    final settings = super.toJson();
    settings['intervalFrequencies'] = json.encode(
      intervalFrequencies.map(
        (key, value) => MapEntry(
          key.toString(),
          value.toString(),
        ),
      ),
    );
    settings['maxTwoAccidentals'] = maxTwoAccidentals;
    return settings;
  }

  //------------ Interval Frequency:

  Map<IntervalEnum, int> intervalFrequencies = {};

  /// The default frequency for each interval is one.
  void initFrequencies() {
    for (final interval in IntervalEnum.values) {
      intervalFrequencies[interval] = 1;
    }
  }

  /// Helper function. This mechanism prevents the user from setting all modes to zero.
  int validMinimum() {
    if (intervalFrequencies.values.any((frequency) => frequency > 0)) {
      return 0;
    } else {
      return 1;
    }
  }

  /// Change the frequency of this interval only if this still leaves at least one value > 0.
  void updateFrequency(IntervalEnum interval, int n) {
    if (n >= validMinimum()) {
      intervalFrequencies[interval] = n;
    } else {
      intervalFrequencies[interval] = 1;
    }
  }
}
