import 'package:muwi/constants/global_constants.dart';
import 'package:muwi/constants/viewmodel_constants.dart';
import 'package:muwi/model/settings/settings.dart';
import 'package:muwi/model/tonfall_shared_preferences.dart';

/// This class contains every information about the settings.
class TriadSettings extends Settings {
  // This unnamed constructor is NOT implemented by default!
  TriadSettings();

  /// Constructor to decode a JSON-Object back to an instance of Settings.
  TriadSettings.fromJson(Map<String, dynamic>? jsonInput) {
    if (jsonInput != null) {
      copySettings(jsonInput);

      // Booleans:

      if (jsonInput['snowmenOnly'] != null) {
        snowmenOnly = jsonInput['snowmenOnly'] as bool;
      } else {
        snowmenOnly = ViewModelConstants.snowmenOnly;
      }

      // Frequencies:

      if (jsonInput['majorFrequency'] != null) {
        majorFrequency = jsonInput['majorFrequency'] as int;
      } else {
        majorFrequency = ViewModelConstants.majorFrequency;
      }

      if (jsonInput['minorFrequency'] != null) {
        minorFrequency = jsonInput['minorFrequency'] as int;
      } else {
        minorFrequency = ViewModelConstants.minorFrequency;
      }

      if (jsonInput['augmentedFrequency'] != null) {
        augmentedFrequency = jsonInput['augmentedFrequency'] as int;
      } else {
        augmentedFrequency = ViewModelConstants.augmentedFrequency;
      }

      if (jsonInput['diminishedFrequency'] != null) {
        diminishedFrequency = jsonInput['diminishedFrequency'] as int;
      } else {
        diminishedFrequency = ViewModelConstants.diminishedFrequency;
      }
    } else {
      // If no value was given, the default-values are used.
      restoreDefault();
    }
  }

  @override
  String modeName = GlobalConstants.uebungsmodus;
  @override
  String otherMode = GlobalConstants.intervalmodus;

  //---------------- VALUES ADJUSTABLE FOR TRIADS

  //----------- The frequencies of major, minor, diminished and augmented.

  /// Frequency of augmented triads.
  int augmentedFrequency = ViewModelConstants.augmentedFrequency;

  /// Frequency of diminished triads.
  int diminishedFrequency = ViewModelConstants.diminishedFrequency;

  /// Frequency of major triads.
  int majorFrequency = ViewModelConstants.majorFrequency;

  /// Frequency of minor triads.
  int minorFrequency = ViewModelConstants.minorFrequency;

  //------------ Misc:

  /// Set this flag to true if user wants to opt out chord rearrangements.
  bool snowmenOnly = ViewModelConstants.snowmenOnly;

  //------------------- Changing settings
  /// This text is displayed when the general settings contain unsaved changes
  @override
  String saveChanges = 'Diese Einstellungen auch für Intervalle übernehmen';

  /// This text is displayed after the changes to the general settings have been copied to the other mode.
  @override
  String changesSaved = 'Einstellungen für Intervalle übernommen';

  /// The text which is currently displayed on the button for copying the general settings to the other mode.
  @override
  String saveButtonText = '';

  /// Restores default settings
  @override
  void restoreDefault() {
    super.restoreDefault();

    augmentedFrequency = ViewModelConstants.augmentedFrequency;
    diminishedFrequency = ViewModelConstants.diminishedFrequency;
    majorFrequency = ViewModelConstants.majorFrequency;
    minorFrequency = ViewModelConstants.minorFrequency;
    snowmenOnly = ViewModelConstants.snowmenOnly;
  }

  //-------------------- Updating settings

  /// Method updating the settings.
  @override
  void updateSettings() {
    TonfallSharedPreferences.setSettings('${modeName}settings', this);
  }

  /// Helper function. This mechanism prevents the user from setting all modes to zero.
  int validMinimum() {
    if (majorFrequency + diminishedFrequency + augmentedFrequency + minorFrequency > 1) {
      return 0;
    } else {
      return 1;
    }
  }

  /// Change major frequency only if this still leaves at least one value > 0.
  void updateMajor(int n) {
    if (n >= validMinimum()) {
      majorFrequency = n;
    } else {
      majorFrequency = 1;
    }
  }

  /// Change minor frequency only if this still leaves at least one value > 0.
  void updateMinor(int n) {
    if (n >= validMinimum()) {
      minorFrequency = n;
    } else {
      minorFrequency = 1;
    }
  }

  /// Change diminished frequency only if this still leaves at least one value > 0.
  void updateDiminished(int n) {
    if (n >= validMinimum()) {
      diminishedFrequency = n;
    } else {
      diminishedFrequency = 1;
    }
  }

  /// Change augmented frequency only if this still leaves at least one value > 0.
  void updateAugmented(int n) {
    if (n >= validMinimum()) {
      augmentedFrequency = n;
    } else {
      augmentedFrequency = 1;
    }
  }

  //------------ Serialization:

  @override
  Map<String, dynamic> toJson() {
    final settings = super.toJson();
    settings['augmentedFrequency'] = augmentedFrequency;
    settings['diminishedFrequency'] = diminishedFrequency;
    settings['majorFrequency'] = majorFrequency;
    settings['minorFrequency'] = minorFrequency;
    settings['snowmenOnly'] = snowmenOnly;
    return settings;
  }
}
