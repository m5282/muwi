import 'dart:convert';

import 'package:muwi/constants/viewmodel_constants.dart';
import 'package:muwi/model/accidental.dart';
import 'package:muwi/model/chord_quality.dart';
import 'package:muwi/model/clef.dart';
import 'package:muwi/model/note_class.dart';
import 'package:muwi/model/tonfall_shared_preferences.dart';

/// This class contains every information about the settings.
class Settings {
  // This unnamed constructor is NOT implemented by default!
  Settings();

  /// Constructor to decode a JSON-Object back to an instance of Settings.
  Settings.fromJson(Map<String, dynamic>? jsonInput) {
    if (jsonInput != null) {
      final clefPreferencesJson = jsonInput['clefPreferences'] as String?;
      if (clefPreferencesJson != null) {
        final decodingStepOne = json.decode(clefPreferencesJson) as Map<String, dynamic>;
        final decodingStepTwo = decodingStepOne.map(
          (key, value) => MapEntry(
            Clef.fromString(key),
            json.decode(value as String) as bool,
          ),
        );
        // The Map storing the <Clef,bool> mappings.
        clefPreferences = decodingStepTwo;
      } else {
        clefPreferences = ViewModelConstants.clefPreferences;
      }

      // Ints:
      if (jsonInput['maxKeySignatureAccidentals'] != null) {
        maxKeySignatureAccidentals = jsonInput['maxKeySignatureAccidentals'] as int;
      } else {
        maxKeySignatureAccidentals = ViewModelConstants.maxKeySignatureAccidentals;
      }
      if (jsonInput['numberOfLedgerLines'] != null) {
        numberOfLedgerLines = jsonInput['numberOfLedgerLines'] as int;
      } else {
        numberOfLedgerLines = ViewModelConstants.numberOfLedgerLines;
      }

      // Booleans:
      if (jsonInput['standardClefArrangement'] != null) {
        standardClefArrangement = jsonInput['standardClefArrangement'] as bool?;
      } else {
        standardClefArrangement = ViewModelConstants.standardClefArrangement;
      }
      if (jsonInput['cumulativeAccidentals'] != null) {
        cumulativeAccidentals = jsonInput['cumulativeAccidentals'] as bool;
      } else {
        cumulativeAccidentals = ViewModelConstants.cumulativeAccidentals;
      }
    } else {
      // If no value was given, the default-values are used.
      restoreDefault();
    }
  }

  String get modeName {
    return '';
  }

  String get otherMode {
    return '';
  }

  /// This text is displayed when the general settings contain unsaved changes
  String get saveChanges {
    return '';
  }

  /// This text is displayed after the changes to the general settings have been copied to the other mode.
  String get changesSaved {
    return '';
  }

  /// The text which is currently displayed on the button for copying the general settings to the other mode.
  String get saveButtonText {
    return '';
  }

  set saveButtonText(String text) {
    saveButtonText = text;
  }

  //------------ GENERAL SETTINGS:

  /// Set this flag to get g clef in upper display and f clef in lower display.
  /// This value can be assigned null for debugging!
  bool? standardClefArrangement = ViewModelConstants.standardClefArrangement;

  /// If this flag is set to true, x flat flat (x in [c,d,e,f,g,a,b]) will be displayed as x flat if x flat is already present in the key signature.
  /// If this flag is set to false, x flat flat will be displayed as x flat flat even if x flat is present in the key signature.
  /// Same for x sharp sharp.
  bool cumulativeAccidentals = ViewModelConstants.cumulativeAccidentals;

  /// Number of key signature accidentals allowed by the user. Default = 6;
  int maxKeySignatureAccidentals = ViewModelConstants.maxKeySignatureAccidentals;

  //NEW settings for the lessong_training
  //TODO: Comment and order them in a meaningful way
  bool oneKeySignature = false;
  int numKeySignatureAccidentals = 0;
  Accidental accidental = Accidental.none;
  Map<NoteClass, ChordQuality>? patternToChoseFrom;
  Clef? upperClef;
  Clef? lowerClef;
  static num numberOfTasksLeft = 5;

  /// This map stores the choice of clefs occurring in the tasks. It maps each clef to a boolean value.
  Map<Clef, bool> clefPreferences = {
    Clef.treble: true,
    Clef.bass: true,
    Clef.tenor: false,
    Clef.alto: false,
  };

  /// The maximum number of ledger lines above and below the regular lines. Default = 1;
  int numberOfLedgerLines = ViewModelConstants.numberOfLedgerLines;

  //------------------- Changing settings

  /// Restores default settings
  void restoreDefault() {
    standardClefArrangement = ViewModelConstants.standardClefArrangement;
    cumulativeAccidentals = ViewModelConstants.cumulativeAccidentals;
    maxKeySignatureAccidentals = ViewModelConstants.maxKeySignatureAccidentals;
    clefPreferences[Clef.treble] = ViewModelConstants.clefPreferences[Clef.treble]!;
    clefPreferences[Clef.bass] = ViewModelConstants.clefPreferences[Clef.bass]!;
    clefPreferences[Clef.tenor] = ViewModelConstants.clefPreferences[Clef.tenor]!;
    clefPreferences[Clef.alto] = ViewModelConstants.clefPreferences[Clef.alto]!;
    numberOfLedgerLines = ViewModelConstants.numberOfLedgerLines;
  }

  /// Toggles the clef only if at least one clef is still chosen.
  void toggleClef(Clef clef) {
    final numberOfClefs = clefPreferences.keys.where((clef) => clefPreferences[clef]!).length;
    if (numberOfClefs > 1 || !clefPreferences[clef]!) {
      clefPreferences[clef] = !clefPreferences[clef]!;
    }
  }

  /// Method updating the settings.
  void updateSettings() {
    TonfallSharedPreferences.setSettings('${modeName}settings', this);
  }

  void copySettings(Map<String, dynamic>? jsonInput) {
    final settings = Settings.fromJson(jsonInput);
    clefPreferences = settings.clefPreferences;
    cumulativeAccidentals = settings.cumulativeAccidentals;
    standardClefArrangement = settings.standardClefArrangement;
    numberOfLedgerLines = settings.numberOfLedgerLines;
    maxKeySignatureAccidentals = settings.maxKeySignatureAccidentals;
  }

  ///  Method copying the general settings to the other mode.
  Future<void> updateOtherSettings(String mode) async {
    Settings maybeSettings;
    try {
      maybeSettings = Settings.fromJson(
        await TonfallSharedPreferences.getSettings('${mode}settings'),
      );
    } on Exception {
      maybeSettings = Settings();
    }
    maybeSettings
      ..numberOfLedgerLines = numberOfLedgerLines
      ..maxKeySignatureAccidentals = maxKeySignatureAccidentals
      ..standardClefArrangement = standardClefArrangement
      ..cumulativeAccidentals = cumulativeAccidentals
      ..clefPreferences = clefPreferences;
    await TonfallSharedPreferences.setSettings('${mode}settings', maybeSettings);
    saveButtonText = changesSaved;
  }

  //------------ Serialization:

  Map<String, dynamic> toJson() => {
        'cumulativeAccidentals': cumulativeAccidentals,
        'standardClefArrangement': standardClefArrangement,
        'maxKeySignatureAccidentals': maxKeySignatureAccidentals,
        'clefPreferences': json.encode(
          clefPreferences.map((key, value) => MapEntry(key.shortName, value.toString())),
        ),
        'numberOfLedgerLines': numberOfLedgerLines,
      };
}
