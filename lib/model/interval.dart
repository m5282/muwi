import 'package:flutter/foundation.dart';
import 'package:muwi/enums/interval_enum.dart';
import 'package:muwi/enums/interval_quality.dart';
import 'package:muwi/interfaces/i_task.dart';

/// A note interval is roughly speaking an offset
/// between notes. It is represented as both a number
/// of semitones (in the 12-tone chromatic scale) and
/// a number of diatonic steps (in the 7-tone scale).
/// The latter is necessary to choose the correct
/// enharmonic equivalents e.g. when constructing triads.
@immutable
class Interval implements ITask {
  const Interval._({
    required this.semitones,
    required this.diatonicSteps,
    required this.intervalEnum,
    required this.intervalQuality,
  });

  factory Interval.forQuality(IntervalEnum interval, IntervalQuality quality) => Interval._(
        semitones: interval.getSemitoneSteps(quality),
        diatonicSteps: interval.diatonicSteps,
        intervalEnum: interval,
        intervalQuality: quality,
      );

  final int semitones;
  final int diatonicSteps;
  final IntervalEnum intervalEnum;
  final IntervalQuality intervalQuality;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Interval &&
          runtimeType == other.runtimeType &&
          semitones == other.semitones &&
          diatonicSteps == other.diatonicSteps &&
          intervalEnum == other.intervalEnum &&
          intervalQuality == other.intervalQuality;

  @override
  int get hashCode =>
      semitones.hashCode ^
      diatonicSteps.hashCode ^
      intervalEnum.hashCode ^
      intervalQuality.hashCode;

  @override
  String toString() {
    return intervalQuality.germanAbbreviation + intervalEnum.germanAbbreviation;
  }
}
