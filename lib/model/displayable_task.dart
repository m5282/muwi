import 'package:muwi/interfaces/i_task.dart';
import 'package:muwi/model/note.dart';

class DisplayableTask {
  const DisplayableTask({required this.task, required this.listOfNotes});

  /// This class contains only the data the user needs to provide.
  final ITask task;

  /// This list is relevant for the display: which are the actual notes this chord consists of.
  final List<Note>? listOfNotes;
}
