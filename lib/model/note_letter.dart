import 'package:muwi/interfaces/i_answer.dart';

/// A step on the 7-tone diatonic C scale,
/// independent of the octave.
enum NoteLetter implements IAnswer {
  c,
  d,
  e,
  f,
  g,
  a,
  b;

  /// This returns the label to be displayed on a key.
  @override
  String get keyLabel {
    return germanName;
  }
}

extension NoteLetterExtension on NoteLetter {
  String get name {
    switch (this) {
      case NoteLetter.c:
        return 'c';
      case NoteLetter.d:
        return 'd';
      case NoteLetter.e:
        return 'e';
      case NoteLetter.f:
        return 'f';
      case NoteLetter.g:
        return 'g';
      case NoteLetter.a:
        return 'a';
      case NoteLetter.b:
        return 'b';
    }
  }

  String get germanName {
    switch (this) {
      case NoteLetter.c:
        return 'c';
      case NoteLetter.d:
        return 'd';
      case NoteLetter.e:
        return 'e';
      case NoteLetter.f:
        return 'f';
      case NoteLetter.g:
        return 'g';
      case NoteLetter.a:
        return 'a';
      case NoteLetter.b:
        return 'h';
    }
  }

  int get semitone {
    switch (this) {
      case NoteLetter.c:
        return 0;
      case NoteLetter.d:
        return 2;
      case NoteLetter.e:
        return 4;
      case NoteLetter.f:
        return 5;
      case NoteLetter.g:
        return 7;
      case NoteLetter.a:
        return 9;
      case NoteLetter.b:
        return 11;
    }
  }

  NoteLetter operator +(int diatonicSteps) =>
      NoteLetter.values[(index + diatonicSteps) % NoteLetter.values.length];
}
