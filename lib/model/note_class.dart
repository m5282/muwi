import 'package:flutter/foundation.dart';
import 'package:muwi/model/accidental.dart';
import 'package:muwi/model/note_letter.dart';

/// A note independent of octave. Since a NoteClass is
/// represented using a note letter and an accidental,
/// pitches on the 12-tone scale do not have a unique
/// representation (e.g. we could have B#, C, Db, ...).
@immutable
class NoteClass {
  const NoteClass(this.letter, this.accidental);

  final NoteLetter letter;
  final Accidental accidental;

  int get semitone => letter.semitone + accidental.semitones;

  // For convenience we declare a bunch of common note classes.
  // Not strictly necessary, but since we only have a small number
  // of these, they make the code elsewhere a bit less verbose.

  static const NoteClass cFlat = NoteClass(NoteLetter.c, Accidental.flat);
  static const NoteClass c = NoteClass(NoteLetter.c, Accidental.none);
  static const NoteClass cSharp = NoteClass(NoteLetter.c, Accidental.sharp);
  static const NoteClass dFlat = NoteClass(NoteLetter.d, Accidental.flat);
  static const NoteClass d = NoteClass(NoteLetter.d, Accidental.none);
  static const NoteClass dSharp = NoteClass(NoteLetter.d, Accidental.sharp);
  static const NoteClass eFlat = NoteClass(NoteLetter.e, Accidental.flat);
  static const NoteClass e = NoteClass(NoteLetter.e, Accidental.none);
  static const NoteClass eSharp = NoteClass(NoteLetter.e, Accidental.sharp);
  static const NoteClass fFlat = NoteClass(NoteLetter.f, Accidental.flat);
  static const NoteClass f = NoteClass(NoteLetter.f, Accidental.none);
  static const NoteClass fSharp = NoteClass(NoteLetter.f, Accidental.sharp);
  static const NoteClass gFlat = NoteClass(NoteLetter.g, Accidental.flat);
  static const NoteClass g = NoteClass(NoteLetter.g, Accidental.none);
  static const NoteClass gSharp = NoteClass(NoteLetter.g, Accidental.sharp);
  static const NoteClass aFlat = NoteClass(NoteLetter.a, Accidental.flat);
  static const NoteClass a = NoteClass(NoteLetter.a, Accidental.none);
  static const NoteClass aSharp = NoteClass(NoteLetter.a, Accidental.sharp);
  static const NoteClass bFlat = NoteClass(NoteLetter.b, Accidental.flat);
  static const NoteClass b = NoteClass(NoteLetter.b, Accidental.none);
  static const NoteClass bSharp = NoteClass(NoteLetter.b, Accidental.sharp);

  NoteClass withLetter(NoteLetter newLetter) => NoteClass(newLetter, accidental);

  NoteClass withAccidental(Accidental newAccidental) => NoteClass(letter, newAccidental);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is NoteClass &&
          runtimeType == other.runtimeType &&
          letter == other.letter &&
          accidental == other.accidental;

  @override
  int get hashCode => letter.hashCode ^ accidental.hashCode;

  @override
  String toString() => '${letter.name}$accidental';
}
