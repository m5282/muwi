import 'package:flutter_test/flutter_test.dart';
import 'package:muwi/model/mode.dart';
import 'package:muwi/model/note.dart';
import 'package:muwi/model/note_class.dart';
import 'package:muwi/model/scale.dart';

void main() {
  test('Diatonic scales', () {
    const cMajor = Scale(baseNote: Note(NoteClass.c, 1), mode: Mode.major);
    expect(cMajor.notes, const [
      Note(NoteClass.c, 1),
      Note(NoteClass.d, 1),
      Note(NoteClass.e, 1),
      Note(NoteClass.f, 1),
      Note(NoteClass.g, 1),
      Note(NoteClass.a, 1),
      Note(NoteClass.b, 1),
    ]);

    const cMinor = Scale(baseNote: Note(NoteClass.c, 1), mode: Mode.minor);
    expect(cMinor.notes, const [
      Note(NoteClass.c, 1),
      Note(NoteClass.d, 1),
      Note(NoteClass.eFlat, 1),
      Note(NoteClass.f, 1),
      Note(NoteClass.g, 1),
      Note(NoteClass.aFlat, 1),
      Note(NoteClass.bFlat, 1),
    ]);

    const bFlatMinor = Scale(baseNote: Note(NoteClass.bFlat, 4), mode: Mode.minor);
    expect(bFlatMinor.notes, const [
      Note(NoteClass.bFlat, 4),
      Note(NoteClass.c, 5),
      Note(NoteClass.dFlat, 5),
      Note(NoteClass.eFlat, 5),
      Note(NoteClass.f, 5),
      Note(NoteClass.gFlat, 5),
      Note(NoteClass.aFlat, 5),
    ]);

    const aMajor = Scale(baseNote: Note(NoteClass.a, -3), mode: Mode.major);
    expect(aMajor.notes, const [
      Note(NoteClass.a, -3),
      Note(NoteClass.b, -3),
      Note(NoteClass.cSharp, -2),
      Note(NoteClass.d, -2),
      Note(NoteClass.e, -2),
      Note(NoteClass.fSharp, -2),
      Note(NoteClass.gSharp, -2),
    ]);
  });
}
