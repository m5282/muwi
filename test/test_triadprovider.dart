import 'package:flutter_test/flutter_test.dart';
import 'package:muwi/model/displayable_task.dart';
import 'package:muwi/model/note_letter.dart';
import 'package:muwi/model/settings/triad_settings.dart';
import 'package:muwi/view_model/position.dart';
import 'package:muwi/view_model/taskproviders/triad_provider.dart';

void main() {
  final chordProvider = TriadProvider();
  final triadSettings = TriadSettings();

  const highestTreblePos = Position(NoteLetter.e, 6);
  const lowestTreblePos = Position(NoteLetter.f, 3);
  const highestBassPos = Position(NoteLetter.g, 4);
  const lowestBassPos = Position(NoteLetter.a, 1);

  // Somewhat expensive test making sure the triads do not leave their range.
  // Can be omitted in the future.
  test('Create some random triads for debugging', () {
    final lowerRange = <DisplayableTask>[];
    final entireRange = <DisplayableTask>[];
    final upperRange = <DisplayableTask>[];
    for (var i = 0; i <= 10; i++) {
      final triad1 = chordProvider.generateTask(
        settings: triadSettings,
        lowerBound: lowestBassPos,
        upperBound: highestTreblePos,
      );
      final notes1 = triad1.listOfNotes!;
      expect(notes1.isNotEmpty, true);
      for (final n in notes1) {
        expect(
          Position(n.letter, n.octave) < highestTreblePos + 1,
          true,
        );
        expect(
          lowestBassPos < Position(n.letter, n.octave) + 1,
          true,
        );
      }
      entireRange.add(triad1);
      final triad2 = chordProvider.generateTask(
        settings: triadSettings,
        lowerBound: lowestTreblePos,
        upperBound: highestTreblePos,
      );
      final notes2 = triad2.listOfNotes!;
      expect(notes2.isNotEmpty, true);
      for (final n in notes2) {
        expect(
          Position(n.letter, n.octave) < highestTreblePos + 1,
          true,
        );
        expect(
          lowestTreblePos < Position(n.letter, n.octave) + 1,
          true,
        );
      }
      upperRange.add(triad2);
      final triad3 = chordProvider.generateTask(
        settings: triadSettings,
        lowerBound: lowestBassPos,
        upperBound: highestBassPos,
      );
      lowerRange.add(triad3);
      final notes3 = triad3.listOfNotes!;
      expect(notes3.isNotEmpty, true);
      for (final n in notes3) {
        expect(
          Position(n.letter, n.octave) < highestBassPos + 1,
          true,
        );
        expect(
          lowestBassPos < Position(n.letter, n.octave) + 1,
          true,
        );
      }
    }
    // Dummy line for the breakpoint.
    // int one = 1;
  });
}
