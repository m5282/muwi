import 'package:flutter_test/flutter_test.dart';
import 'package:muwi/constants/viewmodel_constants.dart';
import 'package:muwi/enums/accidental_enum.dart';
import 'package:muwi/model/accidental.dart';
import 'package:muwi/model/clef.dart';
import 'package:muwi/model/key_signature.dart';
import 'package:muwi/model/mode.dart';
import 'package:muwi/model/note.dart';
import 'package:muwi/model/note_class.dart';
import 'package:muwi/model/note_letter.dart';
import 'package:muwi/model/settings/triad_settings.dart';
import 'package:muwi/view_model/drawable_creator.dart';
import 'package:muwi/view_model/drawable_muwi.dart';
import 'package:muwi/view_model/keyboard_vm/triad_keyboard_vm.dart';
import 'package:muwi/view_model/quiz_page_view_model.dart';
import 'package:muwi/view_model/taskproviders/triad_provider.dart';

void main() {
  /// The object to test.
  final qpvm = QuizPageViewModel(
    settings: TriadSettings(),
    taskProvider: TriadProvider(),
    keyboardVM: TriadKeyboardVM(),
  );

  const firstAccidental = ViewModelConstants.xCoordinateOfNotes - 2;
  const firstShiftedAccidental = ViewModelConstants.xCoordinateOfNotes - 3;
  const secondAccidental = ViewModelConstants.xCoordinateOfNotes - 4;
  const secondShiftedAccidental = ViewModelConstants.xCoordinateOfNotes - 5;
  const notePath = ViewModelConstants.notePath;

  test('A note never carries more than 3 accidentals', () {
    for (var i = 0; i < 1000; i++) {
      qpvm.nextChord();
      final upperDrawables = qpvm.upperDrawables;
      final lowerDrawables = qpvm.lowerDrawables;

      var accidentalCounter = 0;
      final lengthOfKeySignature = qpvm.keySignature!.alteredNoteClasses.length;

      for (final drawable in upperDrawables) {
        if (drawable.xCoordinate >
            ViewModelConstants.beginOfKeySignatureKeys + lengthOfKeySignature - 1) {
          // Once the key signature is over, count the accidentals in front of each note.
          if (drawable.xCoordinate == ViewModelConstants.xCoordinateOfNotes) {
            // Reset the counter when a note is encountered.
            accidentalCounter = 0;
          } else {
            // Otherwise increment the counter.
            accidentalCounter++;
          }
        }
        // There should never be more than 3 accidentals.
        expect(true, accidentalCounter <= 3);
      }

      accidentalCounter = 0;
      for (final drawable in lowerDrawables) {
        if (drawable.xCoordinate >
            ViewModelConstants.beginOfKeySignatureKeys + lengthOfKeySignature - 1) {
          // Once the key signature is over, count the accidentals in front of each note.
          if (drawable.xCoordinate == ViewModelConstants.xCoordinateOfNotes) {
            accidentalCounter = 0;
          } else {
            accidentalCounter++;
          }
          expect(true, accidentalCounter <= 3);
        }
      }
    }
  });

  test('Drawable creator does not screw up this specific chord in this specific setting', () {
    final listOfNotes = <Note>[
      const Note(NoteClass.dFlat, 2),
      const Note(NoteClass.gFlat, 3),
      const Note(NoteClass.bFlat, 3),
      const Note(NoteClass.dFlat, 4),
    ];
    final ks = KeySignature(key: NoteClass.a, mode: Mode.major);

    final drawables =
        DrawableCreator.createChordDrawables(TriadSettings(), ks, Clef.bass, listOfNotes);
    final expectedList = <DrawableMuwi>[
      DrawableMuwi(
        firstAccidental,
        -7,
        ViewModelConstants.accScalingFactor,
        ViewModelConstants.verticalOffsetForFlat,
        AccidentalEnum.flat.path,
      ),
      DrawableMuwi(
        ViewModelConstants.xCoordinateOfNotes,
        -7,
        ViewModelConstants.noteScalingFactor,
        ViewModelConstants.verticalOffsetForNotes,
        notePath,
      ),
      DrawableMuwi(
        secondAccidental,
        3,
        ViewModelConstants.accScalingFactor,
        ViewModelConstants.verticalOffsetForNatural,
        AccidentalEnum.natural.path,
      ),
      DrawableMuwi(
        firstAccidental,
        3,
        ViewModelConstants.accScalingFactor,
        ViewModelConstants.verticalOffsetForFlat,
        AccidentalEnum.flat.path,
      ),
      DrawableMuwi(
        ViewModelConstants.xCoordinateOfNotes,
        3,
        ViewModelConstants.noteScalingFactor,
        ViewModelConstants.verticalOffsetForNotes,
        notePath,
      ),
      DrawableMuwi(
        firstShiftedAccidental,
        5,
        ViewModelConstants.accScalingFactor,
        ViewModelConstants.verticalOffsetForFlat,
        AccidentalEnum.flat.path,
      ),
      DrawableMuwi(
        ViewModelConstants.xCoordinateOfNotes,
        5,
        ViewModelConstants.noteScalingFactor,
        ViewModelConstants.verticalOffsetForNotes,
        notePath,
      ),
      DrawableMuwi(
        firstAccidental,
        7,
        ViewModelConstants.accScalingFactor,
        ViewModelConstants.verticalOffsetForFlat,
        AccidentalEnum.flat.path,
      ),
      DrawableMuwi(
        ViewModelConstants.xCoordinateOfNotes,
        7,
        ViewModelConstants.noteScalingFactor,
        ViewModelConstants.verticalOffsetForNotes,
        notePath,
      ),
    ];
    expect(drawables, expectedList);
  });

  test('Drawable creator does not screw up this one either', () {
    final listOfNotes = <Note>[
      const Note(NoteClass.dFlat, 2),
      const Note(NoteClass.gFlat, 3),
      const Note(NoteClass(NoteLetter.b, Accidental(-2)), 3),
      const Note(NoteClass.dFlat, 4),
    ];
    final ks = KeySignature(key: NoteClass.a, mode: Mode.major);

    final drawables =
        DrawableCreator.createChordDrawables(TriadSettings(), ks, Clef.bass, listOfNotes);
    final expectedList = <DrawableMuwi>[
      DrawableMuwi(
        firstAccidental,
        -7,
        ViewModelConstants.accScalingFactor,
        ViewModelConstants.verticalOffsetForFlat,
        AccidentalEnum.flat.path,
      ),
      DrawableMuwi(
        isNote: true,
        ViewModelConstants.xCoordinateOfNotes,
        -7,
        ViewModelConstants.noteScalingFactor,
        ViewModelConstants.verticalOffsetForNotes,
        notePath,
      ),
      DrawableMuwi(
        secondAccidental,
        3,
        ViewModelConstants.accScalingFactor,
        ViewModelConstants.verticalOffsetForNatural,
        AccidentalEnum.natural.path,
      ),
      DrawableMuwi(
        firstAccidental,
        3,
        ViewModelConstants.accScalingFactor,
        ViewModelConstants.verticalOffsetForFlat,
        AccidentalEnum.flat.path,
      ),
      DrawableMuwi(
        isNote: true,
        ViewModelConstants.xCoordinateOfNotes,
        3,
        ViewModelConstants.noteScalingFactor,
        ViewModelConstants.verticalOffsetForNotes,
        notePath,
      ),
      DrawableMuwi(
        secondShiftedAccidental,
        5,
        ViewModelConstants.accScalingFactor,
        ViewModelConstants.verticalOffsetForFlat,
        AccidentalEnum.flat.path,
      ),
      DrawableMuwi(
        firstShiftedAccidental,
        5,
        ViewModelConstants.accScalingFactor,
        ViewModelConstants.verticalOffsetForFlat,
        AccidentalEnum.flat.path,
      ),
      DrawableMuwi(
        isNote: true,
        ViewModelConstants.xCoordinateOfNotes,
        5,
        ViewModelConstants.noteScalingFactor,
        ViewModelConstants.verticalOffsetForNotes,
        notePath,
      ),
      DrawableMuwi(
        firstAccidental,
        7,
        ViewModelConstants.accScalingFactor,
        ViewModelConstants.verticalOffsetForFlat,
        AccidentalEnum.flat.path,
      ),
      DrawableMuwi(
        isNote: true,
        ViewModelConstants.xCoordinateOfNotes,
        7,
        ViewModelConstants.noteScalingFactor,
        ViewModelConstants.verticalOffsetForNotes,
        notePath,
      ),
    ];
    expect(drawables, expectedList);
  });

  test('Drawable creator does not screw up this one scenario', () {
    final listOfNotes = <Note>[const Note(NoteClass.eSharp, 2), const Note(NoteClass.gSharp, 2)];
    final ks = KeySignature(key: NoteClass.gFlat, mode: Mode.major);

    final drawables =
        DrawableCreator.createChordDrawables(TriadSettings(), ks, Clef.bass, listOfNotes);
    final expectedList = <DrawableMuwi>[
      DrawableMuwi(
        secondAccidental,
        -6,
        ViewModelConstants.accScalingFactor,
        ViewModelConstants.verticalOffsetForNatural,
        AccidentalEnum.natural.path,
      ),
      DrawableMuwi(
        firstAccidental,
        -6,
        ViewModelConstants.accScalingFactor,
        ViewModelConstants.verticalOffsetForSharp,
        AccidentalEnum.sharp.path,
      ),
      DrawableMuwi(
        ViewModelConstants.xCoordinateOfNotes,
        -6,
        ViewModelConstants.noteScalingFactor,
        ViewModelConstants.verticalOffsetForNotes,
        notePath,
      ),
      DrawableMuwi(
        secondShiftedAccidental,
        -4,
        ViewModelConstants.accScalingFactor,
        ViewModelConstants.verticalOffsetForNatural,
        AccidentalEnum.natural.path,
      ),
      DrawableMuwi(
        firstShiftedAccidental,
        -4,
        ViewModelConstants.accScalingFactor,
        ViewModelConstants.verticalOffsetForSharp,
        AccidentalEnum.sharp.path,
      ),
      DrawableMuwi(
        ViewModelConstants.xCoordinateOfNotes,
        -4,
        ViewModelConstants.noteScalingFactor,
        ViewModelConstants.verticalOffsetForNotes,
        notePath,
      ),
    ];
    expect(drawables, expectedList);
  });

  test(
      'Test whether the option of noncumulative accidentals works when a note has two sharps and none of them cancels out',
      () {
    final listOfNotes = <Note>[
      const Note(NoteClass.cSharp, 2),
      const Note(NoteClass.eSharp, 2),
      const Note(NoteClass(NoteLetter.g, Accidental(2)), 2),
    ];
    final ks = KeySignature(key: NoteClass.gFlat, mode: Mode.major);

    final noncumulative = TriadSettings()..cumulativeAccidentals = false;

    final drawables =
        DrawableCreator.createChordDrawables(noncumulative, ks, Clef.bass, listOfNotes);
    final expectedList = <DrawableMuwi>[
      DrawableMuwi(
        secondAccidental,
        -8,
        ViewModelConstants.accScalingFactor,
        ViewModelConstants.verticalOffsetForNatural,
        AccidentalEnum.natural.path,
      ),
      DrawableMuwi(
        firstAccidental,
        -8,
        ViewModelConstants.accScalingFactor,
        ViewModelConstants.verticalOffsetForSharp,
        AccidentalEnum.sharp.path,
      ),
      DrawableMuwi(
        isNote: true,
        ViewModelConstants.xCoordinateOfNotes,
        -8,
        ViewModelConstants.noteScalingFactor,
        ViewModelConstants.verticalOffsetForNotes,
        notePath,
      ),
      DrawableMuwi(
        secondShiftedAccidental,
        -6,
        ViewModelConstants.accScalingFactor,
        ViewModelConstants.verticalOffsetForNatural,
        AccidentalEnum.natural.path,
      ),
      DrawableMuwi(
        firstShiftedAccidental,
        -6,
        ViewModelConstants.accScalingFactor,
        ViewModelConstants.verticalOffsetForSharp,
        AccidentalEnum.sharp.path,
      ),
      DrawableMuwi(
        isNote: true,
        ViewModelConstants.xCoordinateOfNotes,
        -6,
        ViewModelConstants.noteScalingFactor,
        ViewModelConstants.verticalOffsetForNotes,
        notePath,
      ),
      DrawableMuwi(
        secondAccidental - 2,
        -4,
        ViewModelConstants.accScalingFactor,
        ViewModelConstants.verticalOffsetForNatural,
        AccidentalEnum.natural.path,
      ),
      DrawableMuwi(
        secondAccidental,
        -4,
        ViewModelConstants.accScalingFactor,
        ViewModelConstants.verticalOffsetForSharp,
        AccidentalEnum.sharp.path,
      ),
      DrawableMuwi(
        firstAccidental,
        -4,
        ViewModelConstants.accScalingFactor,
        ViewModelConstants.verticalOffsetForSharp,
        AccidentalEnum.sharp.path,
      ),
      DrawableMuwi(
        isNote: true,
        ViewModelConstants.xCoordinateOfNotes,
        -4,
        ViewModelConstants.noteScalingFactor,
        ViewModelConstants.verticalOffsetForNotes,
        notePath,
      ),
    ];
    expect(drawables, expectedList);
  });

  test(
      'Test whether the option of noncumulative accidentals works when a note has two flats and none of them cancels out',
      () {
    final listOfNotes = <Note>[
      const Note(NoteClass.dFlat, 2),
      const Note(NoteClass(NoteLetter.f, Accidental(-2)), 2),
      const Note(NoteClass(NoteLetter.a, Accidental(-2)), 2),
    ];
    final ks = KeySignature(key: NoteClass.fSharp, mode: Mode.major);

    final noncumulative = TriadSettings()..cumulativeAccidentals = false;

    final drawables =
        DrawableCreator.createChordDrawables(noncumulative, ks, Clef.bass, listOfNotes);
    final expectedList = <DrawableMuwi>[
      DrawableMuwi(
        secondAccidental,
        -7,
        ViewModelConstants.accScalingFactor,
        ViewModelConstants.verticalOffsetForNatural,
        AccidentalEnum.natural.path,
      ),
      DrawableMuwi(
        firstAccidental,
        -7,
        ViewModelConstants.accScalingFactor,
        ViewModelConstants.verticalOffsetForFlat,
        AccidentalEnum.flat.path,
      ),
      DrawableMuwi(
        isNote: true,
        ViewModelConstants.xCoordinateOfNotes,
        -7,
        ViewModelConstants.noteScalingFactor,
        ViewModelConstants.verticalOffsetForNotes,
        notePath,
      ),
      DrawableMuwi(
        secondShiftedAccidental - 2,
        -5,
        ViewModelConstants.accScalingFactor,
        ViewModelConstants.verticalOffsetForNatural,
        AccidentalEnum.natural.path,
      ),
      DrawableMuwi(
        secondShiftedAccidental,
        -5,
        ViewModelConstants.accScalingFactor,
        ViewModelConstants.verticalOffsetForFlat,
        AccidentalEnum.flat.path,
      ),
      DrawableMuwi(
        firstShiftedAccidental,
        -5,
        ViewModelConstants.accScalingFactor,
        ViewModelConstants.verticalOffsetForFlat,
        AccidentalEnum.flat.path,
      ),
      DrawableMuwi(
        isNote: true,
        ViewModelConstants.xCoordinateOfNotes,
        -5,
        ViewModelConstants.noteScalingFactor,
        ViewModelConstants.verticalOffsetForNotes,
        notePath,
      ),
      DrawableMuwi(
        secondAccidental - 2,
        -3,
        ViewModelConstants.accScalingFactor,
        ViewModelConstants.verticalOffsetForNatural,
        AccidentalEnum.natural.path,
      ),
      DrawableMuwi(
        secondAccidental,
        -3,
        ViewModelConstants.accScalingFactor,
        ViewModelConstants.verticalOffsetForFlat,
        AccidentalEnum.flat.path,
      ),
      DrawableMuwi(
        firstAccidental,
        -3,
        ViewModelConstants.accScalingFactor,
        ViewModelConstants.verticalOffsetForFlat,
        AccidentalEnum.flat.path,
      ),
      DrawableMuwi(
        isNote: true,
        ViewModelConstants.xCoordinateOfNotes,
        -3,
        ViewModelConstants.noteScalingFactor,
        ViewModelConstants.verticalOffsetForNotes,
        notePath,
      ),
    ];
    expect(drawables, expectedList);
  });

  test(
      'Test whether the option of noncumulative accidentals works when a note has two flats and one of then would normally cancel out',
      () {
    final listOfNotes = <Note>[
      const Note(NoteClass.dFlat, 2),
      const Note(NoteClass(NoteLetter.f, Accidental(-2)), 2),
      const Note(NoteClass(NoteLetter.a, Accidental(-2)), 2),
    ];
    final ks = KeySignature(key: NoteClass.gFlat, mode: Mode.major);

    final noncumulative = TriadSettings()..cumulativeAccidentals = false;

    final drawables =
        DrawableCreator.createChordDrawables(noncumulative, ks, Clef.bass, listOfNotes);
    final expectedList = <DrawableMuwi>[
      DrawableMuwi(
        isNote: true,
        ViewModelConstants.xCoordinateOfNotes,
        -7,
        ViewModelConstants.noteScalingFactor,
        ViewModelConstants.verticalOffsetForNotes,
        notePath,
      ),
      DrawableMuwi(
        secondAccidental,
        -5,
        ViewModelConstants.accScalingFactor,
        ViewModelConstants.verticalOffsetForFlat,
        AccidentalEnum.flat.path,
      ),
      DrawableMuwi(
        firstAccidental,
        -5,
        ViewModelConstants.accScalingFactor,
        ViewModelConstants.verticalOffsetForFlat,
        AccidentalEnum.flat.path,
      ),
      DrawableMuwi(
        isNote: true,
        ViewModelConstants.xCoordinateOfNotes,
        -5,
        ViewModelConstants.noteScalingFactor,
        ViewModelConstants.verticalOffsetForNotes,
        notePath,
      ),
      DrawableMuwi(
        secondShiftedAccidental,
        -3,
        ViewModelConstants.accScalingFactor,
        ViewModelConstants.verticalOffsetForFlat,
        AccidentalEnum.flat.path,
      ),
      DrawableMuwi(
        firstShiftedAccidental,
        -3,
        ViewModelConstants.accScalingFactor,
        ViewModelConstants.verticalOffsetForFlat,
        AccidentalEnum.flat.path,
      ),
      DrawableMuwi(
        isNote: true,
        ViewModelConstants.xCoordinateOfNotes,
        -3,
        ViewModelConstants.noteScalingFactor,
        ViewModelConstants.verticalOffsetForNotes,
        notePath,
      ),
    ];
    expect(drawables, expectedList);
  });

  test(
      'Test whether the option of cumulative accidentals works when a note has two flats and one of then cancels out',
      () {
    final listOfNotes = <Note>[
      const Note(NoteClass.dFlat, 2),
      const Note(NoteClass(NoteLetter.f, Accidental(-2)), 2),
      const Note(NoteClass(NoteLetter.a, Accidental(-2)), 2),
    ];
    final ks = KeySignature(key: NoteClass.gFlat, mode: Mode.major);

    final cumulative = TriadSettings();

    final drawables = DrawableCreator.createChordDrawables(cumulative, ks, Clef.bass, listOfNotes);
    final expectedList = <DrawableMuwi>[
      DrawableMuwi(
        isNote: true,
        ViewModelConstants.xCoordinateOfNotes,
        -7,
        ViewModelConstants.noteScalingFactor,
        ViewModelConstants.verticalOffsetForNotes,
        notePath,
      ),
      DrawableMuwi(
        secondAccidental,
        -5,
        ViewModelConstants.accScalingFactor,
        ViewModelConstants.verticalOffsetForFlat,
        AccidentalEnum.flat.path,
      ),
      DrawableMuwi(
        firstAccidental,
        -5,
        ViewModelConstants.accScalingFactor,
        ViewModelConstants.verticalOffsetForFlat,
        AccidentalEnum.flat.path,
      ),
      DrawableMuwi(
        isNote: true,
        ViewModelConstants.xCoordinateOfNotes,
        -5,
        ViewModelConstants.noteScalingFactor,
        ViewModelConstants.verticalOffsetForNotes,
        notePath,
      ),
      DrawableMuwi(
        firstShiftedAccidental,
        -3,
        ViewModelConstants.accScalingFactor,
        ViewModelConstants.verticalOffsetForFlat,
        AccidentalEnum.flat.path,
      ),
      DrawableMuwi(
        isNote: true,
        ViewModelConstants.xCoordinateOfNotes,
        -3,
        ViewModelConstants.noteScalingFactor,
        ViewModelConstants.verticalOffsetForNotes,
        notePath,
      ),
    ];
    expect(drawables, expectedList);
  });

  test(
      'Test whether the option of noncumulative accidentals works when a note has two sharps and one of them would normally cancel out',
      () {
    final listOfNotes = <Note>[
      const Note(NoteClass.cSharp, 2),
      const Note(NoteClass.eSharp, 2),
      const Note(NoteClass(NoteLetter.g, Accidental(2)), 2),
    ];
    final ks = KeySignature(key: NoteClass.fSharp, mode: Mode.major);

    final noncumulative = TriadSettings()..cumulativeAccidentals = false;

    final drawables =
        DrawableCreator.createChordDrawables(noncumulative, ks, Clef.bass, listOfNotes);
    final expectedList = <DrawableMuwi>[
      DrawableMuwi(
        isNote: true,
        ViewModelConstants.xCoordinateOfNotes,
        -8,
        ViewModelConstants.noteScalingFactor,
        ViewModelConstants.verticalOffsetForNotes,
        notePath,
      ),
      DrawableMuwi(
        isNote: true,
        ViewModelConstants.xCoordinateOfNotes,
        -6,
        ViewModelConstants.noteScalingFactor,
        ViewModelConstants.verticalOffsetForNotes,
        notePath,
      ),
      DrawableMuwi(
        ViewModelConstants.xCoordinateOfNotes - 3,
        -4,
        ViewModelConstants.accScalingFactor,
        ViewModelConstants.verticalOffsetForSharp,
        AccidentalEnum.sharp.path,
      ),
      DrawableMuwi(
        ViewModelConstants.xCoordinateOfNotes - 2,
        -4,
        ViewModelConstants.accScalingFactor,
        ViewModelConstants.verticalOffsetForSharp,
        AccidentalEnum.sharp.path,
      ),
      DrawableMuwi(
        isNote: true,
        ViewModelConstants.xCoordinateOfNotes,
        -4,
        ViewModelConstants.noteScalingFactor,
        ViewModelConstants.verticalOffsetForNotes,
        notePath,
      ),
    ];
    expect(drawables, expectedList);
  });

  test(
      'Test whether the option of cumulative accidentals works when a note has two sharps and one of them cancels out',
      () {
    final listOfNotes = <Note>[
      const Note(NoteClass.cSharp, 2),
      const Note(NoteClass.eSharp, 2),
      const Note(NoteClass(NoteLetter.g, Accidental(2)), 2),
    ];
    final ks = KeySignature(key: NoteClass.fSharp, mode: Mode.major);

    final cumulative = TriadSettings();

    final drawables = DrawableCreator.createChordDrawables(cumulative, ks, Clef.bass, listOfNotes);
    final expectedList = <DrawableMuwi>[
      DrawableMuwi(
        isNote: true,
        ViewModelConstants.xCoordinateOfNotes,
        -8,
        ViewModelConstants.noteScalingFactor,
        ViewModelConstants.verticalOffsetForNotes,
        notePath,
      ),
      DrawableMuwi(
        isNote: true,
        ViewModelConstants.xCoordinateOfNotes,
        -6,
        ViewModelConstants.noteScalingFactor,
        ViewModelConstants.verticalOffsetForNotes,
        notePath,
      ),
      DrawableMuwi(
        ViewModelConstants.xCoordinateOfNotes - 2,
        -4,
        ViewModelConstants.accScalingFactor,
        ViewModelConstants.verticalOffsetForSharp,
        AccidentalEnum.sharp.path,
      ),
      DrawableMuwi(
        isNote: true,
        ViewModelConstants.xCoordinateOfNotes,
        -4,
        ViewModelConstants.noteScalingFactor,
        ViewModelConstants.verticalOffsetForNotes,
        notePath,
      ),
    ];
    expect(drawables, expectedList);
  });
}
