import 'dart:math';
import 'package:flutter_test/flutter_test.dart';
import 'package:muwi/enums/bass.dart';
import 'package:muwi/model/accidental.dart';
import 'package:muwi/model/chord.dart';
import 'package:muwi/model/chord_quality.dart';
import 'package:muwi/model/note.dart';
import 'package:muwi/model/note_class.dart';
import 'package:muwi/model/note_letter.dart';
import 'package:muwi/services/triad_arrangement_service.dart';
import 'package:muwi/view_model/position.dart';

/// constants
const cMajorTriad4 = Chord(
  baseNote: Note(NoteClass.c, 4),
  quality: ChordQuality.major,
);
const aMinorTriad4 = Chord(
  baseNote: Note(NoteClass.a, 4),
  quality: ChordQuality.minor,
);
const aMinorTriad3 = Chord(
  baseNote: Note(NoteClass.a, 3),
  quality: ChordQuality.major,
);
const dDimTriad4 = Chord(
  baseNote: Note(NoteClass.d, 4),
  quality: ChordQuality.diminished,
); // (d4 f4 as4)
const aAugTriad2 = Chord(
  baseNote: Note(NoteClass.a, 2),
  quality: ChordQuality.augmented,
);
final TriadArrangementService cMajor4 = TriadArrangementService(
  rootPosition: cMajorTriad4,
  upperBound: highestTreblePos,
  lowerBound: lowestBassPos,
  snowmenOnly: false,
);

Position highestTreblePos = const Position(NoteLetter.e, 6);
Position lowestTreblePos = const Position(NoteLetter.f, 3);
Position highestBassPos = const Position(NoteLetter.g, 4);
Position lowestBassPos = const Position(NoteLetter.a, 1);
Position gClefLowestRegularLine = const Position(NoteLetter.e, 4);
Position gClefHighestRegularLine = const Position(NoteLetter.f, 5);

void main() {
  test(
      'Indices for c major chord in octave 4 with treble and bass clef displays should extend from -6 to 7.',
      () {
    cMajor4.rearrange();
    expect(cMajor4.minIndex, -6);
    expect(cMajor4.maxIndex, 7);
  });

  test(
      'ChordRearrangement does not crash when there are only 4 possible positions and chooses possible notes.',
      () {
    final aMinor4 = TriadArrangementService(
      rootPosition: aMinorTriad4,
      upperBound: gClefHighestRegularLine,
      lowerBound: gClefLowestRegularLine,
      snowmenOnly: false,
    );
    for (var i = 0; i < 100; i++) {
      aMinor4.rearrange();
      expect(
        aMinor4.choiceOfNotes.every(
          (note) => const [
            Note(NoteClass.e, 4),
            Note(NoteClass.a, 4),
            Note(NoteClass.c, 5),
            Note(NoteClass.e, 5),
          ].contains(note),
        ),
        true,
      );
    }
  });

  test(
      'Indices for a minor chord in octave 3 with treble and bass clef displays should range from -6 to 8.',
      () {
    final aMinor3 = TriadArrangementService(
      rootPosition: aMinorTriad3,
      upperBound: highestTreblePos,
      lowerBound: lowestBassPos,
      snowmenOnly: false,
    )
    ..rearrange();
    expect(aMinor3.minIndex, -6);
    expect(aMinor3.maxIndex, 8);
  });

  test(
      'Indices for c major chord in octave 4 with two treble clef displays should range from -1 to 7.',
      () {
    final cMajor4TrebleOnly = TriadArrangementService(
      rootPosition: cMajorTriad4,
      upperBound: highestTreblePos,
      lowerBound: lowestTreblePos,
      snowmenOnly: false,
    )
    ..rearrange();
    expect(cMajor4TrebleOnly.minIndex, -1);
    expect(cMajor4TrebleOnly.maxIndex, 7);
  });

  test(
      'Indices for c major chord in octave 4 with two bass clef displays should extend from -6 to 2.',
      () {
    final cMajor4BassOnly = TriadArrangementService(
      rootPosition: cMajorTriad4,
      upperBound: highestBassPos,
      lowerBound: lowestBassPos,
      snowmenOnly: false,
    )
    ..rearrange();
    expect(cMajor4BassOnly.minIndex, -6);
    expect(cMajor4BassOnly.maxIndex, 2);
  });

  test('Generated rearrangement contains at least one root and one third.', () {
    cMajor4.rearrange();
    expect(
      cMajor4.choiceOfNotes.where((note) => note.noteClass == NoteClass.c).isNotEmpty,
      true,
    );
    expect(
      cMajor4.choiceOfNotes.where((note) => note.noteClass == NoteClass.e).isNotEmpty,
      true,
    );
  });

  test('The choice of notes contains only notes within the defined range', () {
    for (var i = 0; i < 10; i++) {
      cMajor4.rearrange();
      expect(
        cMajor4.choiceOfNotes
            .where(
              (note) => highestTreblePos < Position(note.letter, note.octave),
            )
            .isEmpty,
        true,
      );
      expect(
        cMajor4.choiceOfNotes
            .where(
              (note) => Position(note.letter, note.octave) < lowestBassPos,
            )
            .isEmpty,
        true,
      );
    }
  });

  test('The indices on a scale for a c major triad in octave 4 are converted to the right notes.',
      () {
    cMajor4.rearrange();
    final notes = cMajor4.indicesToNotes([-6, -5, -2, 0, 2, 7]);
    final correctList = <Note>[
      const Note(NoteClass.c, 2),
      const Note(NoteClass.e, 2),
      const Note(NoteClass.e, 3),
      const Note(NoteClass.c, 4),
      const Note(NoteClass.g, 4),
      const Note(NoteClass.e, 6),
    ];
    expect(notes, correctList);
  });

  test('The role of the lowest note is set correctly', () {
    cMajor4.rearrange();
    final head = cMajor4.choiceOfNotes.first.noteClass;
    if (head == NoteClass.c) {
      expect(cMajor4.bass, Bass.root);
    } else if (head == NoteClass.e) {
      expect(cMajor4.bass, Bass.third);
    } else {
      expect(cMajor4.bass, Bass.fifth);
    }
  });

  test('D diminished at octave 4 contains at least the root, one third and one fifth', () {
    for (var i = 0; i < 100; i++) {
      final dDim4TrebleOnly = TriadArrangementService(
        rootPosition: dDimTriad4,
        upperBound: highestTreblePos,
        lowerBound: lowestTreblePos,
        snowmenOnly: false,
      )
      ..rearrange();
      expect(
        dDim4TrebleOnly.choiceOfNotes.where((note) => note.noteClass == NoteClass.d).isNotEmpty,
        true,
      );
      expect(
        dDim4TrebleOnly.choiceOfNotes.where((note) => note.noteClass == NoteClass.f).isNotEmpty,
        true,
      );
      expect(
        dDim4TrebleOnly.choiceOfNotes.where((note) => note.noteClass == NoteClass.aFlat).isNotEmpty,
        true,
      );
    }
  });

  test('A augmented at octave 2 contains at least the root, one third and one fifth', () {
    for (var i = 0; i < 100; i++) {
      final aAugTriad2BassTreble = TriadArrangementService(
        rootPosition: aAugTriad2,
        upperBound: highestTreblePos,
        lowerBound: lowestBassPos,
        snowmenOnly: false,
      )
      ..rearrange();
      expect(
        aAugTriad2BassTreble.choiceOfNotes
            .where((note) => note.noteClass == NoteClass.a)
            .isNotEmpty,
        true,
      );
      expect(
        aAugTriad2BassTreble.choiceOfNotes
            .where((note) => note.noteClass == NoteClass.cSharp)
            .isNotEmpty,
        true,
      );
      expect(
        aAugTriad2BassTreble.choiceOfNotes
            .where((note) => note.noteClass == NoteClass.eSharp)
            .isNotEmpty,
        true,
      );
    }
  });

  test('Any diminished triad contains at least 3 notes', () {
    final maxLetter = NoteLetter.values.length;
    for (var i = 0; i < 100; i++) {
      final letter = NoteLetter.values[Random().nextInt(maxLetter)];
      final acc = Accidental([-1, 0, 2][Random().nextInt(2)]);
      final dimTriad = Chord(
        baseNote: Note(NoteClass(letter, acc), 3),
        quality: ChordQuality.diminished,
      );
      final dimBassTreble = TriadArrangementService(
        rootPosition: dimTriad,
        upperBound: highestTreblePos,
        lowerBound: lowestTreblePos,
        snowmenOnly: false,
      )
      ..rearrange();
      expect(dimBassTreble.choiceOfNotes.length >= 3, true);
    }
  });

  test('Any augmented triad contains at least 3 notes', () {
    final maxLetter = NoteLetter.values.length;
    for (var i = 0; i < 100; i++) {
      final letter = NoteLetter.values[Random().nextInt(maxLetter)];
      final acc = Accidental([-1, 0, 2][Random().nextInt(2)]);
      final augTriad = Chord(
        baseNote: Note(NoteClass(letter, acc), 3),
        quality: ChordQuality.augmented,
      );
      final augBassTreble = TriadArrangementService(
        rootPosition: augTriad,
        upperBound: highestTreblePos,
        lowerBound: lowestTreblePos,
        snowmenOnly: false,
      )
      ..rearrange();
      expect(augBassTreble.choiceOfNotes.length >= 3, true);
    }
  });
}
