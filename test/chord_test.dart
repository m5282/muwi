import 'package:flutter_test/flutter_test.dart';
import 'package:muwi/model/chord.dart';
import 'package:muwi/model/chord_quality.dart';
import 'package:muwi/model/note.dart';
import 'package:muwi/model/note_class.dart';

void main() {
  test('Major/minor chords', () {
    const cMajorTriad = Chord(baseNote: Note(NoteClass.c, 0), quality: ChordQuality.major);
    expect(cMajorTriad.notes, const [
      Note(NoteClass.c, 0),
      Note(NoteClass.e, 0),
      Note(NoteClass.g, 0),
    ]);

    const fMajorTriad = Chord(baseNote: Note(NoteClass.f, 0), quality: ChordQuality.major);
    expect(fMajorTriad.notes, const [
      Note(NoteClass.f, 0),
      Note(NoteClass.a, 0),
      Note(NoteClass.c, 1),
    ]);

    const fMinorTriad = Chord(baseNote: Note(NoteClass.f, -1), quality: ChordQuality.minor);
    expect(fMinorTriad.notes, const [
      Note(NoteClass.f, -1),
      Note(NoteClass.aFlat, -1),
      Note(NoteClass.c, 0),
    ]);

    const dMajorTriad = Chord(baseNote: Note(NoteClass.d, 3), quality: ChordQuality.major);
    expect(dMajorTriad.notes, const [
      Note(NoteClass.d, 3),
      Note(NoteClass.fSharp, 3),
      Note(NoteClass.a, 3),
    ]);
  });

  test('Diminished/augmented chords', () {
    const aAugmented = Chord(baseNote: Note(NoteClass.a, 2), quality: ChordQuality.augmented);
    expect(aAugmented.notes, const [
      Note(NoteClass.a, 2),
      Note(NoteClass.cSharp, 3),
      Note(NoteClass.eSharp, 3),
    ]);

    const dDiminished = Chord(
      baseNote: Note(NoteClass.d, -2),
      quality: ChordQuality.diminished,
    );
    expect(dDiminished.notes, const [
      Note(NoteClass.d, -2),
      Note(NoteClass.f, -2),
      Note(NoteClass.aFlat, -2),
    ]);
  });

  test('Chord inversions', () {
    const fDiminished = Chord(baseNote: Note(NoteClass.f, 4), quality: ChordQuality.diminished);

    expect(fDiminished.notes, const [
      Note(NoteClass.f, 4),
      Note(NoteClass.aFlat, 4),
      Note(NoteClass.cFlat, 5),
    ]);
  });
}
