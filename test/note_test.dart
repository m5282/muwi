import 'package:flutter_test/flutter_test.dart';
import 'package:muwi/constants/musical_constants.dart';
import 'package:muwi/model/accidental.dart';
import 'package:muwi/model/note.dart';
import 'package:muwi/model/note_class.dart';
import 'package:muwi/model/note_letter.dart';

void main() {
  test('Note computes correct enharmonic equivalents', () async {
    const c0 = Note(NoteClass.c, 0);
    expect(
      c0.enharmonicEquivalent(-1),
      const Note(NoteClass.bSharp, -1),
    );
    expect(
      c0.enharmonicEquivalent(1),
      const Note(NoteClass(NoteLetter.d, Accidental(-2)), 0),
    );
    expect(
      c0.enharmonicEquivalent(2),
      const Note(NoteClass(NoteLetter.e, Accidental(-4)), 0),
    );
    expect(
      c0.enharmonicEquivalent(3),
      const Note(NoteClass(NoteLetter.f, Accidental(-5)), 0),
    );

    const d3 = Note(NoteClass.d, 3);
    expect(
      d3.enharmonicEquivalent(-14),
      const Note(NoteClass(NoteLetter.d, Accidental(24)), 1),
    );
  });

  test('Note intervals work as expected', () async {
    const d3 = Note(NoteClass.d, 3);
    expect(d3 + MusicalConstants.perfectUnison, d3);
    expect(
      d3 + MusicalConstants.majorThird,
      const Note(NoteClass.fSharp, 3),
    );
    expect(
      d3 + MusicalConstants.minorThird,
      const Note(NoteClass.f, 3),
    );

    const c2 = Note(NoteClass.c, 2);
    expect(
      c2 + MusicalConstants.minorSecond,
      const Note(NoteClass.dFlat, 2),
    );
    expect(
      c2 + MusicalConstants.minorThird,
      const Note(NoteClass.eFlat, 2),
    );
  });
}
