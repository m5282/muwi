import 'package:flutter_test/flutter_test.dart';
import 'package:muwi/constants/viewmodel_constants.dart';
import 'package:muwi/enums/interval_enum.dart';
import 'package:muwi/model/clef.dart';
import 'package:muwi/model/settings/interval_settings.dart';
import 'package:muwi/model/settings/triad_settings.dart';

/// Testing the settings-class.
void main() {
  //---------------- TriadSettings:
  test('TriadSettings fromJson: One value is null, the default-value is used.', () {
    final testSettings = TriadSettings()
    ..standardClefArrangement = null;
    final json = testSettings.toJson();
    final testVal = TriadSettings.fromJson(json);
    expect(
      testVal.standardClefArrangement,
      ViewModelConstants.standardClefArrangement,
    );
  });

  test('TriadSettings fromJson: If no value is given, the default values are used', () {
    final testVal = TriadSettings.fromJson(null);
    expect(
      testVal.standardClefArrangement,
      ViewModelConstants.standardClefArrangement,
    );
    expect(
      testVal.cumulativeAccidentals,
      ViewModelConstants.cumulativeAccidentals,
    );
    expect(
      testVal.maxKeySignatureAccidentals,
      ViewModelConstants.maxKeySignatureAccidentals,
    );
    expect(
      testVal.clefPreferences[Clef.treble],
      ViewModelConstants.clefPreferences[Clef.treble],
    );
    expect(
      testVal.clefPreferences[Clef.bass],
      ViewModelConstants.clefPreferences[Clef.bass],
    );
    expect(
      testVal.clefPreferences[Clef.tenor],
      ViewModelConstants.clefPreferences[Clef.tenor],
    );
    expect(
      testVal.clefPreferences[Clef.alto],
      ViewModelConstants.clefPreferences[Clef.alto],
    );
    expect(testVal.numberOfLedgerLines, ViewModelConstants.numberOfLedgerLines);
    expect(testVal.snowmenOnly, ViewModelConstants.snowmenOnly);
  });

  //---------------- IntervalSettings:
  test('TriadSettings fromJson: One value is null, the default-value is used.', () {
    final testSettings = IntervalSettings()
    ..standardClefArrangement = null;
    final json = testSettings.toJson();
    final testVal = IntervalSettings.fromJson(json);
    expect(
      testVal.standardClefArrangement,
      ViewModelConstants.standardClefArrangement,
    );
  });

  test('TriadSettings fromJson: If no value is given, the default values are used', () {
    final testVal = IntervalSettings.fromJson(null);
    expect(
      testVal.standardClefArrangement,
      ViewModelConstants.standardClefArrangement,
    );
    expect(
      testVal.cumulativeAccidentals,
      ViewModelConstants.cumulativeAccidentals,
    );
    expect(
      testVal.maxKeySignatureAccidentals,
      ViewModelConstants.maxKeySignatureAccidentals,
    );
    expect(
      testVal.clefPreferences[Clef.treble],
      ViewModelConstants.clefPreferences[Clef.treble],
    );
    expect(
      testVal.clefPreferences[Clef.bass],
      ViewModelConstants.clefPreferences[Clef.bass],
    );
    expect(
      testVal.clefPreferences[Clef.tenor],
      ViewModelConstants.clefPreferences[Clef.tenor],
    );
    expect(
      testVal.clefPreferences[Clef.alto],
      ViewModelConstants.clefPreferences[Clef.alto],
    );
    expect(testVal.numberOfLedgerLines, ViewModelConstants.numberOfLedgerLines);

    for (final interval in IntervalEnum.values) {
      expect(testVal.intervalFrequencies[interval], 1);
    }
  });
}
