import 'package:flutter_test/flutter_test.dart';
import 'package:muwi/model/clef.dart';
import 'package:muwi/model/note.dart';
import 'package:muwi/model/note_class.dart';
import 'package:muwi/model/note_letter.dart';
import 'package:muwi/view_model/position.dart';

void main() {
  /// Reference note params for treble and bass clefs.
  final trebleClefLetter = Clef.treble.note.letter;
  final trebleClefOctave = Clef.treble.note.octave;

  final bassClefLetter = Clef.bass.note.letter;
  final bassClefOctave = Clef.bass.note.octave;

  /// Sample positions
  final h4 = Position(NoteLetter.b, trebleClefOctave);
  const c5 = Position(NoteLetter.c, 5);
  const d6 = Position(NoteLetter.d, 6);
  final d3 = Position(NoteLetter.d, bassClefOctave);
  const a2 = Position(NoteLetter.a, 2);
  const d2 = Position(NoteLetter.d, 2);
  const c4 = Position(NoteLetter.c, 4);
  const e2 = Position(NoteLetter.e, 2);
  const g1 = Position(NoteLetter.g, 1);
  const a1 = Position(NoteLetter.a, 1);
  const e6 = Position(NoteLetter.e, 6);
  const f3 = Position(NoteLetter.f, 3);
  const g4 = Position(NoteLetter.g, 4);

  test('Make sure the constants are still the same', () {
    expect(trebleClefLetter, NoteLetter.g);
    expect(trebleClefOctave, 4);
    expect(bassClefLetter, NoteLetter.f);
    expect(bassClefOctave, 3);
  });

  test('Test offset computation', () {
    expect(f3.getOffset(g1), 13);
    expect(g1.getOffset(f3), -13);
    expect(a1.getOffset(e6), -32);
    expect(e6.getOffset(a1), 32);
  });

  test('positionOfLine computes the correct position for a given line and tenor clef.', () {
    expect(Position.positionOfLine(-10, Clef.tenor), e2);
    expect(Position.positionOfLine(9, Clef.tenor), c5);
    expect(Position.positionOfLine(-2, Clef.tenor), f3);
    expect(Position.positionOfLine(6, Clef.tenor), g4);
    expect(
      Position.positionOfLine(18, Clef.tenor),
      e6,
    ); // Note that this position is unreachable with max. 3 ledger lines.
  });

  test('positionOfLine computes the correct position for a given line and alto clef.', () {
    expect(
      Position.positionOfLine(-12, Clef.alto),
      e2,
    ); // Note that this position is unreachable with max. 3 ledger lines.
    expect(Position.positionOfLine(7, Clef.alto), c5);
    expect(Position.positionOfLine(-4, Clef.alto), f3);
    expect(Position.positionOfLine(4, Clef.alto), g4);
    expect(
      Position.positionOfLine(16, Clef.alto),
      e6,
    ); // Note that this position is unreachable with max. 3 ledger lines.
  });

  test('positionOfLine computes the correct position for a given line and bass clef.', () {
    expect(Position.positionOfLine(-6, Clef.bass), e2);
    expect(
      Position.positionOfLine(13, Clef.bass),
      c5,
    ); // Note that this position is unreachable with max. 3 ledger lines.
    expect(Position.positionOfLine(2, Clef.bass), f3);
    expect(Position.positionOfLine(10, Clef.bass), g4);
    expect(
      Position.positionOfLine(22, Clef.bass),
      e6,
    ); // Note that this position is unreachable with max. 3 ledger lines.
  });

  test('positionOfLine computes the correct position for a given line and treble clef.', () {
    expect(
      Position.positionOfLine(-18, Clef.treble),
      e2,
    ); // Note that this position is unreachable with max. 3 ledger lines.
    expect(Position.positionOfLine(1, Clef.treble), c5);
    expect(Position.positionOfLine(-10, Clef.treble), f3);
    expect(Position.positionOfLine(-2, Clef.treble), g4);
    expect(Position.positionOfLine(10, Clef.treble), e6);
  });

  test('Test lineOfNoteWithClef.', () {
    const a1 = Note(NoteClass.a, 1);
    const c5 = Note(NoteClass.c, 5);
    expect(Position.lineOfNoteWithClef(a1, Clef.bass), -10);
    expect(Position.lineOfNoteWithClef(a1, Clef.treble), -22); // unreachable
    expect(Position.lineOfNoteWithClef(a1, Clef.alto), -16); // unreachable
    expect(Position.lineOfNoteWithClef(a1, Clef.tenor), -14); // unreachable
    expect(Position.lineOfNoteWithClef(c5, Clef.bass), 13); // unreachable
    expect(Position.lineOfNoteWithClef(c5, Clef.treble), 1);
    expect(Position.lineOfNoteWithClef(c5, Clef.alto), 7);
    expect(Position.lineOfNoteWithClef(c5, Clef.tenor), 9);
  });

  test('Test computation for the treble indices used by the view.', () {
    expect(f3.getDisplayIndex(Clef.treble), -10);
    expect(g4.getDisplayIndex(Clef.treble), -2);
    expect(h4.getDisplayIndex(Clef.treble), 0);
    expect(c5.getDisplayIndex(Clef.treble), 1);
    expect(d6.getDisplayIndex(Clef.treble), 9);
  });

  test('Test computation for the tenor clef indices used by the view.', () {
    expect(f3.getDisplayIndex(Clef.tenor), -2);
    expect(g4.getDisplayIndex(Clef.tenor), 6);
    expect(h4.getDisplayIndex(Clef.tenor), 8);
    expect(c5.getDisplayIndex(Clef.tenor), 9);
    expect(e2.getDisplayIndex(Clef.tenor), -10);
  });

  test('Test computation for the alto clef indices used by the view.', () {
    expect(f3.getDisplayIndex(Clef.alto), -4);
    expect(g4.getDisplayIndex(Clef.alto), 4);
    expect(h4.getDisplayIndex(Clef.alto), 6);
    expect(c5.getDisplayIndex(Clef.alto), 7);
    expect(a2.getDisplayIndex(Clef.alto), -9);
  });

  test('Test computation for the bass indices used by the view.', () {
    expect(g4.getDisplayIndex(Clef.bass), 10);
    expect(a2.getDisplayIndex(Clef.bass), -3);
    expect(d3.getDisplayIndex(Clef.bass), 0);
    expect(f3.getDisplayIndex(Clef.bass), 2);
    expect(d2.getDisplayIndex(Clef.bass), -7);
  });

  test('Test (+)-operator on positions.', () {
    expect(d2 + 7, d3);
    expect(a2 + 3, d3);
    expect(c5 + 8, d6);
    expect(d2 + 9, f3);
    expect(g4 + 3, c5);
  });

  test('Test (-)-operator on positions.', () {
    expect(d3 - 7, d2);
    expect(d3 - 3, a2);
    expect(d6 - 8, c5);
    expect(f3 - 9, d2);
    expect(c5 - 3, g4);
  });

  test('Test comparison operators on positions.', () {
    expect(d3 < d6, true);
    expect(d3 <= d6, true);
    expect(d3 <= d3, true);
    expect(d6 == d6, true);
    expect(d2 < a2, true);
    expect(d2 < d2, false);
    expect(d2 == a2, false);
    expect(d2 > a2, false);
    expect(h4 > c4, true);
    expect(h4 >= h4, true);
    expect(h4 >= c4, true);
  });
}
