import 'package:flutter_test/flutter_test.dart';
import 'package:muwi/model/accidental.dart';
import 'package:muwi/model/note_letter.dart';
import 'package:muwi/model/settings/triad_settings.dart';
import 'package:muwi/view_model/keyboard_vm/triad_keyboard_vm.dart';
import 'package:muwi/view_model/quiz_page_view_model.dart';
import 'package:muwi/view_model/taskproviders/triad_provider.dart';

void main() {
  /// The object to test.
  final qpvm = QuizPageViewModel(
    settings: TriadSettings(),
    taskProvider: TriadProvider(),
    keyboardVM: TriadKeyboardVM(),
  );

  test('Random key signature generator produces valid key signatures', () {
    for (var i = 0; i < 100; i++) {
      qpvm.nextChord();
      final ks = qpvm.keySignature!;

      /// A nonempty key signature starts with the correct letters.
      if (ks.alteredNoteClasses.isNotEmpty) {
        if (ks.alteredNoteClasses.first.accidental == Accidental.flat) {
          /// Key signature with flats.
          expect(ks.alteredNoteClasses.first.letter, NoteLetter.b);
        } else {
          /// Key signature with sharps.
          expect(ks.alteredNoteClasses.first.letter, NoteLetter.f);
        }
      }

      /// A key signature of length 7 ends on the correct letters.
      if (ks.alteredNoteClasses.length == 7) {
        if (ks.alteredNoteClasses.first.accidental == Accidental.flat) {
          /// Key signature with flats.
          expect(ks.alteredNoteClasses.last.letter, NoteLetter.f);
        } else {
          /// Key signature with sharps.
          expect(ks.alteredNoteClasses.last.letter, NoteLetter.b);
        }
      }
    }
  });
}
