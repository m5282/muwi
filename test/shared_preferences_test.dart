import 'package:flutter_test/flutter_test.dart';
import 'package:muwi/constants/global_constants.dart';
import 'package:muwi/model/tonfall_shared_preferences.dart';

/// Problem: Flutter does not admit that some snapshot data might be null when the data-String itself is not null.
/// This might be a bug.
/// If null checks cannot be performed on snapshot data fetched from SharedPreferences and these happen to be null, the app breaks on the user's device when these data are processed. We absolutely need to make sure this does not happen.
void main() {
  try {
    test(
        'Test whether loading the new settings breaks the app when there is still an object of the old settings class stored in SharedPreferences',
        () {
      // The identifier of the settings to work on
      const settingsID = '${GlobalConstants.uebungsmodus}settings';

      // First, remove any settings previously stored.
      TonfallSharedPreferences.removeSettings(settingsID);

      // Create an object of the old Settings class where some of the new parameters are missing.
      final dummySettings = DummySettings();

      // Now, store the dummy settings where certain variables present in the new Settings class are missing.
      TonfallSharedPreferences.setSettings(settingsID, dummySettings);
      TonfallSharedPreferences.setVersion('Test');
    });
  } catch (e) {
    throw Exception('The app crashed.');
  }
}

/// This class represents the old settings class which was previously stored in SharedPreferences.
/// The purpose is to test whether the app breaks when loading the new settings while the old settings are still stored in SharedPreferences.
class DummySettings {
  // This unnamed constructor is NOT implemented by default!
  DummySettings();

  /// Constructor to decode a JSON-Object back to an instance of Settings.
  DummySettings.fromJson(Map<String, dynamic> jsonInput) {
    // Ints:
    augmentedFrequency = jsonInput['augmentedFrequency'] as int;
    diminishedFrequency = jsonInput['diminishedFrequency'] as int;
    majorFrequency = jsonInput['majorFrequency'] as int;
    minorFrequency = jsonInput['minorFrequency'] as int;
    maxKeySignatureAccidentals = jsonInput['maxKeySignatureAccidentals'] as int;

    // Booleans:
    snowmenOnly = jsonInput['snowmenOnly'] as bool;
    standardClefArrangement = jsonInput['standardClefArrangement'] as bool;
  }

  /// A parameter which was part of the old settings class
  int augmentedFrequency = 1;

  /// A parameter which was part of the old settings class
  int diminishedFrequency = 1;

  /// A parameter which was part of the old settings class
  int majorFrequency = 2;

  /// A parameter which was part of the old settings class
  int minorFrequency = 2;

  /// A parameter which was part of the old settings class
  bool snowmenOnly = false;

  /// A parameter which was part of the old settings class
  bool standardClefArrangement = false;

  /// A parameter which was part of the old settings class
  int maxKeySignatureAccidentals = 6;

  //------------ Serialization:
  Map<String, dynamic> toJson() => {
        'augmentedFrequency': augmentedFrequency,
        'diminishedFrequency': diminishedFrequency,
        'majorFrequency': majorFrequency,
        'minorFrequency': minorFrequency,
        'snowmenOnly': snowmenOnly,
        'standardClefArrangement': standardClefArrangement,
        'maxKeySignatureAccidentals': maxKeySignatureAccidentals,
      };
}
