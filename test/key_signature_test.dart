import 'package:flutter_test/flutter_test.dart';
import 'package:muwi/model/key_signature.dart';
import 'package:muwi/model/mode.dart';
import 'package:muwi/model/note_class.dart';

void main() {
  test('Key signatures', () {
    final noAccidentals = KeySignature(key: NoteClass.c, mode: Mode.major);
    expect(noAccidentals.alteredNoteClasses, <NoteClass>[]);

    final twoFlats = KeySignature(key: NoteClass.bFlat, mode: Mode.major);
    expect(twoFlats.alteredNoteClasses, [NoteClass.bFlat, NoteClass.eFlat]);

    final oneSharp = KeySignature(key: NoteClass.g, mode: Mode.major);
    expect(oneSharp.alteredNoteClasses, [NoteClass.fSharp]);

    final twoSharps = KeySignature(key: NoteClass.b, mode: Mode.minor);
    expect(twoSharps, KeySignature(key: NoteClass.d, mode: Mode.major));
    expect(twoSharps.alteredNoteClasses, [NoteClass.fSharp, NoteClass.cSharp]);

    final threeSharps = KeySignature(key: NoteClass.a, mode: Mode.major);
    expect(threeSharps, KeySignature(key: NoteClass.fSharp, mode: Mode.minor));
    expect(
      threeSharps.alteredNoteClasses,
      [NoteClass.fSharp, NoteClass.cSharp, NoteClass.gSharp],
    );

    final fourSharps = KeySignature(key: NoteClass.e, mode: Mode.major);
    expect(fourSharps.alteredNoteClasses, [
      NoteClass.fSharp,
      NoteClass.cSharp,
      NoteClass.gSharp,
      NoteClass.dSharp,
    ]);

    final fiveSharps = KeySignature(key: NoteClass.gSharp, mode: Mode.minor);
    expect(fiveSharps.alteredNoteClasses, [
      NoteClass.fSharp,
      NoteClass.cSharp,
      NoteClass.gSharp,
      NoteClass.dSharp,
      NoteClass.aSharp,
    ]);

    final sixSharps = KeySignature(key: NoteClass.fSharp, mode: Mode.major);
    expect(sixSharps.alteredNoteClasses, [
      NoteClass.fSharp,
      NoteClass.cSharp,
      NoteClass.gSharp,
      NoteClass.dSharp,
      NoteClass.aSharp,
      NoteClass.eSharp,
    ]);

    final sixFlats = KeySignature(key: NoteClass.gFlat, mode: Mode.major);
    expect(sixFlats.alteredNoteClasses, [
      NoteClass.bFlat,
      NoteClass.eFlat,
      NoteClass.aFlat,
      NoteClass.dFlat,
      NoteClass.gFlat,
      NoteClass.cFlat,
    ]);
  });
}
