# Tonfall

[![pipeline status](https://gitlab.com/m5282/muwi/badges/development/pipeline.svg)](https://gitlab.com/m5282/muwi/-/commits/development)

This cross-platform app is maintained by three students of computer science and was initially designated for musicology students at Kiel University. However, it is available on Google PlayStore and iOS AppStore and can be used by anyone interested in musicology. It allows users to train in identifying triads and intervals by sight. Note that it is not an ear training app. This project is constantly evolving and we are working on a bunch of new features right now. There is still a lot to do and we are only a small team. Things may therefore take a while sometimes, but we are really passionate about this. Please contact us if you have any feedback, questions or suggestions. 

_Web App:_
[Gitlab Pages](https://m5282.gitlab.io/muwi/).

_User manual (german):_
[Tonfall Anleitung.pdf](documentation/Tonfall-Anleitung.pdf)

_Appstore:_

![QR-Code Appstore](screenshots/iOS-qr.png){ width=25% }

_Google Playstore:_

![QR-Code Playstore](screenshots/android-qr.png){ width=25% }
